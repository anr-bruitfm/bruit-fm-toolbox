# BRUIT-FM Toolbox

This project contains several python scripts and functions related to the [ANR BRUIT-FM project (WP4)](https://www.bruit-fm.org/). It has been developped with a focus on seafloor compliance noise removal applied on Broadband Ocean Bottom Seismometers (BOBS) data.
The source code is provided as is and some bugs can still remain.


## bruitfm package structure

### Scripts
- **data_downloader_streamlit.py** is a streamlit web interface wrapper for the *obspy* data downloader API. It allows the user to easily download Ocean Bottom Seismometers (OBS) data referenced by the FDSN organisation.
- **data_visualiser_streamlit.py** is a streamlit web interface that allows the user to quickly show time series, power spectral density and spectrogram of a given data file.
- **mseed_file_merger.py** is a python script that allows the user to remove the instrument response from a set of data files (contained within a directory) which data have been recorded by the same station. An option makes possible the use of multiples threads in parallel to speed up the processing. It is ran as a command line with option in a terminal.
- **mseed_denoiser.py** is a python script that allows the user to apply a Wiener filter on one channel given a reference channel for a set of data files. An option makes possible the use of multiples threads in parallel to speed up the processing. It is ran as a command line with option in a terminal.
- **data_denoiser_streamlit.py** is a streamlit web interface that allows the user to apply a Wiener filter on one channel given a reference channel for one data file.

### Modules

- **utils** is a python module that contains useful functions related to stream manipulation.
- **transfer_function_tooblox.py** is a python script that contains a function applying a Wiener filter following the methodologies described at [1]
- **signal_utils** is a module that contains various signal and spectral related classes and functions.
- **preprocessing** is a module dedicated to the data conditionning.

### Documentation

A complete documentation is provided in the docs directory. You can access it by opening the /docs/build/html/index.html file into your favorite web browser.

## installation

You can clone this repository and use the scripts as is. 
It is recommended to use a conda distribution to avoid mesing with the system python.
It requires several packages to work, which can be installed in **two ways**:

### 1. Using conda an a new environment:

``` 
cd existing_repo # place yourself in the repos directory
conda env create --file=BruitFMToolbox_conda_env.yml # create the new environement using the provided yaml file
conda activate bruit-fm-toolbox-env # activate the environment
pip install . # Optionnal, install the bruitfm package in your environment
```

### 2. In an existing conda environment (or pip):
```
pip install .
```

It is not mandatory to install the bruitfm package, but it will allow you to import it from any python script you would develop.
Do note that using directly the second option will install the dependencies with the bruitfm package using the pipy repository.


## Running the streamlit scripts

As mentionned before, some scripts use the streamlit library, which is a powerful tool that allows the user to make interactive webpages written in python.
To launch a streamlit script, just run:
```
streamlit run yourscript.py
```
it will start a server instance and open the interface in your web browser.
Do note that the streamlit library and binaries are installed during the previously described installation step. 

## Documentation

A complete documentation about the function, classes and scripts provided by this toolbox is available as a web page by opening ```docs/build/html/index.hml``` in your web browser.

