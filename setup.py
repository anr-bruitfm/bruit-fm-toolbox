import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()
    
version={}
with open("version.py") as fp:
    exec(fp.read(),version)

setuptools.setup(
    name="bruitfm",
    version=version['__version__'],
    author="Simon Rebeyrol",
    author_email="sareqe28.nuzale58@murena.io",
    description="Bruit-FM Toolbox for Work Package 4.",
    long_description=long_description,
    long_description_content_type="text/markdown; charset=UTF-8",
    url="",
    packages=setuptools.find_packages(),
    include_package_data=True,
    install_requires=['obspy>=1.3.0','pandas', 'numpy', 'scipy', 'matplotlib==3.6.2',
                      'spectral', "tqdm", "tiskitpy", "statsmodels", "streamlit", "h5py"],
    entry_points={
         'console_scripts': [
            'bruitfm=bruitfm:main'
         ]
    },
    python_requires='>=3.9',
    classifiers=[
        "Development Status :: 4 - Beta",
        "Environment :: Console",
        "Intended Audience :: Science/Research",
        "Intended Audience :: Developers",
        "License :: OSI Approved :: GNU General Public License v3 (GPLv3)",
        "Natural Language :: English",
        "Operating System :: OS Independent",
        "Programming Language :: Python :: 3.9",
        "Topic :: Scientific/Engineering",
        "Topic :: Scientific/Engineering :: Physics"
    ],
    keywords='oceanography, marine, OBS, seafloor compliance, signal processing'
)
