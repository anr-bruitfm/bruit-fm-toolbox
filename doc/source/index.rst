.. BruitFM Work Package 4 Toolbox documentation master file, created by
   sphinx-quickstart on Fri Jul 21 09:11:05 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to BruitFM Work Package 4 Toolbox's documentation!
==========================================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   modules


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
