# Configuration file for the Sphinx documentation builder.
#
# For the full list of built-in configuration values, see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Project information -----------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#project-information

import sphinx.ext.autodoc
import pathlib
import sys
import os
# sys.path.insert(0, pathlib.Path(__file__).resolve().parents[1].as_posix())
# sys.path.insert(0,'./')
# sys.path.insert(0,'../')
sys.path.insert(0, os.path.abspath("../../../"))
sys.path.insert(0, os.path.abspath("../../"))
sys.path.insert(0, os.path.abspath("../"))

sys.path.insert(0, os.path.abspath("../../../bruitfm"))
sys.path.insert(0, os.path.abspath("../../bruitfm"))
sys.path.insert(0, os.path.abspath("../bruitfm"))

project = 'Bruit-FM Work Package 4 Toolbox'
copyright = '2023, Simon Rebeyrol'
author = 'Simon Rebeyrol'

# -- General configuration ---------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#general-configuration

extensions = ['sphinx.ext.autodoc',
              'sphinx.ext.autosummary',
              'sphinx.ext.autosectionlabel',
              'sphinx.ext.napoleon']

autosectionlabel_prefix_document = True

templates_path = ['_templates']
#exclude_patterns = ['_build', 'Thumbs.db', '.DS_Store']

exclude_patterns = []

# -- Options for HTML output -------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#options-for-html-output

html_theme = 'sphinx_rtd_theme'
html_static_path = ['_static']
html_domain_indices = True
autodoc_default_options = {
    'members': True,
    'undoc-members': True,
    'show-inheritance': True,
    'special_members': '__init__'
}
