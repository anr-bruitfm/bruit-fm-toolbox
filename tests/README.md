This directory contains bash scripts that have been used to get results that have been submitted to Geophysical Journal International.

## command_run_denoising.sh

bash script to compute the seafloor compliance signal removal analysis. It contains the parameters of the methods used for the study.

## run_remove_instrument_response.pbs

script to merge a collection of mseed file and apply all the preprocessing.