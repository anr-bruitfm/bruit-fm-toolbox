""" 
This toolbox is dedicated to the synthesis of InfraGravity (IG) wave signals and seafloor compliance.
It has not been extensively tested so bugs may remain.

References:
    - [1] AUCAN, Jérome & ARDHUIN, Fabrice. "Infragravity waves in the deep ocean: An upward revision". Geophysical Research Letters. 2013, vol 40, num. 13, p. 3435–3439. DOI: 10.1002/grl.50321
    - [2] CRAWFORD, Wayne C., WEBB, Spahr C., HILDEBR,  & , John A.. "Estimating shear velocities in the oceanic crust from compliance measurements by two-dimensional finite difference modeling". Journal of Geophysical Research: Solid Earth. 1998, vol 103, num. B5, p. 9895–9916. DOI: 10.1029/97jb03532
    - [3] WEBB, Spahr C., ZHANG, Xin & CRAWFORD, Wayne. "Infragravity waves in the deep ocean". Journal of Geophysical Research: Oceans. 1991, vol 96, num. C2, p. 2723–2736. DOI: 10.1029/90jc02212
    - [4] AN, Chao, WEI, S. Shawn, CAI, Chen & YUE, Han. Frequency Limit for the Pressure Compliance Correction of Ocean-Bottom Seismic Data. Seismological Society of America (SSA), 2020, 967–976. (91).  DOI: 10.1785/0220190259.

"""








import numpy as np
from scipy.fft import irfft, rfft, rfftfreq

from scipy.interpolate import interp1d
import scipy.signal as sig
from scipy.ndimage import convolve1d

try :
    from ..utils import * 
    from ..signal_utils import *
except ImportError:
    from utils import *
    from signal_utils import *






def generate_IG_waves(N, fs, k, depth, P0=1e3, phase="linear", const=np.pi/2, n_taper=None, fft_norm="backward"):
    '''
    Generate the IG wave signal at the seafloor level according to the parameters values of the model described in [1,3]
    
    Args:
        N (int) : the number of samples to generate
        fs (float) : the sampling frequency 
        k (float) : the wavenumber in 1/m
        depth (float) : the depth where the spectrum is to be simulated
        P0 (float) : the initial pressure at the sealevel (Pa). It is equal to a*rho*g (m * kg/m³ * m/s²).
        g (float) : the Earth gravity acceleration
        phase (str) : "linear", "constant", "mps", "all"
        const (float) : the constant to use in the case where phase="const"
        n_taper (int) : the number of sample to tape at the edge of the signal, default is no taper. Must be small enough
                        just to erase the side effects and not the actual signal
        fft_norm (str) : define where the normalization occurs, see numpy.fft documentation for more details, default="backward".
    
    --------
    Outputs:
    --------

    Outputs:
        t (ndarray) - the output time vector
        Pt (ndarray) - the output infragravity wave pressure signal 
        f_target (ndarray) - the output frequency vector
        Pf (ndarray) - the frequency response of the infra gravity wave pressure signal 
    '''


    f_target = np.linspace(0,fs/2,int(np.ceil(N/2))) # the frequencies 
    f_disp = np.sqrt(g*k*np.tanh(k*depth))/(2*np.pi) # dispersion equation
    period = N*fs 
    t = np.arange(0,period, 1/fs)

    # Making the taper mask
    if n_taper is not None :
        mask = np.ones(N)
        mask[:n_taper] = 0. 
        mask[-n_taper:] = 0.
        nkernel = 30
        smoothing_core = sig.windows.gaussian(nkernel, std=9)
        smoothing_core /= np.sum(smoothing_core)
        smoothed_mask = convolve1d(np.expand_dims(mask.astype(float), axis=0), smoothing_core, mode="nearest", axis=-1).squeeze()
        smoothed_mask = np.roll(smoothed_mask, 1, axis=-1) ## for some reason convolve1d translate the result by 1 sample


    ###  equation (1) in [1]
    normalization_factor = 2*fs
    Pf = P0/(np.cosh(k*depth)) * normalization_factor # to be clarified, huge inconsistence between real and synth signal magnitude...

    Pf_disp = Pf.copy()

    ### Building the phase component
    np.random.seed(0)
    if phase=="mps" :
        Pf = spmps(Pf)
    elif phase=="constant":
        Pf = Pf*np.exp(1.j*const)
    elif phase=="linear":
        phi = np.linspace(0,2*np.pi, len(Pf))
        Pf = Pf*np.exp(1.j*phi)
    elif phase=="all":
        Pf = spmps(Pf)
        Pf = Pf*np.exp(1.j*const)
        phi = np.linspace(0,2*np.pi, len(Pf)) + np.random.randn(len(Pf))*np.pi*2
        Pf = Pf*np.exp(1.j*phi)
    elif phase=="random":
        phi = np.random.randn(len(Pf))*np.pi*2
        Pf = Pf*np.exp(1.j*phi)
    else :
        print("WARNING - generate_IG_waves: unknown phase type, skipping", flush=True)


    interp_real = interp1d(f_disp, Pf.real, kind="linear", fill_value=0., bounds_error=False)
    interp_imag = interp1d(f_disp, Pf.imag, kind="linear", fill_value=0., bounds_error=False)
    Pf = interp_real(f_target) + 1.j*interp_imag(f_target)


    Pt = irfft(Pf, n=N, norm=fft_norm).real

    if n_taper is not None :
        Pt *= smoothed_mask

    return(t, Pt, f_target, Pf)
    

    
def generate_IG_waves_trace(trace, k, depth,  P0=1e3, phase="linear", const=np.pi/2, n_taper=None, plot=False, fft_norm="forward") :
    """
    Wrapper func for generate_IG_waves given a obspy trace object.
    
    Args:
        trace (obspy.core.trace.Trace) : an example trace, whose data will be replaced by the synthetised IG wave signal values.
        k (float) : the wavenumber in 1/m
        depth (float) : the depth where the spectrum is to be simulated
        P0 (float) : the initial pressure at the sealevel (Pa). It is equal to a*rho*g (m * kg/m³ * m/s²).
        g (float) : the Earth gravity acceleration
        phase (str) : "linear", "constant", "mps", "all"
        const (float) : the constant to use in the case where phase="const"
        n_taper (int) : the number of sample to tape at the edge of the signal, default is no taper. Must be small enough
                        just to erase the side effects and not the actual signal
        fft_norm (str) : define where the normalization occurs, see numpy.fft documentation for more details, default="backward".
    
    Returns:
        trace (obspy.core.trace.Trace) : the output synthetised trace.
    """
    fs = trace.stats.sampling_rate 
    N = trace.stats.npts 
    trace_out = trace.copy()
    t, Pt, f_target, Pf = generate_IG_waves(N, fs, k, depth, P0=P0, phase=phase, const=const, n_taper=n_taper, plot=plot, fft_norm=fft_norm)
    trace_out.data = Pt
    return(trace_out)




class ComplianceTransferFunctionFrequencyResponse :
    '''
    Basic class that hold a compliance transfer function with useful methods, may not fully works.
    Generate a seafloor compliance transfer function frequency response given the model for an uniform isotropic half-space described in [2].

    Args:
        k (array) : the considered wavenumbers in 1/m.
        N (int) : the number of samples to generate.
        fs (float) : the sampling frequency of the desired synthetised signal. 
        Vs (float) : the shear S-wave velocity (m/s), also known as ß.
        Vp (float) : the compressional P-wave velocity (m/s), also known as ɑ.    
        depth (float) : the depth where the spectrum is to be simulated (m).
        density (float) : the density of the material filling the halfspace (kg/m³).
        g (float) : the gravitationnal field acceleration of the Earth (m/s²).

    -----------
    Attributes:
    -----------

    Attributes
    ----------
        depth (float) : the depth where the spectrum is to be simulated (m).
        g (float) : the gravitationnal field acceleration of the Earth (m/s²).
        k (array) : the considered wavenumbers in 1/m.
        f_disp (array) : the frequencies from the dispersion equation
        f (array) : the frequency vector.
        Vs (float) : the shear S-wave velocity (m/s), also known as ß.
        Vp (float) : the compressional P-wave velocity (m/s), also known as ɑ.  
        compliance_TFFR_k (array) : the compliance transfer function according to the wavenumbers
        compliance_TFFR_f (array) : the compliance transfer function according to the frequencies


    '''

    def __init__(self, k, N, fs, Vp, Vs, depth, density,  g=9.81, ) :
        '''Constructor'''

        self.depth = depth 
        self.g = g

        ### calculating frequencies
        self.k = k 
        omega_square = g*k*np.tanh(k*depth)
        self.f_disp = np.sqrt(g*k*np.tanh(k*depth))/(2*np.pi) # dispersion equation
        self.f = np.linspace(0,fs/2,int(np.ceil(N/2)))

        self.Vs = Vs
        self.Vp = Vp

        ### computing the transfer function frequency response for each layer
        self.compliance_TFFR_k = Vp**2/(Vs**2 * 2 * (Vp**2-Vs**2) * density) * omega_square/k
        interpolator = interp1d(self.f_disp, self.compliance_TFFR_k, kind="linear", fill_value=0., bounds_error=False)
        self.compliance_TFFR_f = interpolator(self.f)      



    def plot(self) :
        """Plot the TFFR"""
        import matplotlib.pyplot as plt
        fig, [ax_abs, abs_angl] = plt.subplots(2)
        fig.suptitle(r"Compliance at {} m depth for $V_p$ = {} km/s and $Vs$ = {} km/s".format(self.depth, self.Vp, self.Vs))
        ax_abs.semilogx(self.f, np.abs(self.compliance_TFFR_f), label="interpolated")
        # ax_abs.semilogx(self.f_disp, 10*np.log10(np.abs(self.TFFR_disp)), label="from dispersion")
        ax_abs.set_ylabel(r"Compliance ($m.s^{-2}.Pa^{-1}$)")
        ax_abs.legend()
        ax_abs.grid()
        abs_angl.semilogx(self.f, np.unwrap(np.angle(self.compliance_TFFR_f)))
        abs_angl.set_ylabel(r"Phase (rad)")
        abs_angl.set_xlabel("Frequency (Hz)")
        abs_angl.grid()
        plt.tight_layout()
        return(fig)

    

    def apply(self, trace_pressure, trace_seis):
        '''Apply the seafloor compliance noise on the trace_seis using the trace_pressure as reference.
        Very basic, no additional noise control available.

        Args:
            trace_pressure (obspy.core.trace.Trace) : the pressure trace containing the IG wave pressure signal to be applied.
            trace_seis (obspy.core.trace.Trace) : the vertical component trace which will be noised by the IG wave signal.
        
        Returns:
            trace_seis_out (obspy.core.trace.Trace) : the vertical component trace which has been noised by the IG wave signal.

        '''
        fft_seis_original = rfft(trace_seis.data)
        f_seis_original = rfftfreq(trace_seis.stats.npts, trace_seis.stats.delta)
        interp_seis_real = interp1d(f_seis_original, fft_seis_original.real, kind="linear", fill_value=0., bounds_error=False)
        interp_seis_imag = interp1d(f_seis_original, fft_seis_original.imag, kind="linear", fill_value=0., bounds_error=False)
        fft_seis_original = interp_seis_real(self.f) + 1.j*interp_seis_imag(self.f)

        fft_pressure_orinal = rfft(trace_pressure.data)
        f_pressure_original = rfftfreq(trace_pressure.stats.npts, trace_pressure.stats.delta)
        interp_pressure_real = interp1d(f_pressure_original, fft_pressure_orinal.real, kind="linear", fill_value=0., bounds_error=False)
        interp_pressure_imag = interp1d(f_pressure_original, fft_pressure_orinal.imag, kind="linear", fill_value=0., bounds_error=False)
        fft_pressure_orinal = interp_pressure_real(self.f) + 1.j*interp_pressure_imag(self.f)

        fft_seis_original += fft_pressure_orinal * self.compliance_TFFR_f 
        
        interp_seis_real = interp1d(self.f, fft_seis_original.real, kind="linear", fill_value=0., bounds_error=False)
        interp_seis_imag = interp1d(self.f, fft_seis_original.imag, kind="linear", fill_value=0., bounds_error=False)
        fft_seis_applied = interp_seis_real(f_seis_original) + 1.j*interp_seis_imag(f_seis_original)

        trace_seis_out = trace_seis.copy()
        trace_seis_out.data = irfft(fft_seis_applied, n=trace_seis.stats.npts)

        return(trace_seis_out)

        

def fold(r) :
    """ 
    translated to python from fold.m provided by J.O Smith
    [rw] = fold(r)
    Fold left wing of vector in FFT buffer format
    onto right wing
    J.O. Smith, 1982-2002
    """

    n = r.shape[0]
    if n < 3 :
        return r
    elif n%2==1 :
        nt = int((n+1)/2)
        rw = np.zeros(n, dtype=complex)
        rw[0] = r[0]
        rw[1:nt] = r[1:nt] + np.conj(np.flip(r[nt:]))

    else :
        nt = int(n/2)
        rw = np.zeros(n, dtype=complex)
        rw[0] = r[0]
        rf = np.zeros(nt, dtype=complex)
        rf[:nt] = r[1:nt+1]
        rf = rf + np.flip(r[nt:]).conj()
        rw[1:nt+1] = rf
    return rw

def mps(spectrum) :
    # adapted from mps.m provided by J.O Smith
    new_spectrum = np.exp(fft(fold(ifft(np.log(np.clip(spectrum.real*1e15, 1e-16, np.inf)))), n=len(spectrum)))/1e15
    return(new_spectrum)





################### DEPRECATED

def _compare_IGwaves_model(params, trace, k, max_freq=None, div="NRMSE") :
    """Evaluated the difference between the synth IGwaves and a real trace psds - may not work well """
    a, depth = params 
    trace_noise = generate_IG_waves_trace(trace.copy(), k, depth, a=a, rho=1.037e3, phase="random", n_taper=60)
    f, psd_real = psd(trace, "hanning", 2048,1024, return_SpectralStats=True, verbose=False)
    f, psd_model = psd(trace_noise, "hanning", 2048,1024, return_SpectralStats=True, verbose=False)
    if max_freq is not None :
        mask = f<max_freq 
    else :
        mask=np.ones(len(f), dtype=bool)
    
    if div in "NRMSE" :
        metric = NRMSE(np.abs(psd_real["median"]),  np.abs(psd_model["median"]), bandmask=mask)
    elif div in "SID" :
        metric = SID(np.abs(psd_real["median"]),  np.abs(psd_model["median"]), bandmask=mask)
    elif div in "SAM" :
        metric = SAM(np.abs(psd_real["median"]),  np.abs(psd_model["median"]), bandmask=mask)
    else :
        metric = NRMSE(np.abs(psd_real["median"]),  np.abs(psd_model["median"]), bandmask=mask)*SID(np.abs(psd_real["median"]),  np.abs(psd_model["median"]), bandmask=mask)*SAM(np.abs(psd_real["median"]),  np.abs(psd_model["median"]), bandmask=mask)
    return(metric)

def _fit_IGwaves_model(init, trace, k, max_freq=None, div="NRMSE"):
    """ fit the IG wave model parameters"""
    # init = np.array([1e-2, 1e3])

    params = [trace.copy(), k, max_freq, div]

    from scipy.optimize import least_squares
    final_ak = least_squares(compare_IGwaves_model, init,  args=params, gtol=1e-10, ftol=1e-10, xtol=1e-10, bounds=[[1e-4, 100],[1e5, 6000]], verbose=2)

    return(final_ak)
