#! /usr/bin/bash
### Author  :  SIMON REBEYROL
### Company :  IFREMER, UMR GEOOCEAN
 

import sys
import os
os.environ["OMP_NUM_THREADS"] = "4" # export OMP_NUM_THREADS=4
os.environ["OPENBLAS_NUM_THREADS"] = "4" # export OPENBLAS_NUM_THREADS=4
os.environ["MKL_NUM_THREADS"] = "4" # export MKL_NUM_THREADS=6
os.environ["VECLIB_MAXIMUM_THREADS"] = "4" # export VECLIB_MAXIMUM_THREADS=4
os.environ["NUMEXPR_NUM_THREADS"] = "4" # export NUMEXPR_NUM_THREADS=6
import numpy as np
from scipy.stats import median_abs_deviation 
import scipy.signal as sig


from .toolbox import *

def SAM(ref_spectra, spectra, radian=True, bandmask=None):
    '''compute the SAM between the ref_spectra and the spectra

        Args:
            ref_spectra  (array (Mxl))  : containing  the reference spectra.
            spectra  (array (Nxl)) : containing the spectra

        output:
            NxM ndarray (N  vectors) containing the SAM coefficient between the corresponding reference spectra and all the spectra
    '''
    #print("Performing the SAM calculation ")
    if isinstance(bandmask,np.ndarray) :
        pass
    else :
        bandmask = np.ones(spectra.shape[-1]).astype('bool')
    if radian ==True:
        unit = 1
    else:
        unit = 180/np.pi


    out_temp = np.sum(ref_spectra[bandmask]*spectra[bandmask], axis=2)/(np.sqrt(np.sum(ref_spectra[bandmask]*ref_spectra[bandmask], axis=2))*np.sqrt(np.sum(spectra[bandmask]*spectra[bandmask],axis=2)))
    if np.any(out_temp >1.0) :
        out_temp[np.where(out_temp>1.0)] = 1.0
    out = np.arccos(out_temp)*unit

    return(out.squeeze())


def SID(ref_spectra, spectra, bandmask=None):
    from scipy.special import  kl_div
    if isinstance(bandmask,np.ndarray) :
        pass
    else :
        bandmask = np.ones(spectra.shape[-1]).astype('bool')

    sid = np.sum(kl_div(ref_spectra[bandmask], spectra[bandmask]) + kl_div(spectra[bandmask], ref_spectra[bandmask]))
    return(sid)

def NRMSE(ref_spectra, spectra, bandmask=None) :
    if isinstance(bandmask,np.ndarray) :
        pass
    else :
        bandmask = np.ones(spectra.shape[-1]).astype('bool')

    nrmse = np.sqrt(np.mean((ref_spectra[bandmask] - spectra[bandmask])**2)/np.mean((ref_spectra[bandmask])**2))

    return(nrmse)


def trace_deviations(trace, skipedges=True) :
    """Compute the std and median absolute deviations of the given trace.
    
    Args:
        trace  (obspy.core.trace.Trace) : the trace to be analysed.
        skipedges  (bool), optionnal  : if True, the trace is cropped by 1% of its edges data.
    
    Outputs
    -------
        std  (float) : the standard deviation.
        mad  (float) :  the median absolute deviation scaled to a normal distribution.
    """
    data = trace.data.copy()

    if skipedges:
        ### deleting the edges
        nbedgescount= int(0.005*trace.stats.npts)
        data[:nbedgescount] = np.nan 
        data[-nbedgescount:] = np.nan
        mask = np.isnan(data)
        data = data[~mask]

    std = np.std(data) 
    mad = median_abs_deviation(data, scale="normal") 

    return(std, mad)


def PSDdiffTrace(trace1, trace2,  Nws=900, Noverlaps=450, windowtype=('dpss', 4), average="mean", freqmin=None, freqmax=None) :
    '''Calculate the mean difference between two power spectral densities in the given frequency range.

    Args:
        trace1  (obspy.core.trace.Trace) : the trace number one.
        trace2  (obspy.core.trace.Trace) : the trace number two.
        Nws  (int, float), default 900, : the length of the spectral window in seconds.
        Noverlaps  (int, float), default 450, : the length of the overlap between two windows in seconds.
        windowtype  (str, tuple), default ('dpss', 4), : the taper window type according to scipy.signal.get_window().
        average  (str) default mean, : the type of average used during the Welch method.
        freqmin  (float), optional : the min frequency.
        freqmax (float), optional  : the max frequency.

    Outputs
    -------
        mean_diff_dB  (float)  : the mean dB difference between freqmin and freqmax between the power spectral densities of trace1 and trace2
    '''

    cpt = 0
    Nw = int(Nws * trace1.stats.sampling_rate)
    Noverlap = int(Noverlaps * trace1.stats.sampling_rate)
    window = get_window(windowtype, Nw)

    # fig.set_title("PSDs {} - {} - {}".format(trace.stats.starttime, trace.stats.starttime, trace.get_id()))
    f1, S1 = sig.welch(trace1.data, fs=trace1.stats.sampling_rate, nperseg=Nw, noverlap=Noverlap, window=window, average=average)
    f2, S2 = sig.welch(trace2.data, fs=trace2.stats.sampling_rate, nperseg=Nw, noverlap=Noverlap, window=window, average=average)

    freqmask = np.ones(len(f1), dtype=bool)
    if isinstance(freqmin, float) :
        freqmask[f1<freqmin] = False 
    if isinstance(freqmax, float) :
        freqmask[f1>freqmax] = False   

    logS1 = 10*np.log10(np.abs(S1))
    logS2 = 10*np.log10(np.abs(S2))

    S = logS1[freqmask] - logS2[freqmask]


    return(np.mean(S), np.max(S))
    

