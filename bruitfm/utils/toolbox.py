#! /usr/bin/bash
### Author  :  SIMON REBEYROL
### Company :  IFREMER, UMR GEOOCEAN


'''
Contain miscellaneous functions, not all documented.
'''




import sys
import os
os.environ["OMP_NUM_THREADS"] = "4" # export OMP_NUM_THREADS=4
os.environ["OPENBLAS_NUM_THREADS"] = "4" # export OPENBLAS_NUM_THREADS=4
os.environ["MKL_NUM_THREADS"] = "4" # export MKL_NUM_THREADS=6
os.environ["VECLIB_MAXIMUM_THREADS"] = "4" # export VECLIB_MAXIMUM_THREADS=4
os.environ["NUMEXPR_NUM_THREADS"] = "4" # export NUMEXPR_NUM_THREADS=6
import numpy as np
import obspy
import obspy.core as obs
import matplotlib.pyplot as plt
import scipy.signal as sig
import scipy.linalg as lin
from scipy.ndimage import convolve1d
import scipy as sc
from scipy.fft import fft, ifft, fftfreq, fft, ifft, fftfreq, fftshift, rfft, irfft, rfftfreq, irfft
from itertools import product
import warnings
import pandas as pd
from datetime import datetime, timedelta


#from spectral_toolbox import SpectralStats, psd, csd, coherence
try :
    from ..signal_utils import get_window
except ImportError:
    from signal_utils import get_window

warnings.filterwarnings( "ignore", module = "matplotlib\..*" )
warnings.filterwarnings(action='ignore', category=RuntimeWarning) # setting ignore as a parameter and further adding category




available_windowtype = {"boxcar":False, "triang":False, "blackman":False, "hamming":False, "hann":False, "barlett":False, "flattop":False,
                        "parzen":False, "bohman":False, "blackmanharris":False, "nuttal":False, "barthann":False, "cosine":False, "exponential":False,
                        "tukey":False, "taylor":False,"kaiser":True, "gaussian":True, "general_hamming":True, "dpss":True, "chewbwin":True, "flathanning":False}





class TraceNormalizationParameter :

    def __init__(self, mean, std):
        self.std = std
        self.mean = mean

class StreamNormalizer :
    '''
    Normalize the data in each trace of the stream and return the normalized stream and the normalization mean and std
    '''
    def __init__(self, stream):

        self.normalizing_parameters = {}

        for trace in stream :
            mean = trace.data.mean()
            std = trace.data.std()
            self.normalizing_parameters[trace.get_id()] = TraceNormalizationParameter(mean, std)

    def normalize(self,stream) :
        out_stream = stream.copy()
        for trace in out_stream :
            trace.data[:] = (trace.data[:]-self[trace.get_id()].mean)/self[trace.get_id()].std
        return(out_stream)

    def denormalize(self, stream) :
        out_stream = stream.copy()
        for trace in out_stream :
            trace.data[:] = trace.data[:]*self[trace.get_id()].std + self[trace.get_id()].mean
        return(out_stream)

    def __getitem__(self, get):
        return self.normalizing_parameters[get]




# #################################################################################
# #################################################################################

def mask_like(stream) :
    mask={}
    for trace in stream :
        mask[trace.stats.channel] = np.zeros(trace.stats.npts, dtype=bool)
    return(mask)

def update_stream_unit(stream, source_unit, target_unit) :
    """
    Update the string of the unit matching source_unit to target_unit.
    It does not convert the data.
    """

    for trace in stream :
        try :
            if trace.stats.response.response_stages[0].input_units == source_unit :
                trace.stats.response.response_stages[0].input_units = target_unit 
        except :
            print("Warning: no responses attached to stream")
    return(stream) 

def time_converter(t) :
    """
    Convert a time vector into the appropriate unit prior to plotting
    """
    if np.max(t) > 2*29*24*3600 :
        t /= (29*24*3600)
        time_unit="months"
    elif np.max(t) > 2*24*3600 :
        t /= (24*3600)
        time_unit="days"
    elif np.max(t) > 2*3600 :
        t /=3600
        time_unit="hours"
    elif np.max(t) >2*60 :
        t /=60
        time_unit="minutes"
    else :
        time_unit="seconds"
    
    return(t, time_unit)



def datetime_vector(datetime_in, t_array):
    """Generate a list of datetimes from a time array and an initial datetime."""
    datetime_vector_out = np.zeros(len(t_array), dtype=datetime)
    for k, t in enumerate(t_array) :
        datetime_vector_out[k] = datetime_in + timedelta(seconds=t)

    return(datetime_vector_out.tolist())


def select_channel_not(stream, channel) :
    """Will return a stream with all the traces that are not associated with channel.

    Args:
        stream (obspy.core.stream.stream) : the input stream.
        channel (str) : the channel to pop, can be the full channel name or just a character.

    Output :
        out_stream (obspy.core.stream.stream) : the output stream
    """
    out_stream = stream.copy()
    inds = []
    for k, tr in enumerate(out_stream) :
        if channel in tr.stats.channel :
            inds.append(k)
    for ind in np.flip(inds) :
        out_stream.pop(ind)
    return(out_stream)

def select_related_channels(stream, char_cha_1, char_cha_2, char_cha_3) :
    """Will return a stream with all the traces that are related with given channels.

    Args:
        stream (obspy.core.stream.stream) : the input stream
        char_cha_1 (list of char) : the desired first character in the channel names (band code)
        char_cha_2 (list of char) : the desired second character in the channel names (instrument code)
        char_cha_3 (list of char) : the desired third character in the channel names (orientation)

    Output :
        out_stream (obspy.core.stream.stream) : the output stream
    """
    flagfirst = True
    if '*' in char_cha_1 :
        char_cha_1 = 'FGDCESHBMLVURPTQAO' ### see https://www.fdsn.org/pdf/SEEDManual_V2.4.pdf
    if '*' in char_cha_2 :
        char_cha_2 = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ' ### putting the whole alphabet because of lazyness...
    if '*' in char_cha_3 :
        char_cha_3 = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890' ### see https://www.fdsn.org/pdf/SEEDManual_V2.4.pdf
    for tr in stream:
        chaname = tr.stats.channel
        if (chaname[0] in char_cha_1) and (chaname[1] in char_cha_2) and (chaname[2] in char_cha_3):
            if flagfirst :
                output_stream = obspy.core.stream.Stream(traces=tr)
                flagfirst=False
            else :
                output_stream += tr
        else :
            print("WARNING: Empty stream detected in select_related_channels.", flush=True)
            if flagfirst :
                output_stream=obspy.core.stream.Stream(traces=tr)
                flagfirst=False 
            else :
                output_stream += tr
    return(output_stream)


def crop_stream_to_minimal_time_footprint(stream) :
    """Crop the stream to the time range where all the traces of stream are defined."""
    for k, tr in enumerate(stream) :
        if k==0 :
            mindate=tr.stats.starttime 
            maxdate=tr.stats.endtime 
        else :
            if tr.stats.starttime > mindate :
                mindate=tr.stats.starttime 
            if tr.stats.endtime < maxdate : 
                maxdate=tr.stats.endtime 
    stream.trim(starttime=mindate, endtime=maxdate)
    return(stream)



def SubstractStream(stream1, stream2) :
    '''
    Return stream1 - stream2, the number and position of traces MUST match
    '''
    stream_out = stream1.copy()
    for trace1, trace2, trace_out in zip(stream1, stream2, stream_out) :
        trace_out.data = trace1.data - trace2.data
    return(stream_out)

def AddStream(stream1, stream2) :
    '''Return stream1 + stream2, the number and position of traces MUST match.'''
    stream_out = stream1.copy()
    for trace1, trace2, trace_out in zip(stream1, stream2, stream_out) :
        trace_out.data = trace1.data + trace2.data
    return(stream_out)



def detect_minimal_time_range(stream) :
    """Detect the time range where all the traces of stream are defined."""
    for k, tr in enumerate(stream) :
        if k==0 :
            start = tr.stats.starttime
            end = tr.stats.endtime

        if start < tr.stats.starttime :
            start = tr.stats.starttime

        if end > tr.stats.endtime :
            end = tr.stats.endtime
    return(start,end)




def fill_holes_in_mask(mask) :
    """Fill the holes (up to size 3) in a mask."""
    mask_out = mask.copy()
    if np.any(mask_out==True) :
        for ind, value in enumerate(mask_out) :
            if value :
                first_ind = ind 
                break
        for ind, value in enumerate(np.flip(mask_out)) :
            if value :
                last_ind = len(mask_out)-(ind+1)
                break
        mask_out[first_ind+3:last_ind+1] = True
        # print(mask_out != mask)
    return(mask_out)


def costfun(a,b) :
    """A cost function to evaluate the distance between two sets of 3axis signals"""
    theta = a[0]
    azimuth = a[1]
    x = b[0]
    y = b[1]
    z = b[2]
    # c = np.cos(theta)
    # s = np.sin(theta)
    # Rotation_matrix = np.asarray([[0.5+0.5*c, -0.5 + 0.5*c, -1/np.sqrt(2)*s],
    #                               [-0.5*c, 0.5+0.5*c, 1/np.sqrt(2)*s],
    #                               [1/np.sqrt(2)*s, 1/np.sqrt(2)*s, c]])

    [xr, yr, zr] = Rotate3Daxis(theta, azimuth, x, y, z)
    c_xz = np.corrcoef(zr,xr)[0,1]
    c_yz = np.corrcoef(zr,yr)[0,1]
    return(np.abs(c_xz*c_yz))


def Rotate3DStream(theta, x,y,z) :
    '''Rotate a 3D signal by theta around the [1;-1;0]/sqrt(2) direction.

    Args:
        theta (float) - the rotation angle in degree.
        x (array) : the x signal.
        y (array) : the y signal.
        z (array)  - the vertical signal.

    Outputs:
    --------

        xr, yr, zr (array) - the rotated signals.
    '''
    theta *= np.pi/180
    c = np.cos(theta)
    s = np.sin(theta)
    R = np.asarray([[0.5+0.5*c, -0.5 + 0.5*c, -1/np.sqrt(2)*s],
                    [-0.5*c, 0.5+0.5*c, 1/np.sqrt(2)*s],
                    [1/np.sqrt(2)*s, 1/np.sqrt(2)*s, c]])
    [xr, yr, zr] = R@np.stack([x,y,z])
    return(xr, yr, zr)

def RotateStream(theta, azimuth, stream) :
    '''Rotate a 3D signal by theta around the [1;-1;0]/sqrt(2) direction.

    Args:
        theta (float) : the rotation angle in degree.
        azimuth (float) : the azimuth in degree of the xy rotation around which the rotation will be performed.
        stream (obspy:stream)  : the stream containing the 3 components signals to be rotated.

    Outputs:
    --------
        -xr, yr, zr (array) - the rotated signals.
    '''

    if len(stream) > 4 :
        raise ValueError("The stream you want to rotate contains too many channels")
    ### isolate the accelerometer traces
    ind_tr = [1,2,3] # dumb init
    trace_len = stream[0].stats.npts
    for ind, trace in enumerate(stream) :
        if trace.get_id()[-1] in ["1", "N"] :
            ind_tr[0] = ind
        elif trace.get_id()[-1] in ["2", "E"] :
            ind_tr[1] = ind
        elif trace.get_id()[-1] in ["Z"] :
            ind_tr[2] = ind
        # else :
        #     raise ValueError("The channel naming doesn't seem to be standard : {}".format(trace.get_id()[-1]))

        if trace.stats.npts != trace_len :
            raise ValueError("The traces are not of the same length")

    ### preping the rotation
    array_tr = np.zeros([3,trace_len])
    array_tr[0] = stream[ind_tr[0]].data
    array_tr[1] = stream[ind_tr[1]].data
    array_tr[2] = stream[ind_tr[2]].data
    u = np.array([np.cos(azimuth), np.sin(azimuth), 0])
    theta *= np.pi/180
    c = np.cos(theta)
    s = np.sin(theta)
    R = np.asarray([[c + (1-c)*u[0]**2 , u[0]*u[1]*(1-c)-u[2]*s , u[0]*u[2]*(1-c) + u[1]*s],
                                  [u[1]*u[0]*(1-c) + u[2]*s , c + u[1]*u[1]*(1-c) , u[1]*u[2]*(1-c)-u[0]*s],
                                  [u[2]*u[0]*(1-c)-u[1]*s , u[2]*u[1]*(1-c)+u[0]*s , c + u[2]*u[2]*(1-c)]])
    ### making the rotation
    array_tr_rot = R@array_tr
    ### storing the result in a new stream
    stream_rot = stream.copy()
    stream_rot[ind_tr[0]].data = array_tr_rot[0]
    stream_rot[ind_tr[1]].data = array_tr_rot[1]
    stream_rot[ind_tr[2]].data = array_tr_rot[2]

    return(stream_rot)



def Stream_sampling_rate_check(stream):
    """Check if all traces are sampled the same way"""
    isallequal = True
    for k, tr in enumerate(stream) :
        if k == 0 :
          minfs = tr.stats.sampling_rate
        if minfs != tr.stats.sampling_rate :
            isallequal = False
            if minfs > tr.stats.sampling_rate:
                minfs = tr.stats.sampling_rate
    return(isallequal, minfs)



def count_continuous_mask(mask):
    "Will count the number of continuous True segment"
    label = np.zeros(len(mask), dtype=int)
    last_label=0
    if mask[0] :
        label[0] = 1
        last_label = 1
    else :
        label[0] = 0
    for k in range(1,len(mask),1) :

        if (mask[k-1] == mask[k]) and mask[k] :
            label[k] = label[k-1]
        elif  not mask[k] :
            label[k] = 0
        else :
            last_label += 1
            label[k] = last_label 
    return(len(np.unique(label)))
            


#############################################################################################
################################## Ploting functions ########################################
#############################################################################################

def PlotMagSpectralStats(spsts, labels, average="mean") :
    """Plots a list of SpectralStats instance on one plot."""
    fig, ax = plt.subplots(1, figsize=(6.4*1.5,4.8*1.5))
    if average =="mean" :
        deviation = "std" 
    elif average == "median":
        deviation="mad"

    for spst, label in zip(spsts, labels) :
        # line, = ax.loglog(spst.f, np.abs(spst.arrays[average]), label="{}/{} - {}".format(average, deviation, label), linewidth=1.5)
        # ax.fill_between(spst.f, np.abs(spst.arrays[average]) - np.abs(spst.arrays[deviation]/2 ), np.abs(spst.arrays[deviation]/2) + np.abs(spst.arrays[average]), alpha=0.3, color=line.get_color())
        line, = ax.loglog(spst.f, np.abs(spst.arrays[average]), label="{}/{} - {}".format(average, deviation, label), linewidth=1.5)
        ax.fill_between(spst.f, np.abs(spst.arrays[average] - spst.arrays[deviation]/2 ), np.abs(spst.arrays[deviation]/2 + spst.arrays[average]), alpha=0.3, color=line.get_color())
    ax.grid()
    ax.set_xlabel("Frequency (Hz)")
    ax.set_ylabel(spst.unit)
    ax.legend()
    return(fig)

def PlotStreamPSD(stream, fig_ax=None, label=None, plotphase=False, Nws=2048, Noverlaps=0,
                  channels=["BDH", "BH1", "BH2", "BHZ"], windowtype=('dpss', 1), correct_response=False, linestyle="solid", linewidth=1.,
                  additionnal_title_content='', figsizecoeff=1, color=None, average="median", freqmin=None, freqmax=None) :
    '''Plot the power spectral density representation of one or several signals (superposed).

    Args:
        stream (obspy.core.stream) : the N signals
        fig_ax (tuple) : if one want to superpose plots, a tuple containing the
                        fig object and ax must be given.
        label (str) : in case of superposed plot, a labels
        window_unit (str) : define the unit of Nw, can be "sample" or "second"

    Output:
    -------
        - figure (pyplot.Figure) : the figure.
        - ax (pyplot.axe) : the axes of the figure.
    '''


    if fig_ax is None :
        if plotphase :
            fig, ax = plt.subplots((len(channels),2),sharex=True, figsize=[6.4*figsizecoeff, 4.8*figsizecoeff])
        else :
            fig, ax = plt.subplots(len(channels),sharex=True, figsize=[6.4*figsizecoeff, 4.8*figsizecoeff])
    else :
        fig, ax = fig_ax

    cpt = 0
    for ind, trace in enumerate(stream)  :
        if trace.stats.channel in channels :

            Nw = int(Nws * trace.stats.sampling_rate)
            Noverlap = int(Noverlaps * trace.stats.sampling_rate)
            window = get_window(windowtype, Nw)

            # fig.set_title("PSDs {} - {} - {}".format(trace.stats.starttime, trace.stats.starttime, trace.get_id()))
            f, S = sig.welch(trace.data, fs=trace.stats.sampling_rate, nperseg=Nw, noverlap=Noverlap, window=window, average="median")
            if correct_response :
                resp = trace.stats.response
                evalresp, freqs = resp.get_evalresp_response(
                    t_samp=1 / trace.stats.sampling_rate, nfft=Nw, output="ACC")
                assert np.all(f == freqs)
                # Get the amplitude response (squared)
                S /= np.absolute(evalresp * np.conjugate(evalresp))
            modS = np.abs(S)
            modlog = 10*np.log10(modS)
            if len(channels) > 1 :
                if plotphase :
                    ax[cpt, 0].loglog(f, modS.T, label=label, linestyle=linestyle, linewidth=linewidth, color=color)
                    if cpt==0 :
                        ax[cpt,0].legend()
                    ax[cpt,0].grid("on")
                    try :
                        unit = trace.stats.response.response_stages[0].input_units
                        ax[cpt,0].set_ylabel(r" $\frac{}\left({}\right)^2{}{}$".format("{",unit,"}", "{Hz}" ))
                    except  :
                        unit = ""
                        ax[cpt,0].set_ylabel(r"Amplitude (db)" )
                    ax[cpt,0].set_xlabel("Frequency (Hz)")
                    ax[cpt,0].set_title(trace.get_id() + "\n" + additionnal_title_content)
                    lines = ax[cpt,1].semilogx(f, np.angle(S).T, label=label, linestyle=linestyle, linewidth=linewidth, color=color)
                    ax[cpt,1].legend()
                    ax[cpt,1].grid("on")
                    ax[cpt,1].set_ylabel("Phase (rad)")
                    ax[cpt,1].set_xlabel("Frequency (Hz)")
                else :
                    ax[cpt].loglog(f, modS.T, label=label, linestyle=linestyle, linewidth=linewidth, color=color)
                    if cpt==0 :
                        ax[cpt].legend()
                    ax[cpt].grid("on")
                    try :
                        unit = trace.stats.response.response_stages[0].input_units
                        ax[cpt].set_ylabel(r"$\frac{}\left({}\right)^2{}{}$".format("{",unit,"}", "{Hz}" ))
                    except :
                        unit = ""
                        ax[cpt].set_ylabel(r"Amplitude (db)" )
                    ax[cpt].set_xlabel("Frequency (Hz)")
                    ax[cpt].set_title(trace.get_id()+ "\n" + additionnal_title_content)
                    if isinstance(freqmin, float) :
                        ax[cpt].axvline(freqmin, color="black")
                    if isinstance(freqmax, float) :
                        ax[cpt].axvline(freqmax, color="black")
            else :
                if plotphase :
                    ax[0].loglog(f, modS.T, label=label, linestyle=linestyle, linewidth=linewidth, color=color)
                    ax[0].legend()
                    ax[0].grid("on")
                    try :
                        unit = trace.stats.response.response_stages[0].input_units
                        ax[0].set_ylabel(r" $\frac{}\left({}\right)^2{}{}$".format("{",unit,"}", "{Hz}" ))
                    except :
                        unit = ""
                        ax[0].set_ylabel(r"Amplitude (db)" )
                    ax[0].set_xlabel("Frequency (Hz)")
                    ax[0].set_title(trace.get_id()+ "\n" + additionnal_title_content)
                    lines = ax[0,1].semilogx(f, np.angle(S).T, label=label, linestyle=linestyle, linewidth=linewidth, color=color)
                    ax[1].legend()
                    ax[1].grid("on")
                    ax[1].set_ylabel("Phase (rad)")
                    ax[1].set_xlabel("Frequency (Hz)")
                else :
                    ax.loglog(f, modS.T, label=label, linestyle=linestyle, linewidth=linewidth, color=color)
                    ax.legend()
                    ax.grid("on")
                    try :
                        unit = trace.stats.response.response_stages[0].input_units
                        ax.set_ylabel(r" $\frac{}\left({}\right)^2{}{}$".format("{",unit,"}", "{Hz}" ))
                    except :
                        unit = ""
                        ax.set_ylabel(r"Amplitude (db)" )
                    ax.set_xlabel("Frequency (Hz)")
                    ax.set_title(trace.get_id()+ "\n" + additionnal_title_content)
                    if isinstance(freqmin, float) :
                        ax.axvline(freqmin, color="black")
                    if isinstance(freqmax, float) :
                        ax.axvline(freqmax, color="black")
            cpt+=1
            plt.tight_layout()
    return(fig, ax)









def PlotStream(stream,  kind='-', linewidth=1.  ) :
    '''Plot all the traces in the stream.
    
    Args:
        stream (obspy.core.stream) : the stream whose traces are to be plotted
        centered (bool) : if True, the trace means are removed before plotting

    returns:
        fig (matplotlib.Figure) : the figure
    '''
    fig, ax = plt.subplots(len(stream),sharex=True, sharey=False, figsize=(5*len(stream), 8), height_ratios=[1.]*len(stream))


    for ind, trace in enumerate(stream) :
        nb_sec = trace.stats.endtime - trace.stats.starttime
        nbpts = trace.stats.npts
        dt =  stream[0].stats.delta
        t = np.arange(nbpts)*dt

        new_xticks = np.linspace(0,len(t), 8, dtype=int, endpoint=False)
        new_t = t[new_xticks]
        new_datetimes = [date_time.replace(microsecond=0) for date_time in datetime_vector(trace.stats.starttime.datetime, new_t)]

        try :
            unit = trace.stats.response.response_stages[0].input_units
        except AttributeError :
            unit=''

        # fig.suptitle(" {} - {} ".format(trace.stats.starttime, trace.stats.starttime))

        ax[ind].set_ylabel("{}\n{}".format(trace.get_id(), unit))
        ax[ind].grid()
        

  
        ax[ind].plot(trace.data, kind, linewidth=linewidth)
        ax[ind].set_xticks(new_xticks, new_datetimes, fontsize="small")
        plt.setp(ax[ind].get_xticklabels(), rotation=45)
        # ax[ind].legend()
        # ax[0].set_ylabel(r"$\frac{m}{s^2}$")
    ax[ind].set_xlabel("Date and time")
    plt.tight_layout()

    return(fig)

def PlotTrace(trace, fig=None, kind='-', linewidth=1., rasterize=False, alpha=1., label=None, color=None, percentile_maxmin=None, skipedges=True ) :
    '''Plot the trace.

    Args:
        trace (obspy.core.trace.Trace) : the trace
        fig_ax (tuple) : if one want to superpose plots, a tuple containing the
                        fig object and ax must be given.
        label (str) : in case of superposed plot, a labels
    
    returns:
        fig (matplotlib.Figure) : the figure
    '''
    if fig is None :
        fig, ax = plt.subplots(1, figsize=(10,3))
    else :
        fig, ax = fig, fig.axes[0]


    try :
        unit = trace.stats.response.response_stages[0].input_units
    except AttributeError :
        unit=''
    
    data = trace.data.copy()

    if skipedges:
        ### deleting the edges
        nbedgescount= int(0.01*trace.stats.npts)
        data[:nbedgescount] = np.nan 
        data[-nbedgescount:] = np.nan

    nb_sec = trace.stats.endtime - trace.stats.starttime
    nbpts = trace.stats.npts
    dt =  trace.stats.delta
    t = np.arange(nbpts)*dt
    new_xticks = np.linspace(0,len(t), 8, dtype=int, endpoint=False)
    new_t = t[new_xticks]
    new_datetimes = [date_time.replace(microsecond=0) for date_time in datetime_vector(trace.stats.starttime.datetime, new_t)]

    #ax.set_title(trace.get_id())

    ax.plot(t, data, kind, linewidth=linewidth, rasterized=rasterize, label=label, color=color)
    ax.set_xticks(new_xticks, new_datetimes, fontsize="small")
    plt.setp(ax.get_xticklabels(), rotation=25)
    if fig is not None :
        h,l = ax.get_legend_handles_labels()
        ax.legend(h,l,fontsize=10)
    else :
        ax.legend(fontsize=10)
    if isinstance(percentile_maxmin, (int, float)):
        ax.set_ylim(bottom=np.nanpercentile(data,100-percentile_maxmin), top=np.nanpercentile(data,percentile_maxmin))
    ax.grid(visible=True)
    ax.set_xlabel("Date and time")
    ax.set_ylabel("{}\n{}".format(trace.get_id(), unit))

    plt.tight_layout()

    return(fig)


def PlotTraceHistogram(trace, fig=None, kind='-', linewidth=1., rasterize=True, alpha=1., label=None, color=None, bins=1000, skipedges=True ) :
    '''Plot the trace histogram.

    Args:
        trace (obspy.core.trace.Trace) : the trace
        fig_ax (tuple) : if one want to superpose plots, a tuple containing the
                        fig object and ax must be given.
        label (str) : in case of superposed plot, a labels
    
    returns:
        fig (matplotlib.Figure) : the figure
    '''
    if fig is None :
        fig, ax = plt.subplots(1, figsize=(10,3))
    else :
        fig, ax = fig, fig.axes[0]


    try :
        unit = trace.stats.response.response_stages[0].input_units
    except AttributeError :
        unit=''

    data = trace.data.copy()

    if skipedges:
        ### deleting the edges
        nbedgescount= int(0.005*trace.stats.npts)
        data[:nbedgescount] = np.nan 
        data[-nbedgescount:] = np.nan


    
    hist, bin_edges = np.histogram(data, bins=bins, range=(np.nanpercentile(trace.data,0), np.nanpercentile(trace.data,100)))
    bin_center = (bin_edges[:-1] + bin_edges[1:])/2
    std = np.nanstd(data)
    #ax.set_title(trace.get_id())

    ax.plot(bin_center, hist, kind, linewidth=linewidth, rasterized=rasterize, label=label + "\n std={:.3E} {}".format(std, unit), color=color)
    ax.set_yscale("log")
    if fig is not None :
        h,l = ax.get_legend_handles_labels()
        ax.legend(h,l)
    else :
        ax.legend()

    ax.grid(visible=True)
    ax.set_xlabel("{}\n{}".format(trace.get_id(), unit))
    ax.set_ylabel("Count")

    plt.tight_layout()

    return(fig)

def PlotTraceScatter(trace1, trace2,  label1, label2,  rasterize=True, alpha=1., label=None, color=None, bins=1000 ) :
    '''Plot the trace histogram.

    Args:
        trace1 (obspy.core.trace.Trace) : the trace1
        trace1 (obspy.core.trace.Trace) : the trace2
        fig_ax (tuple) : if one want to superpose plots, a tuple containing the
                        fig object and ax must be given.
        label (str) : in case of superposed plot, a labels
    
    returns:
        fig (matplotlib.Figure) : the figure
    '''
    fig, ax = plt.subplots(1, figsize=(10,3))



    try :
        unit1 = trace1.stats.response.response_stages[0].input_units
    except AttributeError :
        unit1=''
    
    try :
        unit2 = trace2.stats.response.response_stages[0].input_units
    except AttributeError :
        unit2=''

    

    ax.plot(np.abs(trace1.data), np.abs(trace2.data), '.' , rasterized=rasterize, color=color)
    if fig is not None :
        h,l = ax.get_legend_handles_labels()
        ax.legend(h,l)
    else :
        ax.legend()

    ax.grid(visible=True)
    ax.set_xlabel("{}\n{}".format(label1, unit1))
    ax.set_ylabel("{}\n{}".format(label2, unit2))

    plt.tight_layout()

    return(fig)

# def PlotStream_plotly(stream, centered=False, fig=None, label=None, kind='-', linewidth=1.  ) :
#     '''
#     Plot all the traces in the stream using the plotly library
#     Parameters :
#     -------------
#     stream (obspy.core.stream) - the stream whose traces are to be plotted
#     centered (bool) - if True, the trace means are removed before plotting
#     fig_ax (tuple) - if one want to superpose plots, a tuple containing the
#                      fig object and ax must be given.
#     label (str) - in case of superposed plot, a labels
#     '''
#     from plotly.subplots import make_subplots
#     import plotly.graph_objects as go
#     if fig is None :
#         fig = make_subplots(rows=len(stream),shared_xaxes=True)
#     else :
#         pass

#     if len(stream) > 4 :
#         raise ValueError("The stream you want to plot contains too many channels")
#     ### isolate the accelerometer and pressure traces
#     ind_tr = [1,2,3,4] # dumb init
#     trace_len = stream[0].stats.npts

#     for ind, trace in enumerate(stream) :
#         nb_sec = trace.stats.endtime - trace.stats.starttime
#         nbpts = trace.stats.npts
#         dt =  stream[0].stats.delta
#         t = np.arange(nbpts)*dt

#         #fig.suptitle(" {} - {} ".format(trace.stats.starttime, trace.stats.starttime))
#         if trace.get_id()[-1] in ["1", "N"] :
#             ind_tr = 0
#         elif trace.get_id()[-1] in ["2", "E"] :
#             ind_tr = 1
#         elif trace.get_id()[-1] in ["Z"] :
#             ind_tr = 2
#         elif trace.get_id()[-1] in ["H"] :
#             ind_tr = 3

#         fig['layout'][f'xaxis{ind_tr+1}'].update(title=trace.get_id())
#             #fig.update_title(trace.get_id(), row=ind_tr+1)
#         if label is not None :
#             fig.add_trace(go.Scatter(x=t, y=trace.data, mode="lines",name=label+trace.get_id()), row=ind_tr+1 , col=1)#, kind, label=label + trace.get_id(), linewidth=linewidth)
#         else :
#              fig.add_trace(go.Scatter(x=t, y=trace.data, mode="lines"), row=ind_tr+1, col=1)

#         # ax[0].set_ylabel(r"$\frac{m}{s^2}$")
#         fig.update_xaxes(title_text="Time (s)", row=ind_tr+1, col=1)
#     #plt.tight_layout()

#     return(fig)

# def PlotTraceEnergy(Data, nperseg, window="boxcar", title="") :
#     '''
#     Parameters
#     -----------
#         Data (list of obspy.core.trace.Trace) - the list containing the 4 sliced
#                                                 OBS traces. The user must be
#                                                 careful on how much data he
#                                                 gives to the function, which may
#                                                 takes a large amount of RAM.
#         nperseg (int) : the length -1  of the window
#         window (str) : the type of window to use, see [1], default is a square
#                         window.
#     Outputs
#     ---------
#         fig (pyplot.Figure) - the figure on which the energies are drawn.
#         [Ea, Eb, Ec, Ed] - tuple containing the energies (ndarray)
#         [ta, tb, tc, td] - tuple containing the time series (ndarray)
#     '''
#     Ea = SlidingPowerEstimation(Data[0].data, 32, window=window)
#     Eb = SlidingPowerEstimation(Data[1].data, 32, window=window)
#     Ec = SlidingPowerEstimation(Data[2].data, 32, window=window)
#     Ed = SlidingPowerEstimation(Data[3].data, 32, window=window)
#     ### generating time axis for the time series
#     ta_seconds = Data[0].stats.endtime-Data[0].stats.starttime
#     ta = np.linspace(0,ta_seconds+Data[0].stats.delta,Data[0].stats.npts)
#     tb_seconds = Data[1].stats.endtime-Data[1].stats.starttime
#     tb = np.linspace(0,tb_seconds + Data[1].stats.delta,Data[1].stats.npts)
#     tc_seconds = Data[2].stats.endtime-Data[2].stats.starttime
#     tc = np.linspace(0,tc_seconds + Data[2].stats.delta,Data[2].stats.npts)
#     td_seconds = Data[3].stats.endtime-Data[3].stats.starttime
#     td = np.linspace(0,td_seconds + Data[3].stats.delta,Data[3].stats.npts)

#     fig_E, axe_E = plt.subplots(4, sharex=True, sharey=True)
#     axe_E[0].plot(ta,Ea)
#     axe_E[0].set_ylabel(tr_a.stats.channel)
#     axe_E[0].margins(0,0.1)
#     axe_E[1].plot(tb, Eb)
#     axe_E[1].set_ylabel(tr_b.stats.channel)
#     axe_E[1].margins(0,0.1)
#     axe_E[2].plot(tc, Ec)
#     axe_E[2].set_ylabel(tr_c.stats.channel)
#     axe_E[2].margins(0,0.1)
#     axe_E[3].plot(td, Ed)
#     axe_E[3].set_ylabel(tr_d.stats.channel)
#     axe_E[3].margins(0,0.1)
#     axe_E[3].set_xlabel("Time (sec)")
#     fig.suptitle(title)

#     return(fig, [Ea, Eb, Ec, Ed], [ta, tb, tc, td])

# def PlotSpectrograms(f, t, spectrograms, plot_phase=False, log_amp=True, titles=None) :
#     '''
#     Plot the given spectrograms
#     Parameters :
#     --------------
#         f (array) - the frequency vector
#         t (array) - the time vector
#         spectrograms (ndarray or list/tuple of ndarray) - the spectrogram(s) to be drawn, ndarray
#         titles (str or list of str) - the title(s) of the spectrogram(s).
#                                                           dimensions should be len(f) times len(t)
#         plot_phase (bool) - not implemented yet
#     Outputs :
#     --------------
#         fig (pyplot.figure) - the figure
#     '''
#     if isinstance(spectrograms, list) or isinstance(spectrograms, tuple):
#         nb_spec = len(spectrograms)
#     elif isinstance(spectrograms, np.ndarray) :
#         nb_spec = 1
#         spectrograms = [spectrograms]

#     if plot_phase :
#         fig, ax = plt.subplots(nb_spec,2 , sharex=True, sharey=False)
#     else:
#         fig, ax = plt.subplots(nb_spec, sharex=True, sharey=True)

#     if titles is None :
#         titles = [""]*nb_spec
#     if nb_spec == 1 :
#         ax = [ax]
#     for k, spec in enumerate(spectrograms) :
#         if log_amp:
#             spa_im = ax[k].pcolormesh(t, f, 10*np.log10(np.abs(spec)))
#         else :
#             spa_im = ax[k].pcolormesh(t, f, np.abs(spec))
#         ax[k].set_xlabel("time (seconds)")
#         ax[k].set_ylabel("Frequency (Hz)")
#         ax[k].set_yscale("symlog", linthresh=f[1])
#         ax[k].set_title(titles[k])

#     return(fig)


# def plot_time_series_and_spectrograms(Data, window="hamming", nperseg=256, noverlap=None, title="") :
#     """
#     Plot the time series and their spectrogram for the 4 axis of OBS data using
#     Scipy instead of the OBS spectrogram function.
#     Inputs :
#         Data (list of obspy.core.trace.Trace) - the list containing the 4 sliced
#                                                 OBS traces. The user must be
#                                                 careful on how much data he
#                                                 gives to the function, which may
#                                                 takes a large amount of RAM.
#         window (str)   - The window which will be used by the scipy spectrogram
#                          function, for more infos, refers to ¹. Default Hamming
#         nperseg (int)  - The number of samples covered by each window. Default
#                          256
#         noverlap (int) - The number of samples which are overlapping between
#                          each windows. Default nperseg//8

#     """
#     if noverlap is None :
#         noverlap = nperseg//8

#     ### creating the figure subplots with the shared x axis
#     fig, ax = plt.subplots(4,2, sharex=True, sharey=False)

#     ### generating time axis for the time series
#     ta_seconds = Data[0].stats.endtime-Data[0].stats.starttime
#     ta = np.linspace(0,ta_seconds+Data[0].stats.delta,Data[0].stats.npts)
#     tb_seconds = Data[1].stats.endtime-Data[1].stats.starttime
#     tb = np.linspace(0,tb_seconds + Data[1].stats.delta,Data[1].stats.npts)
#     tc_seconds = Data[2].stats.endtime-Data[2].stats.starttime
#     tc = np.linspace(0,tc_seconds + Data[2].stats.delta,Data[2].stats.npts)
#     td_seconds = Data[3].stats.endtime-Data[3].stats.starttime
#     td = np.linspace(0,td_seconds + Data[3].stats.delta,Data[3].stats.npts)

#     ### calculating the spectrograms

#     fa, taw, Sxxa = sig.spectrogram(Data[0].data, fs=Data[0].stats.sampling_rate, window=window, nperseg=nperseg, noverlap=noverlap)
#     fb, tbw, Sxxb = sig.spectrogram(Data[1].data, fs=Data[1].stats.sampling_rate, window=window, nperseg=nperseg, noverlap=noverlap)
#     fc, tcw, Sxxc = sig.spectrogram(Data[2].data, fs=Data[2].stats.sampling_rate, window=window, nperseg=nperseg, noverlap=noverlap)
#     fd, tdw, Sxxd = sig.spectrogram(Data[3].data, fs=Data[3].stats.sampling_rate, window=window, nperseg=nperseg, noverlap=noverlap)

#     ### Ploting the time series
#     ax[0,0].plot(ta, Data[0].data)
#     ax[0,0].set_title("Channel : {}".format(Data[0].stats.channel))
#     # ax[0,0].set_ylabel() #to be completed

#     ax[2,0].plot(ta, Data[1].data)
#     ax[2,0].set_title("Channel : {}".format(Data[1].stats.channel))
#     # ax[3,0].set_ylabel() #to be completed

#     ax[0,1].plot(ta, Data[2].data)
#     ax[0,1].set_title("Channel : {}".format(Data[2].stats.channel))
#     # ax[0,2].set_ylabel() #to be completed

#     ax[2,1].plot(ta, Data[3].data)
#     ax[2,1].set_title("Channel : {}".format(Data[3].stats.channel))
#     # ax[3,2].set_ylabel() #to be completed

#     ### Ploting the spectrograms
#     spa_im = ax[1,0].pcolormesh(taw, fa, Sxxa)
#     ax[1,0].set_xlabel("time (seconds)")
#     ax[1,0].set_ylabel("Frequency (Hz)")
#     ax[1,0].set_yscale("symlog")
#     spb_im = ax[3,0].pcolormesh(tbw, fb, Sxxb)
#     ax[3,0].set_xlabel("time (seconds)")
#     ax[3,0].set_ylabel("Frequency (Hz)")
#     ax[3,0].set_yscale("symlog")
#     spc_im = ax[1,1].pcolormesh(tcw, fc, Sxxc)
#     ax[1,1].set_xlabel("time (seconds)")
#     ax[1,1].set_ylabel("Frequency (Hz)")
#     ax[1,1].set_yscale("symlog")
#     spd_im = ax[3,1].pcolormesh(tdw, fd, Sxxd)
#     ax[3,1].set_xlabel("time (seconds)")
#     ax[3,1].set_ylabel("Frequency (Hz)")
#     ax[3,1].set_yscale("symlog")


#     fig.suptitle(title)
#     plt.tight_layout()
#     return(fig)


# def PlotFrequencyResponse(f, H, title='', shift=False, fig_ax=None, label="", figsizecoeff=1., unwrap=True, plot_imreal=False, xscale="log", ytransform="log", linestyle="solid") :
#     if fig_ax is None :
#         if plot_imreal :
#             fig, ax = plt.subplots(3,sharex=True, sharey=False, figsize=[6.4*figsizecoeff, 4.8*figsizecoeff])
#         else:
#             fig, ax = plt.subplots(2,sharex=True, sharey=False, figsize=[6.4*figsizecoeff, 4.8*figsizecoeff])
#     else :
#         fig, ax = fig_ax
#     fig.suptitle(title)

#     for ind in range(len(ax)) :
#         ax[ind].set_xscale(xscale)

#     if shift :
#         f1 = fftshift(f)
#         H1 = fftshift(H)
#     else :
#         f1 = f
#         H1 = H

#     if ytransform == "log" :
#         mod = 10*np.log10(np.abs(H1))
#     elif ytransform == "linear" :
#         mod = np.abs(H1)

#     if unwrap :
#         arg = np.unwrap(np.angle(H1), period=2*np.pi)
#     else :
#         arg = np.angle(H1)



#     ax[0].set_ylabel("Magnitude (db)")
#     #if xscale=="log" :
#         #ax[0].semilogx(f1, mod, label=label)
#         #ax[1].semilogx(f1, arg, label=label)
#     #elif xscale=="linear" :
#     ax[0].plot(f1, mod, label=label, linestyle=linestyle)
#     ax[1].plot(f1, arg, label=label, linestyle=linestyle)
#     #else :
#         #raise ValueError("Unsupported xscale")
#     if plot_imreal :
#         ax[2].plot(f1, H1.real, label="real part of " + label)
#         ax[2].plot(f1, H1.imag, label="imaginary part of " + label)
#     ax[0].grid()
#     ax[0].legend()
#     ax[1].set_ylabel("Phase (rad)")
#     ax[1].set_xlabel("Frequency (Hz)")
#     ax[1].grid()

#     return(fig, ax)

# def PlotCoherence(f, C, title='', shift=False, linestyle='solid', fig_ax=None, label=None) :
#     if fig_ax is None :
#         fig, ax = plt.subplots(1,sharex=True, sharey=False)
#     else :
#         fig, ax = fig_ax
#     fig.suptitle(title)

#     ax.set_ylabel("Correlation")
#     if shift :
#         ax.semilogx(fftshift(f), np.abs(fftshift(C)), linestyle=linestyle, label=label)
#     else :
#         ax.semilogx(f, np.abs(C), linestyle=linestyle, label=label)
#     ax.grid()
#     ax.set_xlabel("Frequency (Hz)")
#     ax.grid()
#     ax.legend()
#     return(fig, ax)




####################### DEPRECATED 
#def get_window(windowtype, N) :
#     '''
#     An alternative to scipy.signal.get_window() function which seems to be buggy regarding windows that needs several argument (dpss, kaiser...)
#     take exactly the same inputs as signal.get_window
#     forked from scipy.signal.get_window()
#     '''
#     winfuncdict = dict({(key, var) for key, var in vars(sig.windows).items() if not key.startswith('__')})

#     if isinstance(windowtype, str) :
#         try :
#             winfunc = winfuncdict[windowtype]
#         except KeyError :
#             raise ValueError("Unknown windowtype : {}".format(windowtype))
#         window = winfunc(N)
#     elif isinstance(windowtype, tuple) :
#         try :
#             #print(dict(winfuncdict).keys())
#             #print(windowtype[0])
#             winfunc = winfuncdict[windowtype[0]]
#         except KeyError :
#             raise ValueError("Unknown windowtype : {}".format(windowtype))
#         params = (N,) + windowtype[1:]
#         window = winfunc(*params)
#     elif isinstance(windowtype, np.ndarray) : ### convenience condition if the windowtype ise already a window ndarray
#         window=windowtype
#     else :
#         raise ValueError("windowtype must be a str or a tuple, not {}".format(type(windowtype)))
#     return(window)


# def NoisefulWindowStreamEstimation(stream, Nw, windowtype="boxcar", channels = ["1", "2", "Z", "H"], plot=False, return_ind=False) :
#     '''
#     Determine a UTCDateTime window where the data contains only noise in order to compute the frequency response
#     that would be used to clean the data afterward. It is based on an energy criterion, the selected window is
#     the one containing the least energy. It is assumed that the noise level is way below the signal level.
#     Parameters :
#     ------------
#         stream (obspy.Stream) : The stream which will be analysed
#         Nw (int) : the length of the analysing window in seconds
#         window_type (str) : a str descibing the window (see the scipy.signal.get_window() documentation
#         channels (list of str) : a list describing the channels to be used
#     Output :
#     ------------
#         starttime (obspy.UTCDateTime) : the start of the window
#         endtime   (obspy.UTCDateTime) : the end of the window
#     '''

#     stream_copy = stream.copy()


#     if len(stream) > 4 :
#         raise ValueError("The stream you want to rotate contains too many channels")
#     trace_len = stream[0].stats.npts
#     fs = stream[0].stats.sampling_rate
#     len_window = int(fs * Nw)
#     starttime = stream[0].stats.starttime
#     endtime = stream[0].stats.endtime



#     for ind, trace in enumerate(stream) : ### necessary checks
#         if trace.stats.npts != trace_len :
#             raise ValueError("The traces are not of the same length")
#         if trace.stats.sampling_rate != fs :
#             raise ValueError("The traces are not of the same sampling rate")

#     Energy = np.zeros([len(channels), trace_len])

#     if windowtype in ['prol1pi', 'prol4pi', 'hanning']:
#         window = choose_window_scipy(windowtype, len_window)
#     else :
#         window = get_window(windowtype, len_window)


#     cpt = 0
#     for ind, trace in enumerate(stream) :
#         if trace.get_id()[-1] in channels :
#             Energy[cpt, :] = sig.convolve(trace.data**2, window, mode="same")
#             cpt += 1

#     if len(channels) >1 :
#         Crit_Energy = np.product(Energy, axis=0)
#     else :
#         Crit_Energy = Energy
#     Crit_Energy[:len_window] = np.max( Crit_Energy)
#     Crit_Energy[-1-len_window:] = np.max( Crit_Energy)

#     indmin = np.argmin(Crit_Energy) # getting the indice of the minimal energy window center
#     start_delta= (indmin - len_window//2)
#     end_delta = (indmin + len_window//2)
#     if return_ind == False :
#         ### getting the UTCDateTime
#         window_starttime = starttime + start_delta/fs
#         window_endtime = starttime + end_delta/fs
#     else :
#         window_starttime = start_delta
#         window_endtime = end_delta

#     grad_Crit_Energy = np.gradient(Crit_Energy)

#     if plot :
#         fig, ax =  plt.subplots(2)
#         ax[0].set_title("Energy criterion")
#         ax[0].plot(np.arange(trace_len)/fs, Crit_Energy)
#         ax[0].set_xlabel("Time (s)")
#         ax[0].grid()
#         ax[1].set_title("Energy criterion gradient")
#         ax[1].plot(np.arange(trace_len)/fs, grad_Crit_Energy)
#         ax[1].set_xlabel("Time (s)")
#         ax[1].grid()

#     del grad_Crit_Energy, Crit_Energy, Energy

#     return(window_starttime, window_endtime)

# def PlotStreamPSD_plotly(stream, fig=None, label=None, plotphase=False, Nws=2048, Noverlaps=0,
#                   channels=["H", "1", "2", "Z"], windowtype=('dpss', 1), correct_response=False, linestyle="solid", linewidth=1.,
#                   additionnal_title_content='', figsizecoeff=1) :
#     '''
#     Plot the power spectral density representation of one or several signals
#     (superposed)
#     Parameters
#     ------------
#     stream (obspy.core.stream) - the N signals
#     fig (plotly.fig) - if one want to superpose plots, a tuple containing the
#                      fig object and ax must be given.
#     label (str) - in case of superposed plot, a labels
#     window_unit (str) - define the unit of Nw, can be "sample" or "second"

#     Output
#     ------------
#     figure (plotly.Figure) - the figure
#     '''
#     from plotly.subplots import make_subplots
#     import plotly.graph_objects as go

#     if label is None :
#         label=""

#     if fig is None :
#         #fig = go.Figure(height=int(800*figsizecoeff), width=int(600*figsizecoeff))
#         if plotphase :
#             fig = make_subplots(rows=len(stream),cols=2, shared_xaxes=True)
#         else :
#             fig = make_subplots(rows=len(stream),shared_xaxes=True)
#     else :
#         pass

#     cpt = 0
#     for ind, trace in enumerate(stream)  :
#         if trace.get_id()[-1] in channels :

#             Nw = int(Nws * trace.stats.sampling_rate)
#             Noverlap = int(Noverlaps * trace.stats.sampling_rate)
#             window = get_window(windowtype, Nw)

#             # fig.set_title("PSDs {} - {} - {}".format(trace.stats.starttime, trace.stats.starttime, trace.get_id()))
#             f, S = sig.welch(trace.data, fs=trace.stats.sampling_rate, nperseg=Nw, noverlap=Noverlap, window=window)
#             if correct_response :
#                 resp = trace.stats.response
#                 evalresp, freqs = resp.get_evalresp_response(
#                     t_samp=1 / trace.stats.sampling_rate, nfft=Nw, output="ACC")
#                 assert np.all(f == freqs)
#                 # Get the amplitude response (squared)
#                 S /= np.absolute(evalresp * np.conjugate(evalresp))
#             modS = np.abs(S)
#             modlog = 10*np.log10(modS)

#             #dfmod = pd.DataFrame(data=modlog, index=f, columns=[label] )
#             #dfang = pd.DataFrame(data=np.angle(S), index=f, columns=[label] )
#             #if len(channels) > 1 :
#             #fig['layout'][f'xaxis{cpt+1}'].update(title=trace.get_id())
#             fig.update_layout(title="Power spectral densities of station {}".format(trace.stats.station))
#             #fig.update_layout(annotations=[{ "text":"Power spectral densities of station {}".format(trace.stats.channel)}, row=cpt+1, col=1])
#             linemod = go.Scatter(x=f, y=modlog, name=label + trace.stats.channel)
#             fig.add_trace(linemod, row=cpt+1 , col=1)
#             fig.update_xaxes(title_text="frequency (Hz)", row=cpt+1, col=1, type="log")
#             fig.update_yaxes(title_text="Magnitude (dB)", row=cpt+1, col=1)
#             if plotphase :
#                 fig.add_trace(go.line(dfang, log_x=True), row=cpt+1 , col=2)
#                 fig.update_xaxes(title_text="frequency (Hz)", row=cpt+1, col=2)
#                 fig.update_yaxes(title_text="Phase (rad)", row=cpt+1, col=2)
#             fig.update_layout(height=int(800*figsizecoeff), width=int(600*figsizecoeff))
#             #else :
#                 #if plotphase :
#                     #ax[0].semilogx(f, modlog.T, label=label, linestyle=linestyle, linewidth=linewidth)
#                     #ax[0].legend()
#                     #ax[0].grid("on")
#                     #ax[0].set_ylabel("Amplitude (dB)")
#                     #ax[0].set_xlabel("Frequency (Hz)")
#                     #ax[0].set_title(trace.get_id()+ "\n" + additionnal_title_content)
#                     #lines = ax[0,1].semilogx(f, np.angle(S).T, label=label, linestyle=linestyle, linewidth=linewidth)
#                     #ax[1].legend()
#                     #ax[1].grid("on")
#                     #ax[1].set_ylabel("Phase (rad)")
#                     #ax[1].set_xlabel("Frequency (Hz)")
#                 #else :
#                     #ax.semilogx(f, modlog.T, label=label, linestyle=linestyle, linewidth=linewidth)
#                     #ax.legend()
#                     #ax.grid("on")
#                     #ax.set_ylabel("Amplitude (dB)")
#                     #ax.set_xlabel("Frequency (Hz)")
#                     #ax.set_title(trace.get_id()+ "\n" + additionnal_title_content)
#             cpt+=1
#     return(fig)