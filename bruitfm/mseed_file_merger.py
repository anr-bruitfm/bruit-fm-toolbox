 #! /usr/bin/bash
### Author  :  SIMON REBEYROL
### Company :  IFREMER, UMR GEOOCEAN

""" 
This script let the user merge multiple mseed seismological files bunched in a directory as well as optionnaly remove 
the instrument response, detect and remove raw anomalies, resample the data and detect and remove transcient events.
The flowchart, allowing all the option, is described as follow:
    1. Reading all the files and concatenating them into a single stream object. 
    2. Removing the unprecised channels from each traces in the stream. 
    3. Merging the traces of the stream using the 1st method of stream.merge. 
    4. Detecting RAW anomalies like 0 and saturated data and forcing the corresponding sample values to 0. 
    5. Resampling the data using a linear interpolation conducted after an anti-aliasing filtering.
    6. Converting the RAW data to the target unit using the provided instrument response.
    7. Detecting all transcient event using a std threshold. The threshold is calculated from the 95 percentile std value
       on a sliding std window. Then, force the corresponding event sample values to 0.

Several warning must be given:
    - Be very careful about what data are to be merged, espacially about the seismological ones in acceleration unit. Removing
      the instrument response may produce unwanted trend in the final data if some files contains a high amount of corrupted data.
    - Using this script on a huge amount of file (> several months worth of data) will end up in high computation cost (RAM and time).
    - The number of thread option is only useful to speed up the reading of the file, parallelization does not comes with the rest of 
      the (pre)processing steps.

bash call  ``python mseed_file_merger.py  [parameters]``.

Parameters :
------------

    -h, --help                      :   
                                        Show this help.

    -i, --input_dir   [str]         :   
                                        The path to the  directory containing the mseed files
                                        whose instrument response is to be removed.

    -r, --input_response  [str]     :   
                                        The path to the XML file containing the station instrument response.
    
    -u, --unit [str]                :   
                                        The unit towards what the seismometer components will be converted.
                                        Can be "VEL" for velocity or "ACC" for acceleration, or RAW (for no
                                        conversion), default = RAW.
                                        If "RAW" is given, the instrument response is not removed.

    -n, --nb_threads [int]          :   
                                        The number of threads to create (each thread will be tasked to
                                        remove the instrument response of one mseed file), default = 1.

    -c, --channels [str]            :   
                                        The channels to be processed and kept in the final files. Each
                                        channel name must be separated by a semicolon ;.
                                        A wrong channel will lead to an error during the process.
                                        Default,  keep and process all the channels.


    --dpg_correction_factor [float] : 
                                        The correction factor of the differential pressure gauge, for more info, see the
                                        supplement material in [5]. Default = 1.0.
                                        

    -f, --sampling_frequency [float]:  
                                        Resample the data to the given sampling frequency, default=None .

    -a, --remove_anomalies          :   
                                        Set all the raw anomalies to  zeros after all the processings, 
                                        with a smoothing factor to avoid most of the Gibbs oscillations.

    -e, --detect_events             :   
                                        Automatically detect events using a threshold based on the data
                                        distribution. May produce false positive.


    --nmirror       [int]           :   
                                        The amount of sample mirroring used to mitigate the stabilization 
                                        effect during the Fourier deconvolution. Keep in mind that too 
                                        many samples will transform the signal (half the signal size
                                        for instance). default = 0.

    --highpass          [float]     :   
                                        Apply a highpass 10th order butterworth phaseless filter with a cutting frequency
                                        given by the user to filter any spurious, default= 1e-4 Hz
                                        trend, and the mean value. Very efficient to avoid problems on very long-term
                                        acceleration signals.

    --lowpass                       :   
                                        Apply a lowpass 10th order butterworth phaseless filter with a cutting frequency
                                        located a decade under the maximal frequency to filter any transcient impulse.

    --remove_impulses               :   
                                        Detect residuals impulses values and put the corresponding signal values to 0, 
                                        make sure to use the --highpass option.


OUTPUTS :
    a subdirectory containing the merged mseed file within the input directory will be created.

"""


import sys
import os
os.environ["OMP_NUM_THREADS"] = "4" # export OMP_NUM_THREADS=4
os.environ["OPENBLAS_NUM_THREADS"] = "4" # export OPENBLAS_NUM_THREADS=4
os.environ["MKL_NUM_THREADS"] = "4" # export MKL_NUM_THREADS=6
os.environ["VECLIB_MAXIMUM_THREADS"] = "4" # export VECLIB_MAXIMUM_THREADS=4
os.environ["NUMEXPR_NUM_THREADS"] = "4" # export NUMEXPR_NUM_THREADS=6
import numpy as np
import obspy
import matplotlib.pyplot as plt
from tqdm import tqdm
import platform as pl
import warnings
warnings.filterwarnings( "ignore", module = "obspy\..*" )

import getopt

try :
    from bruitfm.utils import *
    from bruitfm import signal_utils as sptl 
    from bruitfm import preprocessing as pre
except ImportError:
    from utils import *
    import signal_utils as sptl
    import preprocessing as pre

if pl.system() in 'Linux' :
    path_separator = '/'
elif pl.system() in 'Darwin' : # for MacOS
    path_separator = '/'
elif pl.system() in 'Windows' :
    path_separator = '\\'
else :
    raise OSError("Unsuported OS, the suported ones are Linux, MacOS and Windows, you are on : " +  pl.system())

if __name__=="__main__" :

    ################################################################################
    ##################### Parameters management with getopt ########################
    ################################################################################
    shortopts = "hi:r:n:u:c:f:ae"
    longopts = ["help", "input_dir=", "input_response=", 
                "nb_threads=", "unit=", "channels=", "nmirror=",
                "sampling_frequency=", "remove_anomalies", "detect_events",
                "highpass=", "lowpass", "remove_impulses", "dpg_correction_factor="]
    try:
        opts, args = getopt.gnu_getopt(sys.argv[1:], shortopts, longopts)
    except getopt.GetoptError as msg:
        #Print help information and exit
        print(msg)
        print("For help use --help")
        sys.exit(2)

    flag = {}
    flag["input_dir"] = False
    flag["input_response"] = False
    flag["nb_threads"] = False
    flag["unit"] = False
    flag["channels"] = False
    flag["sampling_frequency"] = False
    flag["remove_anomalies"] = False
    flag["detect_events"] = False
    flag["nmirror"] = False
    flag["highpass"] = False
    flag["lowpass"] = False
    flag["remove_impulses"] = False
    flag["dpg_correction_factor"] = False

    ### Cheking inputs parameters
    for o, a in opts:

        if o in ("-h", "--help"):
            print(__doc__)
            sys.exit(0)

        elif o in ("-i", "--input_dir"):
            flag["input_dir"] = True
            if a[-1] == path_separator :
                input_dir = a
            else :
                input_dir = a + path_separator

        elif o in ("-r", "--input_response"):
            flag["input_response"] = True
            input_response = obspy.read_inventory(a)

        elif o in ("--nmirror"):
            flag["nmirror"] = True
            nmirror = int(a)
        
        elif o in ("-u", "--unit") :
            flag["unit"] = True 
            unit = a

        elif o in ("-c", "--channels") :
            flag["channels"] = True 
            channels = a.split(";")

        elif o in ("-f", "--sampling_frequency") :
            flag["sampling_frequency"] = True 
            sampling_frequency = float(a)
        
        elif o in ("--dpg_correction_factor") :
            flag["dpg_correction_factor"] = True 
            dpg_correction_factor = float(a)

        elif o in ("-a", "--remove_anomalies") :
            flag["remove_anomalies"] = True

        elif o in ("-e", "--detect_events") :
            flag["detect_events"] = True 

        elif o in ("--highpass") :
            flag["highpass"] = True
            highpass_fc = float(a)

        elif o in ("--lowpass") :
            flag["lowpass"] = True
            
        elif o in ("--remove_impulses") :
            flag["remove_impulses"] = True

    ### Dealing with default parameters and input errors
    if flag["input_dir"] == False :
        print("INPUT ERROR : you must provide the input folder, see help with -h.", flush=True)
        sys.exit(1)

    if not flag["input_response"] :
        flag["input_response"] = True 
        input_response = None
    if (unit!="RAW") and not flag["input_response"]  :
        print("INPUT ERROR : you must provide the instrument response XML file, see help with -h.", flush=True)
        sys.exit(1)

    if flag["nb_threads"] == False :
        nb_threads = 1

    if not flag["unit"]  :
        unit = "RAW"

    if not flag["dpg_correction_factor"] :
        dpg_correction_factor = 1.

    if not flag["sampling_frequency"] :
        sampling_frequency = None

    if unit not in ["VEL", "ACC", "RAW"] :
        print("INPUT ERROR : unkown input unit, se help with -h.", flush=True)

    if not flag["nmirror"] :
        nmirror = 0

    if not flag["highpass"] :
        highpass_fc = 1e-4

    flag["remove_anomalies_event"] = flag["remove_anomalies"] or flag["detect_events"]
    ################################################################################
    ################################################################################
    ################################################################################

    list_input_file = os.listdir(input_dir)
    file_paths = [input_dir + in_file for in_file in  list_input_file]

    if not os.path.exists(input_dir + "corrected_stream") :
        os.mkdir(input_dir + "corrected_stream")

    print("Reading the files to merge\n", flush=True)
    for file_path in tqdm(file_paths) :
        try :
            if "stream" not in locals() :
                stream = obspy.read(file_path)
            else :
                stream = stream + obspy.read(file_path)
        except IOError as error :
            pass

    print("Selecting the channels\n", flush=True)
    stream.sort(["starttime"])
    if flag["channels"] :
        char_cha_1 = [a[0] for a in channels]
        char_cha_2 = [a[1] for a in channels]
        char_cha_3 = [a[2] for a in channels]
        stream = select_related_channels(stream, char_cha_1, char_cha_2, char_cha_3)

    print("Merging the streams\n", flush=True)
    stream.merge(method=1, fill_value=0, interpolation_samples=0)

    print("Croping the stream to the minimal common trace timeprint\n", flush=True)
    preprocessor = pre.StreamPreprocessor(stream, input_response)

    stream = crop_stream_to_minimal_time_footprint(stream)



    if flag["remove_anomalies"] :
        # preprocessor.remove_anomalies(taper_time=1800)
        preprocessor.remove_anomalies(taper_time=60)

    if flag["sampling_frequency"] :
        print("Resampling the stream to {}Hz\n".format(sampling_frequency), flush=True)
        preprocessor.resample(resampling_frequency=sampling_frequency)
        print(preprocessor.stream) 


    if unit != "RAW" :
        print("Removing the instrument response\n", flush=True)
        preprocessor.remove_instrument_response(target_seis_unit=unit, nmirror=nmirror, water_level=40)


    if flag["dpg_correction_factor"] :
        preprocessor.dpg_correction(dpg_correction_factor)

    if flag["highpass"] :
        print("Applying a highpass filter at fc={}Hz ".format(highpass_fc))
        preprocessor.filter(3, highpass_fc, btype="highpass")

    if flag["lowpass"] :
        max_freq = preprocessor.stream[0].stats.sampling_rate/2 / 10
        print("Applying a lowpass filter at fc={}Hz ".format(max_freq))
        preprocessor.filter(3, max_freq, btype="lowpass")


    if flag["detect_events"] :
        print("Detecting transcient events\n", flush=True)
        preprocessor.detect_events()


    stream = preprocessor.stream

    start_date = stream[0].stats.starttime 
    end_date = stream[0].stats.endtime 
    station = stream[0].stats.station

    strout = "" 
    if flag["remove_anomalies"] :
        strout += "_anomalies-cleaned"
    if flag["sampling_frequency"] :
        strout += "_resampled-to-{}Hz".format(sampling_frequency)
    if flag["detect_events"] :
        strout += "_event-cleaned"
    if flag["highpass"] :
        strout += "_highpassed"
    if flag["lowpass"] :
        strout += "_lowpassed"
    if flag["remove_impulses"] :
        strout += "_impulses-removed"
    out_full_file = "{}corrected_stream/Merged_streams_{}_{}_{}_{}{}.mseed".format(input_dir, station, start_date, end_date, unit, strout)
    out_full_file = out_full_file.replace(":","-")
    stream.write(out_full_file)
    print("Merging Done and saved at {}!".format(out_full_file), flush=True)

