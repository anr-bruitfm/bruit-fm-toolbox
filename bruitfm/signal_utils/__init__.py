""" 
Module dedicated to signal processing and analysis.
It contains 3 subpackages which are :

    - ``signal_toolbox`` : signal processing related classes and functions in the time domain.
    - ``spectral_toolbox`` : signal processing related classes and functions in the Fourier domain.
    - ``stats_toolbox`` : time series analysis to assess stationarity.
"""


from .spectral_toolbox import *
from .signal_toolbox import *
from .stats_toolbox import * # deprecated

