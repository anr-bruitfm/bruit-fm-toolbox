#! /usr/bin/bash
### Author  :  SIMON REBEYROL
### Company :  IFREMER, UMR GEOOCEAN


'''
This toolbox is dedicated to signal processing  related classes and functions in the spectral domain. Some function may be hidden mainly because they are deprecated and unused, but can still be useful in some cases. 

References :
    1. https://docs.scipy.org/doc/scipy/reference/generated/scipy.signal.get_window.html#scipy.signal.get_window
    2. WELCH, Peter D.. "The use of fast Fourier transform for the estimation of power spectra: A method based on time averaging over short, modified periodograms". IEEE Transactions on Audio and Electroacoustics. 1967, vol 15, num. 2, p. 70–73. DOI: 10.1109/tau.1967.1161901
    3. THOMPSON, Rory O. R. Y.. "Coherence Significance Levels". Journal of the Atmospheric Sciences. 1979, vol 36, num. 10, p. 2020–2021. DOI: 10.1175/1520-0469(1979)036<2020:csl>2.0.co;2
    4. ROUSSEEUW, Peter J. & CROUX, Christophe. Alternatives to the Median Absolute Deviation. Informa UK Limited, 1993, 1273–1283. (88). 
    5. BELL, Samuel W., FORSYTH, Donald W. y RUAN, Youyi. Removing Noise from the Vertical Component Records of Ocean-Bottom Seismometers: Results from Year One of the Cascadia Initiative. Seismological Society of America (SSA), 2015, 300–313. (105). 



'''

import sys
import os
os.environ["OMP_NUM_THREADS"] = "4" # export OMP_NUM_THREADS=4
os.environ["OPENBLAS_NUM_THREADS"] = "4" # export OPENBLAS_NUM_THREADS=4
os.environ["MKL_NUM_THREADS"] = "4" # export MKL_NUM_THREADS=6
os.environ["VECLIB_MAXIMUM_THREADS"] = "4" # export VECLIB_MAXIMUM_THREADS=4
os.environ["NUMEXPR_NUM_THREADS"] = "4" # export NUMEXPR_NUM_THREADS=6
import numpy as np
import obspy
import scipy.signal as sig
from scipy.interpolate import interp1d
from scipy.ndimage import median_filter
from scipy.stats import median_abs_deviation as mad
from scipy.fft import fft, ifft, fftfreq, fft, ifft, fftfreq, fftshift, rfft, irfft, rfftfreq, irfft

from itertools import product
import warnings
from datetime import datetime, timedelta
from tqdm import tqdm
from pandas import DataFrame
import matplotlib.pyplot as plt

# try :
#     from ..utils import *
# except ImportError:
#     from utils import *

try :
    from .. import utils as tl
except ImportError:
    import utils as tl

#from toolbox import  get_window, next_pow2

warnings.filterwarnings( "ignore", module = "matplotlib\..*" )
warnings.filterwarnings(action='ignore', category=RuntimeWarning) # setting ignore as a parameter and further adding category
warnings.filterwarnings("ignore", module="statsmodels\..*")

available_windowtype = {"boxcar":False, "triang":False, "blackman":False, "hamming":False, "hann":False, "barlett":False, "flattop":False,
                        "parzen":False, "bohman":False, "blackmanharris":False, "nuttal":False, "barthann":False, "cosine":False, "exponential":False,
                        "tukey":False, "taylor":False,"kaiser":True, "gaussian":True, "general_hamming":True, "dpss":True, "chewbwin":True}



class NoValidDataError(Exception):
    def __init__(self, *args):
        if args:
            self.message = args[0]
        else :
            self.message = None

    def __str__(self):
        if self.message :
            return("NoValidDataError: {}".format(self.message))
        else :
            return("NoValidDataError")


class SpectralStats :
    """
    A class that contains the statistics of any Welch style computed power spectral distribution

    Args:
        
        spectra  (N x npts ndarray): the Power spectral distribution (can be PSD, CSD) of each part of the signal
        Nw  (int): The length of the spectral window in sample used to generate the Wiener filter.
        Noverlap  (int): The overlap in sample between two spectral windows.
        windowtype  (str) or (list): The scipy.signal.get_window() formatted descriptor of the window taper type.
        f  (ndarray): The frequency vector.
        unit  (str): The unit in which the arrays are.
        kind  (str): the kind of psd, can be either psd or cpsd.
        freqmask  (ndarray): A boolean array masking the frequencies, True values mean valid frequencies.
        nb_valid_windows  (int): The number of valid spectral windows that have been used to generate the fitler.
        nb_windows  (int): The total number of spectral windows.
        channel  (str): The ID of the channel.
        startdate  (datetime.datetime): The date and time from when the spectral analysis has been carried on.
        enddate  (datetime.datetime): The date and time to when the spectral analysis has been carried on.

    -----------
    Attributes:
    -----------

    Attributes
    ----------

        - *arrays*  (dict): A dict containing the arrays (data, median, mean, mad, std, min, max, q3, q1, p95, p5) describing the power spectral density statistics.
        - *Nw*  (int): The length of the spectral window in sample used to generate the Wiener filter.
        - *Noverlap*  (int): The overlap in sample between two spectral windows.
        - *windowtype*  (str) or (list): The scipy.signal.get_window() formatted descriptor of the window taper type.
        - *f*  (ndarray): The frequency vector.
        - *unit*  (str): The unit in which the arrays are.
        - *kind*  (str): the kind of psd, can be either psd or cpsd.
        - *freqmask*  (ndarray): A boolean array masking the frequencies, True values mean valid frequencies.
        - *nb_valid_windows*  (int): The number of valid spectral windows that have been used to generate the fitler.
        - *nb_windows* (int): The total number of spectral windows.
        - *channel*  (str): The ID of the channel.
        - *startdate* (datetime.datetime): The date and time from when the spectral analysis has been carried on.
        - *enddate*  (datetime.datetime): The date and time to when the spectral analysis has been carried on.
        - *arrays*  (dict): dict of array containing the data. The keys are 'data', 'mean', 'median', 'std', 'mad', 'q3', 'q1', 'p95', 'p5', 'min', 'max'.

    ---------------------
    Overloaded operators:
    ---------------------


    ``a*b:`` 
        return a SpectralStats instance with arrays['data'] being the product of a.arrays['data'] and b.arrays['data']
        and the others arrays are the product of  median(a.array) and b.array to propagate statistics. 

    ``a/b:``
        return a SpectralStats instance with arrays['data'] being the true division of a.arrays['data'] and b.arrays['data']
        and the others arrays are the true division of  median(a.array) and b.array to propagate statistics. 


    ``a**b:``
        return a SpectralStats instance with arrays['data'] being the a.arrays['data'] to the power of b.arrays['data']
        and the others arrays are the true division of  median(a.array) and b.array to propagate statistics.


    ``a[key]:`` 
        return a.arrays[key].
    
    ``a[key] = something:``
        set a.arrays[key] as something.
    
    
    --------
    Methods:
    --------

    Methods
    -------

        set_unit(unit) : update the unit attribute.
        interpolate(target_f, kind="cubic") : Return a new instance of the current SpectralStats interpolated to target_f Hz.
        plot(title="", plot_all=False, freqmask=False, fig=None, ax_ind=-1, tight_layout=True) : Plot the data as a Bode-like plot.
        spectrogram(title='') : Plot the data as a spectrogram.
        copy() : return a copy of the current instance.
        setnan(mask) : set all the values of the .arrays instance labelled as False by mask as numpy.nan
        attach_freqmask(freqmask) : update the freqmask attribute with freqmask.

    

    
    

    """
    def __init__(self, spectra=None, Nw=None, Noverlap=None, windowtype=None, f=None, unit="", kind="psd", freqmask=None, t=None, nb_valid_windows=None, nb_windows=None, channel=None, startdate=None, enddate=None) :
        """Constructor of SpectralStats class.
        """

        self.Nw = Nw
        self.Noverlap = Noverlap
        self.windowtype = windowtype
        self.f = f
        self.unit=unit
        self.kind = kind
        self.t = t
        self.nb_valid_windows = nb_valid_windows 
        self.nb_windows = nb_windows
        self.channel = channel
        self.startdate = startdate
        self.enddate = enddate

        if freqmask is not None :
            self.freqmask = freqmask
        else :
            self.freqmask = np.ones(Nw//2+1, dtype=bool)

        self.arrays = {}
        if spectra is not None :
            l = np.arange(nb_valid_windows)+1
            median_bias = np.sum((-1.)**(l+1)/l)
            self.arrays["data"] = spectra
            self.arrays["median"] = np.median(spectra.real, axis=0)/median_bias + 1.j*np.median(spectra.imag, axis=0)/median_bias
            self.arrays["mean"] = np.mean(spectra.real, axis=0) + 1.j*np.mean(spectra.imag, axis=0)#np.mean(spectra, axis=0)#
            self.arrays["std"] = np.std(spectra.real, axis=0) + 1.j*np.std(spectra.imag, axis=0)
            self.arrays["mad"] = mad(spectra.real, axis=0) + 1.j*mad(spectra.imag, axis=0)
            self.arrays["q3"] = np.percentile(spectra.real, 75, axis=0) + 1.j*np.percentile(spectra.imag, 75, axis=0)
            self.arrays["q1"] = np.percentile(spectra.real, 25, axis=0) + 1.j*np.percentile(spectra.imag, 25, axis=0)
            self.arrays["p95"] = np.percentile(spectra.real, 95, axis=0) + 1.j*np.percentile(spectra.imag, 95, axis=0)
            self.arrays["p5"] = np.percentile(spectra.real, 5, axis=0) + 1.j*np.percentile(spectra.imag, 5, axis=0)
            self.arrays["min"] = np.min(spectra.real, axis=0) + 1.j*np.min(spectra.imag, axis=0)
            self.arrays["max"] = np.max(spectra.real, axis=0) + 1.j*np.max(spectra.imag, axis=0)
            ### convienient dict object


    def __getstate__(self):
        state= self.__dict__.copy()
        return state

    def __setstate__(self,state):
        self.__dict__.update(state)

    def set_unit(self, unit) :
        """Set the unit attribute
        Args:
            unit  (str) : the unit.
        """
        self.unit=unit

    def interpolate(self, target_f, kind="cubic") :
        """Return a new instance of the current SpectralStats interpolated to target_f Hz.

        Args :
            target_f  (numpy.ndarray) : the new vector toward wich the data will be interpolated. Any new frequency point outside the range of the current frequency vector will be extrapolated.
            kind  (str) : the kind of interpolation to use, can be 'nearest', 'linear', 'cubic'. For more detail, read the scipy.interpolate.interp1d help.
        
        Output
        ------

            out  (SpectralStats) : the new SpectralStats instance interpolated from the current one.
        """
        out = self.copy()
        for key in self.arrays.keys() :

            spectra_interpolator_real = interp1d(self.f, self.arrays[key].real, kind=kind, fill_value="extrapolate", axis=-1)
            spectra_interpolator_imag = interp1d(self.f, self.arrays[key].imag, kind=kind, fill_value="extrapolate", axis=-1)
            out.arrays[key] = spectra_interpolator_real(target_f) + 1j*spectra_interpolator_imag(target_f)

        out.Nw = int(self.Nw * len(target_f)/len(self.f))
        out.Noverlap = int(self.Noverlap * len(target_f)/len(self.f))
        out.f = target_f

        return(out)

    def plot(self, title="", plot_all=False, freqmask=False, tight_layout=True) :
        """Plot the data.
        Args:
            title (str) : the title to be displayed 
            plot_all  (bool) : draw all the spectra from the arrays['data'] attribute in the background with high transparency.
            freqmask  (bool) : if True, the freqmask attribute will be used.

        Output
        ------

            fig  (matplotlib.Figure) : the figure.

        """
        fig, axs = plt.subplots(2)
        ax, ax_angl = axs


        ax.set_title(title)
        if freqmask  :
            freqmask = self.freqmask
        else :
            freqmask=np.ones(len(self.f), dtype=bool)
        if plot_all :
            ax.loglog(self.f[freqmask], (self.arrays["data"][:,freqmask].T), color="black", alpha=0.1, linewidth=0.2)
            ax_angl.semilogx(self.f[freqmask], np.unwrap(np.angle(self.arrays["data"][:,freqmask].T)), color="black", alpha=0.1, linewidth=0.2)

        ax.loglog(self.f[freqmask], (self.arrays["median"][freqmask]), color="tab:red", label="Median", linewidth=2.0)
        ax.loglog(self.f[freqmask], (self.arrays["mean"][freqmask]), color="tab:blue", label="Mean", linewidth=2.0)
        if self.kind != "cpsd" : ### statistics on complex variables seems not to be relevent (Q3 < median for instance)
            ax.loglog(self.f[freqmask], (self.arrays["q3"][freqmask]), color="tab:orange", label="Q3", linewidth=0.5)
            ax.loglog(self.f[freqmask], (self.arrays["q1"][freqmask]), color="tab:orange", label="Q1", linewidth=0.5)
            ax.loglog(self.f[freqmask], (self.arrays["p95"][freqmask]), color="tab:brown", label="P95", linewidth=0.5)
            ax.loglog(self.f[freqmask], (self.arrays["p5"][freqmask]), color="tab:brown", label="P5", linewidth=0.5)
            ax.fill_between(self.f[freqmask], (self.arrays["q1"][freqmask]), (self.arrays["q3"][freqmask]), alpha=0.3, color="tab:orange")

        ax_angl.semilogx(self.f[freqmask], np.unwrap(np.unwrap(np.angle(self.arrays["median"][freqmask]))), color="tab:red", label="Median", linewidth=2.0)
        ax_angl.semilogx(self.f[freqmask], np.unwrap(np.unwrap(np.angle(self.arrays["mean"][freqmask]))), color="tab:blue", label="Mean", linewidth=2.0)
        #ax_angl.semilogx(self.f[freqmask], np.unwrap(np.angle(self.arrays["q3"][freqmask])), color="tab:orange", label="Q3", linewidth=0.5)
        #ax_angl.semilogx(self.f[freqmask], np.unwrap(np.angle(self.arrays["q1"][freqmask])), color="tab:orange", label="Q1", linewidth=0.5)
        #ax_angl.semilogx(self.f[freqmask], np.unwrap(np.angle(self.arrays["p95"][freqmask])), color="tab:brown", label="P95", linewidth=0.5)
        #ax_angl.semilogx(self.f[freqmask], np.unwrap(np.angle(self.arrays["p5"][freqmask])), color="tab:brown", label="P5", linewidth=0.5)
        if self.kind != "cpsd" : ### statistics on complex variables seems not to be relevent (Q3 < median for instance)
            ax_angl.fill_between(self.f[freqmask], np.unwrap(np.angle(self.arrays["q1"][freqmask])), np.unwrap(np.angle(self.arrays["q3"][freqmask])), alpha=0.3, color="tab:orange")

        ax.grid()
        ax_angl.grid()
        ax.legend()
        ax.set_xlabel("Frequency (Hz)")
        ax.set_ylabel(self.unit)
        ax_angl.set_xlabel("Frequency (Hz)")
        ax_angl.set_ylabel("Phase (rad)")
        if tight_layout :
            fig.tight_layout()
        #fig.update_layout(height=int(800*1.5), width=int(600*1.5))
        return(fig)


        
 

    def spectrogram(self, title="") :
        '''Plot the data as a spectrogram.

        Args:
            f (array) : the frequency vector
            t (array) : the time vector
            spectrograms (ndarray or list/tuple of ndarray) : the spectrogram(s) to be drawn, ndarray
            titles (str or list of str) : the title(s) of the spectrogram(s).
                                                            dimensions should be len(f) times len(t)
            plot_phase (bool) : not implemented yet

        Output
        ------
            fig (pyplot.figure)  : the figure
        '''
        from matplotlib.colors import LogNorm
        fig, ax = plt.subplots()
        t = self.t.copy()
        t, time_unit = tl.time_converter(t)
        Z = np.abs(self.arrays["data"].T)
        spa_im = ax.pcolormesh(t, self.f, Z, rasterized=True, norm=LogNorm(vmin=Z.min(), vmax=Z.max()), cmap="jet")
        ax.set_xlabel("Time ({})".format(time_unit))
        ax.set_ylabel("Frequency (Hz)")
        ax.set_yscale("symlog", linthresh=self.f[1])
        ax.set_title(title)
        cbar = fig.colorbar(spa_im)
        cbar.ax.set_ylabel(self.unit)

        return(fig)


    def copy(self):
        """Copy the current instance."""
        out = SpectralStats(None, self.Nw, self.Noverlap, self.windowtype, 
                            self.f,  unit=self.unit, kind=self.kind, freqmask=self.freqmask.copy(), 
                            t=self.t, channel=self.channel, startdate=self.startdate, enddate=self.enddate)
        for key in self.arrays.keys() :
            out.arrays[key] = self.arrays[key].copy()
        return(out)

    def __mul__(self, other) :
        """
        Return a new SpectralStats instance which is the multlied result of the two input SpectralStats instances.
        The stats are multplied by each others.
        The data are arithmetically multiplied to be consistent with the output median (median(.data) = .median,
        which means that the mean is not consistent with the data anymore but can be used to calculate a
        coherence spectrum
        """
        out = self.copy()
        for key in self.arrays.keys() :
            if key not in "data" :
                out.arrays[key] = out.arrays[key] * other.arrays[key]
            else :
                out.arrays[key] = np.median(out.arrays[key], axis=0) * other.arrays[key]

        out.unit = self.unit + r" \times " + other.unit
        return(out)


    def __truediv__(self, other) :
        """
        Return a new SpectralStats instance which is the divided result of the two input SpectralStats instances.
        The stats are recalculated.
        The data are arithmetically divided to be consistent with the output median (median(.data) = .median,
        which means that the mean is not consistent with the data anymore but can be used to calculate a
        coherence spectrum
        """
        out = self.copy()

        out.unit = self.unit + " / " + other.unit
        for key in self.arrays.keys() :
            if key not in "data" :
                out.arrays[key] = (np.abs(out.arrays[key]) / np.abs(other.arrays[key]))*np.exp(1j*(np.angle(out.arrays[key] - np.angle(other.arrays[key]))))
            else :
                out.arrays[key] = np.median(out.arrays[key], axis=0) / other.arrays[key]

        return(out)

    def __pow__(self, power) :

        out = self.copy()

        for key in self.arrays.keys() :
            out.arrays[key] = np.power(out.arrays[key], power)

        return(out)

    def __getitem__(self, get):
        return(self.arrays[get])

    def __setitem__(self, key, value):
        self.arrays[key] = value

    def setnan(self, mask):
        """Set all the values of the .arrays instance labelled as False by mask as numpy.nan.

        Args:
            mask  (numpy.ndarray) : boolean array matching .arrays[''] dimensions.
        """
        for key in self.arrays.keys() :
            self.arrays[key][~mask] = np.nan

    def attach_freqmask(self, freqmask):
        self.freqmask=freqmask



class SpectralWienerFilter :
    """A class that contains the statistics of any Welch style computed power spectral distribution

    Args:
        arrays  (dict): A dict containing the arrays (median, mean, mad, std) describing the spectral Wiener filter.
        Nw  (int): The length of the spectral window in sample used to generate the Wiener filter.
        Noverlap  (int): The overlap in sample between two spectral windows.
        windowtype (str) or (list):  The scipy.signal.get_window() formatted descriptor of the window taper type.
        f  (ndarray): The frequency vector.
        unit  (str): The unit in which the arrays are.
        freqmask  (ndarray): A boolean array masking the frequencies, True values mean valid frequencies.
        min_coherence  (float): The minimal coherence threshold above which the spectral analysis is carried on. It is converted into significance 95 level during the process.
        nb_valid_windows  (int): The number of valid spectral windows that have been used to generate the fitler.
        nb_windows  (int): The total number of spectral windows.
        ref_channel  (str): The name of the reference channel.
        out_channel  (str): The name of the ouptut channel.
        startdate  (datetime.datetime): The date and time from when the spectral analysis has been carried on.
        enddate  (datetime.datetime): The date and time to when the spectral analysis has been carried on.

    ===========
    Attributes:
    ===========

    Attributes
    ----------

        arrays  (dict): A dict containing the arrays (median, mean, mad, std) describing the spectral Wiener filter.
        Nw  (int): The length of the spectral window in sample used to generate the Wiener filter.
        Noverlap  (int): The overlap in sample between two spectral windows.
        windowtype (str) or (list):  The scipy.signal.get_window() formatted descriptor of the window taper type.
        f  (ndarray): The frequency vector.
        unit  (str): The unit in which the arrays are.
        freqmask  (ndarray): A boolean array masking the frequencies, True values mean valid frequencies.
        min_coherence  (float): The minimal coherence threshold above which the spectral analysis is carried on. It is converted into significance 95 level during the process.
        nb_valid_windows  (int): The number of valid spectral windows that have been used to generate the fitler.
        nb_windows  (int): The total number of spectral windows.
        ref_channel  (str): The name of the reference channel.
        out_channel  (str): The name of the ouptut channel.
        startdate (datetime.datetime): The date and time from when the spectral analysis has been carried on.
        enddate  (datetime.datetime): The date and time to when the spectral analysis has been carried on.

    ========
    Methods:
    ========

    Methods
    -------

        save(out_path) : Save the current instance as a .mat file.
        load(in_path) : Load a saved instance from a .mat file.
        set_unit(unit) : set the unit attribute.
        interpolate (target_f, kind="linear"): Return a new instance of the current SpectralWienerFilter interpolated to target_f Hz.
        plot(title="", freqmask=False,   tight_layout=True): Plot the data as a Bode-like plot.
        copy(): return a copy of the current instance.
        setnan(mask): set all the values of the .arrays instance labelled as False by mask as numpy.nan
        attach_freqmask(freqmask): update the freqmask attribute with freqmask.

    """
    def __init__(self, in_spectralstats = None, nb_valid_windows=None, nb_windows=None,  load_path=None, out_channel=None, ref_channel=None,  startdate=None, enddate=None, min_coherence=None) :
        """Constructor of class SpectralWienerFilter"""  
        if load_path is None :
            if in_spectralstats is not None :
                self.Nw = in_spectralstats.Nw
                self.Noverlap = in_spectralstats.Noverlap
                self.windowtype = in_spectralstats.windowtype
                self.f = in_spectralstats.f
                self.unit= in_spectralstats.unit
                self.freqmask = in_spectralstats.freqmask
                
                self.arrays = {}
                self.arrays["median"] = in_spectralstats["median"]
                self.arrays["mad"] = in_spectralstats["mad"]
                self.arrays["mean"] = in_spectralstats["mean"]
                self.arrays["std"] = in_spectralstats["std"]
                
            else :
                self.Nw = None
                self.Noverlap = None
                self.windowtype = None
                self.f = None
                self.unit= None
                self.freqmask = None
                self.arrays = {}

            self.nb_valid_windows=nb_valid_windows
            self.nb_windows=nb_windows
            self.min_coherence = min_coherence

            self.ref_channel = ref_channel 
            self.out_channel = out_channel

            self.startdate = startdate 
            self.enddate = enddate

        else :
            self = self.load(load_path)

        return(None)

    def save(self, out_path) :
        """Save the current instance as a .mat file.

        Args:
            out_path (str): the path to the .mat file with the extension.
        """        
        from scipy.io import savemat
        out_dict = {}
        out_dict["Nw"] = self.Nw
        out_dict["Noverlap"] = self.Noverlap
        out_dict["windowtype"] = self.windowtype
        out_dict["f"] = self.f
        out_dict["unit"]= str(self.unit)
        out_dict["freqmask"] = self.freqmask
        out_dict["array_names"] = list(self.arrays.keys())
        out_dict["arrays"] = self.arrays.copy()
        out_dict["ref_channel"] = self.ref_channel
        out_dict["out_channel"] = self.out_channel
        out_dict["nb_valid_windows"] =self.nb_valid_windows
        out_dict["nb_windows"] =self.nb_windows
        out_dict["startdate"] = str(self.startdate)
        out_dict["enddate"] = str(self.enddate)
        out_dict["min_coherence"] = self.min_coherence


        for key in out_dict.keys() :
            if out_dict[key] is None :
                out_dict[key] = "None"

        savemat(out_path, out_dict, oned_as="row")
        return(None)

    def load(self, in_path) :
        """Load a saved instance from a .mat file.

        Args:
            in_path  (str): the path to the .mat file to be loaded. 
        """
        from scipy.io import loadmat  
        loaddict = loadmat(in_path)

        arrays = loaddict["arrays"][0,0]
        arrays_temp = {}
        for k in loaddict["array_names"] :
            arrays_temp[k.replace(" ", "")] = arrays[k.replace(" ", "")].squeeze()
        

        self.ref_channel = loaddict["ref_channel"][0].replace(" ", "")
        self.out_channel = loaddict["out_channel"][0].replace(" ", "")
        try :
            self.startdate = datetime.strptime(loaddict["startdate"][0], "%Y-%m-%dT%H:%M:%S.%fZ")
            self.enddate = datetime.strptime(loaddict["enddate"][0], "%Y-%m-%dT%H:%M:%S.%fZ")
        except ValueError:
            self.startdate = datetime.strptime(loaddict["startdate"][0], "%Y-%m-%d %H:%M:%S.%f")
            self.enddate = datetime.strptime(loaddict["enddate"][0], "%Y-%m-%d %H:%M:%S.%f")
  
        unit = loaddict["unit"][0]
        if unit == "None" :
            self.unit=None
        else :
            self.unit=unit

        self.Nw = int(loaddict["Nw"][0,0])
        self.Noverlap = int(loaddict["Noverlap"][0,0])
        self.min_coherence = float(loaddict["min_coherence"][0,0])

        
        if len(loaddict["windowtype"])  <2 : 
            self.windowtype = str(loaddict["windowtype"]).replace(" ", "").replace("'", "").replace("[", "").replace("]", "")
        else :
            self.windowtype = list(loaddict["windowtype"])
            for k, win  in enumerate(self.windowtype):
                self.windowtype[k] = win.replace(" ", "")
                if k > 0 :
                   self.windowtype[k]  = float(self.windowtype[k]) 
            self.windowtype = tuple(self.windowtype)

        self.f = loaddict["f"].squeeze()
        self.freqmask = loaddict["freqmask"].squeeze().astype(bool) 
        self.arrays = arrays_temp
        self.nb_valid_windows = int(loaddict["nb_valid_windows"][0,0])
        self.nb_windows = int(loaddict["nb_windows"][0,0])

        return(None)


    def __getstate__(self):
        state= self.__dict__.copy()
        return state

    def __setstate__(self,state):
        self.__dict__.update(state)

    def set_unit(self, unit) :
        """Set the unit attribute"""
        self.unit=unit



    def interpolate(self, target_f, kind="linear") :
      
        """Return a new instance of the current SpectralWienerFilter interpolated to target_f Hz.

        Args:
            target_f  (numpy.ndarray): the new vector toward wich the data will be interpolated. Any new frequency point outside the range of the current frequency vector will be extrapolated.
            kind (str): the kind of interpolation to use, can be 'nearest', 'linear', 'cubic'. For more detail, read the scipy.interpolate.interp1d help.
        
        Return:
            out  (SpectralWienerFilter): the new SpectralWienerFilter instance interpolated from the current one.
        """
        out = self.copy()
        for key in self.arrays.keys() :
            if self.arrays[key] is not None :
                
                spectra_interpolator_real = interp1d(self.f, self.arrays[key].real, kind=kind, fill_value=0., axis=-1)
                spectra_interpolator_imag = interp1d(self.f, self.arrays[key].imag, kind=kind, fill_value=0., axis=-1)
                out.arrays[key] = spectra_interpolator_real(target_f) + 1.j*spectra_interpolator_imag(target_f)

        interpolator = interp1d(self.f, self.freqmask, kind='nearest', fill_value='extrapolate')
        out.freqmask = interpolator(target_f).astype(bool)

        out.Nw = int(self.Nw * len(target_f)/len(self.f))
        out.Noverlap = int(self.Noverlap * len(target_f)/len(self.f))
        out.f = target_f
    

        return(out)

    def plot(self, title="", freqmask=False,   tight_layout=True,) :
        """Plot the SpectralWienerFilter.

        Args:
            title (str): the title to be displayed 
            freqmask (bool): if True, the freqmask attribute will be used 
            tight_layout  (bool): if True, make the figure render in a tight layout.

        Return:
            fig  (matplotlib.Figure): the figure.

        """
        fig, axs = plt.subplots(2)
        ax, ax_angl = axs


        if (self.nb_valid_windows is not None) and (self.nb_windows is not None) :
            title2 = "\n with {} valid {}s-length windows over {}".format(self.nb_valid_windows, int(self.Nw*np.max(self.f)*2) , self.nb_windows)
        else:
            title2=""
        
        ax.set_title(title + title2)


        median = self.arrays["median"]
        mean = self.arrays["mean"]
        mad = self.arrays["mad"]
        std = self.arrays["std"]
        f = self.f

        if freqmask  :
            freqmask = self.freqmask
        else :
            freqmask=np.ones(len(f), dtype=bool)

        ax.loglog(f[freqmask], (np.abs(median[freqmask])), color="tab:blue", label="Median", linewidth=2.0)
        ax.fill_between(f[freqmask], (np.abs(median[freqmask]) - np.abs(mad[freqmask])/2), 
                                     (np.abs(median[freqmask]) + np.abs(mad[freqmask])/2), 
                                     color="tab:orange", label="Mad", linewidth=1.0, alpha=0.3)
        ax.loglog(f[freqmask], (np.abs(mean[freqmask])), color="tab:red", label="Mean", linewidth=2.0)
        ax.fill_between(f[freqmask], (np.abs(mean[freqmask]) - np.abs(std[freqmask])/2), 
                                     (np.abs(mean[freqmask]) + np.abs(std[freqmask])/2), 
                                     color="tab:green", label="Std", linewidth=1.0, alpha=0.3)

        ax.grid()
        ax.legend()
        ax.set_xlabel("Frequency (Hz)")
        ax.set_ylabel(self.unit)

        ax_angl.semilogx(f[freqmask], np.unwrap(np.angle(median[freqmask])), color="tab:blue", label="Median", linewidth=2.0)
        ax_angl.semilogx(f[freqmask], np.unwrap(np.angle(mean[freqmask])), color="tab:red", label="Mean", linewidth=2.0)

        ax_angl.grid()
        ax_angl.legend()
        ax_angl.set_xlabel("Frequency (Hz)")
        ax_angl.set_ylabel("Phase (rad)")
        if tight_layout :
            fig.tight_layout()
        #fig.update_layout(height=int(800*1.5), width=int(600*1.5))

        return(fig)



    def copy(self):
        """Return a copy of the instance"""
        out = SpectralWienerFilter()
        out.Nw = self.Nw
        out.Noverlap = self.Noverlap
        out.windowtype = self.windowtype
        out.f = self.f.copy()
        out.unit= self.unit
        out.freqmask = self.freqmask.copy()
        out.arrays = {}
        out.arrays["median"] = self["median"].copy()
        out.arrays["mean"] = self["mean"].copy()
        out.arrays["std"] = self["std"].copy()
        out.arrays["mad"] = self["mad"].copy()
        out.nb_valid_windows = self.nb_valid_windows
        out.nb_windows = self.nb_windows
        out.out_channel = self.out_channel 
        out.ref_channel = self.ref_channel
        out.min_coherence = self.min_coherence
        out.startdate = self.startdate 
        out.enddate = self.enddate
        return(out)

    def __getitem__(self, get):
        return(self.arrays[get])

    def __setitem__(self, key, value):
        self.arrays[key] = value

    def setnan(self, mask):
        """Set all the non True values from mask to np.nan"""
        for key in self.arrays.keys() :
            self.arrays[key][~mask] = np.nan

    def attach_freqmask(self, freqmask):
        """Update the freqmask attribute"""
        self.freqmask=freqmask




class Coherence :
    """A class holding the coherence.

    Args:
        cpsd (signal_utils.SpectralStats) : the cross power spectral density between the two traces.
        psd1 (signal_utils.SpectralStats) : the power spectral density of traces 1.
        psd2 (signal_utils.SpectralStats) : the power spectral density of traces 2.

    -----------
    Attributes:
    -----------

    Attributes
    ----------

        data  (numpy.ndarray) : the coherence values.
        std (numpy.ndarray) : the standard deviation evaluated using the std of each power spectral densities.
        uncertainties  (numpy.ndarray) :  the normalized standard deviation (or fractionnal uncertainties) computed using eq (9) from [5].
        f  (numpy.ndarray) : the frequency vector.
        nd  (int): the number of windows used to compute the power spectral densities and the coherence.
        sign95  (float): the 95% significance value computed using [3].
        cpsd  (signal_utils.SpectralStats) : the cross power spectral density between the two traces.
        psd1  (signal_utils.SpectralStats) : the power spectral density of traces 1.
        psd2  (signal_utils.SpectralStats) : the power spectral density of traces 2.

    --------
    Methods:
    --------

    Methods
    -------

        compress() : Delete the spectral densities from the instance, useful to free ram.
        interpolate(target_f, kind="linear, sdtoo=False) : Interpolate the Coherence instance to the target_f frequency vector using cubic spline scipy interpolation.
        plot(fig=None, freqmask=None, title='', min_coherence=1., freqmin=None, freqmax=None) : Plot the coherence
        calculate_signifiance(p) : calculate the p significance
        copy() : return a copy of the current signal_utils.Coherence instance


    """
    def __init__(self, cpsd, psd1, psd2):
        """Constructor        
        """
        self.data = np.abs(cpsd["mean"])**2/(psd1["mean"].real*psd2["mean"].real)
        self.std = np.abs(cpsd["std"])**2/(psd1["std"].real*psd2["std"].real)
        self.nd = len(cpsd["data"])
        try :
            self.sign95 = self.calculate_signifiance(95)
        except ZeroDivisionError:
            print("WARNING: ZeroDivisionError occured while calculating the coherence significance.", flush=True)
            self.sign95 = np.nan
        self.uncertainties = np.sqrt(2/self.nd)*(1-self.data)/np.sqrt(self.data) #np.sqrt((np.ones(coh.shape) - coh) / (2*coh*self.n_windows))
        self.f = cpsd.f
        self.cpsd = cpsd.copy()
        self.psd1 = psd1.copy()
        self.psd2 = psd2.copy()

    def compress(self) :
        "Delete the spectral densities from the instance"
        del self.cpsd, self.psd1, self.psd2
        self.cpsd, self.psd1, self.psd2 = [None, None, None] 


    def interpolate(self, target_f, kind="linear", sdtoo=False):
        """Interpolate the current instance to target_f Hz.

        Args:

            target_f  (numpy.ndarray) : the new vector toward wich the data will be interpolated. Any new frequency point outside the range of the current frequency vector will be extrapolated.
            kind  (str) : the kind of interpolation to use, can be 'nearest', 'linear', 'cubic'. For more detail, read the scipy.interpolate.interp1d help.
            sdtoo  (bool) :  if True, will interpolate the (cross) power spectral densities as well, very ram demanding.
        
        """
        interpolator = interp1d(self.f, self.data, kind=kind, fill_value="extrapolate", axis=-1)
        self.data = interpolator(target_f)
        interpolator = interp1d(self.f, self.std, kind=kind, fill_value="extrapolate", axis=-1)
        self.std = interpolator(target_f)
        interpolator = interp1d(self.f, self.uncertainties, kind=kind, fill_value="extrapolate", axis=-1)
        self.uncertainties = interpolator(target_f)

        if sdtoo :
            self.cpsd = self.cpsd.interpolate(target_f, kind=kind)
            self.psd1 = self.psd1.interpolate(target_f, kind=kind)
            self.psd2 = self.psd2.interpolate(target_f, kind=kind)

        self.f = target_f

    def plot(self, freqmask=None, title='', min_coherence=1., freqmin=None, freqmax=None) :
        """Plot the Coherence.

        Args: 

            freqmask  (numpy.ndarray) :boolean array masking the frequency not to plot.
            title  (str) : the title to be written on the figure.
            min_coherence  (float)  the coherence theshold to be drawn, will be converted in significance.
            freqmin  (float)  the bottom frequency limit.
            freqmax  (float) : the upper frequency limit.
        
        Return:
            fig  (matplotlib.Figure) : the output figure.
        """

        fig, ax = plt.subplots()


        ax_err = ax.twinx()
        ax.set_title(title)
        if freqmask is None :
            freqmask=np.ones(len(self.f), dtype=bool)

        ax.semilogx(self.f[freqmask], self.calculate_signifiance(min_coherence) * np.ones(self.f[freqmask].shape))
        ax.semilogx(self.f[freqmask], self.data[freqmask], color="tab:purple", linewidth=2 )
        ax.set_ylim(0,1)
        ax_err.semilogx(self.f[freqmask], self.uncertainties[freqmask], color="tab:brown", alpha=0.3 )
        ax_err.set_ylabel("Normalized standard deviation", color="tab:brown")
        ax.grid()
        ax.legend()
        ax.set_xlabel("Frequency (Hz)")
        ax.set_ylabel("Coherence", color="tab:purple")
        if isinstance(freqmin, float) :
            ax.axvline(freqmin, color="black")
        if isinstance(freqmax, float) :
            ax.axvline(freqmax, color="black")
            
        fig.tight_layout()
        return(fig)

    def __getitem__(self, ind) :
        return(self.data[ind])

    def calculate_signifiance(self, p) :
        """ Calculate the signifiance as proposed in [3], inspired from the TisKit toolbox (Wayne Crawford)
        
        Args:
            p (float) : the coherence threshold.

        Return:
            s  (float) : the significance value.
        """
         
        a = 1-p
        q = self.nd/2 -1
        return(np.sqrt(1-a**(1./q)))


    def __getstate__(self):
        state= self.__dict__.copy()
        return state

    def __setstate__(self,state):
        self.__dict__.update(state)

    def copy(self):
        """Return a copy of the current Coherence instance""" 
        out_coh = Coherence(self.cpsd.copy(), self.psd1.copy(), self.psd2.copy())
        return(out_coh)





def psd(trace, windowtype, Nws, Noverlaps, return_SpectralStats=True, backend="bruit-fm", verbose=True, masknodata=None, mask_corr=None) :
    '''Compute the power spectral density (PSD) of a trace and return it as a SpectralStats instance.
    
    Args:
        trace (obspy.core.trace.Trace): the trace from which the PSD will be estimated.
        windowtype  (str) or (list)  : the type of window to be used. Uses the scipy.signal.get_window() parameter syntax.
        Nws  (float) : the duration of each window in second.
        Noverlaps  (float) : the duration of overlaps between two windows in second.
        verbose  (bool) : show more infos
        masknodata  (dict) or (numpy.ndarray) :  either a dict of boolean numpy.ndarray whose keys are stations or a boolean numpy ndarray. The ndrarray dimensions must match the one of the trace data. 
            If a dict is provided, all the ndarray from the dict will be merged prior to the masking.
        mask_corr  (numpy.ndarray) : boolean array that will be used to reject windows. Need to be of length equal to the number of windows. Is used in the context of cross-correlation but it can be more general.
        return_SpectralStats  (boolean) : deprecated 
        backend  (str)  : deprecated
    
    :raises NoValidDataError: If not enought valid windows are available to compute the power spectral density.
    
    Returns:
    --------
        f : (numpy.ndarray)
            the frequency vector corresponding to the PSD.
        specout  : (signal_utils.SpectralStats) 
            the PSD as a SpectralStats instance.
    '''
    if masknodata is not None :
        if isinstance(masknodata, dict) :
            mask = np.zeros(trace.stats.npts, dtype=bool)
            for k in masknodata.keys() :
                mask = mask | masknodata[k]
                trace.data[mask] = 0.
        elif isinstance(masknodata, np.ndarray) :
            mask = masknodata 
            trace.data[mask] = 0.
        else: 
            print("WARNING: wrong masknodata type in psd(), got {}.".format(type(masknodata)))
        
    ### defining window taper
    Nw = int(Nws*trace.stats.sampling_rate)
    Noverlap = int(Noverlaps*trace.stats.sampling_rate)
    window = get_window(windowtype, Nw)
    ### getting the sliced and taped signal parts
    sliced_time, sliced_data = signal_slicing(trace.data, Nw, Noverlap, windowtype, step=trace.stats.delta)
    valid_window_mask = (np.sum((np.abs(sliced_data)< 1e-15), axis=0) ) == 0#< 0.001*Nw ### more than 99% of the data must be valid
    # if int(np.sum(valid_window_mask))==0:
    #     raise NoValidDataError("There is not enought valid data available to compute the psd.")
    # elif int(np.sum(valid_window_mask))<sliced_data.shape[-1] and verbose:
    #     print("There have been {}/{} rejected windows for trace {}".format(sliced_data.shape[-1]-int(np.sum(valid_window_mask)),sliced_data.shape[-1] , trace.get_id()))
    
    ### applying the correlation mask
    if isinstance(mask_corr, np.ndarray) :
        valid_window_mask = valid_window_mask & mask_corr
    if int(np.sum(valid_window_mask))==0:
        raise NoValidDataError("There is not enought valid data available to compute the psd.")
    elif int(np.sum(valid_window_mask))<sliced_data.shape[-1] and verbose:
        print("There have been {}/{} rejected windows for trace {}".format(sliced_data.shape[-1]-int(np.sum(valid_window_mask)),sliced_data.shape[-1] , trace.get_id()))

    ### discarding bad windows
    nb_valid_windows = int(np.sum(valid_window_mask))
    nb_windows = sliced_data.shape[-1]
    sliced_data = sliced_data[:,valid_window_mask]

    ### rfft computation
    f = rfftfreq(sliced_data.shape[0], trace.stats.delta) # Nw might have changed if the signal has been padded
    sliced_data_rfft = rfft(sliced_data, axis=0,)

    psds = 2*(sliced_data_rfft.conj()*sliced_data_rfft).T / (trace.stats.sampling_rate * np.sum(window*window))

    try :
        unit = trace.stats.response.response_stages[0].input_units
    except  :
        unit='unit'

    unit = r"$\frac{}\left({}\right)^2{}{}$".format("{",unit,"}", "{Hz}" )
    specout = SpectralStats(psds, Nw, Noverlap, windowtype, f, kind='psd', unit=unit,t=sliced_time[valid_window_mask], nb_valid_windows=nb_valid_windows, nb_windows=nb_windows, channel=trace.get_id(), startdate=trace.stats.starttime, enddate=trace.stats.endtime)
    return(specout.f, specout)


def cpsd(trace1, trace2, windowtype, Nws, Noverlaps, return_SpectralStats=False, backend="bruit-fm", verbose=True, masknodata=None, mask_corr=None) :
    '''Compute the cross-power spectral density (CPSD) between two traces and return it as a SpectralStats instance.
    
    Args:
        trace1 (obspy.core.trace.Trace) : the first trace from which the CPSD will be estimated.
        trace2 (obspy.core.trace.Trace) : the second trace from which the CPSD will be estimated.
        windowtype  (str) or (list)  : the type of window to be used. Uses the scipy.signal.get_window() parameter syntax.
        Nws  (float) : the duration of each window in second.
        Noverlaps  (float) : the duration of overlaps between two windows in second.
        verbose  (bool) : show more infos
        masknodata  (dict) or (numpy.ndarray) :  either a dict of boolean numpy.ndarray whose keys are stations or a boolean numpy ndarray. The ndrarray dimensions must match the one of the trace data. 
            If a dict is provided, all the ndarray from the dict will be merged prior to the masking.
        mask_corr  (numpy.ndarray) : boolean array that will be used to reject windows. Need to be of length equal to the number of windows. Is used in the context of cross-correlation but it can be more general.
        return_SpectralStats  (boolean) : deprecated 
        backend  (str)  : deprecated
    
    :raises NoValidDataError: If not enought valid windows are available to compute the power spectral density.

    
    Returns:
    --------
        f : (numpy.ndarray)
            the frequency vector corresponding to the PSD.
        specout  : (signal_utils.SpectralStats) 
            the CPSD as a SpectralStats instance.
    '''


    if trace1.stats.sampling_rate != trace2.stats.sampling_rate :
        raise ValueError("The sampling rate of the two trace should be identical prior to the csd calculation")
    if trace1.stats.npts != trace2.stats.npts :
        raise ValueError("The length of the two trace should be identical prior to the csd calculation")

    if masknodata is not None :
        if isinstance(masknodata, dict) :
            mask = np.zeros(trace1.stats.npts, dtype=bool)
            for k in masknodata.keys() :
                mask = mask | masknodata[k]
                trace1.data[mask] = 0.
                trace2.data[mask] = 0.
        elif isinstance(masknodata, np.ndarray) :
            mask = masknodata 
            trace1.data[mask] = 0.
            trace2.data[mask] = 0.
        else: 
            print("WARNING: wrong masknodata type in psd(), got {}.".format(type(masknodata)))

    ### defining the window taper
    Nw = int(Nws*trace1.stats.sampling_rate)
    Noverlap = int(Noverlaps*trace1.stats.sampling_rate)
    window = get_window(windowtype, Nw)

    ### getting the sliced and taped signal parts
    sliced_time1, sliced_data1 = signal_slicing(trace1.data, Nw, Noverlap, windowtype, step=trace1.stats.delta)
    sliced_time2, sliced_data2 = signal_slicing(trace2.data, Nw, Noverlap, windowtype, step=trace2.stats.delta)

    valid_window_mask = np.sum((np.abs(sliced_data1) < 1e-15)|(np.abs(sliced_data2) < 1e-15), axis=0) == 0 #< 0.001*Nw ### more than 99% of the data must be valid
    if int(np.sum(valid_window_mask))==0:
        raise NoValidDataError("There is not enought valid data available to compute the cpsd.")
    elif (int(np.sum(valid_window_mask))<sliced_data1.shape[-1] or (np.sum(valid_window_mask))<sliced_data2.shape[-1])  and verbose:
        pass
    print("There have been {}/{} rejected windows for traces {} and {}".format(sliced_data1.shape[-1]-int(np.sum(valid_window_mask)),sliced_data1.shape[-1] , trace1.get_id(), trace2.get_id()), flush=True)
        # print("There have been {}/{} rejected windows for trace {}".format(sliced_data2.shape[-1]-int(np.sum(valid_window_mask)),sliced_data2.shape[-1] , trace2.get_id()))
    
    if isinstance(mask_corr, np.ndarray) :
        valid_window_mask = valid_window_mask & mask_corr

    nb_valid_windows = int(np.sum(valid_window_mask))
    nb_windows = sliced_data1.shape[-1]
    sliced_data1 = sliced_data1[:,valid_window_mask]
    sliced_data2 = sliced_data2[:,valid_window_mask]

    ### rfft computation
    f = rfftfreq(sliced_data1.shape[0], trace1.stats.delta)
    sliced_data1_rfft = rfft(sliced_data1, axis=0, )
    sliced_data2_rfft = rfft(sliced_data2, axis=0, )
    ### getting the cross power spectral density scaling in unit1*unit2/hZ
    csds = 2*(sliced_data1_rfft.conj()*sliced_data2_rfft).T / (trace1.stats.sampling_rate *np.sum(window*window))#Nws)#)


    try :
        unit1 = trace1.stats.response.response_stages[0].input_units
        unit2 = trace2.stats.response.response_stages[0].input_units
    except  :
        unit1='unit'
        unit2='unit'
    unit = r"$\frac{}\left({} \times {}\right){}{}$".format("{",unit1,unit2,"}", "{Hz}" )

    specout = SpectralStats(csds, Nw, Noverlap, windowtype, f, kind='cpsd', unit=unit, t=sliced_time1[valid_window_mask], nb_valid_windows=nb_valid_windows, nb_windows=nb_windows, channel=trace1.get_id() + "*" + trace2.get_id(), startdate=trace1.stats.starttime, enddate=trace1.stats.endtime)
    return(specout.f, specout)



def _compute_coherence(trace1, trace2, windowtype, Nws, Noverlaps, return_CoherenceClass=False) :

    if trace1.stats.sampling_rate != trace2.stats.sampling_rate :
        raise ValueError("The sampling rate of the two traces should be identical prior to the coherence calculation")
    if trace1.stats.npts != trace2.stats.npts :
        raise ValueError("The length of the two traces should be identical prior to the coherence calculation")

    Nw = int(Nws*trace1.stats.sampling_rate)
    Noverlap = int(Noverlaps*trace1.stats.sampling_rate)
    window = get_window(windowtype, Nw)
    if return_CoherenceClass :
        f, csd  = cpsd(trace1, trace2, windowtype, Nws, Noverlaps, return_SpectralStats=True)
        f, psd1 = psd(trace1, windowtype, Nws, Noverlaps, return_SpectralStats=True)
        f, psd2 = psd(trace2, windowtype, Nws, Noverlaps, return_SpectralStats=True)
        Coh = Coherence(csd, psd1, psd2)
        return(Coh.f, Coh)

    else :
        f, csd  = cpsd(trace1, trace2, windowtype, Nws, Noverlaps)
        f, psd1 = psd(trace1, windowtype, Nws, Noverlaps)
        f, psd2 = psd(trace2, windowtype, Nws, Noverlaps)
        Coh = np.abs(csd)**2/(psd1.real*psd2.real)
        return(f, Coh)


def sliding_window_iterator(Nw, Nsignal, Noverlap) :
    '''Return an iterator of slices that allows one to slide a window over an array.

    Args:
        Nw (int) : the length of the window
        Nsignal (int) :  the length of the signal to be sliced
        Noverlap (int) :  the number of sample that will overlap between two neighbouring windows

    Yield:
    ------
        slice : (numpy.slice_)
            the slice instance.
    '''
    windows_spacing =  int(Nw - Noverlap) # the sampling rate of the windows beginning
    for center in range(0,Nsignal-windows_spacing,windows_spacing) :
        sl = slice(center, center+Nw)
        yield(sl)

def signal_slicing(signal, Nw, Noverlap, window, step=1., keep_last_window=False, detrend=True) :
    '''Slice the signal into multiple subsignals using Welch's method.

    Args:
        Nw (int) : the length of the window
        Noverlap (int) : the number of sample that will overlap between two neighbouring windows
        window (str, tuple or ndarray) : the desired window taper

    Returns:
    --------
        t :(ndarray) 
         the time vector containing the center time for each sliced subsignal
        subsigs :(Nf*Nt ndarray) 
         the matrix containing the sliced and windowed subsigs
    '''

    new_Nw = Nw

    if isinstance(window, tuple) or isinstance(window, list) :
        window = get_window(window, new_Nw)
    elif isinstance(window, str) :
        window = get_window(window, new_Nw)
    elif isinstance(window, np.ndarray) :
        pass
    else :
        raise ValueError("Window must be of type tuple, list, str or numpy.ndarray, got {}.".format(type(window)))

    N = len(signal)
    windows_spacing =  int(Nw - Noverlap + 1 ) # the sampling rate of the windows beginning
    t = np.arange(0,N+windows_spacing,windows_spacing)*step# + step*Nw//2 #making the time vector


    subsigs = np.zeros([len(t),Nw])

    valid_last_window = True
    for ind, sl in enumerate(sliding_window_iterator(Nw, N, Noverlap)) :
        subsig = signal[sl] #slicing the signal
        # print(sl, subsig.shape, subsigs.shape, ind)
        if (len(subsig) != Nw) :
            if  keep_last_window :
                nmirror = (Nw - len(subsig))
                mirror_subsig = np.pad(subsig.copy(), nmirror, mode="symmetric")
                subsigs[ind,:] = mirror_subsig[Nw - len(subsig) : ] #making sure the subsig is of same length than the
                valid_last_window = nmirror < Nw//4
        else:
            try :
                subsigs[ind,:] = subsig
            except IndexError :
                pass

    # print(subsigs.shape, window.shape)
    if detrend :
        mask_nodata = np.abs(subsigs) < 1e-15 # saving nodata values locations
        subsigs = sig.detrend(subsigs, axis=-1, type="linear")*window # numpy broadcasting operation
        subsigs[mask_nodata] = 0. # refilling no data values 
    else :
        subsigs = subsigs*window # numpy broadcasting operation
    # subsigs = subsigs*window
    if keep_last_window and valid_last_window :
        pass
    else :
        subsigs = subsigs[:-1,:]
        t = t[:-1]

    return(t, subsigs.T)

def _signal_unslicing(sliced_data, Nw, Noverlap, N) :
    """
    Very experimental 
    """
    len_slice = len(sliced_data)
    signal = np.zeros(N, dtype=sliced_data.dtype)
    for k, (ind,  slice_) in enumerate(zip(sliding_window_iterator(Nw//2, N, 0), sliced_data.T)) :
        if k == 0 :
            signal[ind] = slice_[:Nw//2] 
        elif k<signal.shape[-1]:
            signal[ind] = slice_[Noverlap//2:-Noverlap//2]
        else :
            signal[ind] = slice_[Nw//2:]   
    return(signal) 

def calc_freq_mask(f, fmin, fmax, is_rfft=False):
    if is_rfft:
        freqmask =  (f<=fmax) & (f>fmin)
    else :
        testpos = (f<=fmax) & (f>fmin)
        testneg = (f>= -fmax) & (f<-fmin)
        freqmask = testneg | testpos
    return(freqmask)




def _flathanning(N) :
    """generate  a flathanning window"""
    Nhann = N//2
    winhan = get_window("hann",Nhann)
    win = np.ones(N)
    win[:Nhann//2] = winhan[:Nhann//2]
    win[-Nhann//2:] = winhan[-Nhann//2:]
    return(win)


def get_window(windowtype, N) :
    '''
    An alternative to scipy.signal.get_window() function which seems to be buggy regarding windows that needs several argument (dpss, kaiser...)
    take exactly the same inputs as signal.get_window
    forked from scipy.signal.get_window()
    '''
    winfuncdict = dict({(key, var) for key, var in vars(sig.windows).items() if not key.startswith('__')})
    winfuncdict["flathanning"] = _flathanning
    winfuncdict["flathann"] = _flathanning

    if isinstance(windowtype, str) :
        try :
            winfunc = winfuncdict[windowtype]
        except KeyError :
            raise ValueError("Unknown windowtype : {}".format(windowtype))
        window = winfunc(N)
    elif isinstance(windowtype, tuple) :
        try :
            winfunc = winfuncdict[windowtype[0]]
        except KeyError :
            raise ValueError("Unknown windowtype : {}".format(windowtype))
        params = (N,) + windowtype[1:]
        window = winfunc(*params)
    elif isinstance(windowtype, np.ndarray) : ### convenience condition if the windowtype ise already a window ndarray
        window=windowtype
    else :
        raise ValueError("windowtype must be a str or a tuple, not {}".format(type(windowtype)))
    return(window)

def _next_pow2(n) :
    """
    Return the closest upper power of two of n
    """
    return(2**(int(n-1).bit_length()))



def RemoveStreamResponse(stream, verbose=False, wiener_deconvolution=False, SNR=1, nmirror=None, unit="VEL", mask_nodata=None, water_level=None) :
    '''
    Act like stream.remove_response() but make different choice on the output parameter
    and implement a Wiener deconvolution filter.

    Args:
        stream   (obspy.core.stream.Stream)  : the input stream containing the trace to deconvolve
        verbose  (bool) :  if True print some infos.
        wiener_deconvolution : (bool) : if True, a Wiener deconvolution will be conducted on the trace  
              instead of a classic Fourier deconvolution, providing the SNR
              parameter dict contains a suitable SNR(f) vector.
        SNR  (dict) :  a dict containing dicts for each channel (key=channel) containing
        nmirror  (int) : the number of sample that will be mirrored to mitigate stability issues. Default to npts/2.
        unit  (str)  :unit for assessing the seismometer instrument response,can be 'VEL' or 'ACC'.
              It doesn't affect the pressure channel.         
        mask_nodata (dict of boolean ndarray)  : The keys of the dict must be consistent to the stream channels.    
        water_level  (float) : a water level in dB which will set the minimum value of the instrument frequency response to sabilize the division.                        
    
    Returns:
        stream (obspy.core.stream.Stream) : the input stream containing the trace to deconvolve

    '''
    from scipy.ndimage import binary_dilation

    mask=True
    if mask_nodata is None :
        mask=False
        mask_nodata = tl.mask_like(stream)
    elif not isinstance(mask_nodata, dict) :
        raise TypeError("You are giving a mask to the RemoveStreamResponse function that is not a dict")
    else :
        maskedge=mask_nodata.copy()



    for trace in stream :
        if nmirror is None :
            nmirror = trace.stats.npts//2 ### make the signal "periodic" to avoid fft errors
        ### removing invalid data values if required
        data = trace.data.copy()[~mask_nodata[trace.stats.channel]]
        if verbose :
            print(r"{} % of data are valid".format(len(data)/trace.stats.npts*100), flush=True)

        if nmirror != 0 :
            reflected_data = np.pad(data, nmirror, mode="symmetric")
        else :
            reflected_data = data

        new_npts = int(len(data) + 2*nmirror)

        if trace.get_id()[-1] in ["1", "N", "2", "E", "Z"] :
            response, f = trace.stats.response.get_evalresp_response(trace.stats.delta,
                                                                        new_npts,
                                                                        output=unit)
            if "ACC" in unit :                                                                  
                trace.stats.response.response_stages[0].input_units = "M/S^2"
                unit_converter = 1.#j*2*np.pi*f
            else :
                unit_converter = 1.

            if verbose :
                print("Removing the instrumental response for trace {} with {} output unit".format(trace.get_id(), unit), flush=True)
        elif trace.get_id()[-1] in ["H"] :
            response, f = trace.stats.response.get_evalresp_response(trace.stats.delta,
                                                                        new_npts,
                                                                        output="DEF")
            if verbose :
                print("Removing the instrumental response for trace {} with {} output unit".format(trace.get_id(), "DEF"), flush=True)
            unit_converter = 1.

        if isinstance(water_level, float) or isinstance(water_level, int):
            ### doing the obspy way
            response_abs = np.abs(response)
            response_angle = np.angle(response)
            water_level_abs = response_abs.max() *10**(-water_level/20)
            response_abs[response_abs < water_level_abs] = water_level_abs 
            response = response_abs*np.exp(1.j*response_angle)
            

        if wiener_deconvolution :
            G = 1/response * (1/(1+1/(np.abs(response)**2 * SNR))) * unit_converter
        else :
            G = 1/response*unit_converter
        
        FTtrace = rfft(reflected_data, new_npts)
        FTtrace = FTtrace*G 
        FTtrace[np.isnan(FTtrace)] = 0.j
        FTtrace[np.isinf(FTtrace)] = 0.j
        temp_data = irfft(FTtrace, new_npts).real


        if mask and (np.sum(mask_nodata[trace.stats.channel])>0):
            trace.data[~mask_nodata[trace.stats.channel]] = temp_data[nmirror: new_npts - nmirror]
            trace.data[mask_nodata[trace.stats.channel]] = 0.
        else :
            trace.data = temp_data[nmirror: new_npts - nmirror]

    return(stream)

def _ApplyStreamResponse(stream,  verbose=False, nmirror=None) :
    '''
    Act like stream.simulate_response() but make different choice on the output parameter
    Deprecated.
    '''

    for trace in stream :
        if nmirror is None :
            nmirror = trace.stats.npts//2 ### make the signal "periodic" to avoid fft errors
        if nmirror != 0 :
            reflected_data = np.pad(trace.data.copy(), nmirror, mode="symmetric")
        else :
            reflected_data = trace.data.copy()

        new_npts = int(trace.stats.npts + 2*nmirror)

        if trace.get_id()[-1] in ["1", "N", "2", "E", "Z"] :
            response, f = trace.stats.response.get_evalresp_response(trace.stats.delta,
                                                                        new_npts,
                                                                        output="VEL")
            # trace.stats.response.response_stages[0].input_units = "M/S^2"
            if verbose :
                print("Removing the instrumental response for trace {} with {} output unit".format(trace.get_id(), "ACC"), flush=True)
        elif trace.get_id()[-1] in ["H"] :
            response, f = trace.stats.response.get_evalresp_response(trace.stats.delta,
                                                                        new_npts,
                                                                        output="DEF")
            if verbose :
                print("Removing the instrumental response for trace {} with {} output unit".format(trace.get_id(), "DEF"), flush=True)
        FTtrace = rfft(reflected_data, new_npts)
        FTtrace = np.abs(FTtrace)*np.abs(response) * np.exp( 1j*(np.angle(FTtrace) + np.angle(response)))
        FTtrace[np.isnan(FTtrace)] = 0.j
        FTtrace[np.isinf(FTtrace)] = 0.j
        temp_data = irfft(FTtrace, new_npts).real

        trace.data = temp_data[nmirror: new_npts - nmirror]
    return(stream)



def _zero_bad(goods, n_to_reject):
    """
    Remove wholes smaller than n_to_reject in a mask.
    Parameters
    ----------
    goods : (numpy.ndarray)
        - the mask.
    n_to_reject : (int)
        - the maximum size of the whole to be filled. 
    
    Output
    ------
    goods : (numpy.ndarray)
        - the updated mask.
    """
    from scipy.ndimage import binary_opening
    goods = np.expand_dims(goods, axis=0).astype(int)
    if n_to_reject > 0:
        ### making a binary opening which removes the objects (series of consecutives ones) smaller than n_to_reject
        goods = binary_opening(goods, structure=np.ones((1,n_to_reject), dtype=int), iterations=1).squeeze().astype(bool) 
    return(goods)


