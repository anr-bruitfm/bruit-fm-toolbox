#! /usr/bin/bash
### Author  :  SIMON REBEYROL
### Company :  IFREMER, UMR GEOOCEAN


'''
This toolbox is dedicated to the stationarity assessments.
It is kept in the BRUITFM project but was not really used as it was found to be not reliable for seismological data.
The documentation is therefore not complete.
Uses the statsmodels python library.

References:
    - A. DICKEY, David y A. FULLER,  Wayne. Distribution of the Estimators for Autoregressive Time Series with a Unit Root. Informa UK Limited, 1979, 427–431. (74). https://doi.org/10.1080/01621459.1979.10482531
    - KWIATKOWSKI, Denis, C.B. PHILLIPS,  Peter, PETER SCHMIDT,  y YONGCHEOL SHIN, . Testing the null hypothesis of stationarity against the alternative of a unit root. Elsevier BV, 1992, 159–178. (54). https://doi.org/10.1016/0304-4076(92)90104-y
    - APARICIO, Felipe, ESCRIBANO, Alvaro y SIPOLS, Ana E.. Range Unit-Root (RUR) Tests: Robust against Nonlinearities, Error Distributions, Structural Breaks and Outliers. Wiley, 2006, 545–576. (27).  https://doi.org/10.1111/j.1467-9892.2006.00474.x

'''


import numpy as np
import obspy
from scipy.interpolate import interp1d

from itertools import product
import warnings
from datetime import datetime, timedelta
from tqdm import tqdm
from pandas import DataFrame
import matplotlib.pyplot as plt


from statsmodels.tsa.stattools import adfuller, kpss, range_unit_root_test

try :
    from .. import utils as tl
except ImportError:
    import utils as tl


class StationnarityAssessment :
    """ 
    This object allows to test the stationnarity of a stream instance.
    
    Args:
        stream (obspy.core.stream.Stream) : the stream whose stationarity will be assessed.

    """
    

    def __init__(self, stream) :
        self.stream = stream
        self.original_stream = stream.copy()
        self.adf_flag = False 
        self.kpss_flag = False 
        self.rur_flag = False
        self.results = {}
        self.names = {"adf" : "augmented Dickey-Fuller",
                      "kpss": "Kwiatkowski-Phillips-Schmidt-Shin",
                      "rur" : "Range-unit root"}
        self.counts = None
        self.deviation = None

        
    def attach_deviation(self, deviation) :
        """Attach a StreamTimeSeriesDeviation instance"""
        if not isinstance(deviation, StreamTimeSeriesDeviation) :
            raise TypeError("deviation must be of class StreamTimeSeriesDeviation, got {}".format(type(deviation)))
        self.deviation = deviation


    def compute(self,test, window_length, window_overlap) :
        """Compute the stationarity given test for the window_length and window_overlap

        Args:
            test (str) : which test you want to conduct, can be either 'adf' (Dickey et al. 1979), 'kpss' (KWIATKOWSKI et al. 1992) or 'rur' (APARICIO et al. 2006).
            window_length (float) : the length of each segment in seconds.
            window_overlap (float) : the overlap in second between to segments.
        
        Returns:
            results (dict) : the results embbedded in a multi-layer dict 
            mask_stationnarity (numpy.ndarray) : the stationnarity mask
        
        """
        if test not in ("adf", "rur", "kpss") :
            raise ValueError("test must be either adf, rur or kpss, got {}".format(test))
        
        self.results[test] = {}
        from tqdm import tqdm
        print("Conducting the {} test :".format(self.names[test]), flush=True)
        mask_stationnarity = {}
        mask_stationnarity_window = {}
        for trace in self.stream :
            window_step = window_length- window_overlap
            nb_window = int(np.ceil((trace.stats.endtime - trace.stats.starttime)/window_step))
            target_t = np.linspace(0, trace.stats.npts*trace.stats.delta, trace.stats.npts)
            windowed_t = np.linspace(0, trace.stats.npts*trace.stats.delta, nb_window)
            results = {"values" : np.zeros(nb_window),
                        "p_values" : np.zeros(nb_window),
                        "critical_value_5%" : np.zeros(nb_window),
                        "datetimes" : np.zeros(nb_window, dtype=datetime), 
                        "window_length" : window_length,
                        "window_overlap" : window_overlap,
                        "error" : np.zeros(nb_window, dtype=bool)}
            print("--> for trace {}".format(trace), flush=True)
            for k, windowed_trace in tqdm(enumerate(trace.slide(window_length, window_step, include_partial_windows=True, nearest_sample=False)),total=nb_window) :
                x = DataFrame(windowed_trace.data)
                try :
                    if  test == "adf" :
                        current_result = adfuller(x, regression="ctt")
                        results["critical_value_5%"][k] = current_result[4]["5%"]
                    elif test == "kpss" :
                        current_result = kpss(x)
                        results["critical_value_5%"][k] = current_result[3]["5%"]
                    elif test== "rur" :
                        current_result = range_unit_root_test(x)
                        results["critical_value_5%"][k] = current_result[2]["5%"]
                    
                    results["values"][k] = current_result[0]
                    results["p_values"][k] = current_result[1]
                    
                except ValueError :
                    results["values"][k] = np.nan
                    results["p_values"][k] = np.nan
                    results["critical_value_5%"][k] = np.nan
                    results["error"][k] = True

                
                results["datetimes"][k] = windowed_trace.stats.starttime.datetime

            if test == "adf" :
                mask_temp = results["values"] < results["critical_value_5%"]
            elif test == "kpss" :
                mask_temp = results["p_values"] < 0.05#results["KPSS_values"] < results["critical_value_5%"]
            elif test == "rur" :
                mask_temp = results["values"] < results["critical_value_5%"]

            if len(mask_temp) > nb_window :
                mask_temp = mask_temp[:nb_window]
            mask_interpolator = interp1d(windowed_t ,mask_temp, kind='nearest', bounds_error=False)
            mask_stationnarity = mask_interpolator(target_t).astype(bool)
            mask_stationnarity_window = mask_temp
            
            self.results[test][trace.stats.channel] = { "mask_stationnarity_window" : mask_stationnarity_window,
                                    "mask_stationnarity" : mask_stationnarity,
                                    "results" : results,
                                    "window_length" : window_length,
                                    "window_overlap" : window_overlap,
                                    "datetimes" : results["datetimes"]}

        return(results, mask_stationnarity)




    def plot_results(self, test, channels=None, StreamTimeSeriesDeviation_instance=None) :
        """Plot the results of the stationarity analysis, can attach a StreamTimeSeriesDeviation instance to overlap."""
        if test not in ("adf", "rur", "kpss") :
            raise ValueError("test must be either adf, rur or kpss, got {}".format(test))
        
        
        if channels is None :
            channels = self.results[test].keys()
            
        else :
            if isinstance(channels, str) :
                channels = [channels]


        if StreamTimeSeriesDeviation_instance is not None :
            if not isinstance(StreamTimeSeriesDeviation_instance, StreamTimeSeriesDeviation) :
                raise TypeError("StreamTimeSeriesDeviation_instance must be of class StreamTimeSeriesDeviation, got {}".format(type(StreamTimeSeriesDeviation_instance)))
        else :
            StreamTimeSeriesDeviation_instance = self.deviation
        
        figure, axes = plt.subplots(len(channels), figsize=[6.4, 3.8*len(channels)])
        figure.suptitle("Stationnarity results {} for station {}\n from {} to {}".format(test.upper(), self.stream[0].stats.station, self.stream[0].stats.starttime, self.stream[0].stats.endtime))
        
        if len(channels) == 1 :
            axes = [axes]
        for k, channel in enumerate(channels) :
            from datetime import timedelta
            trace_results = self.results[test][channel]
            mask = trace_results["mask_stationnarity_window"]
            datetime_t = trace_results["datetimes"]
            min_value = np.min((trace_results["results"]["values"].min(), trace_results["results"]["critical_value_5%"].min())) * 1.02
            max_value = np.max((trace_results["results"]["values"].max(), trace_results["results"]["critical_value_5%"].max())) * 1.02
            timespan = trace_results["results"]["window_length"] - trace_results["results"]["window_overlap"] #self.window_length_adf - self.window_overlap_adf
            if min_value < 0:
                axes[k].bar(datetime_t, (mask* min_value)  , color="tab:green", width=timedelta(seconds=timespan), alpha=0.2, align="edge")
                axes[k].bar(datetime_t, (~mask*min_value)  , color="tab:red", width=timedelta(seconds=timespan), alpha=0.2, align="edge")
            if max_value > 0:
                axes[k].bar(datetime_t, (mask* max_value)  , color="tab:green", width=timedelta(seconds=timespan), alpha=0.2, align="edge")
                axes[k].bar(datetime_t, (~mask* max_value)  , color="tab:red", width=timedelta(seconds=timespan), alpha=0.2, align="edge")
            
            axes[k].plot(datetime_t, trace_results["results"]["values"], label="adf metric", color="tab:blue", linewidth=0.4)
            axes[k].plot(datetime_t, trace_results["results"]["critical_value_5%"], label="Critical value 5%", color="tab:blue", linewidth=0.4, linestyle="dashdot")
            axes[k].set_title("{} channel stationnarity check".format(channel))
            axes[k].set_xlabel("Date and time")
            axes[k].set_ylabel("{} metric".format(test.upper()),  color="tab:blue")
            axes[k].grid()
            if StreamTimeSeriesDeviation_instance is not None :
                axe_dev = axes[k].twinx()

                
                mad_trace = StreamTimeSeriesDeviation_instance.stream_mad.select(channel=channel)[0]
                try :
                    unit = mad_trace.stats.response.response_stages[0].input_units
                except AttributeError :
                    unit=''
                axe_dev.set_ylabel("Median absolute deviation ({})".format(unit),color="tab:orange" )
                t = np.linspace(0, mad_trace.stats.npts/mad_trace.stats.delta, mad_trace.stats.npts)
                datetime_t = tl.datetime_vector(mad_trace.stats.starttime.datetime, t)
                axe_dev.plot(datetime_t, mad_trace.data, label="mad", color="tab:orange")

            plt.setp(axes[k].get_xticklabels(), rotation=45)
        plt.tight_layout()

        return(figure)


    def plot_masked_stream(self, test):
        if test not in ("adf", "rur", "kpss") :
            raise ValueError("test must be either adf, rur or kpss, got {}".format(test))
        stream = self.original_stream.copy()
        for trace in stream :
            
            trace.data[~self.results[test][trace.stats.channel]["mask_stationnarity"]] = np.nan
        
        figure = stream.plot(handle=True, block=False, equal_scale=False)
        return(figure)


    def mask_stream(self, test, stream=None): 
        if test not in ("adf", "rur", "kpss") :
            raise ValueError("test must be either adf, rur or kpss, got {}".format(test))
        if not isinstance(stream, obspy.core.stream.Stream) :
            stream = self.stream
        for trace in stream :
            trace.data[~self.results[test][trace.stats.channel]["mask_stationnarity"]] = 0.
        return(stream)

    def count_non_stationary_window(self) :

        # nb_channel = self.stream.count()
        # masks_stationarity = 
        mask_stationnarity = {}
        counts = {}
        for k, test in enumerate(self.results.keys()) :
            for l, channel in enumerate(self.results[test].keys()) :
                if channel not in mask_stationnarity.keys() :
                    mask_stationnarity[channel] = {}
                mask_stationnarity[channel][test] = self.results[test][channel]["mask_stationnarity_window"]

        for k, channel in enumerate(mask_stationnarity.keys()) :
            for l, test in enumerate(mask_stationnarity[channel].keys()) :
                if l ==0 :
                    channel_mask = mask_stationnarity[channel][test]
                else :
                    channel_mask = channel_mask & mask_stationnarity[channel][test]
                    
            counts[channel] = np.sum(channel_mask)/len(mask_stationnarity[channel][test])*100

        self.counts = counts