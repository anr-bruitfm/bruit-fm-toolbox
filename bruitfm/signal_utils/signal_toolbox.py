#! /usr/bin/bash
### Author  :  SIMON REBEYROL
### Company :  IFREMER, UMR GEOOCEAN


'''
This toolbox is dedicated to signal processing related classes and functions in the time domain. Some function may be hidden mainly because they are deprecated and unused, but can still be useful in some cases. 

References:

    1. https://docs.scipy.org/doc/scipy/reference/generated/scipy.signal.get_window.html#scipy.signal.get_window
    2. WELCH, Peter D.. "The use of fast Fourier transform for the estimation of power spectra: A method based on time averaging over short, modified periodograms". IEEE Transactions on Audio and Electroacoustics. 1967, vol 15, num. 2, p. 70–73. DOI: 10.1109/tau.1967.1161901
    3. THOMPSON, Rory O. R. Y.. "Coherence Significance Levels". Journal of the Atmospheric Sciences. 1979, vol 36, num. 10, p. 2020–2021. DOI: 10.1175/1520-0469(1979)036<2020:csl>2.0.co;2
    4. ROUSSEEUW, Peter J. & CROUX, Christophe. Alternatives to the Median Absolute Deviation. Informa UK Limited, 1993, 1273–1283. (88). 

'''


import numpy as np
import obspy
from obspy import UTCDateTime
import scipy.signal as sig
from scipy.interpolate import interp1d
from scipy.stats import median_abs_deviation as mad
from scipy.ndimage import median_filter

from datetime import datetime, timedelta
from tqdm import tqdm
import matplotlib.pyplot as plt

try :
    from .. import utils as tl
except ImportError:
    import utils as tl




class StreamTimeSeriesDeviationCollection:
    """A class which  allow to analyse sliding deviation statistics with multiple scales.



    Args:
        list_of_dev (list of StreamTimeSeriesDeviation) : a collection of StreamTimeSeriesDeviation. If set, will build the instance with this collection and bypass the analysis, default to None.
        stream (obspy.core.stream.Stream) : the stream to be analysed, default to None.
        sliding_window_sizes (list) : the list of scales, default to None.
        size_unit (str) : the unit of the scales, can be either 'second' or 'sample', default to None.
        freqmin (float) : the min corner frequency to filter the signals prior to the deviation analysis, default to None.
        freqmax (float) : the max corner frequency to filter the signals prior to the deviation analysis, default to None.
        elevation (float) : the elevation of the corresponding stream station, just for display, default to None.

    -----------
    Attributes:
    -----------

    Attributes
    ----------

        collection (list) : the collection of StreamTimeSeriesDeviation.
        sizes (list) :  a list of int describing the sizes (scales) of the sliding windows for each StreamTimeSeriesDeviation instance in collection.
        size_unit (str) : the unit of the sizes.
        mad (dict) : the dictionnary that contains the median absolute deviation results, which keys correspond to the channel names of the stream.
        std (dict) : the dictionnary that contains the standard deviation results, which keys correspond to the channel names of the stream.
        channels (list) : the list containing the channel names.
        min_value (dict) : the min values for each channel that is used for plotting.
        max_value (dict) : the max values for each channel that is used for plotting.
        unit (dict) : the unit of the deviation for each channel.
        station (str) : the name of the analysed station.
        elevation (float) : the station elevation.
        t (numpy.ndarray) : the time values for each sample, starting from 0 s.
        datetimes (numpy.ndarray) : the datetime vector for each samples.

    --------
    Methods:
    --------

    Methods
    -------

        plot(plot_mad=True, plot_std=True, channel=None, display_size_unit="second", cmap="viridis", startdate=None, enddate=None, equal_scale=True, gridspace=None, maxval=None, minval=None, display_ylabel=True, display_yticks=True, display_colorbar=True, tight_layout=True) :
            Plot the deviation collection as a set of scale vs time vs value chart.
        plot_gradient(plot_mad=True, plot_std=True, channel=None, display_size_unit="second", cmap="viridis", startdate=None, enddate=None, equal_scale=True, gridspace=None) :
            Plot the deviation gradient of the collection as a set of scale vs time vs value chart.
        save_memmap(out_path):
            Save the collection as a memmap + header ENVI files using the spectral library.
        load_memmap(hdr_path):
            Load a saved collection from a memmap + header ENVI files using the spectral library.


    """


    def __init__(self, list_of_dev=None, stream=None, sliding_window_sizes=None, size_unit=None,  freqmin=None, freqmax=None, elevation=None) :

        if list_of_dev is not None :
            self._init2(list_of_dev)
        elif isinstance(stream, obspy.core.stream.Stream) :
            self._init1(stream, sliding_window_sizes, size_unit,  freqmin=freqmin, freqmax=freqmax,  elevation=elevation)
        else :
            print("Warning: empty StreamTimeSeriesDeviationCollection, please load a saved file using the .load() method.")
        


    def _init1(self, stream, sliding_window_sizes, size_unit,  freqmin=None, freqmax=None, elevation=None) :
        """Constructor 1, will compute everything"""

        self.sizes = sliding_window_sizes
        self.size_unit = size_unit
        mad = {}
        std = {}
        mask_mad = {}
        mask_std = {}
        channels = []
        self.collection = []
        self.min_value = {}
        self.max_value = {}
        self.unit = {}
        self.station = stream[0].stats.station
        self.elevation = elevation
        

        for trace in stream :
            if trace.stats.channel not in channels :
                channels.append(trace.stats.channel)
                self.min_value[trace.stats.channel] = np.inf #init
                self.max_value[trace.stats.channel] = 0 #init
                try :
                    self.unit[trace.stats.channel] = "(" + trace.stats.response.response_stages[0].input_units + ")"
                except :
                    self.unit[trace.stats.channel] = ""
         
        print("Computing the sliding deviations:")
        for size in tqdm(sliding_window_sizes) :
            current_dev = StreamTimeSeriesDeviation(stream, size, size_unit, freqmin=freqmin, freqmax=freqmax,  verbose=False) 
            self.collection.append(current_dev)


        for channel in channels :
            mad[channel] = []
            std[channel] = []
            mask_mad[channel] = []
            mask_std[channel] = []

        for channel in channels :
            for k, current_dev in enumerate(self.collection) :
                mad[channel].append(current_dev.stream_mad.select(channel=channel)[0].data)
                std[channel].append(current_dev.stream_std.select(channel=channel)[0].data)
                mask_mad[channel].append(np.isnan(current_dev.stream_mad.select(channel=channel)[0].data) | (current_dev.stream_mad.select(channel=channel)[0].data < 1e-15))
                mask_std[channel].append(np.isnan(current_dev.stream_std.select(channel=channel)[0].data) | (current_dev.stream_std.select(channel=channel)[0].data < 1e-15))
        


        for channel in channels :
            
            mask_mad[channel] = np.sum(np.stack(mask_mad[channel], axis=0),axis=0) > 0
            mask_std[channel] = np.sum(np.stack(mask_std[channel], axis=0),axis=0) > 0
            

            mad[channel] = np.stack(mad[channel], axis=0)
            std[channel] = np.stack(std[channel], axis=0)
            mad[channel][:,mask_mad[channel]] = np.nan
            std[channel][:,mask_std[channel]] = np.nan
            

            self.min_value[channel] = min(np.percentile(mad[channel][:,~mask_mad[channel]], 5), np.percentile(std[channel][:,~mask_std[channel]], 5))
            self.max_value[channel] = max(mad[channel][:,~mask_mad[channel]].max(), std[channel][:,~mask_std[channel]].max())

        self.mad = mad 
        self.std = std

        self.channels = channels
        self.t = np.linspace(0, current_dev.stream_mad[0].stats.npts/current_dev.stream_mad[0].stats.delta, current_dev.stream_mad[0].stats.npts)
        self.datetimes = tl. tl.datetime_vector(current_dev.stream_mad[0].stats.starttime.datetime,self.t)

    
    def _init2(self, list_of_dev) :
        """Constructor 2, create the instance from a precomputed set of StreamTimeSeriesDeviation."""

        self.sizes = []
        mad = {}
        std = {}
        channels = []
        self.collection = []
        self.min_value = {}
        self.max_value = {}
        self.unit = {}
        self.station = ""
        self.elevation = None

        for trace in stream :
            if trace.stats.channel not in channels :
                channels.append(trace.stats.channel)
                self.min_value[trace.stats.channel] = np.inf #init
                self.max_value[trace.stats.channel] = 0 #init
                try :
                    self.unit[trace.stats.channel] = "(" + trace.stats.response.response_stages[0].input_units + ")"
                except :
                    self.unit[trace.stats.channel] = ""

        for channel in channels :
            mad[channel] = []
            std[channel] = []
        for channel in channels :
            for k, current_dev in enumerate(self.collection) :
                self.sizes.append(current_dev.size)
                self.size_unit = current_dev.size_unit
                mad[channel].append(current_dev.stream_mad.select(channel=channel)[0].data)
                std[channel].append(current_dev.stream_std.select(channel=channel)[0].data)
                
                temp_min = (mad[channel][-1].min(), std[channel][-1].min())
                if self.min_value[channel] > min(temp_min) :
                    self.min_value[channel] = min(temp_min)
                temp_max = (mad[channel][-1].max(), std[channel][-1].max())
                if self.max_value[channel] < max(temp_max) :
                    self.max_value[channel] = max(temp_max)
        for channel in channels :
            mad[channel] = np.stack(mad[channel], axis=0)
            std[channel] = np.stack(std[channel], axis=0)
        
        self.mad = mad 
        self.std = std

        self.channels = channels
        self.t = np.linspace(0, current_dev.stream_mad[0].stats.npts/current_dev.stream_mad[0].stats.delta, current_dev.stream_mad[0].stats.npts)
        self.datetimes =  tl.datetime_vector(current_dev.stream_mad[0].stats.starttime.datetime,self.t)

    def plot(self, plot_mad=True, plot_std=True, channel=None, display_size_unit="second", cmap="viridis", startdate=None, enddate=None, equal_scale=True, gridspace=None, maxval=None, minval=None, display_ylabel=True, display_yticks=True, display_colorbar=True, tight_layout=True) :
        """Plot the deviation collection as a set of scale vs time vs value chart.

        Args:
            plot_mad (bool) : draw the median absolute deviation if True, default to True.
            plot_std (bool) : draw the standard deviation if True, default to True.
            channel (list) : the list of channel name whose deviation are to be drawn.
            display_size_unit (str) : in which time unit the yticks should be converted to and written, can be 'second', 'minute', 'hour', 'day', default to 'second'.
            cmap (str) : the colormap to use, see the matplotlib colormap documentation for help, default to 'viridis'.
            startdate (datetime.datetime or obspy.UTCDateTime) : the date from which the chart should start, default to None.
            enddate (datetime.datetime or obspy.UTCDateTime) : the date to which the chart should end, default to None.
            equal_scale (bool) : set the same colorbar scale between the mad and std plots, using the min/max_value attribute or the given ones, default to True.
            gridspace (float) : set a vertical white-dotted line grid seperated by gridspace s, default to None (nogrid).
            maxval (dict) : the max values for each channel that is used for plotting.
            minval (dict) : the min values for each channel that is used for plotting.
            display_ylabel (bool) : display the vertical labels, default to True. 
            display_yticks (bool) : display the vertical ticks, default to True. 
            display_colorbar (bool) : display the colorbar, default to True. 
            tight_layout (bool) : make the figure tight, default to True. 

        Returns:
        --------
            figure : (matplotlib.Figure)
                the drawn figure.
        """
        from matplotlib.colors import LogNorm
        from obspy import UTCDateTime

        if display_size_unit == "second" :
            sizes = np.asarray(self.sizes) 
        elif display_size_unit == "minute" : 
            sizes = np.asarray(self.sizes)/60
        elif display_size_unit == "hour" :
            sizes = np.asarray(self.sizes)/3600
        elif display_size_unit == "day" :
            sizes = np.asarray(self.sizes)/3600/24  
        else :
            sizes = np.asarray(self.sizes)
            print("WARNING: unrecognized display_size_unit")
            display_size_unit = "second"  


        if plot_std and plot_mad :
            std_ind = 0
            mad_ind = 1
            cbar_ind = 2
            nb_col = 3
        elif plot_std and not plot_mad :
            std_ind = 0
            cbar_ind = 1
            nb_col = 2
        elif not plot_std and plot_mad :
            mad_ind = 0
            cbar_ind = 1
            nb_col = 2

        if channel is None :
            channels = self.channels 
        else :
            channels = [channel]

        if display_colorbar :
            fig, axes = plt.subplots(len(channels),nb_col, figsize=(10, 8), width_ratios=[0.95/(nb_col-1)]*(nb_col-1) + [0.05,], squeeze=False)
        else :
            fig, axes = plt.subplots(len(channels),nb_col-1, figsize=(10, 0.95*8), squeeze=False)
        if self.elevation is not None :
            elevation = str(self.elevation) + " m"
        fig.suptitle("Time-Scale plots of deviations for station {} - {}".format(self.station, elevation))
       
        if plot_std and plot_mad:
            axes[0,std_ind].set_title("Standard deviation")
            axes[0,mad_ind].set_title("Median absolute deviation")

        datetimes = np.asarray(self.datetimes)
        mask_datetimes = np.ones(len(datetimes), dtype=bool)

        if isinstance(startdate, datetime) :
            mask_datetimes = mask_datetimes & (startdate < datetimes)
        elif isinstance(startdate, UTCDateTime) :
            mask_datetimes = mask_datetimes & (startdate.datetime < datetimes)
        elif startdate is None :
            pass
        else :
            print("WARNING: startdate must be datetime.datetime or obspy.UTCDateTime, got {}".format(type(startdate)))

        if isinstance(enddate, datetime) :
            mask_datetimes = mask_datetimes & (enddate > datetimes)
        elif isinstance(enddate, UTCDateTime) :
            mask_datetimes = mask_datetimes & (enddate.datetime > datetimes)
        elif enddate is None :
            pass
        else :
            print("WARNING: enddate must be datetime.datetime or obspy.UTCDateTime, got {}".format(type(enddate)))

        datetimes = datetimes[mask_datetimes]
        min_val_std = {}
        max_val_std = {}
        min_val_mad = {}
        max_val_mad = {}

        for k, channel in enumerate(self.channels) :
        
            # imstd = axes[k,0].pcolormesh(self.datetimes, np.arange(len(self.sizes)), self.std[channel], rasterized=True, norm=LogNorm(vmin=self.min_value[channel], vmax=self.max_value[channel]), antialiased=False)#, vmin=self.min_value[channel], vmax=self.max_value[channel]
            if plot_std :
                if equal_scale :
                    if minval is None :
                        min_val_std[channel] = self.min_value[channel]
                    else :
                        min_val_std[channel] = minval[channel]
                    if maxval is None :
                        max_val_std[channel] = self.max_value[channel]
                    else :
                        max_val_std[channel] = maxval[channel]
                else :
                    if minval is None :
                        min_val_std[channel] = np.nanpercentile(self.std[channel][:,mask_datetimes],0) 
                    else :
                        min_val_std[channel] = minval[channel]
                    if maxval is None :
                        max_val_std[channel] = np.nanpercentile(self.std[channel][:,mask_datetimes],100)
                    else :
                        max_val_std[channel] = maxval[channel]
                idmev = axes[k,std_ind].imshow(self.std[channel][:,mask_datetimes], rasterized=True, norm=LogNorm(vmin=min_val_std[channel], vmax=max_val_std[channel]), aspect="auto", cmap=cmap, interpolation="nearest")
                axes[k,std_ind].set_yticks(np.arange(len(sizes)), sizes)
                if k == len(self.channels)-1 :
                    new_xticks = np.linspace(0,len(datetimes), 8, dtype=int, endpoint=False)
                    new_datetimes = [str(current_date).replace(" ", "\n") for current_date in datetimes[new_xticks]]
                    axes[k,std_ind].set_xticks(new_xticks, new_datetimes, fontsize="small")
                    plt.setp(axes[k,std_ind].get_xticklabels(), rotation=35)
                    axes[k,std_ind].set_xlabel("Date and time")
                else :
                    new_xticks = np.linspace(0,len(datetimes), 8, dtype=int, endpoint=False)
                    axes[k,std_ind].set_xticks(new_xticks, [""]*len(new_xticks), fontsize="small")
                if isinstance(gridspace, (int, float)) :
                    axes[k,std_ind].vlines(np.arange(0,np.sum(mask_datetimes), gridspace), -0.5, len(sizes)-0.5, color="white", linestyle="dashed", linewidth=0.5)

            if plot_mad :
                if equal_scale :
                    if minval is None :
                        min_val_mad[channel] = self.min_value[channel]
                    else :
                        
                        min_val_mad[channel] = minval[channel]

                    if maxval is None :
                        max_val_mad[channel] = self.max_value[channel]
                    else :
                        max_val_mad[channel] = maxval[channel]
                else :
                    if minval is None :
                        min_val_mad[channel] = np.nanpercentile(self.mad[channel][:,mask_datetimes],0)
                    else :
                        min_val_mad[channel] = minval[channel]
                    if maxval is None :
                        max_val_mad[channel] = np.nanpercentile(self.mad[channel][:,mask_datetimes],100)
                    else :
                        max_val_mad[channel] = maxval[channel]


                idmev = axes[k,mad_ind].imshow(self.mad[channel][:,mask_datetimes], rasterized=True, norm=LogNorm(vmin=min_val_mad[channel], vmax=max_val_mad[channel]), aspect="auto", cmap=cmap, interpolation="nearest")
                if display_yticks:
                    axes[k,mad_ind].set_yticks(np.arange(len(sizes)), sizes)
                else :
                    axes[k,mad_ind].set_yticks(np.arange(len(sizes)), [""]*len(sizes))
                if k == len(self.channels)-1 :
                    new_xticks = np.linspace(0,len(datetimes), 8, dtype=int, endpoint=False)
                    new_datetimes = [str(current_date).replace(" ", "\n") for current_date in datetimes[new_xticks]]
                    axes[k,mad_ind].set_xticks(new_xticks, new_datetimes, fontsize="small")
                    plt.setp(axes[k,mad_ind].get_xticklabels(), rotation=35)
                    axes[k,mad_ind].set_xlabel("Date and time")
                else :
                    new_xticks = np.linspace(0,len(datetimes), 8, dtype=int, endpoint=False)
                    axes[k,mad_ind].set_xticks(new_xticks, [""]*len(new_xticks), fontsize="small")
                if isinstance(gridspace, (int, float)) :
                    axes[k,mad_ind].vlines(np.arange(0,np.sum(mask_datetimes), gridspace), -.5, len(sizes)-0.5, color="white", linestyle="dashed", linewidth=0.5)

            if display_colorbar :
                cbarmad = fig.colorbar(idmev, cax=axes[k,nb_col-1])
                if plot_mad and not plot_std :
                    typedev = "MAD"
                elif plot_std and not plot_mad :
                    typedev = "STD"
                else :
                    typedev=""
                cbarmad.ax.set_ylabel("{} ({})".format(typedev, self.unit[channel]), fontsize="medium")
            # plt.setp(axes[k,nb_col-2].get_xticklabels(), rotation=45)

            if display_ylabel:
                axes[k,0].set_ylabel("{} channel \n Scale ({})".format(channel, display_size_unit))

        if tight_layout :
            fig.tight_layout()
        return(fig)

    def plot_gradient(self, plot_mad=True, plot_std=True, channel=None, display_size_unit="second", cmap="viridis", startdate=None, enddate=None, equal_scale=True, fs=1., gridspace=None) :
        """ Plot the deviation gradient of the collection as a set of scale vs time vs value chart.

        Args:
            plot_mad (bool) : draw the median absolute deviation if True, default to True.
            plot_std (bool) : draw the standard deviation if True, default to True.
            channel (list) : the list of channel name whose deviation are to be drawn.
            display_size_unit (str) : in which time unit the yticks should be converted to and written, can be 'second', 'minute', 'hour', 'day', default to 'second'.
            cmap (str) : the colormap to use, see the matplotlib colormap documentation for help, default to 'viridis'.
            startdate (datetime.datetime or obspy.UTCDateTime) : the date from which the chart should start, default to None.
            enddate (datetime.datetime or obspy.UTCDateTime) : the date to which the chart should end, default to None.
            equal_scale (bool) : set the same colorbar scale between the mad and std plots, using the min/max_value attribute or the given ones, default to True.
            gridspace (float) : set a vertical white-dotted line grid seperated by gridspace s, default to None (nogrid).

        
        Returns:
        --------

            figure : (matplotlib.Figure)
                the drawn figure.
        """
        from matplotlib.colors import SymLogNorm, AsinhNorm
        from obspy import UTCDateTime

        if display_size_unit == "second" :
            sizes = np.asarray(self.sizes) 
        elif display_size_unit == "minute" : 
            sizes = np.asarray(self.sizes)/60
        elif display_size_unit == "hour" :
            sizes = np.asarray(self.sizes)/3600
        elif display_size_unit == "day" :
            sizes = np.asarray(self.sizes)/3600/24  
        else :
            sizes = np.asarray(self.sizes)
            print("WARNING: unrecognized display_size_unit")
            display_size_unit = "second"  


        if plot_std and plot_mad :
            std_ind = 0
            mad_ind = 1
            cbar_ind = 2
            nb_col = 3
        elif plot_std and not plot_mad :
            std_ind = 0
            cbar_ind = 1
            nb_col = 2
        elif not plot_std and plot_mad :
            mad_ind = 0
            cbar_ind = 1
            nb_col = 2

        if channel is None :
            channels = self.channels 
        else :
            channels = [channel]

        fig, axes = plt.subplots(len(channels),nb_col, figsize=(10, 8), width_ratios=[0.95/(nb_col-1)]*(nb_col-1) + [0.05,])

        if self.elevation is not None :
            elevation = str(self.elevation) + " m"
        fig.suptitle("Time-Scale plots of deviation gradient for station {} - {}".format(self.station, elevation))
       
        if plot_std :
            axes[0,std_ind].set_title("Standard deviation")
        if plot_mad :
            axes[0,mad_ind].set_title("Median absolute deviation")

        datetimes = np.asarray(self.datetimes)
        mask_datetimes = np.ones(len(datetimes), dtype=bool)

        if isinstance(startdate, datetime) :
            mask_datetimes = mask_datetimes & (startdate < datetimes)
        elif isinstance(startdate, UTCDateTime) :
            mask_datetimes = mask_datetimes & (startdate.datetime < datetimes)
        elif startdate is None :
            pass
        else :
            print("WARNING: startdate must be datetime.datetime or obspy.UTCDateTime, got {}".format(type(startdate)))

        if isinstance(enddate, datetime) :
            mask_datetimes = mask_datetimes & (enddate > datetimes)
        elif isinstance(enddate, UTCDateTime) :
            mask_datetimes = mask_datetimes & (enddate.datetime > datetimes)
        elif enddate is None :
            pass
        else :
            print("WARNING: enddate must be datetime.datetime or obspy.UTCDateTime, got {}".format(type(enddate)))

        datetimes = datetimes[mask_datetimes]
        min_val_std = {}
        max_val_std = {}
        min_val_mad = {}
        max_val_mad = {}

        temp_val_max = {}
        temp_val_min = {}



        for k, channel in enumerate(self.channels) :
        
            # imstd = axes[k,0].pcolormesh(self.datetimes, np.arange(len(self.sizes)), self.std[channel], rasterized=True, norm=LogNorm(vmin=self.min_value[channel], vmax=self.max_value[channel]), antialiased=False)#, vmin=self.min_value[channel], vmax=self.max_value[channel]
            if plot_std :
                diff_std = np.gradient(self.std[channel][:,mask_datetimes], fs, axis=1)
                std = self.std[channel][:,mask_datetimes]
                min_val_std[channel] = np.nanpercentile(diff_std,5)
                max_val_std[channel] = np.nanpercentile(diff_std,95)
            if plot_mad :
                diff_mad = np.gradient(self.mad[channel][:,mask_datetimes], fs, axis=1)
                mad = self.mad[channel][:,mask_datetimes]
                min_val_mad[channel] = np.nanpercentile(diff_mad,5)
                max_val_mad[channel] = np.nanpercentile(diff_mad,95)

            if plot_mad and plot_std :
                temp_val_max = max(max_val_std[channel], max_val_mad[channel])
                temp_val_min = min(min_val_std[channel], min_val_mad[channel])
                max_val_std[channel] = temp_val_max
                max_val_mad[channel] = temp_val_max
                min_val_std[channel] = temp_val_min
                min_val_mad[channel] = temp_val_min


            if plot_std :


                linthresh = np.nanpercentile(diff_std[diff_std >0.],5)
 
                #, norm=SymLogNorm(np.abs(np.percentile(diff_std,75)), vmin=min_val_std[channel], vmax=max_val_std[channel])
                idmev = axes[k,std_ind].imshow(diff_std, rasterized=True, aspect="auto", cmap=cmap, interpolation="nearest", vmin=-max_val_mad[channel], vmax=max_val_mad[channel])#, norm=AsinhNorm(vmin=min_val_std[channel], vmax=max_val_std[channel]))
                axes[k,std_ind].set_yticks(np.arange(len(sizes)), sizes)
                if k == len(self.channels)-1 :
                    new_xticks = np.linspace(0,len(datetimes), 8, dtype=int, endpoint=False)
                    new_datetimes = [current_date for current_date in datetimes[new_xticks]]
                    axes[k,std_ind].set_xticks(new_xticks, new_datetimes, fontsize="small")
                    plt.setp(axes[k,std_ind].get_xticklabels(), rotation=45)
                else :
                    new_xticks = np.linspace(0,len(datetimes), 8, dtype=int, endpoint=False)
                    axes[k,std_ind].set_xticks(new_xticks, [""]*len(new_xticks), fontsize="small")

                if isinstance(gridspace, (int, float)) :
                    axes[k,std_ind].vlines(np.arange(0,np.sum(mask_datetimes), gridspace), 0, len(sizes)-1, color="black", linestyle="dashed", linewidth=0.3)

            if plot_mad :

                linthresh = np.nanpercentile(diff_mad[diff_mad >0.],0)

                # diff_mad = np.gradient(self.mad[channel][:,mask_datetimes], fs, axis=-1)
                #, norm=SymLogNorm(np.abs(np.percentile(diff_mad,85)), vmin=min_val_mad[channel], vmax=max_val_mad[channel]),
                idmev = axes[k,mad_ind].imshow(diff_mad, rasterized=True, aspect="auto", cmap=cmap, interpolation="nearest", vmin=-max_val_mad[channel], vmax=max_val_mad[channel])#, norm=SymLogNorm(linthresh, vmin=min_val_mad[channel], vmax=max_val_mad[channel]))
                axes[k,mad_ind].set_yticks(np.arange(len(sizes)), sizes)
                if k == len(self.channels)-1 :
                    new_xticks = np.linspace(0,len(datetimes), 8, dtype=int, endpoint=False)
                    new_datetimes = [current_date for current_date in datetimes[new_xticks]]
                    axes[k,mad_ind].set_xticks(new_xticks, new_datetimes, fontsize="small")
                    plt.setp(axes[k,mad_ind].get_xticklabels(), rotation=45)
                else :
                    new_xticks = np.linspace(0,len(datetimes), 8, dtype=int, endpoint=False)
                    axes[k,mad_ind].set_xticks(new_xticks, [""]*len(new_xticks), fontsize="small")

                if isinstance(gridspace, (int, float)) :
                    axes[k,mad_ind].vlines(np.arange(0,np.sum(mask_datetimes), gridspace), -.5, len(sizes)-0.5, color="black",  linewidth=0.5)


            cbarmad = fig.colorbar(idmev, cax=axes[k,nb_col-1])
            cbarmad.ax.set_ylabel("{}".format(r"$\nabla$"+self.unit[channel]), fontsize="medium")
            # plt.setp(axes[k,nb_col-2].get_xticklabels(), rotation=45)


            axes[k,0].set_ylabel("{} channel \n Sliding window size ({})".format(channel, display_size_unit))

        fig.tight_layout()
        return(fig)


    def _plot_gradient2(self, plot_mad=True, plot_std=True, channel=None, display_size_unit="second", cmap="viridis", startdate=None, enddate=None, equal_scale=True, fs=1.) :
        """Very similar to plot_gradient()"""
        from matplotlib.colors import SymLogNorm, AsinhNorm
        from obspy import UTCDateTime

        if display_size_unit == "second" :
            sizes = np.asarray(self.sizes) 
        elif display_size_unit == "minute" : 
            sizes = np.asarray(self.sizes)/60
        elif display_size_unit == "hour" :
            sizes = np.asarray(self.sizes)/3600
        elif display_size_unit == "day" :
            sizes = np.asarray(self.sizes)/3600/24  
        else :
            sizes = np.asarray(self.sizes)
            print("WARNING: unrecognized display_size_unit")
            display_size_unit = "second"  


        if plot_std and plot_mad :
            std_ind = 0
            mad_ind = 1
            cbar_ind = 2
            nb_col = 3
        elif plot_std and not plot_mad :
            std_ind = 0
            cbar_ind = 1
            nb_col = 2
        elif not plot_std and plot_mad :
            mad_ind = 0
            cbar_ind = 1
            nb_col = 2

        if channel is None :
            channels = self.channels 
        else :
            channels = [channel]

        fig, axes = plt.subplots(len(channels),nb_col, figsize=(10, 8), width_ratios=[0.95/(nb_col-1)]*(nb_col-1) + [0.05,])

        if self.elevation is not None :
            elevation = str(self.elevation) + " m"
        fig.suptitle("Time-Scale plots of deviation gradient for station {} - {}".format(self.station, elevation))
    
        if plot_std :
            axes[0,std_ind].set_title("Standard deviation")
        if plot_mad :
            axes[0,mad_ind].set_title("Median absolute deviation")

        datetimes = np.asarray(self.datetimes)
        mask_datetimes = np.ones(len(datetimes), dtype=bool)

        if isinstance(startdate, datetime) :
            mask_datetimes = mask_datetimes & (startdate < datetimes)
        elif isinstance(startdate, UTCDateTime) :
            mask_datetimes = mask_datetimes & (startdate.datetime < datetimes)
        elif startdate is None :
            pass
        else :
            print("WARNING: startdate must be datetime.datetime or obspy.UTCDateTime, got {}".format(type(startdate)))

        if isinstance(enddate, datetime) :
            mask_datetimes = mask_datetimes & (enddate > datetimes)
        elif isinstance(enddate, UTCDateTime) :
            mask_datetimes = mask_datetimes & (enddate.datetime > datetimes)
        elif enddate is None :
            pass
        else :
            print("WARNING: enddate must be datetime.datetime or obspy.UTCDateTime, got {}".format(type(enddate)))

        datetimes = datetimes[mask_datetimes]
        min_val_std = {}
        max_val_std = {}
        min_val_mad = {}
        max_val_mad = {}

        temp_val_max = {}
        temp_val_min = {}



        for k, channel in enumerate(self.channels) :
        
            # imstd = axes[k,0].pcolormesh(self.datetimes, np.arange(len(self.sizes)), self.std[channel], rasterized=True, norm=LogNorm(vmin=self.min_value[channel], vmax=self.max_value[channel]), antialiased=False)#, vmin=self.min_value[channel], vmax=self.max_value[channel]
            if plot_std :
                diff_std = np.gradient(self.std[channel][:,mask_datetimes], fs, axis=0)
                min_val_std[channel] = np.percentile(diff_std,1)
                max_val_std[channel] = np.percentile(diff_std,99)
            if plot_mad :
                diff_mad = np.gradient(self.mad[channel][:,mask_datetimes], fs, axis=0)
                min_val_mad[channel] = np.percentile(diff_mad,1)
                max_val_mad[channel] = np.percentile(diff_mad,99)

            if plot_mad and plot_std :
                temp_val_max = max(max_val_std[channel], max_val_mad[channel])
                temp_val_min = min(min_val_std[channel], min_val_mad[channel])
                max_val_std[channel] = temp_val_max
                max_val_mad[channel] = temp_val_max
                min_val_std[channel] = temp_val_min
                min_val_mad[channel] = temp_val_min


            if plot_std :


                linthresh = np.percentile(diff_std[diff_std >0.],5)

                #, norm=SymLogNorm(np.abs(np.percentile(diff_std,75)), vmin=min_val_std[channel], vmax=max_val_std[channel])
                linedev = axes[k,std_ind].semilogy(np.abs(diff_std).T, rasterized=True, aspect="auto", cmap=cmap, interpolation="nearest")

                axes[k,std_ind].set_yticks(np.arange(len(sizes)), sizes)
                if k == len(self.channels)-1 :
                    new_xticks = np.linspace(0,len(datetimes), 8, dtype=int, endpoint=False)
                    new_datetimes = [current_date for current_date in datetimes[new_xticks]]
                    axes[k,std_ind].set_xticks(new_xticks, new_datetimes, fontsize="small")
                    plt.setp(axes[k,std_ind].get_xticklabels(), rotation=45)
                else :
                    new_xticks = np.linspace(0,len(datetimes), 8, dtype=int, endpoint=False)
                    axes[k,std_ind].set_xticks(new_xticks, [""]*len(new_xticks), fontsize="small")

                if isinstance(gridspace, (int, float)) :
                    axes[k,std_ind].vlines(np.arange(0,np.sum(mask_datetimes), gridspace), 0, len(sizes)-1, color="black", linestyle="dashed", linewidth=0.3)


            if plot_mad :

                linthresh = np.percentile(diff_mad[diff_mad >0.],5)

                # diff_mad = (np.gradient(self.mad[channel][:,mask_datetimes], fs, axis=-1))
                diff_mad[diff_mad==0.] = np.nan
                base_color = np.asarray([0.3,0.5,0.8])
                colors = np.tile(base_color, (len(sizes),1)).T/3*np.log(np.arange(len(sizes))+1)
                colors /= colors.max()
                #, norm=SymLogNorm(np.abs(np.percentile(diff_mad,85)), vmin=min_val_mad[channel], vmax=max_val_mad[channel]),
                for l, current_diff_mad in enumerate(diff_mad) :
                    axes[k,mad_ind].plot(current_diff_mad,   label=sizes[l], color= colors[:,l])
                
                # axes[k,mad_ind].set_yticks(np.arange(len(sizes)), sizes)
                if k == len(self.channels)-1 :
                    new_xticks = np.linspace(0,len(datetimes), 8, dtype=int, endpoint=False)
                    new_datetimes = [current_date for current_date in datetimes[new_xticks]]
                    axes[k,mad_ind].set_xticks(new_xticks, new_datetimes, fontsize="small")
                    plt.setp(axes[k,mad_ind].get_xticklabels(), rotation=45)
                    axes[k,mad_ind].legend(loc='lower center', bbox_to_anchor=[0.5,1.], fancybox=True, shadow=True, ncols=7, fontsize=11)

                else :
                    new_xticks = np.linspace(0,len(datetimes), 8, dtype=int, endpoint=False)
                    axes[k,mad_ind].set_xticks(new_xticks, [""]*len(new_xticks), fontsize="small")
                axes[k,mad_ind].grid()

                if isinstance(gridspace, (int, float)) :
                    axes[k,mad_ind].vlines(np.arange(0,np.sum(mask_datetimes), gridspace), 0, len(sizes)-1, color="black", linestyle="dashed", linewidth=0.3)


            # cbarmad = fig.colorbar(idmev, cax=axes[k,nb_col-1])
            # cbarmad.ax.set_ylabel("{}".format(self.unit[channel]), fontsize="medium")
            # plt.setp(axes[k,nb_col-2].get_xticklabels(), rotation=45)


            axes[k,0].set_ylabel("{} channel \n Sliding window size ({})".format(channel, display_size_unit))

        fig.tight_layout()
        return(fig)
        
    def save_memmap(self, out_path) :
        """Save the collection as a memmap + header ENVI files using the spectral library.

        Args:
            out_path (str) : the path to the output file without extension.
        """
        import spectral as sp 

        ### the memmap will be of size : len(self.sizes) * len(datetimes) * (len(channel)*2)
        savedict = {"DESCRIPTION" : "THIS IS A SAVED StreamTimeSereiesDeviationCollection python instance",
                    "sizes" : self.sizes,
                    "size_unit" : self.size_unit,
                    "min_value" : [self.min_value[k] for k in self.channels],
                    "max_value" : [self.max_value[k] for k in self.channels],
                    "unit" : [self.unit[k] for k in self.channels],
                    "channels" : self.channels,
                    "t" : self.t,
                    "datetimes" : self.datetimes,
                    "station" : self.station,
                    "elevation" : self.elevation}
        outspy = sp.envi.create_image(out_path + ".hdr", metadata=savedict, force=True, dtype="float32", shape=(len(self.sizes) , len(self.datetimes) , (len(self.channels)*2)))
        out_memmap = outspy.open_memmap(writable=True)

        for k, key in enumerate(self.channels) :
            out_memmap[:,:,k] = self.std[key]
        for k, key in enumerate(self.channels) :
            out_memmap[:,:,k+len(self.channels)] = self.mad[key]
        out_memmap.flush()

    def load_memmap(self, hdr_path) :
        """Load a saved collection from a memmap + header ENVI files using the spectral library.

        Args:
            hdr_path (str) : the path to the .hdr file.
        """
        import spectral as sp 
        from obspy import UTCDateTime
        from datetime import datetime
        inspy = sp.open_image(hdr_path) 
        in_memmap = inspy.open_memmap()
        indict = inspy.metadata

        self.channels = indict["channels"]
        self.sizes = [float(size) for size in indict["sizes"]]
        self.size_unit = indict["size_unit"]
        self.min_value = {}
        for k, channel in enumerate(self.channels) :
            self.min_value[channel] = indict["min_value"][k]
        self.max_value = {}
        for k, channel in enumerate(self.channels) :
            self.max_value[channel] = indict["max_value"][k]

        self.unit = {}
        for k, channel in enumerate(self.channels) :
            self.unit[channel] = indict["unit"][k]   
        
        
        # self.t = indict["t"]
        self.t = [float(current_t) for current_t in indict["t"]]
        self.datetimes = []
        for date_time in indict["datetimes"] :
            try :
                self.datetimes.append(datetime.strptime(date_time, "%Y-%m-%d %H:%M:%S.%f").replace(microsecond=0))

            except ValueError :
                self.datetimes.append(datetime.strptime(date_time, "%Y-%m-%d %H:%M:%S"))
        # self.datetimes = [datetime.strptime(date_time, "%Y-%m-%d %H:%M:%S.%f") for date_time in indict["datetimes"]]
        self.station = indict["station"]
        self.elevation = indict["elevation"]

        self.mad = {}
        self.std = {}
        for k, key in enumerate(self.channels) :
            self.std[key] = in_memmap[:,:,k]
        for k, key in enumerate(self.channels) :
            self.mad[key] = in_memmap[:,:,k+len(self.channels)]


    def __str__(self):
        strout = "StreamTimeSeriesDeviationCollection instance :\n-- sizes : {}\n-- size_unit : {}\n-- channels : {}\n-- nb samples : {}".format(self.sizes, self.size_unit, self.channels, len(self.datetimes))
        print(strout, flush=True)
        return("")

class StreamTimeSeriesDeviation:
    """Objects that holds sliding deviations metrics as obspy.core.stream.Stream objects.

    Args: 
        stream (obspy.core.stream.Stream) : the stream to be analysed.
        sliding_window_size (float/int) : the size value of the sliding window.
        size_unit (str) : the unit of the size value, either "samples" or "seconds".
        freqmin (float) : the corner frequency of the highpass filter that have been processed while creating the instance.
        freqmax (float) : the corner frequency of the lowpass filter that have been processed while creating the instance.
        verbose (bool) : give some infos.
        compute_mad (bool) : compute or not the median absolute deviation.
        compute_std (bool) : compute or not the standard deviation.
        mask_nodata (dict of numpy.ndarray) : a dict containing boolean array for each channel in the stream. True value means invalid.
        
    -----------
    Attributes:
    -----------

    Attributes
    ----------

        original_stream (obspy.core.stream.Stream) : the original stream object
        stream_mad (obspy.core.stream.Stream) : the stream containing the median absolute deviation for each input traces 
        stream_std (obspy.core.stream.Stream) : the stream containing the standard deviation for each input traces 
        size (float/int) : the size value of the sliding window
        size_unit (str) : the unit of the size value, either "samples" or "seconds" 
        freqmin (float) : the corner frequency of the highpass filter that have been processed while creating the instance 
        freqmax (float) : the corner frequency of the lowpass filter that have been processed while creating the instance 
        channels (list) : a list containing all the channels in the stream.
        mask_nodata (dict of numpy.ndarray) : a dict containing boolean array for each channel in the stream. True value means invalid.
        mask_events (dict of numpy.ndarray) : a dict containing boolean array for each channel in the stream. True value means event.
        threshold (dict of dict of numpy.ndarray) : the threshold value dict, first layer of keys are 'std' and 'mad', then the name of channel.

    --------
    Methods:
    --------

    Methods
    -------

        estimate_threshold_sigma(starttime=None, endtime=None, sigma_factor=3., window_length=7200) :
            Estimate the transient event detection threshold values for each window_length-long segment using the median of the deviation times sigma_factor between starttime and endtime.
        plot_deviations(starttime=None, endtime=None,  threshold=None, channels=None) :
            Plot the deviation instance.
        detect_events_from_stream(std_or_mad="std"):
            Generate and update the mask_events attribute using the threshold attribute.
        count_events(min_hole_size=0):
            Count the number of event according to the mask_events attribute.
        

    """
    def __init__(self, stream, sliding_window_size, size_unit, freqmin=None, freqmax=None, verbose=True, compute_mad=True, compute_std=True, mask_nodata=None) :
        """
        Create a StreamTimeSeriesDeviation instance:
        """
        import time


        self.original_stream = stream.copy()
        self.size = sliding_window_size 
        self.size_unit = size_unit
        self.freqmin = freqmin 
        self.freqmax = freqmax 
        if mask_nodata is None :
            self.mask_nodata = tl.mask_like(stream)
        else :
            self.mask_nodata = mask_nodata
        self.threshold = {"mad" : {},
                          "std" : {}}

        self.mask_events = {}

        channels = []
        for trace in stream :
            if trace.stats.channel not in channels :
                channels.append(trace.stats.channel)
        self.channels = channels

        stream_copy = stream.copy()
        if freqmin is not None :
            stream_copy = FilterStream(stream_copy, 3, freqmin, btype='highpass', nmirror=None,  verbose=False, mask_nodata=mask_nodata) 
        if freqmax is not None :
            stream_copy = FilterStream(stream_copy, 3, freqmax, btype='lowpass', nmirror=None,  verbose=False, mask_nodata=mask_nodata) 
        
        
        if compute_mad :
            if verbose :
                print("-> Computing the sliding MAD estimation for current stream with a window length of {}".format(sliding_window_size), flush=True)
            start = time.process_time()
            mad = StreamSlidingMADEstimation(stream_copy, sliding_window_size, size_unit=size_unit, mask_nodata=mask_nodata)                    
            self.stream_mad = stream_copy.copy()
            for trace in self.stream_mad :
                trace.data = mad[trace.stats.channel]
            if verbose :
                print("--> Sliding MAD estimated in {}s".format(time.process_time()-start), flush=True)
        else :
            self.stream_mad = None

        

        if compute_std :
            start = time.process_time()
            if verbose :
                print("-> Computing the sliding STD estimation for current stream with a window length of {}".format(sliding_window_size), flush=True)
            std = StreamSlidingStdEstimation(stream_copy, sliding_window_size, size_unit=size_unit, mask_nodata=mask_nodata)
            self.stream_std = stream_copy.copy()
            for trace in self.stream_std :
                trace.data = std[trace.stats.channel]
            if verbose :
                print("--> Sliding STD estimated in {}s".format(time.process_time()-start), flush=True)
        else :
            self.stream_std = None


        self._threshold_std_stream = stream.copy()
        for tr in self._threshold_std_stream:
            tr.data[:] = np.nan

        self._threshold_mad_stream = stream.copy()
        for tr in self._threshold_mad_stream:
            tr.data[:] = np.nan

    def estimate_threshold_sigma(self, starttime=None, endtime=None, sigma_factor=3., window_length=7200) :
        """Estimate the transient event detection threshold values for each window_length-long segment using the median of the deviation times sigma_factor between starttime and endtime.

        Args:
            starttime (obspy.UTCDateTime) : the starttime, if not of UTCDateTime instance, will try to convert it
            endtime (obspy.UTCDateTime) : the endtime, if not of UTCDateTime instance, will try to convert it
            sigma_factor (float) : the factor that multiply the median of the deviation.
            window_length (int) : the length in second of each segment, default to 7200 (2h).
        
        Returns:
        --------

            ``dict:``
                - 'mad' - mad_threshold (dict) : the dict for every channel names as keys holding the value of the treshold for the mad estimator
                - 'std' - std_threshold (dict) : the dict for every channel names as keys holding the value of the treshold for the mad estimator
        """

        if self["mad"] is not None :
            stream_mad = self["mad"].copy()
            # stream_mad.trim(starttime, endtime)
            mad_threshold = {}
            stream_threshold_mad = stream_mad.copy()

            for  trace_mad, trace_threshold_mad in  zip(stream_mad, stream_threshold_mad) :
                for k, current_trace_mad in  enumerate(trace_mad.slide(window_length, window_length+1, include_partial_windows=True)) :
                    if k == 0 :
                        mad_threshold[current_trace_mad.stats.channel] = []
                    data_notnan = current_trace_mad.data#[~np.isnan(current_trace_mad.data)]
                    mad_threshold[current_trace_mad.stats.channel].append(np.asarray([np.median(data_notnan)*sigma_factor]*(current_trace_mad.stats.npts+1)) )
                mad_threshold[current_trace_mad.stats.channel] = np.hstack(mad_threshold[current_trace_mad.stats.channel])[:trace_mad.stats.npts]
                trace_threshold_mad.data[:] = mad_threshold[current_trace_mad.stats.channel].copy()

            self.threshold["mad"] = mad_threshold 
            self._threshold_mad_stream = stream_threshold_mad

        

        if self["std"] is not None :
            
            stream_std = self["std"].copy()
            # stream_std.trim(starttime, endtime)
            std_threshold = {}
            stream_threshold_std = stream_std.copy()
            
            for trace_std, trace_threshold_std in  zip(stream_std, stream_threshold_std) :
                testnum = 0
                for k, current_trace_std in  enumerate(trace_std.slide(window_length, window_length+1, include_partial_windows=True)) :
                    if k == 0 :
                        std_threshold[current_trace_std.stats.channel] = []
                    data_notnan = current_trace_std.data#[~np.isnan(current_trace_std.data)]
                    std_threshold[current_trace_std.stats.channel].append(np.asarray([np.median(data_notnan)*sigma_factor]*(current_trace_std.stats.npts+1)))
                    testnum += current_trace_std.stats.npts
                    
                std_threshold[current_trace_std.stats.channel] = np.hstack(std_threshold[current_trace_std.stats.channel])[:trace_std.stats.npts] ### need a last crop as hstack fill the last unwanted values
                trace_threshold_std.data[:] = std_threshold[current_trace_std.stats.channel].copy()
            self.threshold["std"] = std_threshold
            self._threshold_std_stream = stream_threshold_std
            
           
            
        



        return(self.threshold)

    
        
    def plot_deviations(self, starttime=None, endtime=None,  threshold=None, channels=None):
        """Plot the deviation instance.

        Args:
            starttime (obspy.UTCDateTime) : the starttime, if not of UTCDateTime instance, will try to convert it
            endtime (obspy.UTCDateTime) : the endtime, if not of UTCDateTime instance, will try to convert it
            threshold (dict) :  a replacement of the threshold attribute if provided. Should be the same format as the output of estimate_threhsold_sigma.
            channels (list) : a list containing all the channels in the stream.
        """
        if (self["mad"] is None) and (self["std"] is None) :
            print("WARNING: empty collection")
            return(plt.figure)
        
        if threshold is None:
            threshold=self.threshold
            threshold_std = self._threshold_std_stream
            threshold_mad = self._threshold_mad_stream

        if isinstance(starttime, datetime):
            starttime = UTCDateTime(starttime)
        if isinstance(endtime, datetime):
            endtime = UTCDateTime(endtime)

        mad, std = False, False 
        
        if self["mad"] is not None :
            mad=True
            stream_mad = self["mad"].copy().trim(starttime, endtime)
            origin_sdt, origin_edt = tl.detect_minimal_time_range(stream_mad)
            original_t = np.arange(stream_mad[0].stats.npts)*stream_mad[0].stats.delta
            original_dt = np.asarray( tl.datetime_vector(origin_sdt.datetime, original_t))
            try :
                mask_dt = original_dt >= starttime & original_dt <= endtime
            except TypeError :
                mask_dt = np.ones(len(original_dt), dtype=bool)
            
        if self["std"] is not None :
            std=True
            stream_std = self["std"].copy().trim(starttime, endtime)
            origin_sdt, origin_edt = tl.detect_minimal_time_range(stream_std)
            original_t = np.arange(stream_std[0].stats.npts)*stream_std[0].stats.delta
            original_dt = np.asarray( tl.datetime_vector(origin_sdt.datetime, original_t))
            try :
                mask_dt = original_dt >= starttime & original_dt <= endtime
            except TypeError :
                mask_dt = np.ones(len(original_dt), dtype=bool)
            origin_std, origin_edt = tl.detect_minimal_time_range(self["std"])
            

        if channels is None :
            figure, axes = plt.subplots(len(self.channels), figsize=[6.4, 3.8*len(self.channels)])
            channels = self.channels
        else :
            if isinstance(channels, str) :
                channels = [channels]

            figure, axes = plt.subplots(len(channels), figsize=[6.4, 3.8*len(channels)])
        if mad :
            figure.suptitle("Sliding deviation metrics for station {}, {}-{} window".format(stream_mad[0].stats.station, self.size, self.size_unit, ))
        if std : 
            figure.suptitle("Sliding deviation metrics for station {}, {}-{} window".format(stream_std[0].stats.station, self.size, self.size_unit, ))
    
        if len(channels) == 1 :
            axes = [axes]
        # print(axes)
        for k, channel in enumerate(channels) :
            
            if channel in channels :
                ################ PLOTING MAD
                if mad :
                    mad_trace = stream_mad.select(channel=channel)[0]
                    try :
                        unit = mad_trace.stats.response.response_stages[0].input_units
                    except AttributeError :
                        unit=''

                    t = np.arange(mad_trace.stats.npts)*mad_trace.stats.delta#np.linspace(0, mad_trace.stats.npts/mad_trace.stats.delta, mad_trace.stats.npts)
                    datetime_t =  tl.datetime_vector(mad_trace.stats.starttime.datetime, t)
                    xticks = np.arange(len(t))
                    new_xticks = np.linspace(0,len(t)-1, 8, dtype=int, endpoint=True)
                    new_t = t[new_xticks]
                    new_datetimes = [date_time.replace(microsecond=0) for date_time in  tl.datetime_vector(mad_trace.stats.starttime.datetime, new_t)]

                    if len(mad_trace.data) < 1e6 :
                        axes[k].plot(xticks, mad_trace.data, label="mad", color="tab:blue", linewidth=0.4)
                        
                    else :
                        axes[k].plot(xticks, mad_trace.data, label="mad", color="tab:blue", marker=".", linestyle="None")
                    axes[k].set_ylim(bottom = 0 ,top=np.nanpercentile(mad_trace.data, 98)*4)
                    axes[k].set_xticks(new_xticks, new_datetimes)

                ################ PLOTING STD
                if std :
                    std_trace = stream_std.select(channel=channel)[0]
                    try :
                        unit = std_trace.stats.response.response_stages[0].input_units
                    except AttributeError :
                        unit=''

                    t = np.arange(std_trace.stats.npts)*std_trace.stats.delta#np.linspace(0, std_trace.stats.npts/std_trace.stats.delta, std_trace.stats.npts)
                    datetime_t =  tl.datetime_vector(std_trace.stats.starttime.datetime, t)
                    xticks = np.arange(len(t))
                    new_xticks = np.linspace(0,len(t)-1, 8, dtype=int, endpoint=True)
                    new_t = t[new_xticks]
                    new_datetimes = [date_time.replace(microsecond=0) for date_time in  tl.datetime_vector(std_trace.stats.starttime.datetime, new_t)]

                    if len(std_trace.data) < 1e6 :
                        axes[k].plot(xticks, std_trace.data, label="std", color="tab:orange", linewidth=0.4)
                        
                    else :
                        axes[k].plot(xticks, std_trace.data, label="std", color="tab:orange", marker=".", linestyle="None")
                    axes[k].set_ylim(bottom = 0 , top=np.nanpercentile(std_trace.data, 98)*4)
                    axes[k].set_xticks(new_xticks, new_datetimes)
                      
                if  std :
                    try :
                        axes[k].plot(xticks, threshold_std.select(channel=channel)[0].trim(starttime, endtime),  color="tab:red", label="Event threshold std", linestyle="dashed")
                    except UnboundLocalError :
                        pass
                if  mad :
                    try :
                        axes[k].plot(xticks, threshold_mad.select(channel=channel)[0].trim(starttime, endtime), color="tab:red", label="Event threshold mad", linestyle="dashed")
                    except UnboundLocalError :
                        pass
                
                axes[k].set_xticks(new_xticks, new_datetimes)

                axes[k].set_title("{} channel deviation".format(channel))
                axes[k].set_xlabel("Date and time")
                axes[k].set_ylabel(unit)
                axes[k].grid()
                axes[k].legend()

                plt.setp(axes[k].get_xticklabels(), rotation=35)
        plt.tight_layout()
        return(figure)


    def __getitem__(self, ind) :
        out = {}
        out["mad"] = self.stream_mad
        out["std"] = self.stream_std
        return(out[ind])

    def detect_events_from_stream(self, std_or_mad="std"):
        """Generate and update the mask_events attribute using the threshold attribute.

        Args:
            std_or_mad (str) : the deviation to use for the detection, can be std or mad, default to std.

        """
        
        mask_event = {}
        for tr_dev in  self[std_or_mad]:
            mask_event[tr_dev.stats.channel] = tr_dev.data > self.threshold[std_or_mad][tr_dev.stats.channel]
        self.mask_events = mask_event

        return(self.mask_events)

    def count_events(self, min_hole_size=0):
        """Count the number of event according to the mask_events attribute.

        Args:
            min_hole_size (float) : the minimal duration (in second) that is allowed between two contiguous detected events.
                if two events are separated by a shorter time than min_hole_size, they are counted as one event.
        
        Returns: 
        --------

            count (int) : The number of detected events
        """
        from scipy.ndimage import binary_fill_holes


        mask = self.mask_events.copy()
        count = {}
        
        for key in mask.keys():
            if min_hole_size > 0 :
                min_hole_size = int(min_hole_size*self["std"][0].stats.sampling_rate)
                mask[key] = binary_fill_holes(mask[key], structure=np.ones(min_hole_size, dtype=bool))
            count[key] = tl.count_continuous_mask(mask[key])
        return(count)

def SlidingStdEstimation(x, Nw, mask_nodata=None ) :
    """Calculate the sliding std of the signal x .

    Parameters
    -------
        x (ndarray) : the signal to be analysed
        Nw (int) : the length of the window (sample)
        mask_nodata (array of bool) : array where True means invalid data and False means valid data, default=all data are valid
                                      must be the same lenght of x

    Output:
    -------
        - std (ndarray)
    """
    # print(x.shape, Nw)
    if mask_nodata is None :
        mask_nodata = np.zeros(len(x), dtype=bool)
    std = np.zeros(len(x))
    window = np.ones(Nw)
    x_padded = np.pad(x[~mask_nodata], Nw, mode="symmetric")
    sliding_mean = sig.fftconvolve(x_padded, window, mode="same")/Nw
    sliding_square = sig.fftconvolve(x_padded**2, window, mode="same")/Nw
    std[~mask_nodata] = np.sqrt(sliding_square - sliding_mean**2)[Nw:-Nw]
    # std = np.zeros(len(x))
    # for ind, sl in enumerate(sliding_window_iterator(Nw, len(x), Nw-1)) :
    #     std[ind] = np.std(x[sl])
    # std[-1] = std[-2]

    return(std)

def StreamSlidingStdEstimation(stream, Nw,  mask_nodata=None, size_unit="sample") :
    """Calculate the sliding std of the signal x.

    Args:
        stream (obspy.core.stream.Stream) : the stream to be analysed.
        Nw (int) : the length of the window in size_unit.
        mask_nodata (dict of boolean array) : take into account the mask_nodata custom attribute of stream.
        size_unit (str) : possible choices : "samples", "seconds", default=samples.

    Output:
    --------

        - std (dict) : the dict of ndarray which keys are channels name

    """
    window = np.ones(Nw)
    in_stream = stream.copy()
    std = {}
    Nw_original = Nw 

    mask=True
    if mask_nodata is None :
        mask=False
        mask_nodata = tl.mask_like(stream)
    elif not isinstance(mask_nodata, dict) :
        raise TypeError("You are giving a mask to StreamSlidingStdEstimation function that is not a dict")
    else :
        pass

    for trace in in_stream :
        if size_unit == "seconds" :
            Nw = int(Nw_original*trace.stats.sampling_rate)
        
        mask_tr = ~mask_nodata[trace.stats.channel]
        data = trace.data[mask_tr]

        from scipy.ndimage import uniform_filter
        sliding_mean = uniform_filter(data, Nw)
        sliding_square = uniform_filter(data**2, Nw)
        std[trace.stats.channel] = np.full(len(trace.data), np.nan)
        if np.sum(mask_tr) >0:
            std[trace.stats.channel][mask_tr] = np.sqrt(sliding_square - sliding_mean**2)
        else : 
            std[trace.stats.channel][:] = np.sqrt(sliding_square - sliding_mean**2)

    return(std)


def SlidingMADEstimation(x, size,  mask_nodata=None  ) :
    """Calculate the sliding median absolute deviation of the signal x.

    Args:
        x (numpy.ndarray) : the signal to be analysed
        size (int) : the length of the sliding window (sample)
        mask_nodata (numpy.array) : array where True means invalid data and False means valid data, default=all data are valid
                                      must be the same lenght of x
    Output:
    -------
        - std (ndarray) : the sliding std.

    """
    if mask_nodata is None :
        mask_nodata = np.zeros(len(x), dtype=bool)
    mad = np.full(len(x), np.nan)
    sliding_mad = median_filter(x, size=(size,), mode="reflect")
    mad[~mask_nodata] = median_filter(np.abs(x - sliding_mad), size=(size,), mode="reflect")*1.4826 # see [4]
    return(mad)

def StreamSlidingMADEstimation(stream, size, size_unit="sample", mask_nodata=None ) :
    """
    Calculate the sliding median absolute deviation of the signal x.

    Args:
        stream (obspy.core.stream.Stream) : the stream to be analysed.
        size (int/float) : the length of the sliding window in size_unit.
        size_unit (str) : can be either "sample" or "seconds"
        mask_nodata (dict of boolean array) : take into account the mask_nodata custom attribute of stream.

    Output:
    -------

        - mad (numpy.ndarray) : the dict of ndarray which keys are channels name
    """
    in_stream = stream.copy()
    mad = {}
    original_size = size

    mask=True
    if mask_nodata is None :
        mask=False
        mask_nodata = tl.mask_like(stream)
    elif not isinstance(mask_nodata, dict) :
        raise TypeError("You are giving a mask to StreamResampler function that is not a dict")
    else :
        pass

    for trace in in_stream :
        if size_unit == "seconds" :
            size = int(original_size*trace.stats.sampling_rate)

        mask_tr = ~mask_nodata[trace.stats.channel]
        data = trace.data[mask_tr]

        mad[trace.stats.channel] = np.full(len(trace.data), np.nan)
        sliding_mad = median_filter(data, size=size, mode="reflect")
        if np.sum(mask_tr) >0:
            mad[trace.stats.channel][mask_tr] = median_filter(np.abs(data - sliding_mad), size=size, mode="reflect") * 1.4826# see [4]
        else :
            mad[trace.stats.channel] = median_filter(np.abs(data - sliding_mad), size=size, mode="reflect") * 1.4826# see [4]
        
    return(mad)


def FilterStream(stream, N, fn, btype='lowpass', nmirror=None, mask_nodata=None, verbose=True) :
    '''Filter forward and backward the traces of the stream object with a butterworth filter.
    The data can be mirrored to avoid stability issues.

    Args:
        stream (obspy.core.stream.Stream) : the stream object containing the traces to filter.
        N (int) : the order of the filter
        fn (array) : the cutting frequencies (only one for lowpass or hypass)
        btype (str) : {‘lowpass’, ‘highpass’, ‘bandpass’, ‘bandstop’}
        nmirror (int) : how many sample are to be mirrored to avoid stabilization effects, default (None) = 1/8 the length of each trace
        mask_nodata (dict of boolean ndarray) : The keys of the dict must be consistent to the stream channels.

    Output:
    -------
        - stream (obspy.core.stream.Stream) : the filtered stream.

    '''
    from scipy.ndimage import binary_dilation
    mask=True
    if mask_nodata is None :
        mask=False
        mask_nodata = tl.mask_like(stream)
    elif not isinstance(mask_nodata, dict) :
        raise TypeError("You are giving a mask to StreamResampler function that is not a dict")
    else :
        pass

    
    maskedge = mask_nodata.copy()
    filt_stream = stream.copy()
    ### fitler every traces
    for tr in filt_stream :
        if nmirror is None :
            nmirror = tr.stats.npts//8 ### make a minimum amount stabilization padding
        if verbose :
            print("Filtering channel {} containing {} samples with a {}-order {} butterworth filter given at {}Hz ".format(tr.stats.channel, tr.stats.npts, N, btype, fn) )

        fs = tr.stats.sampling_rate
        sos = sig.butter(N, fn ,fs=fs, btype=btype, output="sos")
        data = tr.data.copy()[~mask_nodata[tr.stats.channel]]

        ### mirroring the data
        if nmirror != 0 :
            reflected_data = np.pad(data, nmirror, mode="symmetric")
        else :
            reflected_data = data
        new_npts = len(reflected_data)

        reflected_filtered_data = sig.sosfiltfilt(sos, reflected_data)
        filtered_data = reflected_filtered_data[nmirror:new_npts-nmirror]
        tr.data[~mask_nodata[tr.stats.channel]] =  filtered_data 
        if mask :
            out_data = np.zeros(tr.stats.npts)
            out_data[~mask_nodata[tr.stats.channel]] = filtered_data#[nmirror: new_npts - nmirror]
            ### make a dilation of 2 samples 
            dilated_masknodata = binary_dilation(mask_nodata[tr.stats.channel], structure=np.ones(3, dtype=int), iterations=1)
            ### extract the edges of the mask to interpolate properly 
            maskedge[tr.stats.channel] = dilated_masknodata ^ mask_nodata[tr.stats.channel]


            target_t = np.linspace(0,tr.stats.npts*tr.stats.sampling_rate, tr.stats.npts)
            
            source_t = target_t[maskedge[tr.stats.channel]]
            edge_data = out_data[maskedge[tr.stats.channel]]

            
            try :
                raise ValueError("")
                tr_interp = interp1d(source_t, edge_data, kind="linear", fill_value="extrapolate")
                edge_data_interped = tr_interp(target_t)
                tr.data[mask_nodata[tr.stats.channel]] = edge_data_interped[mask_nodata[tr.stats.channel]]
            except Exception :
                tr.data[mask_nodata[tr.stats.channel]] = 0.

            tr.data[~mask_nodata[tr.stats.channel]] = out_data[~mask_nodata[tr.stats.channel]]

            


        else :
            tr.data = filtered_data#[nmirror: new_npts - nmirror]

    return(filt_stream)

def resample_mask(stream, mask, new_fs): 
    """Resample a mask masking stream to new_fs, useful to resample along a stream.

    Args:
        stream (obspy.core.stream.Stream) : a stream from which the mask was generated.
        mask (dict) : the dict containing the numpy.ndarray boolean masks for each trace in stream.
        new_fs (float) : the target frequency sample.
    
    Returns:
    --------

        - mask_nodata (dict) : the dict containing the resampled numpy.ndarray boolean masks.
    """
    mask_nodata = {}
    for tr in stream :
        source_t = np.arange(0, tr.stats.npts*tr.stats.delta, tr.stats.delta)
        target_t = np.arange(0, tr.stats.npts*tr.stats.delta, 1/new_fs)
        interpolator = interp1d(source_t, mask[tr.stats.channel], kind='nearest', fill_value='extrapolate')
        mask_nodata[tr.stats.channel] = interpolator(target_t).astype(bool)
    return(mask_nodata)

def StreamResampler(stream, new_fs, mask_nodata=None, verbose=True, antialiased_order=3) :
    """
    Resample the stream to the new_fs frequency using a linear interpolation.
    The stream will be low-pass filtered using a new_fs/2.5 cutoff frequency prior
    to the resampling.
    If any mask_nodata is given, the filtering and interpolation will be processed only on the 
    healthy data, and the signals will be rebuilt with 0 in places of the corrupted data.

    Args:
        stream (obspy.core.stream.Stream) : The stream to resample.
        new_fs (float) : the target sampling frequency
        mask_nodata (dict of numpy.ndarray) : the original mask_nodata, will be resampled as well.
                                        The keys of the dict must be consistent to the stream channel names.
        verbose (bool) : show progress
        antialiased_order (int) : the order of the antialiasing Butterworth filter (applied forward and backward), default to 3.

    Returns:
    --------

        - stream (obspy.core.stream.Stream) : the resampled stream.
    """
    from scipy.ndimage import binary_dilation
    mask=True
    if mask_nodata is None :
        mask=False
        mask_nodata = tl.mask_like(stream)
    elif not isinstance(mask_nodata, dict) :
        raise TypeError("You are giving a mask to StreamResampler function that is not a dict")
    else :
        pass


    if verbose :
        print("-----------------------\nResampling stream at {}Hz".format(new_fs))
    stream_int = stream.copy()
    
    ### anti-aliasing filter with zerophase
    stream_int = FilterStream(stream_int, antialiased_order, new_fs/2.5,"lowpass", mask_nodata=mask_nodata, verbose=verbose)

    
    masknodata_resampled = resample_mask(stream_int, mask_nodata, new_fs)
    maskedge_resampled = masknodata_resampled.copy()


    if verbose :
        print(stream_int, flush=True)
    stream_out = stream_int.copy()
    stream_out.interpolate(new_fs, method="linear")
    if mask :
        if verbose :
            print("--> interpolating the masked data", flush=True)
        for tr in stream_out :
            if masknodata_resampled[tr.stats.channel].sum() > 0 :
                if verbose :
                    print("----> dilating the mask from the {} channel".format(tr.stats.channel), flush=True)
                ### make a dilation of 2 samples 
                current_masknodata_resampled = binary_dilation(masknodata_resampled[tr.stats.channel], structure=np.ones(3, dtype=int), iterations=1)
                ### extract the edges of the mask to interpolate properly 
                maskedge_resampled[tr.stats.channel] = current_masknodata_resampled ^ masknodata_resampled[tr.stats.channel]

                if verbose :
                    print("----> resampling the {} channel".format(tr.stats.channel), flush=True)

                source_t = np.linspace(0,tr.stats.npts*tr.stats.sampling_rate, tr.stats.npts)
                target_t = np.arange(0, tr.stats.npts*tr.stats.sampling_rate, new_fs)

                source_t = source_t[maskedge_resampled[tr.stats.channel]]
                edge_data = tr.data[maskedge_resampled[tr.stats.channel]]

                tr_interp = interp1d(source_t, edge_data, kind="linear", fill_value="extrapolate")
                edge_data_interped = tr_interp(target_t)
                tr.data[masknodata_resampled[tr.stats.channel]] = 0.#edge_data_interped[masknodata_resampled[tr.stats.channel]] 

                del tr_interp
    if verbose :
        print("Resampled stream :", flush=True )
        print(stream_out, flush=True)

    return(stream_out, masknodata_resampled)
