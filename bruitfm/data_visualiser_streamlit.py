""" 
Graphical user interface built with streamlit (web-based) to quickly and easely visualise a .mseed file, with integrated preprocessing tool.
To launch it, run ``streamlit run data_visualiser_streamlit.py``. It will open a window in your default web browser.
"""



import os
import sys
import numpy as np
import obspy


import platform as pl
import datetime as dt
from obspy import UTCDateTime
try :
    from bruitfm.utils import *
    from bruitfm.preprocessing import StreamPreprocessor 
    from bruitfm.signal_utils import *
except ImportError :
    from utils import *
    from preprocessing import StreamPreprocessor 
    from signal_utils import *
# import signal_utils as sptl
# import preprocessing as pre

import matplotlib.pyplot as plt 


if __name__=="__main__":

    import streamlit as st


    plt.rc('font', family='stix')
    plt.rcParams.update({'font.size': 9})


    st.set_page_config(layout="wide")
    st.title("OBS DATA VISUALISER")
    #nb_files = st.sidebar.slider("files to analyse", min_value=1, max_value=4, value=1)

    input_file = st.text_input("Please enter the path of the file containing the data:")
    input_inventory = st.text_input("Please enter the path of the xml file containing the instrument response:", help="Please, note that this does not mean that the instrument response will be removed, this step is just helpful to get the input data unit for example.")


    st.sidebar.markdown("### Data resampling")
    data_resampling = st.sidebar.checkbox("Downsample data ?", help="Will fasten up the vizualizer", value=True)
    if data_resampling :
        new_fs = st.sidebar.number_input("Resampling factor:", value=1.0)
    else :
        new_fs=np.inf

    savefig = st.sidebar.checkbox("Save figures ?")
    if savefig :
        figdir = st.sidebar.text_input("saving directory")

    st.sidebar.markdown("### Spectral analysis parameter definition")
    spectral_analysis= st.sidebar.checkbox("conduct a spectral analysis ?")
    if spectral_analysis :
        Nws = st.sidebar.number_input("Length of the design window in second", value=3600)
        Noverlaps = st.sidebar.number_input("Length of the overlaps between windows in second", value=1800)
        taperfunc = st.sidebar.selectbox("Taper function to use for the design window", options=available_windowtype.keys(), index=19)
        if available_windowtype[taperfunc] :
            taper_params = st.sidebar.number_input("Enter the taper parameter", value=4.)
            windowtype_design = (taperfunc, taper_params)
        else :
            windowtype_design = taperfunc

    if input_file != "" :
        stream = obspy.read(input_file)
    else :
        st.stop()


    with st.form(key="my_form"):

        ### checking min datetime and max datetime
        for ind, tr in enumerate(stream) :
            if ind == 0 :
                mindate=tr.stats.starttime.datetime
                maxdate=tr.stats.endtime.datetime
            if mindate > tr.stats.starttime.datetime :
                mindate = tr.stats.starttime.datetime
            if maxdate < tr.stats.endtime.datetime :
                maxdate = tr.stats.endtime.datetime

        st.markdown("Choose a date range for the signal to be **analysed**:")
        col_date_min, col_time_min, col_date_max, col_time_max = st.columns([1,1,1,1])
        startdate_trim = col_date_min.date_input("Start date:", value=mindate,  min_value=mindate, max_value=maxdate)
        enddate_trim = col_date_max.date_input("End date:", value=maxdate,  min_value=mindate, max_value=maxdate)
        starttime_trim = col_time_min.time_input("Start time:", value=mindate  )
        endtime_trim = col_time_max.time_input("End time:", value=maxdate)

        st.markdown("Choose a date range for the signal to be **displayed**:")
        startdate_disp = col_date_min.date_input("Start date:", value=startdate_trim,  min_value=startdate_trim, max_value=enddate_trim, key="666")
        enddate_disp = col_date_max.date_input("End date:", value=enddate_trim,  min_value=startdate_trim, max_value=enddate_trim, key="667")
        starttime_disp = col_time_min.time_input("Start time:", value=starttime_trim , key="668" )
        endtime_disp = col_time_max.time_input("End time:", value=endtime_trim, key="669")
        # (startdate_trim, enddate_trim) = st.slider("Time range for the display of time series:", value=(mindate, maxdate), min_value=mindate, max_value=maxdate, step=dt.timedelta(minutes=30))

        startdate_trim = datetime(startdate_trim.year, startdate_trim.month, startdate_trim.day, starttime_trim.hour, starttime_trim.minute, starttime_trim.second)
        enddate_trim = datetime(enddate_trim.year, enddate_trim.month, enddate_trim.day, endtime_trim.hour, endtime_trim.minute, endtime_trim.second)
        
        startdate_disp = datetime(startdate_disp.year, startdate_disp.month, startdate_disp.day, starttime_disp.hour, starttime_disp.minute, starttime_disp.second)
        enddate_disp = datetime(enddate_disp.year, enddate_disp.month, enddate_disp.day, endtime_disp.hour, endtime_disp.minute, endtime_disp.second)
        elapsed_day = (UTCDateTime(enddate_trim) - UTCDateTime(startdate_trim))/60/60/24
        ### checking minimal frequency res
        min_freq = 0
        max_freq = np.inf
        for tr in stream :
            temp_min_freq = tr.stats.sampling_rate/tr.stats.npts
            temp_max_freq = tr.stats.sampling_rate/2
            if temp_min_freq > min_freq :
                min_freq=temp_min_freq # getting the highest minimal frequency 
            if temp_max_freq < max_freq :
                max_freq = temp_max_freq

        filtering = st.checkbox("Filter the data ?")
        st.markdown("Choose a frequency range for the signal to be bandpass-filtered (default to IG wave frequency range)")
        col_filter_min, col_filter_max = st.columns([1,1])
        filter_min_freq = col_filter_min.number_input("Min corner frequency:", value=1e-3, step=1e-4, format="%f", min_value=min_freq, max_value=max_freq)
        filter_max_freq = col_filter_max.number_input("Max corner frequency:", value=5e-2, step=1e-4, format="%f", min_value=min_freq, max_value=max_freq)
        # (filter_min_freq, filter_max_freq) = st.slider("Frequency range, default= no filtering", value=(min_freq, max_freq), min_value=min_freq, max_value=max_freq, step=min_freq)

        detect_anomalies = st.checkbox("Detect anomalies in the RAW data ?", help="Detect anomalies like zeros or saturated values, needs the RAW data", value=True)
        correct_response = st.checkbox("Does the data need to be corrected from the instrument response ?", help="Please, note that it can take some times to process, espacially for broadband channels longer than an hour or shortband channels")
        event_cols1, event_cols2 = st.columns([1,1])
        detect_events = event_cols1.checkbox("Detect transcient events in the stream ?", help="Please, note that it can take some times to process, espacially for broadband channels longer than an hour or shortband channels")
        
        event_cols2.markdown("\n")
        event_cols2.markdown("\n")
        event_cols2.number_input("Sigma factor", value=3.0, help="The factor which will multiply the median of the deviation values to get the enveloppe (threshold)")
        # (startdate_noevent, enddate_noevent) = event_cols2.slider("Please, give a time range during no events occured :", value=(startdate_trim, enddate_trim), min_value=startdate_trim, max_value=enddate_trim, step=dt.timedelta(seconds=30))
        nb_samples_std = event_cols1.number_input("Number of samples to compute the sliding std", value=225, help="This is the size of the sliding window used to detect the transcient event")

        event_cols12, event_cols22 = st.columns([1,1])
        compute_deviations = event_cols12.checkbox("Compute sliding deviation statistics ?", value=False)
        nb_samples_dev = event_cols12.number_input("Number of samples to compute the sliding deviations", value=225, help="This is the size of the sliding window")



        st.markdown("Stationarity assessment")
        statio_cols1, statio_cols2, statio_cols3 = st.columns([1,1,1])
        compute_adf = statio_cols1.checkbox("Compute ADF test", value=False)
        compute_kpss = statio_cols2.checkbox("Compute KPSS test", value=False)
        compute_rur = statio_cols3.checkbox("Compute RUR test", value=False)

        submit_button = st.form_submit_button("Submit")


    if st.button("Display") :
        pass
    else :
        st.stop()

    inventory_present = False
    if input_inventory != "" :
        inventory = obspy.read_inventory(input_inventory)
        inventory_present = True
        stream.attach_response(inventory)
    else :
        inventory = None



    channel_names = []

    with st.expander("Basic stream informations:", expanded=True) :
        for tr in stream :
            st.write(tr)
            channel_names.append(tr.stats.channel)


    stream = stream.trim(UTCDateTime(startdate_trim), UTCDateTime(enddate_trim))

    preprocessor = StreamPreprocessor(stream, inventory, starttime=startdate_trim, endtime=enddate_trim)
    preprocessor.stream = update_stream_unit(preprocessor.stream , "M/S", "M/S²")

    if detect_anomalies :
        with st.spinner("Removing anomalies"):
            preprocessor.remove_anomalies()

    if data_resampling :
        with st.spinner("Resampling data"):
            preprocessor.resample(resampling_frequency=new_fs) 

    if inventory_present and correct_response :
        with st.spinner("Removing instrument response, it might take a minute depending on how huge the data are") :
            preprocessor.remove_instrument_response()

    if filtering :
        with st.spinner("Filtering the stream"): 
            preprocessor.filter(10, [filter_min_freq, filter_max_freq], btype='bandpass', verbose=True)
    



    if detect_events :
        with st.spinner("Detecting transcient events") :
            preprocessor.detect_events(window_length=nb_samples_std)


    if compute_deviations :
        with st.spinner("Computing sliding deviations") :
            deviations = StreamTimeSeriesDeviation(preprocessor.stream, nb_samples_std, "sample")
            if  spectral_analysis :
                deviations_Nw = StreamTimeSeriesDeviation(preprocessor.stream,  Nws, "second")
            else :
                deviations_Nw = None
    else :
        deviations_Nw = None


    stream_original = preprocessor.original_stream
    stream = preprocessor.stream



    if spectral_analysis :
        Nw = int(tr.stats.sampling_rate * Nws)
        Noverlap = int(tr.stats.sampling_rate * Noverlaps)
        if (compute_adf or compute_kpss or compute_rur) :
            stationary_results = StationnarityAssessment(stream)
            if compute_adf :
                with st.spinner("Computing ADF stationnarity test") :
                    results_adf, mask_stationnarity_adf = stationary_results.compute("adf", Nws, Noverlaps)
            if compute_kpss :
                with st.spinner("Computing KPSS stationnarity test") :    
                    results_kpss, mask_stationnarity_kpss = stationary_results.compute("kpss",Nws, Noverlaps)
            if compute_rur :
                with st.spinner("Computing RUR stationnarity test") :    
                    results_rur, mask_stationnarity_rur = stationary_results.compute("rur",Nws, Noverlaps)


    tabs = st.tabs(channel_names)
    for tab, tr in zip(tabs, stream) :
        if spectral_analysis :
            
            try :
                psd = psd(tr, windowtype_design, Nws, Noverlaps, return_SpectralStats=True)[1]
                current_fig_PSD = psd.plot(title="Power spectral density for {} channel".format(tr.stats.channel), plot_all=True)
                current_fig_spectrogram = psd.spectrogram()
                validdataflag = True
            except NoValidDataError :
                validdataflag = False
            if (compute_adf or compute_kpss or compute_rur) :
                stationary_results.count_non_stationary_window()
                if compute_adf :
                    current_fig_adf = stationary_results.plot_results("adf", channels=tr.stats.channel, StreamTimeSeriesDeviation_instance=deviations_Nw)
                if compute_kpss :
                    current_fig_kpss = stationary_results.plot_results("kpss",channels=tr.stats.channel, StreamTimeSeriesDeviation_instance=deviations_Nw)
                if compute_rur :
                    current_fig_rur = stationary_results.plot_results("rur",channels=tr.stats.channel, StreamTimeSeriesDeviation_instance=deviations_Nw)
        # current_fig_trace = tr.trim(starttime=UTCDateTime(startdate_trim), endtime=UTCDateTime(enddate_trim)).plot(handle=True, show=False, color='black',  draw=False)
        current_fig_trace = preprocessor.plot_stream(startdate=startdate_disp, enddate=enddate_disp, channels=tr.stats.channel)
        # current_fig_trace.savefig("/home/srebeyro/Documents/rapports techniques/figures/fig_trace_transient_example.pdf")


        try :
            unit = tr.stats.response.response_stages[0].input_units
        except AttributeError :
            unit='unkown unit'


        ### plotting rms fig
        if detect_events :
            fig_dev = preprocessor.deviations.plot_deviations( channels=[tr.stats.channel], starttime=startdate_disp, endtime=enddate_disp)
            # fig_dev.savefig("/home/srebeyro/Documents/rapports techniques/figures/fig_dev_transient_example.pdf")

        if compute_deviations : 
            fig_dev2 = deviations.plot_deviations(channels=[tr.stats.channel], starttime=startdate_disp, endtime=enddate_disp)   
            if spectral_analysis :
                fig_dev_Nw = deviations_Nw.plot_deviations(threshold=None, channels=[tr.stats.channel], starttime=startdate_disp, endtime=enddate_disp)

        exp = tab.expander("Time serie", expanded=False)
        exp.pyplot(current_fig_trace)
        if spectral_analysis :
            exp = tab.expander("Power spectral density", expanded=False)
            if validdataflag :
                exp.pyplot(current_fig_PSD)
                exp = tab.expander("Spectrogram", expanded=False)
                exp.pyplot(current_fig_spectrogram)
            else :
                exp.warning("There are not enough valid data for spectral analysis. It can be due to a bad conditionning of data (too much anomalies...) or the window length may be too long.")
                exp = tab.expander("Spectrogram", expanded=False)
                exp.warning("There are not enough valid data for spectral analysis. It can be due to a bad conditionning of data (too much anomalies...) or the window length may be too long.")
            exp = tab.expander("Stationnarity tests", expanded=False)
            if (compute_adf or compute_kpss or compute_rur) :
                exp.markdown("### {:.2f}% of the analysed windows are considered stationnary".format(stationary_results.counts[tr.stats.channel]))
                if compute_adf :
                    exp.pyplot(current_fig_adf)
                if compute_kpss :
                    exp.pyplot(current_fig_kpss)
                if compute_rur :
                    exp.pyplot(current_fig_rur)
        exp = tab.expander("Sliding variance", expanded=False)
        exp.markdown("# Standard deviation over time")
        if detect_events :
            exp.pyplot(fig_dev)
        if compute_deviations :
            exp.pyplot(fig_dev2)       
            if spectral_analysis :
                exp.pyplot(fig_dev_Nw)
