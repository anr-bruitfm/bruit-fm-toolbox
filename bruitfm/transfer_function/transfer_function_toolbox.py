#! /usr/bin/bash
### Author  :  SIMON REBEYROL
### Company :  IFREMER, UMR GEOOCEAN


'''Contains classes to remove the reference signal component from the output signal using transfer functions [6].

References :
    1. https://docs.scipy.org/doc/scipy/reference/generated/scipy.signal.get_window.html#scipy.signal.get_window
    2. CRAWFORD, Wayne C. & WEBB, Spahr C.. "Identifying and Removing Tilt Noise from Low-Frequency (&lt\mathsemicolon{}{0}{.}1 Hz) Seafloor Vertical Seismic Data". Bulletin of the Seismological Society of America. 2000, vol 90, num. 4, p. 952–963. DOI: 10.1785/0119990121
    3. BELL, Samuel W., FORSYTH, Donald W. & RUAN, Youyi. "Removing Noise from the Vertical Component Records of Ocean-Bottom Seismometers: Results from Year One of the Cascadia Initiative". Bulletin of the Seismological Society of America. 2015, vol 105, num. 1, p. 300–313. DOI: 10.1785/0120140054
    4. WIDROW, Bernard & WALACH, Eugene. Adaptive Signal Processing for Adaptive Control. San Francisco, CA, USA: Elsevier BV. 1983.p. 7–12. ISBN: 9. DOI: 10.1016/s1474-6670(17)62348-6
    5. THOMPSON, Rory O. R. Y.. "Coherence Significance Levels". Journal of the Atmospheric Sciences. 1979, vol 36, num. 10, p. 2020–2021. DOI: 10.1175/1520-0469(1979)036<2020:csl>2.0.co;2
    6. BENDAT, Julius S. y PIERSOL, Allan G.. "Chapter 6: Single-Input/Output Relationships". Random data: Analysis and Measurement Procedures. Fourth ed. New York: Wiley-Interscience. 2011.p. 407. 


'''




import sys
import os
os.environ["OMP_NUM_THREADS"] = "1" # export OMP_NUM_THREADS=1
os.environ["OPENBLAS_NUM_THREADS"] = "1" # export OPENBLAS_NUM_THREADS=1
os.environ["MKL_NUM_THREADS"] = "1" # export MKL_NUM_THREADS=1
os.environ["VECLIB_MAXIMUM_THREADS"] = "1" # export VECLIB_MAXIMUM_THREADS=1
os.environ["NUMEXPR_NUM_THREADS"] = "1" # export NUMEXPR_NUM_THREADS=1

import numpy as np
import obspy.core as obs
import matplotlib.pyplot as plt
import scipy as sc
from scipy.fft import fft, ifft, fftfreq, fft, ifft, rfftfreq, fftshift, rfft, irfft
from scipy.stats import median_abs_deviation as mad

import time

try :
    from ..utils import *
    # import signal_utils as sptl
    from ..signal_utils import *
    from ..preprocessing import taper_stream
except ImportError:
    from utils import *
    # import signal_utils as sptl
    from signal_utils import *
    from preprocessing import taper_stream   


class OBSWienerDataCleanerResults :
    """A convenient class to aggregate results from a spectral wiener filter correction process.

    Args:
        
        TransferFunctionFrequencyResponse  (signal_utils.SpectralWienerFilter) :  the transfer function frequency response that have been used to correct the output channel signal.
        Coherence_before  (signal_utils.Coherence) : the coherence between the reference and output channel signal before correction.
        Coherence_after  (signal_utils.Coherence) : the coherence between the reference and output channel signal after correction.
        corrected_stream  (obspy.core.stream.Stream) : the stream whose output channel trace has been corrected from the reference channel trace component.
        error_flag  (bool) : tell if there have been an error during the correction process.
        out_chan  (str) :  the output channel name.
        ref_chan  (str) : the reference channel name.
        startdate  (datetime.datetime) or (obspy.UTCDateTime) :  the date from wich the stream has been processed.
        enddate  (datetime.datetime) or (obspy.UTCDateTime) :  the date up to wich the stream has been processed.
        stationarity : (signal_utils.StationnarityAssessment) : the results of the stationarity check.
    
    -----------
    Attributes:
    -----------

    Attributes
    ----------

        TransferFunctionFrequencyResponse  (signal_utils.SpectralWienerFilter) : the transfer function frequency response that have been used to correct the output channel signal.
        Coherence_before  (signal_utils.Coherence) : the coherence between the reference and output channel signal before correction.
        Coherence_after  (signal_utils.Coherence) : the coherence between the reference and output channel signal after correction.
        corrected_stream  (obspy.core.stream.Stream) : the stream whose output channel trace has been corrected from the reference channel trace component.
        error_flag  (bool) : tell if there have been an error during the correction process.
        out_chan  (str) : the output channel name.
        ref_chan  (str) : the reference channel name.
        startdate  (datetime.datetime) or (obspy.UTCDateTime) : the date from wich the stream has been processed.
        enddate  (datetime.datetime) or (obspy.UTCDateTime) : the date up to wich the stream has been processed.
        stationarity  (signal_utils.StationnarityAssessment) : the results of the stationarity check.

    -------- 
    Methods:
    --------
    
    Methods
    -------

        add_TFFR(TFFR) :  Attach a transfer function frequency response
        add_Coherence_before(Coh) :  Attach the coherence before correction.
        add_Coherence_after(Coh) : Attach the coherence after correction.
        compress() :  Remove unnecessary subinstances (stationnarity, corrected_stream, coherences) from this instance.
        savepickle(out_path) : save the current instance a pickle file.
        savefig(directory, additionnal_figs=[], additionnal_fig_names=[]) :  Save the results as figures in svg, pdf and png file format.
    """
    def __init__(self, TransferFunctionFrequencyResponse=None, Coherence_before=None,  Coherence_after=None, corrected_stream=None, error_flag=False,
                 output_channel="---", reference_channel="---",startdate=None, enddate=None, stationnarity=None) :
        """Constructor of class OBSWienerDataCleanerResults.

        """
        self.TransferFunctionFrequencyResponse = TransferFunctionFrequencyResponse.copy()
        self.Coherence_before = Coherence_before
        self.Coherence_after = Coherence_after
        self.corrected_stream = corrected_stream
        self.error_flag = error_flag
        self.out_chan = output_channel 
        self.ref_chan = reference_channel
        self.startdate = startdate
        self.enddate = enddate
        self.stationnarity = stationnarity


    def add_TFFR(self, TFFR) :
        """Attach a transfer function frequency response.

        Args:
            TransferFunctionFrequencyResponse  (signal_utils.SpectralWienerFilter) : the transfer function frequency response that have been used to correct the output channel signal.
        """ 
        self.TransferFunctionFrequencyResponse = TFFR

    def add_Coherence_before(self, Coh) :
        """Attach the coherence before correction.

        Args:
            Coh  (signal_utils.Coherence) : the coherence between the reference and output channel signal before correction.
        """ 
        self.Coherence_before = Coh

    def add_Coherence_after(self, Coh) :
        """Attach the coherence after correction.
        Args:
            Coh  (signal_utils.Coherence) : the coherence between the reference and output channel signal after correction.
        """ 
        self.Coherence_after = Coh

    def compress(self) :
        "Remove unnecessary subinstances (stationnarity, corrected_stream, coherences) from this instance."
        del self.stationnarity, self.corrected_stream
        self.stationnarity, self.corrected_stream = None, None 
        self.Coherence_after.compress()
        self.Coherence_before.compress()

    def savepickle(self, out_path):
        """Save the current instance as a pickle file."""
        import pickle as pickle 
        with open(out_path, "wb") as outfile :
            pickle.dump(self, outfile, pickle.HIGHEST_PROTOCOL)

    def savefig(self, directory, additionnal_figs=[], additionnal_fig_names=[]) :
        """Save the results as figures in svg, pdf and png file format.

        Args:
            directory  (str) : the path of the directory in which the figures will be saved.
            additionnal_figs  (list of matplotlib.Figure) or (tuple of matplotlib.Figure) : a list of additionnal figure that will be saved alongside the regular ones.
            additionnal_fig_names  (list of str) or (tuple of str) : the file name (without extension) of the additionnal figures.

        """
        import platform as pl 
        if pl.system() in 'Linux' :
            path_separator = '/'
        elif pl.system() in 'Darwin' : # for MacOS
            path_separator = '/'
        elif pl.system() in 'Windows' :
            path_separator = '\\'
        else :
            raise OSError("Unsuported OS, the suported ones are Linux, MacOS and Windows, you are on : " +  pl.system())

        if (self.startdate is not None) & (self.enddate is not None) :
            elapsed_time = str((self.enddate-self.startdate)/24/60/60 ).replace(".", ",")
        else :
            elapsed_time = "Unknown"


        if directory[-1] != path_separator :
            directory = directory + path_separator

        pdf_dir =  directory + "pdf" + path_separator
        png_dir =  directory + "png" + path_separator
        svg_dir =  directory + "svg" + path_separator

        os.makedirs(pdf_dir, exist_ok=True)
        os.makedirs(png_dir, exist_ok=True)
        os.makedirs(svg_dir, exist_ok=True)

        figTF_masked = self.TransferFunctionFrequencyResponse.plot(freqmask=True, title="Estimation over {} days".format(elapsed_time))
        figTF = self.TransferFunctionFrequencyResponse.plot(freqmask=False, title="Estimation over {} days".format(elapsed_time))
        figCohb = self.Coherence_before.plot(min_coherence=self.TransferFunctionFrequencyResponse.min_coherence, title="Estimation over {} days".format(elapsed_time))
        figCoha = self.Coherence_after.plot(min_coherence=self.TransferFunctionFrequencyResponse.min_coherence, title="Estimation over {} days".format(elapsed_time))
        figPsd_out_before = self.Coherence_before.psd1.plot(title="Estimation over {} days".format(elapsed_time))
        figPsd_out_after = self.Coherence_after.psd1.plot(title="Estimation over {} days".format(elapsed_time))
        figPsd_ref = self.Coherence_before.psd2.plot(title="Estimation over {} days".format(elapsed_time))
        figSpectrogram_out = self.Coherence_before.psd1.spectrogram()
        figSpectrogram_ref = self.Coherence_before.psd2.spectrogram()
        if self.stationnarity is not None :
            fig_ADF = self.stationnarity.plot_results("adf")
            fig_stream_masked_adf = self.stationnarity.plot_masked_stream("adf")
            fig_kpss = self.stationnarity.plot_results("kpss")
            fig_stream_masked_kpss = self.stationnarity.plot_masked_stream("kpss")
            fig_rur = self.stationnarity.plot_results("rur")
            fig_stream_masked_rur = self.stationnarity.plot_masked_stream("rur")

        figTF.savefig(svg_dir + "TransferFunctionFrequencyResponse-{}_vs_{}-station_{}_{}_days.svg".format(self.out_chan, self.ref_chan, self.corrected_stream[0].stats.station,elapsed_time))
        figTF_masked.savefig(svg_dir + "TransferFunctionFrequencyResponse-{}_vs_{}-station_{}_masked.svg".format(self.out_chan, self.ref_chan, self.corrected_stream[0].stats.station,elapsed_time))
        figCohb.savefig(svg_dir + "Coherence_before_correction-{}_vs_{}-station_{}_{}_days.svg".format(self.out_chan, self.ref_chan, self.corrected_stream[0].stats.station,elapsed_time))
        figCoha.savefig(svg_dir + "Coherence_after_correction-{}_vs_{}-station_{}_{}_days.svg".format(self.out_chan, self.ref_chan, self.corrected_stream[0].stats.station,elapsed_time))
        figPsd_out_before.savefig(svg_dir + "PSD_before_correction-channel-{}_station-{}_{}_days.svg".format(self.out_chan, self.corrected_stream[0].stats.station,elapsed_time))
        figPsd_out_after.savefig(svg_dir + "PSD_after_correction-channel-{}_station-{}_{}_days.svg".format(self.out_chan, self.corrected_stream[0].stats.station,elapsed_time))
        figPsd_ref.savefig(svg_dir + "PSD_channel-{}_station-{}.svg".format(self.ref_chan, self.corrected_stream[0].stats.station,elapsed_time))
        figSpectrogram_out.savefig(svg_dir + "SPECTROGRAM_channel-{}_station-{}.svg".format(self.ref_chan, self.corrected_stream[0].stats.station,elapsed_time))
        figSpectrogram_ref.savefig(svg_dir + "SPECTROGRAM_channel-{}_station-{}.svg".format(self.ref_chan, self.corrected_stream[0].stats.station,elapsed_time))
        if self.stationnarity is not None :
            fig_ADF.savefig(svg_dir + "ADF_channel-{}_station-{}.svg".format(self.ref_chan, self.corrected_stream[0].stats.station,elapsed_time))
            fig_stream_masked_adf.savefig(svg_dir + "ADF_stream_masked_channel-{}_station-{}.svg".format(self.ref_chan, self.corrected_stream[0].stats.station,elapsed_time))
            fig_kpss.savefig(svg_dir + "KPSS-{}_station-{}.svg".format(self.ref_chan, self.corrected_stream[0].stats.station,elapsed_time))
            fig_stream_masked_kpss.savefig(svg_dir + "KPSS_stream_masked_channel-{}_station-{}.svg".format(self.ref_chan, self.corrected_stream[0].stats.station,elapsed_time))
            fig_rur.savefig(svg_dir + "RUR-{}_station-{}.svg".format(self.ref_chan, self.corrected_stream[0].stats.station,elapsed_time))
            fig_stream_masked_rur.savefig(svg_dir + "RUR_stream_masked_channel-{}_station-{}.svg".format(self.ref_chan, self.corrected_stream[0].stats.station,elapsed_time))

        figTF.savefig(pdf_dir + "TransferFunctionFrequencyResponse-{}_vs_{}-station_{}_{}_days.pdf".format(self.out_chan, self.ref_chan, self.corrected_stream[0].stats.station,elapsed_time))
        figTF_masked.savefig(pdf_dir + "TransferFunctionFrequencyResponse-{}_vs_{}-station_{}_masked_{}_days.pdf".format(self.out_chan, self.ref_chan, self.corrected_stream[0].stats.station,elapsed_time))
        figCohb.savefig(pdf_dir + "Coherence_before_correction-{}_vs_{}-station_{}_{}_days.pdf".format(self.out_chan, self.ref_chan, self.corrected_stream[0].stats.station,elapsed_time))
        figCoha.savefig(pdf_dir + "Coherence_after_correction-{}_vs_{}-station_{}_{}_days.pdf".format(self.out_chan, self.ref_chan, self.corrected_stream[0].stats.station,elapsed_time))
        figPsd_out_before.savefig(pdf_dir + "PSD_before_correction-channel-{}_station-{}_{}_days.pdf".format(self.out_chan, self.corrected_stream[0].stats.station,elapsed_time))
        figPsd_out_after.savefig(pdf_dir + "PSD_after_correction-channel-{}_station-{}_{}_days.pdf".format(self.out_chan, self.corrected_stream[0].stats.station,elapsed_time))
        figPsd_ref.savefig(pdf_dir + "PSD_channel-{}_station-{}_{}_days.pdf".format(self.ref_chan, self.corrected_stream[0].stats.station,elapsed_time))
        figSpectrogram_out.savefig(pdf_dir + "SPECTROGRAM_channel-{}_station-{}.pdf".format(self.ref_chan, self.corrected_stream[0].stats.station,elapsed_time))
        figSpectrogram_ref.savefig(pdf_dir + "SPECTROGRAM_channel-{}_station-{}.pdf".format(self.ref_chan, self.corrected_stream[0].stats.station,elapsed_time))
        if self.stationnarity is not None :
            fig_ADF.savefig(pdf_dir + "ADF_channel-{}_station-{}.pdf".format(self.ref_chan, self.corrected_stream[0].stats.station,elapsed_time))
            fig_stream_masked_adf.savefig(pdf_dir + "ADF_stream_masked_channel-{}_station-{}.pdf".format(self.ref_chan, self.corrected_stream[0].stats.station,elapsed_time))
            fig_kpss.savefig(pdf_dir + "KPSS-{}_station-{}.pdf".format(self.ref_chan, self.corrected_stream[0].stats.station,elapsed_time))
            fig_stream_masked_kpss.savefig(pdf_dir + "KPSS_stream_masked_channel-{}_station-{}.pdf".format(self.ref_chan, self.corrected_stream[0].stats.station,elapsed_time))
            fig_rur.savefig(pdf_dir + "RUR-{}_station-{}.pdf".format(self.ref_chan, self.corrected_stream[0].stats.station,elapsed_time))
            fig_stream_masked_rur.savefig(pdf_dir + "RUR_stream_masked_channel-{}_station-{}.pdf".format(self.ref_chan, self.corrected_stream[0].stats.station,elapsed_time))

        figTF.savefig(png_dir + "TransferFunctionFrequencyResponse-{}_vs_{}-station_{}_{}_days.png".format(self.out_chan, self.ref_chan, self.corrected_stream[0].stats.station,elapsed_time), dpi=225)
        figTF_masked.savefig(png_dir + "TransferFunctionFrequencyResponse-{}_vs_{}-station_{}_masked_{}_days.png".format(self.out_chan, self.ref_chan, self.corrected_stream[0].stats.station,elapsed_time), dpi=225)
        figCohb.savefig(png_dir + "Coherence_before_correction-{}_vs_{}-station_{}_{}_days.png".format(self.out_chan, self.ref_chan, self.corrected_stream[0].stats.station,elapsed_time), dpi=225)
        figCoha.savefig(png_dir + "Coherence_after_correction-{}_vs_{}-station_{}_{}_days.png".format(self.out_chan, self.ref_chan, self.corrected_stream[0].stats.station,elapsed_time), dpi=225)
        figPsd_out_before.savefig(png_dir + "PSD_before_correction-channel-{}_station-{}_{}_days.png".format(self.out_chan, self.corrected_stream[0].stats.station,elapsed_time), dpi=225)
        figPsd_out_after.savefig(png_dir + "PSD_after_correction-channel-{}_station-{}_{}_days.png".format(self.out_chan, self.corrected_stream[0].stats.station,elapsed_time), dpi=225)
        figPsd_ref.savefig(png_dir + "PSD_channel-{}_station-{}_{}_days.png".format(self.ref_chan, self.corrected_stream[0].stats.station,elapsed_time), dpi=225)
        figSpectrogram_out.savefig(png_dir + "SPECTROGRAM_channel-{}_station-{}.png".format(self.ref_chan, self.corrected_stream[0].stats.station,elapsed_time), dpi=225)
        figSpectrogram_ref.savefig(png_dir + "SPECTROGRAM_channel-{}_station-{}.png".format(self.ref_chan, self.corrected_stream[0].stats.station,elapsed_time), dpi=225)
        if self.stationnarity is not None :
            fig_ADF.savefig(png_dir + "ADF_channel-{}_station-{}.png".format(self.ref_chan, self.corrected_stream[0].stats.station,elapsed_time))
            fig_stream_masked_adf.savefig(png_dir + "ADF_stream_masked_channel-{}_station-{}.png".format(self.ref_chan, self.corrected_stream[0].stats.station,elapsed_time))
            fig_kpss.savefig(png_dir + "KPSS-{}_station-{}.png".format(self.ref_chan, self.corrected_stream[0].stats.station,elapsed_time))
            fig_stream_masked_kpss.savefig(png_dir + "KPSS_stream_masked_channel-{}_station-{}.png".format(self.ref_chan, self.corrected_stream[0].stats.station,elapsed_time))
            fig_rur.savefig(png_dir + "RUR-{}_station-{}.png".format(self.ref_chan, self.corrected_stream[0].stats.station,elapsed_time))
            fig_stream_masked_rur.savefig(png_dir + "RUR_stream_masked_channel-{}_station-{}.png".format(self.ref_chan, self.corrected_stream[0].stats.station,elapsed_time))


        if len(additionnal_figs) > 0 :
            for fig, name in zip(additionnal_figs, additionnal_fig_names):
                fig.savefig(pdf_dir + name + ".pdf")
                fig.savefig(png_dir + name + ".png")
                fig.savefig(svg_dir + name + ".svg")

class OBSWienerDataCleanerResultCollection: 
    """A convinient class to stack OBSWienerDataCleanerResults instance and especially transfer function frequency responses.
    All attribute are not available straight away, stack_collection() must be called first.

    Args:
        superwindowsize  (float) : The length of the overall signal from which every transfer function frequency response were estimated.
        OBSWienerDataCleanerResultsList   (list of transfer_function.OBSWienerDataCleanerResults) : the collection of results to be stacked. Can be empty.
        stack (bool) :  if True, will stack the collection .

    -----------
    Attributes:
    -----------
    
    Attributes
    ----------

        collection  (list) : the collection of OBSWienerDataCleanerResults.
        Nw  (list of int) or (tuple of int) :the number of sample of the spectral windows of each estimated transfer function.
        Noverlap  (list of int) or (tuple of int) : the number of overlaping samples of the spectral windows of each estimated transfer function.
        windowtype  (list of list) or (list of str) : the window taper types for each estimated transfer function frequency response.
        min_coherence  (list of float) : the coherence threshold used for estimate the transfer function frequency response.
        f  numpy.ndarray : the frequency vector.
        unit  (str) : the unit of the transfer function frequency responses (the same for every one)
        freqmask  numpy.ndarray : the best frequency mask found in the collection. 
        arrays  (dict of numpy.ndarray) : same as signal_utils.SpectralWienerFilter.arrays, contains the stacked transfer function frequency response.
            dict keys are 'mean', 'median', 'std', 'mad'.
        arrays_coherence  (dict of numpy.ndarray) : same as arrays, contains the stacked coherence between the output and reference signals before correction.
            dict keys are 'mean', 'median', 'std', 'mad'.
        nb_valid_windows  (int) : the total number of valid windows.
        nb_windows  (int) : the total number of windows.
        startdate  (datetime.datetime)  : The date and time of the first transfer function frequency response (and coherence).
        enddate  (datetime.datetime)  : The date and time of the last transfer function frequency response (and coherence).
        superwindowsize  (float) : The length of the overall signal from which every transfer function frequency response were estimated.
        flag_stack  (bool) : Tell if the stacking has been conducted or not.

    
    
    --------
    Methods:
    --------
    
    Methods
    -------

        save(outpath) : save the stack as .mat file, need the extension. Can be loaded by the signal_utils.SpectralWienerFilter class.
        load(inpath) : Load a saved collection.
        append(result) : Append an OBSWienerDataCleanerResults isntance to the collection.
        stack_collection(interpolate_f=False, median_or_mean="mean") : stack the collection. The stacking related attribute will be available afterward.
        plot(title="", freqmask=False, tight_layout=True, plot_mean=True, plot_median=False, display_title=False, display_left_labels=True, minmax_mag=None, minmax_phase=None, color=None) : Plot the stacked transfer function frequency response.
    
    
    """


    def __init__(self, super_window_size, OBSWienerDataCleanerResultsList=[], stack=False) :
        """ The constructor of class  OBSWienerDataCleanerResultCollection.
        """

        self.collection = OBSWienerDataCleanerResultsList        
        self.Nw = []
        self.Noverlap = []
        self.windowtype = []
        self.min_coherence=[]
        self.f = None
        self.unit= None
        self.freqmask = None
        self.arrays = {}
        self.arrays_coherence = {}
        self.nb_valid_windows=None
        self.nb_windows=None
        self.startdate=None 
        self.enddate=None
        self.superwindowsize=super_window_size
        
        if stack and (len(OBSWienerDataCleanerResultsList)>1):
            self.stack_collection()
        elif stack and (len(OBSWienerDataCleanerResultsList)<1):
            print("WARNING: can't stack empty collection.")
            self.flag_stack = False
        


    def save(self, outpath):
        """save the stack as .mat file, need the extension. Can be loaded by the signal_utils.SpectralWienerFilter class."""
        from scipy import io as scio
        if not self.flag_stack :
            raise ValueError("Can't save an empty stack, you must stack the collection first.")

        dates = []
        for date in self.dates :
            dates.append(str(date))
        out_dict = {} 
        out_dict["Nw"] = np.unique(self.Nw).squeeze()
        out_dict["array_names"] = list(self.arrays.keys())
        out_dict["Noverlap"] = np.unique(self.Noverlap).squeeze()
        out_dict["windowtype"] = self.windowtype[0]
        out_dict["f"] = self.f 
        out_dict["unit"] = self.unit
        out_dict["freqmask"] = self.freqmask 
        out_dict["arrays"] = self.arrays.copy()
        out_dict["arrays_coherence"] = self.arrays_coherence.copy()
        out_dict["min_coherence"] = np.unique(self.min_coherence).squeeze()
        out_dict["nb_valid_windows"] = self.nb_valid_windows
        out_dict["nb_windows"] = self.nb_windows
        out_dict["startdate"] = str(self.startdate )
        out_dict["enddate"] = str(self.enddate)
        out_dict["dates"] = dates
        out_dict["TFFRs"] = self.TFFRs
        out_dict["Coherences"] = self.coherences
        out_dict["ref_channel"] = self.ref_channel
        out_dict["out_channel"] = self.out_channel
        out_dict["valid_win_ratio"] = self.valid_win_ratio

        # for key in out_dict.keys() :
        #     if out_dict[key] is None :
        #         out_dict[key] = "None"

        scio.savemat(outpath, out_dict, oned_as="column")

    def load(self, inpath) :
        """ Load a saved collection. """
        from scipy.io import loadmat  
        loaddict = loadmat(inpath)


        arrays = loaddict["arrays"][0,0]
        arrays_coherence = loaddict["arrays_coherence"][0,0]
        arrays_temp = {}
        array_temp_coh = {}
        for k in loaddict["array_names"] :
            arrays_temp[k.replace(" ", "")] = arrays[k.replace(" ", "")].squeeze()
            array_temp_coh[k.replace(" ", "")] = arrays_coherence[k.replace(" ", "")].squeeze()


        self.ref_channel = loaddict["ref_channel"][0].replace(" ", "")
        self.out_channel = loaddict["out_channel"][0].replace(" ", "")
        self.startdate = datetime.strptime(loaddict["startdate"][0], "%Y-%m-%dT%H:%M:%S.%fZ")
        self.enddate = datetime.strptime(loaddict["enddate"][0], "%Y-%m-%dT%H:%M:%S.%fZ")

        self.dates = loaddict["dates"].squeeze()
        try :
            self.dates = [datetime.strptime(date, "%Y-%m-%d %H:%M:%S.%fZ") for date in self.dates]
        except ValueError :
            self.dates = [datetime.strptime(date, "%Y-%m-%d %H:%M:%S.%f") for date in self.dates]
  
        unit = loaddict["unit"][0]
        if unit == "None" :
            self.unit=None
        else :
            self.unit=unit

        self.Nw = int(loaddict["Nw"][0,0])
        self.Noverlap = int(loaddict["Noverlap"][0,0])
        self.min_coherence = float(loaddict["min_coherence"][0,0])

        
        if len(loaddict["windowtype"])  <2 : 
            self.windowtype = str(loaddict["windowtype"]).replace(" ", "").replace("'", "").replace("[", "").replace("]", "")
        else :
            self.windowtype = list(loaddict["windowtype"])
            for k, win  in enumerate(self.windowtype):
                self.windowtype[k] = win.replace(" ", "")
                if k > 0 :
                   self.windowtype[k]  = float(self.windowtype[k]) 
            self.windowtype = tuple(self.windowtype)

        self.f = loaddict["f"].squeeze()
        self.freqmask = loaddict["freqmask"].squeeze().astype(bool) 
        self.valid_win_ratio = loaddict["valid_win_ratio"].squeeze()
        self.arrays = arrays_temp
        self.arrays_coherence = array_temp_coh
        self.nb_valid_windows = int(loaddict["nb_valid_windows"][0,0])
        self.nb_windows = int(loaddict["nb_windows"][0,0])

        self.TFFRs = loaddict["TFFRs"].squeeze()
        self.coherences = loaddict["Coherences"].squeeze()


        return(None)

    def append(self, result) :
        """Append an OBSWienerDataCleanerResults isntance to the collection."""
        if isinstance(result, OBSWienerDataCleanerResults) :
            self.collection.append(result)
        else :
            raise TypeError("input must be of class OBSWienerDataCleanerResults, not {}".format(type(result)))

    def stack_collection(self,  interpolate_f=False, median_or_mean="mean") :
        """Stack the collection. The stacking related attribute will be available afterward.

        Parameters
        ----------
        interpolate_f : (bool)
            - if True, will try to interpolate the spectral results to the first spectral resolution of the collection if the frequencies are not the same.
        median_or_mean : (str)
            - Set wich individual transfer function frequency response average type should be used prior to the stacking. 
              This is not related to the average of the stack. Can be either mean or median. 
        """
        if median_or_mean in ["mean", "median"] :
            self.median_or_mean = median_or_mean
        else :
            raise ValueError(("median_or_mean must be either 'median' or 'mean', got {}".format(average_type)))

        nb_collection = len(self.collection)
        dates = []
        error_array = np.ones(nb_collection, dtype=bool)
        valid_win_ratio = np.full(nb_collection, np.nan)

        ### iterating over the collection

        temp_coherence = np.zeros([len(self.collection[0].TransferFunctionFrequencyResponse["median"]), nb_collection], dtype=complex)
        for k, result in enumerate(self.collection) :
            TFFR = result.TransferFunctionFrequencyResponse
            temp_coherence[:,k] = result.Coherence_before.data
            self.Nw.append(TFFR.Nw)
            self.Noverlap.append(TFFR.Noverlap)
            self.min_coherence.append(TFFR.min_coherence)
            
            if k == 0 :
                startdate = result.startdate
                enddate = result.enddate

                
                # temp[:,k] = Coherence["median"]
                if self.median_or_mean == "median" :
                    temp = np.zeros([len(TFFR["median"]), nb_collection], dtype=complex)
                    temp[:,k] = TFFR["median"]
                    

                elif self.median_or_mean == "mean" :
                    temp = np.zeros([len(TFFR["mean"]), nb_collection], dtype=complex)
                    temp[:,k] = TFFR["mean"]
 
                self.nb_windows = TFFR.nb_windows 
                self.windowtype.append(TFFR.windowtype)
                self.nb_valid_windows = TFFR.nb_valid_windows
                self.f = TFFR.f
                self.unit = TFFR.unit
                self.freqmask = TFFR.freqmask
                self.out_channel=TFFR.out_channel
                self.ref_channel=TFFR.ref_channel

            else :

                self.freqmask = self.freqmask | TFFR.freqmask

                try :
                    if np.any(self.f != TFFR.f) :
                        if not interpolate_f :
                            raise ValueError("Can't stack collection with different frequency vector")
                        else :
                            pass
                    else :
                        self.f = TFFR.f
                    
                    if TFFR.unit != self.unit :
                        raise ValueError("Can't stack collection with different unit")
                    if TFFR.out_channel != self.out_channel :
                        raise ValueError("Can't stack collection with different output channel")
                    if TFFR.ref_channel != self.ref_channel :
                        raise ValueError("Can't stack collection with different reference channel")

                    if self.median_or_mean == "median" :
                        temp[:,k] = TFFR["median"]
                    elif self.median_or_mean == "mean" :
                        temp[:,k] = TFFR["mean"]

                    if startdate > result.startdate :
                        startdate = result.startdate 
                    if enddate < result.enddate :
                        enddate = result.enddate 



                    if interpolate_f :
                            spectra_interpolator_real = sc.interpolate.interp1d(TFFR.f, TFFR[self.median_or_mean].real, kind="linear", fill_value="extrapolate", axis=-1)
                            spectra_interpolator_imag = sc.interpolate.interp1d(TFFR.f, TFFR[self.median_or_mean].imag, kind="linear", fill_value="extrapolate", axis=-1)
                            temp[:,k] = spectra_interpolator_real(self.f) + 1j*spectra_interpolator_imag(self.f)
                    else :
                        temp[:,k] = TFFR[self.median_or_mean]


                    self.nb_windows += TFFR.nb_windows 
                    self.nb_valid_windows += TFFR.nb_valid_windows
                except ValueError as error :
                    print("ValueError WARNING: index {} - {} - {}".format(k,error, sys.exc_info()[-1].tb_lineno), flush=True)
                    error_array[k] = False

            valid_win_ratio[k] = TFFR.nb_valid_windows/TFFR.nb_windows*100
            dates.append(result.startdate.datetime)

        temp = temp[:,error_array]
        temp_coherence = temp_coherence[:,error_array]
        self.arrays["median"]  = np.nanmedian(temp.real, axis=-1) + 1.j*np.nanmedian(temp.imag, axis=-1)
        self.arrays["mad"] =  mad(temp.real, axis=-1) + 1.j*mad(temp.imag, axis=-1)
        self.arrays["mean"] = np.nanmean(temp.real, axis=-1) + 1.j*np.nanmean(temp.imag, axis=-1)
        self.arrays["std"] = np.nanstd(temp.real, axis=-1) + 1.j*np.nanstd(temp.imag, axis=-1)

        self.arrays_coherence["median"]  = np.nanmedian(temp_coherence.real, axis=-1) + 1.j*np.nanmedian(temp_coherence.imag, axis=-1)
        self.arrays_coherence["mad"] =  mad(temp_coherence.real, axis=-1) + 1.j*mad(temp_coherence.imag, axis=-1)
        self.arrays_coherence["mean"] = np.nanmean(temp_coherence.real, axis=-1) + 1.j*np.nanmean(temp_coherence.imag, axis=-1)
        self.arrays_coherence["std"] = np.nanstd(temp_coherence.real, axis=-1) + 1.j*np.nanstd(temp_coherence.imag, axis=-1)


        
        self.startdate = startdate
        self.enddate = enddate
        self.valid_win_ratio = valid_win_ratio[error_array]
        self.TFFRs = temp
        self.coherences = temp_coherence
        self.dates = (np.asarray(dates)[error_array]).tolist()
        self.flag_stack = True
        print("There have been {} sucessfully stacked and stored results over {}".format(np.sum(error_array), np.sum(~error_array) + np.sum(error_array)))

        return(None)



    def plot(self, title="", freqmask=False,  tight_layout=True, plot_mean=True, plot_median=False, display_title=False, display_left_labels=True, minmax_mag=None, minmax_phase=None, color=None):
        """Plot the stacked transfer function frequency response.

        Args:
            title (str) : the title to be displayed.
            freqmask (bool) : if True, will use the freqmask attribute when drawing.  
            tight_layout (bool) : make the figure tight.
            plot_mean (bool) : if True, plot the mean average.
            plot_median (bool) : if True, plot the median average.
            display_title (bool) : if True, plot the title.
            display_left_labels (bool) : if True, display the left ylabels.
            minmax_mag (list of float) : min and max values to crop the magnitude plot.
            minmax_phase (list of float) : min and max values to crop the phase plot.
            color (str) : the color to use for drawing.
        
        Output:
        -------

            - figure (matplotlib.Figure) : the figure.
        
        """
        import matplotlib.ticker as ticker
        fig = plt.figure()
        gs = fig.add_gridspec(2, 1, height_ratios=(1,1),hspace=0.05)
        ax = fig.add_subplot(gs[0,0])
        ax_angl = fig.add_subplot(gs[1,0])
        

        if (self.nb_valid_windows is not None) and (self.nb_windows is not None) :
            title2 = "\n with {} valid {}s-length windows over {},\n divided into {} super windows".format(self.nb_valid_windows, (np.unique(self.Nw)*np.max(self.f)*2).astype(int) , self.nb_windows, len(self.valid_win_ratio))
        else:
            title2=""
        if display_title :
            ax.set_title(title + title2, fontsize=11)

        # average = np.stack((self.arrays["mean"], self.arrays["median"]), axis=0).T
        # deviation = np.stack((self.arrays["std"], self.arrays["mad"]), axis=0).T

        mean = self.arrays["mean"]
        std = self.arrays["std"]
        median = self.arrays["median"]
        mad = self.arrays["mad"]

        f = self.f

        if freqmask  :
            freqmask_original = self.freqmask
            # min_freq = f[freqmask_original].min()
            # max_freq = f[freqmask_original].max()

            # freqmask=np.ones(len(f), dtype=bool) 
            # freqmask[f>max_freq] = False 
            # freqmask[f<min_freq] = False
            freqmask = self.freqmask

        else :
            freqmask=np.ones(len(f), dtype=bool) 


        # ax.loglog(f[freqmask], (np.abs(average[freqmask])), color="tab:blue", label=["mean", "median"], linewidth=2.0)
        if plot_mean :
            # ax.loglog(f[freqmask], np.abs(mean[freqmask]), color="tab:blue", marker=".", label="mean")
            # ax.fill_between(f[freqmask], (np.abs(mean[freqmask]) - np.abs(std[freqmask])/2), 
            #                          (np.abs(mean[freqmask]) + np.abs(std[freqmask])/2), 
            #                          color="tab:blue", label="std", linewidth=1.0, alpha=0.3)

            mean = np.mean((np.abs(self.TFFRs[freqmask])), axis=1)
            std = np.std((np.abs(self.TFFRs[freqmask])), axis=1)
            
            # ax_angl.semilogx(f[freqmask], np.unwrap(np.angle(mean[freqmask])), color="tab:blue", marker=".", label="mean")
            # ax_angl.fill_between(f[freqmask], np.unwrap(np.angle(mean[freqmask])) - np.unwrap(np.angle(std[freqmask]))/2, 
            #                         np.unwrap(np.angle(mean[freqmask])) + np.unwrap(np.angle(std[freqmask]))/2, 
            #                         color="tab:blue", label="std", linewidth=1.0, alpha=0.3)
            ax.loglog(f[freqmask], mean, color="tab:blue", marker=".", label="mean")
            ax.fill_between(f[freqmask], mean - std/2, 
                                    mean + std/2, 
                                    color="tab:blue", label="sd", linewidth=1.0, alpha=0.3)
        if plot_median :
            ax.loglog(f[freqmask], np.abs(median[freqmask]), color="tab:red", marker=".", label="median")
            ax.fill_between(f[freqmask], (np.abs(median[freqmask]) - np.abs(std[freqmask])/2), 
                                     (np.abs(median[freqmask]) + np.abs(std[freqmask])/2), 
                                     color="tab:red", label="mad", linewidth=1.0, alpha=0.3)
        # ax.set_xticks(ax.get_xticks(), [""]*len(ax.get_xticks()))
        if isinstance(minmax_mag, (tuple, list)) :
            ax.set_ylim(bottom=minmax_mag[0], top=minmax_mag[1])
        
        nticks = 9
        maj_loc = ticker.LogLocator(numticks=nticks)
        maj_loc2 = ticker.LogLocator(numticks=nticks)
        ax.set_xticks(ax.get_xticks(), [""]*len(ax.get_xticks()))
        ax.set_xticks(ax.get_xticks(minor=True), [""]*len(ax.get_xticks(minor=True)),minor=True)
        ax.set_xlim(left=f[freqmask].min() - f[freqmask].min()/10 , right=f[freqmask].max()+ f[freqmask].max()/10)
        min_loc = ticker.LogLocator(subs='all', numticks=nticks)
        min_loc2 = ticker.LogLocator(subs='all', numticks=nticks)
        ax.yaxis.set_major_locator(maj_loc)
        ax.yaxis.set_minor_locator(min_loc)
        # ax.xaxis.set_major_locator(maj_loc)
        # ax.xaxis.set_minor_locator(min_loc)
        

        # ax.set_yticks(ax.get_yticks(), fontsize=11)
        # ax.fill_between(f[freqmask], (np.abs(average[:,0][freqmask]) - np.abs(deviation[:,0][freqmask])/2), 
        #                              (np.abs(average[:,0][freqmask]) + np.abs(deviation[:,0][freqmask])/2), 
        #                              color="tab:orange", label="std", linewidth=1.0, alpha=0.3)
        # ax.fill_between(f[freqmask], (np.abs(average[:,1][freqmask]) - np.abs(deviation[:,1][freqmask])/2), 
        #                              (np.abs(average[:,1][freqmask]) + np.abs(deviation[:,1][freqmask])/2), 
        #                              color="tab:orange", label="mad", linewidth=1.0, alpha=0.3)
        

        ax.grid()
        ax.legend( fancybox=True, shadow=True, ncols=7, fontsize=11)#, loc='upper center', bbox_to_anchor=[0.5,1.1],)
        # ax.set_xlabel("Frequency (Hz)")
        if display_left_labels :
            ax.set_ylabel(self.unit, fontsize=11)

        # ax_angl.semilogx(f[freqmask], np.unwrap(np.angle(average[freqmask])), color="tab:blue", label="Median", linewidth=2.0)

        if plot_mean :
            # ax_angl.semilogx(f[freqmask], np.unwrap(np.angle(self.TFFRs[freqmask])), color="black", marker=".", label="mean", alpha=0.1)
            mean = np.unwrap(np.mean(np.unwrap(np.angle(self.TFFRs[freqmask]), period=np.pi*2), axis=1))
            std = np.unwrap(np.std(np.unwrap(np.angle(self.TFFRs[freqmask]), period=np.pi*2), axis=1))
            
            # ax_angl.semilogx(f[freqmask], np.unwrap(np.angle(mean[freqmask])), color="tab:blue", marker=".", label="mean")
            # ax_angl.fill_between(f[freqmask], np.unwrap(np.angle(mean[freqmask])) - np.unwrap(np.angle(std[freqmask]))/2, 
            #                         np.unwrap(np.angle(mean[freqmask])) + np.unwrap(np.angle(std[freqmask]))/2, 
            #                         color="tab:blue", label="std", linewidth=1.0, alpha=0.3)
            ax_angl.semilogx(f[freqmask], mean, color="tab:blue", marker=".", label="mean")
            ax_angl.fill_between(f[freqmask], mean - np.abs(std)/2, 
                                    mean + np.abs(std)/2, 
                                    color="tab:blue", label="sd", linewidth=1.0, alpha=0.3)
        if plot_median :
            ax_angl.semilogx(f[freqmask], np.unwrap(np.angle(median[freqmask])), color="tab:blue", marker=".", label="median")
            ax_angl.fill_between(f[freqmask], np.unwrap(np.angle(median[freqmask])) - np.unwrap(np.angle(mad[freqmask]))/2, 
                                     np.unwrap(np.angle(median[freqmask])) + np.unwrap(np.angle(mad[freqmask]))/2, 
                                     color="tab:blue", label="mad", linewidth=1.0, alpha=0.3)
                                

        ax_angl.grid()
        # ax_angl.legend(loc)
        ax_angl.set_xlabel("Frequency (Hz)", fontsize=11)
        ax_angl.xaxis.set_major_locator(maj_loc2)
        ax_angl.xaxis.set_minor_locator(min_loc2)
        # angle_ticks = np.arange(-np.pi*2, np.pi*3, np.pi)
        # angle_labels = [r"$-2\pi$", r"$-\pi$", r"0", r"$\pi$", r"$2\pi$"] #[r"$-\frac{3\pi}{2}$", r"$-\pi$", r"$-\frac{\pi}{2}$", "0", r"$\frac{\pi}{2}$", r"$\pi$", r"$\frac{3\pi}{2}$"]
        # ax_angl.set_yticks(angle_ticks, angle_labels, fontsize=11)
        ax_angl.set_xlim(left=f[freqmask].min() - f[freqmask].min()/10 , right=f[freqmask].max()+ f[freqmask].max()/10)
        
        if display_left_labels :
            ax_angl.set_ylabel("Phase\n(rad)", fontsize=11)
        if isinstance(minmax_phase, (tuple, list)) :
            ax_angl.set_ylim(bottom=minmax_phase[0], top=minmax_phase[1])
        # ax_angl.set_xticks(ax_angl.get_xticks(), fontsize=11)
        # ax_angl.set_yticks(ax_angl.get_yticks(), fontsize=11)
        fig.subplots_adjust(left=0.2, bottom=0.12, right=0.95, top=0.95, wspace=0, hspace=0)
        if tight_layout :
            gs.tight_layout(fig)
        
            # fig.tight_layout()
        #fig.update_layout(height=int(800*1.5), width=int(600*1.5))
        return(fig)


    def spectrogram(self, title="", freqmask=False, tight_layout=True,  reject_bad_sup_windows=True, dates_or_sizes="dates"):
        """ 
        Plot the tranfer function frequency responses into a spectrogram-style across time.

        Args:
            title (str) : the title to write.
            freqmask (bool) : if True, the freqmask atribute will be used to mask frequencies.
            tight_layout (bool) : make the figure tight.  
            reject_bad_sup_windows (bool) : if True, will reject the bad windows and display them, if True, will not display the valid window ratio. 
            dates_or_sizes (str) : can be 'dates' or 'sizes'.

        Returns:
        --------
            - figure (matplotlib.Figure) : the figure.
        """
        from matplotlib import transforms
        from matplotlib.colors import LogNorm
        fig = plt.figure(figsize=(10, 9))
        gs = fig.add_gridspec(2, 3, height_ratios=(1,5),hspace=0.02, wspace=0.015, width_ratios=(0.25, 5, 2))

        ax_cb = fig.add_subplot(gs[1, 0])
        if not reject_bad_sup_windows :
            ax_win_ratio = fig.add_subplot(gs[0, 1])
        ax_spec = fig.add_subplot(gs[1, 1])
        ax_av = fig.add_subplot(gs[1, 2])
        
        
        ax_av.yaxis.tick_right()
        ax_av.yaxis.set_label_position("right")
        ax_spec.yaxis.tick_right()
        ax_spec.yaxis.set_label_position("right")

        # title2 = "Evolution of the {} TFFR estimate".format(self.median_or_mean)
        

        # fig.suptitle(title + title2)

        TFFRs = self.TFFRs
        average = self.arrays["mean"]
        mean = self.arrays["mean"]
        median = self.arrays["median"]
        std = self.arrays["std"]
        mad = self.arrays["mad"]
        f = self.f

        if freqmask  :
            freqmask_original = self.freqmask
            min_freq = f[freqmask_original].min()
            max_freq = f[freqmask_original].max()

            freqmask=np.ones(len(f), dtype=bool) 
            freqmask[f>max_freq] = False 
            freqmask[f<min_freq] = False
            freqmask[0] = False

        else :
            freqmask=np.ones(len(f), dtype=bool) 
            freqmask[0] = False

        if not reject_bad_sup_windows :
            timespan = (self.dates[1]- self.dates[0]).seconds
            from datetime import timedelta
            barwidth = timedelta(seconds=timespan)
            ax_win_ratio.bar(self.dates, self.valid_win_ratio, align="edge", width=-1/len(self.dates))
            ax_win_ratio.plot(self.dates, self.valid_win_ratio, "-r*")
            ax_win_ratio.grid()
            ax_win_ratio.set_ylabel("Valid window\nratio (%)")
            ax_win_ratio.set_xticks([])
            ax_win_ratio.set_xlim(xmax=self.dates[0], xmin=self.dates[-1])
        
        Z = np.abs(TFFRs)
        # t, time_unit = tl.time_converter(t)
        if dates_or_sizes == "dates":
            spa_im = ax_spec.pcolormesh(self.dates, f[freqmask], Z[freqmask], rasterized=True, norm=LogNorm(vmin=Z[freqmask].min(), vmax=Z[freqmask].max()), cmap="jet")
            ax_spec.set_xlabel("Date and time")
        elif dates_or_sizes == "sizes":
            spa_im = ax_spec.pcolormesh(np.arange(len(self.Nw)), f[freqmask], Z[freqmask], rasterized=True, norm=LogNorm(vmin=Z[freqmask].min(), vmax=Z[freqmask].max()), cmap="jet")
            
            ax_spec.set_xticks(np.arange(len(self.Nw)), self.Nw)
            ax_spec.set_xlabel("Window size (s)")
        # ax_spec.set_ylabel("Frequency (Hz)")
        ax_spec.set_yscale("log")#, linthresh=f[freqmask][1])#, linscale=0.15)#1e-17)
        ax_spec.set_yticks([])
        ax_spec.set_title(title)
        cbar = fig.colorbar(spa_im, cax=ax_cb)
        cbar.ax.set_ylabel(self.unit)
        plt.setp(ax_spec.get_xticklabels(), rotation=45)
        ax_spec.set_ylim(ymax=f[freqmask].max(), ymin=f[freqmask].min())
        cbar.ax.yaxis.tick_left()
        cbar.ax.yaxis.set_label_position("left")

        ax_av.loglog(Z[freqmask], f[freqmask], color="black", linewidth=0.5, alpha=0.1)
        ax_av.fill_betweenx(f[freqmask], np.abs(mean[freqmask]) - 0.5*np.abs(std[freqmask]), np.abs(mean[freqmask]) + 0.5*np.abs(std[freqmask]),color="tab:purple", linewidth=1.0, label="sd", alpha=0.3)
        ax_av.fill_betweenx( f[freqmask], np.abs(median[freqmask]) - 0.5*np.abs(mad[freqmask]), np.abs(median[freqmask]) + 0.5*np.abs(mad[freqmask]),color="tab:brown", linewidth=1.0, label="mad", alpha=0.3)
        ax_av.loglog((np.abs(mean[freqmask])), f[freqmask], color="tab:blue", linewidth=1.0, label="mean")
        ax_av.loglog((np.abs(median[freqmask])), f[freqmask], color="tab:red", linewidth=1.0, label="median")
        
        ax_av.set_ylim(ymax=f[freqmask].max(), ymin=f[freqmask].min())

        ax_av.grid()
        ax_av.legend()
        ax_av.set_ylabel("Frequency (Hz)")
        ax_av.set_xlabel(self.unit)

        gs.tight_layout(fig)
        
        return(fig)        

    def spectrogram_coherence(self, title="", freqmask=False,  tight_layout=True,  reject_bad_sup_windows=True, dates_or_sizes="dates", display_colorbar=True, display_left_labels=True, display_right_labels=True):
        """ 
        Plot the coherences of the collection into a spectrogram-style across time.

        Args:
            title (str) : the title to write.
            freqmask (bool) : if True, the freqmask atribute will be used to mask frequencies.
            tight_layout (bool) : make the figure tight.  
            reject_bad_sup_windows (bool) : if True, will reject the bad windows and display them, if True, will not display the valid window ratio. 
            dates_or_sizes (str) : can be 'dates' or 'sizes'.
            display_colorbar (bool) : display or not the colorbar.
            display_left_labels (bool) : display or not the left ylabels.
            display_right_labels (bool) : display or not the right ylabels. 

        Returns:
        --------
            - figure (matplotlib.Figure) : the figure.
        """
        from matplotlib import transforms
        from matplotlib.colors import LogNorm
        fig = plt.figure(figsize=(10, 9))
        gs = fig.add_gridspec(3, 3, height_ratios=(1,5,0.5),hspace=0.045, wspace=0.015, width_ratios=(0.25, 5, 2))

        if display_colorbar :
            ax_cb = fig.add_subplot(gs[1, 0])
        if not reject_bad_sup_windows :
            ax_win_ratio = fig.add_subplot(gs[0, 1])
        ax_spec = fig.add_subplot(gs[1, 1])
        ax_av = fig.add_subplot(gs[1, 2])
        
        
        ax_av.yaxis.tick_right()
        ax_av.yaxis.set_label_position("right")
        ax_spec.yaxis.tick_right()
        ax_spec.yaxis.set_label_position("right")

        # title2 = "Evolution of the {} TFFR estimate".format(self.median_or_mean)
        

        # fig.suptitle(title + title2)

        TFFRs = self.coherences
        mean = self.arrays_coherence["mean"]
        median = self.arrays_coherence["median"]
        std = self.arrays_coherence["std"]
        mad = self.arrays_coherence["mad"]
        f = self.f

        if freqmask  :
            freqmask_original = self.freqmask
            min_freq = f[freqmask_original].min()
            max_freq = f[freqmask_original].max()

            freqmask=np.ones(len(f), dtype=bool) 
            freqmask[f>max_freq] = False 
            freqmask[f<min_freq] = False
            freqmask[0] = False

        else :
            freqmask=np.ones(len(f), dtype=bool) 
            freqmask[0] = False

        if not reject_bad_sup_windows :
            timespan = (self.dates[1]- self.dates[0]).seconds
            from datetime import timedelta
            barwidth = timedelta(seconds=timespan)
            ax_win_ratio.bar(self.dates, self.valid_win_ratio, align="edge", width=-1/len(self.dates))
            ax_win_ratio.plot(self.dates, self.valid_win_ratio, "-r*")
            ax_win_ratio.set_ylim(bottom=0, top=100)
            ax_win_ratio.grid()
            if display_left_labels :
                ax_win_ratio.set_ylabel("Valid window\nratio (%)")
            ax_win_ratio.set_xticks([])
            
            ax_win_ratio.set_xlim(xmax=self.dates[0], xmin=self.dates[-1])
        
        Z = np.abs(TFFRs)
        # t, time_unit = tl.time_converter(t)
        if dates_or_sizes == "dates":
            new_xticks = np.linspace(0,len(self.dates), 8, dtype=int, endpoint=False)
            spa_im = ax_spec.pcolormesh(np.arange(len(self.dates)), f[freqmask], Z[freqmask], rasterized=True, cmap="jet")
            ax_spec.set_xlabel("Date and time")
            
            new_datetimes = [str(current_date).split(" ")[0].replace(" ", "\n") for current_date in np.asarray(self.dates)[new_xticks]]
            ax_spec.set_xticks(new_xticks, new_datetimes, fontsize="small")
            plt.setp(ax_spec.get_xticklabels(), rotation=25)
            ax_spec.set_xlabel("Date and time")
            

        elif dates_or_sizes == "sizes":
            spa_im = ax_spec.pcolormesh(np.arange(len(self.Nw)), f[freqmask], Z[freqmask], rasterized=True, cmap="jet")
            
            ax_spec.set_xticks(np.arange(len(self.Nw)), self.Nw)
            ax_spec.set_xlabel("Window size (s)")

            
            
        # ax_spec.set_ylabel("Frequency (Hz)")
        ax_spec.set_yscale("log")#, linthresh=f[freqmask][1])#, linscale=0.15)#1e-17)
        ax_spec.set_yticks([])
        ax_spec.set_yticks([], labels=[], minors=True)
        
        ax_spec.set_title(title)
        if display_colorbar :
            cbar = fig.colorbar(spa_im, cax=ax_cb)
            if display_left_labels :
                cbar.ax.set_ylabel("Coherence level")
            # plt.setp(ax_spec.get_xticklabels(), rotation=45)
            ax_spec.set_ylim(ymax=f[freqmask].max(), ymin=f[freqmask].min())
            cbar.ax.yaxis.tick_left()
            cbar.ax.yaxis.set_label_position("left")

        # ax_av.semilogy(Z[freqmask], f[freqmask], color="black", linewidth=0.5, alpha=0.1)
        ax_av.fill_betweenx(f[freqmask], np.abs(mean[freqmask]) - 0.5*np.abs(std[freqmask]), np.abs(mean[freqmask]) + 0.5*np.abs(std[freqmask]),color="tab:purple", linewidth=1.0, label="sd", alpha=0.3)
        # ax_av.fill_betweenx( f[freqmask], np.abs(median[freqmask]) - 0.5*np.abs(mad[freqmask]), np.abs(median[freqmask]) + 0.5*np.abs(mad[freqmask]),color="tab:brown", linewidth=1.0, label="mad", alpha=0.3)
        ax_av.semilogy((np.abs(mean[freqmask])), f[freqmask], color="tab:blue", linewidth=1.0, label="mean")
        ax_av.set_yticks(ax_av.get_yticks(minor=True), label=ax_av.get_yticks(minor=True), minor=True )
        ax_av.set_ylabel("Frequency (Hz)", horizontalalignment="right", labelpad=5.)
        # ax_av.semilogy((np.abs(median[freqmask])), f[freqmask], color="tab:red", linewidth=1.0, label="median")
        
        ax_av.set_ylim(ymax=f[freqmask].max(), ymin=f[freqmask].min())

        ax_av.grid()
        ax_av.legend(loc='upper center', bbox_to_anchor=[0.5,1.1], fancybox=True, shadow=True, ncols=7, fontsize=11)
        if display_right_labels :
            ax_av.set_ylabel("Frequency (Hz)")
        
        ax_av.set_xlabel("Coherence level")
        if tight_layout :
            gs.tight_layout(fig)
        fig.subplots_adjust(left=0.1, bottom=0.05, right=0.9, top=0.95, wspace=0, hspace=0)
        return(fig)       

class StreamTransferFunction :
    """
    A class designed to estimate a transfer function between two channel of a stream.

    Args:
        stream  (obspy.core.stream.Stream) : the input stream.
        masknodata  (dict) : the dict containing the nodata mask for each traces in stream (keys must be the channels).
                the dimensions of the masks must match the traces dimensions.
        maskevents  (dict) : the dict containing the event mask for each traces in stream (keys must be the channels).
                the dimensions of the masks must match the traces dimensions.
    
    -----------
    Attributes:
    -----------
    
    
    Attributes
    ----------

        stream  (obspy.core.stream.Stream) : the stream instance to be analysed.
        WienerFilter  (signal_utils.SpectralWienerFilter) :  the transfer functionf frequency response that will be used to correct the output signal.
        out_chan  (str) : the channel name of the signal to be corrected from the reference signal.
        ref_chan  (str) : the channel name of the signal to be the reference from wich the noise component will be evaluated.
        windowtype  (str) or (list) : the window taper type that have been used to evaluate WienerFilter. follow the syntax of the scipy.signal.get_window() function.
        Nws  (float) : the length of the windows (s) that have been used to evaluate WienerFilter.
        Noverlaps  (float) : the length of the window overlaps (s) that have been used to evaluate WienerFilter.
        min_coh  (float) : the min coherence.
        backend  (str) : deprecated
        average  (str) : the type of averaging used to estimate the power spectral densities and therefore the transfer function.
            Can be 'mean' or 'median'
        masknodata  (dict) : a dict containing boolean numpy.ndarray that mask invalid data and whose dimensions match the related trace dimensions and who are accessed by the channel name.
        maskevents  (dict) : a dict containing boolean numpy.ndarray that mask data labelled as event and whose dimensions match the related trace dimensions and who are accessed by the channel name.
        freqmask  (numpy.ndarray) : a boolean array that masks frequencies. Built from freqmin, freqmax and the coherence.
        coherence  (numpy.ndarray) : the coherence between ref_chan and out_chan signals before correction.
        coherence_corrected (numpy.ndarray) : the coherence between ref_chan and out_chan signals after correction.
        statcheck  (bool) : a boolean telling if a stationarity check will be conducted using standard stationarity tests.

    --------
    Methods:
    --------
    
    Methods
    -------

    estimate_transfer_function(out_chan,  ref_chan, windowtype , Nws, Noverlaps,freqmin=None, freqmax=None, min_coh=None, verbose=True, backend="bruit-fm", check_stationnarity=False, corr_threshold=0.5) :
         estimate the transfer function frequency response between the ref_chan and out_chan signals using [6]. 
    
    attach_transfer_function(WienerFilter):
         attach a transfer function frequency response that has been already estimated.
    
    correct_output_channel(average="mean", verbose=True, stream=None, force_freqmin=None, force_freqmax=None) :
         correct the output channel signal from the reference channel signal component using the estimated (or attached) transfer function frequency response.
    
    get_results() :
         Return the results of processing as a OBSWienerDataCleanerResults instance.
    """

    def __init__(self, stream=None, masknodata=None, maskevents=None ):
        """Constructor of the StreamTransferFunction class.
        
            
        """
    
        self.stream = stream 
        self.WienerFilter = None 
        self.out_chan = None 
        self.ref_chan = None 
        self.windowtype=None 
        self.Nws = None 
        self.Noverlaps=None 
        self.min_coh=1.
        self.backend="bruit-fm"
        self.average="mean"
        self.masknodata=masknodata
        self.maskevents=maskevents
        self._can_correct=False
        self.freqmask = None
        self.coherence=None
        self.coherence_corrected=None
        self.statcheck=None
        self.corr_window = None
        self.corr_threshold = None
        self.mask_corr = None


    def estimate_transfer_function(self, out_chan,  ref_chan, windowtype , Nws, Noverlaps,
                        freqmin=None, freqmax=None, min_coh=None, verbose=True, backend="bruit-fm", check_stationnarity=False, corr_threshold=0.5 ) :

        """Estimate the transfer function frequency response between out_chan and ref_chan signals [2,4].
        Consequently update the attributes of the instance.
        
        Args:
            out_chan  (str)  : the channel to be cleaned, must be standard FDSN name like "BHZ", "BDH", "BH1", or "BHN"
            ref_chan  (str)    :  the reference channel where the reference noise is, must be standard FDSN name like "BHZ", "BDH", "BH1", or "BHN"
            windowtype  (str or tuple)    : the windowtype following the scipy.signal.get_window() documentation.
            Nws  (float)  : the length of the window in seconds.
            Noverlap  (float) : the number of seconds of overlapping between each window.
            freqmin  (float) :the lowest frequency that will be kept when computing the transfer function frequency response, default=None.
            freqmax  (float)  : the highest frequency that will be kept when computing the transfer function frequency response, default=None.
            min_coh  (float)  : the upper threshold coherence value whose corresponding frequencies shall be processed, default=None.
            corr_threshold  (float)  : if not None, will calculate the correlation coefficient between each window and only keep the spectral windows which have a abs(corr_coeff) above the threhsold
        """
        self.out_chan = out_chan 
        self.ref_chan = ref_chan 
        self.windowtype = windowtype 
        self.Nws = Nws 
        self.Noverlaps = Noverlaps 
        self.freqmin = freqmin 
        self.freqmax = freqmax 
        self.min_coh = min_coh 
        self.backend = backend 
        self.corr_window = corr_threshold 


        if verbose :
            processing_start_time = time.process_time()
        ################################################################
        ################ Checking out the parameters ###################
        ################################################################
        stream = self.stream.copy()
        if isinstance(corr_threshold, (float)) :
            if (freqmin is None) and (freqmax is None) :
                stream_bandpassed = self.stream.copy()
            elif (freqmin is None) and (freqmax is not None) :
                stream_bandpassed = FilterStream(self.stream.copy(), 3, freqmax, btype="lowpass", verbose=True, mask_nodata=self.masknodata)
            elif (freqmin is None) and (freqmax is not None) :
                stream_bandpassed = FilterStream(self.stream.copy(), 3, freqmin, btype="highpass", verbose=True, mask_nodata=self.masknodata)
            else :
                stream_bandpassed = FilterStream(self.stream.copy(), 3, [freqmin, freqmax], btype="bandpass", verbose=True, mask_nodata=self.masknodata)
        
        Nw = int(Nws*stream[0].stats.sampling_rate)
        Noverlap = int(Noverlaps*stream[0].stats.sampling_rate)


        ###############################################################################
        ###### identifying the traces corresponding to the out and in channels ########
        ###### and making basic checkups on the traces to be processed ################
        ###############################################################################
        ind_tr = {}
        flag={}
        flag["ref_chan"] = False
        flag["out_chan"] = False
        flag["nodata"] = True

        ### ind 0 correspond to the ref channel
        ### ind 1 correspond to the out channel
        for ind, trace in enumerate(stream) :
            if trace.stats.channel == ref_chan:
                ind_tr[ref_chan] = ind
                flag["ref_chan"] = True
            if trace.stats.channel == out_chan:
                ind_tr[out_chan] = ind
                flag["out_chan"] = True

        if not flag["ref_chan"] :
            raise ValueError("There is no channel named {}.".format(ref_chan))
        if not flag["out_chan"] :
            raise ValueError("There is no channel named {}.".format(out_chan))

        if stream[ind_tr[ref_chan]].stats.npts != stream[ind_tr[out_chan]].stats.npts :
            raise ValueError("The traces are not of the same length")
        if stream[ind_tr[ref_chan]].stats.sampling_rate != stream[ind_tr[out_chan]].stats.sampling_rate :
            raise ValueError("The traces are not of the same sampling rate")

        # npts
        out_npts = stream[ind_tr[out_chan]].stats.npts
        ref_npts = stream[ind_tr[ref_chan]].stats.npts

        # traces
        out_trace = stream[ind_tr[out_chan]]
        ref_trace = stream[ind_tr[ref_chan]]

        ##############################################################################
        ####################### Checking correlations ################################
        ##############################################################################
        if isinstance(corr_threshold, (float)) :
            out_trace_bp = stream_bandpassed.select(channel=out_chan)[0]
            ref_trace_bp = stream_bandpassed.select(channel=ref_chan)[0]

        
            sliced_time1, sliced_data_out = signal_slicing(out_trace_bp.data, Nw, Noverlap, "boxcar", step=out_trace_bp.stats.delta)
            sliced_time1, sliced_data_ref = signal_slicing(ref_trace_bp.data, Nw, Noverlap, "boxcar", step=ref_trace_bp.stats.delta)
        
            corr_coeffs = np.zeros(sliced_data_out.shape[-1])
            for ind, (current_data_ref, current_data_out) in enumerate(zip(sliced_data_ref.T, sliced_data_out.T)) :
                corr_coeffs[ind] = np.abs(np.corrcoef(current_data_ref, current_data_out))[0,1]

            self.mask_corr = corr_coeffs > corr_threshold

            print("There is {} windows that show enough correlations out of {}".format(np.sum(self.mask_corr), len(self.mask_corr)))
            if int(np.sum(self.mask_corr)) == 0 :
                raise NoValidDataError("No windows availables for power spectral density estimation due to uncorrelated channels, try to lower the correlation threshold.")
        else :
            self.mask_corr=None

        ##############################################################################
        #### DEALING WITH MASKS. We want to have all the nodata and event values zeroed
        #### for the analysis, the psd and csd function will detect thoses and remove
        #### any window contaminated from the psd estimation
        ##############################################################################
        if self.masknodata is None :
            masknodata = {out_chan : np.zeros(out_npts, dtype=bool),
                        ref_chan : np.zeros(ref_npts, dtype=bool)}
        else :
            masknodata = self.masknodata.copy()
        
        if self.maskevents is None :
            maskevents = {out_chan : np.zeros(out_npts, dtype=bool),
                        ref_chan : np.zeros(ref_npts, dtype=bool)}
        else :
            maskevents = self.maskevents.copy()

        #### assuming masknodata and maskevents share the same keys, merging the masks
        masknodataevents = {}
        mergedmasknodataevents = np.zeros(out_npts, dtype=bool)
        for k in masknodata.keys() :
            masknodataevents[k] = masknodata[k] | maskevents[k]
            mergedmasknodataevents = mergedmasknodataevents | masknodataevents[k]



        #### masking, it will be discarded regargind the zero values and not be used after
        out_trace.data[mergedmasknodataevents] = 0. 
        ref_trace.data[mergedmasknodataevents] = 0. 
    
        #### final checks just in case
        merged_mask = np.zeros(out_trace.stats.npts, dtype=bool)
        mask_out = np.abs(out_trace.data) < 1e-15
        mask_ref = np.abs(ref_trace.data) < 1e-15
        mask_merged = mask_out | mask_ref
        out_trace.data[mask_merged] = 0.
        ref_trace.data[mask_merged] = 0.


        if check_stationnarity :
            statcheck = StationnarityAssessment(stream_analysis)
            results_kpss, mask_kpss = statcheck.compute("kpss", Nws, Noverlaps)
            results_adf, mask_adf = statcheck.compute("adf", Nws, Noverlaps)
            results_rur, mask_rur = statcheck.compute("rur", Nws, Noverlaps)
            deviations = StreamTimeSeriesDeviation(stream,  Nws, "second")
            statcheck.attach_deviation(deviations)
            statcheck.count_non_stationary_window()
        else :
            statcheck = None


        ### Designing the window
        window = get_window(windowtype, Nw)
        
        if verbose :
            print("calculating the overlapping spectrograms and PSDs", flush=True)
            start = time.process_time()
        try :
            foo, Soo = psd(out_trace.copy(), windowtype, Nws, Noverlaps, return_SpectralStats=True, backend=backend, masknodata=self.maskevents, mask_corr=self.mask_corr)
            frr, Srr = psd(ref_trace.copy(), windowtype, Nws, Noverlaps, return_SpectralStats=True, backend=backend, masknodata=self.maskevents, mask_corr=self.mask_corr)
            fro, Sor = cpsd(out_trace.copy(), ref_trace.copy(), windowtype, Nws, Noverlaps, return_SpectralStats=True, backend=backend, masknodata=self.maskevents, mask_corr=self.mask_corr)
        except NoValidDataError as error:
            raise ValueError("WARNING: {}.".format(error))
            
        if verbose :
            print("--> PSDs and CPSDs computation done ({})s".format(time.process_time() - start), flush=True)
            start = time.process_time()
        Cor = Coherence(Sor, Soo, Srr)
        ### Getting the coherence
        if verbose :
            print("--> Coherence computation done ({})s".format(time.process_time() - start), flush=True)
            start = time.process_time()
        
        if verbose :
            print("--> Coherence interpolation done ({})s".format(time.process_time() - start), flush=True)
            start = time.process_time()
        ### Estimating the noise canceler transfer function frequency response, Wiener style
        W = SpectralWienerFilter(Sor/Srr, nb_valid_windows=Sor.nb_valid_windows, nb_windows=Sor.nb_windows, ref_channel=self.ref_chan, out_channel=self.out_chan, min_coherence=min_coh, startdate=out_trace.stats.starttime.datetime, enddate=out_trace.stats.endtime.datetime)
        
        if verbose :
            print("--> Wiener filter estimation done ({})s".format(time.process_time() - start), flush=True)
        start = time.process_time()

        ### calculating frequency mask
        freqmask = calc_freq_mask(fro, freqmin, freqmax)
        ### using the coherence value to further mask the transfer function frequency response
        if min_coh is not None :
            ### checking for coherence scignificance
            coherence_mask = Cor[:] > Cor.calculate_signifiance(min_coh)#.sign95 ### check [5]

            
            
            #### merging the masks
            total_mask = freqmask & coherence_mask
            # total_mask = _zero_bad(total_mask, 3*Cor.nd)
            total_mask[:] = fill_holes_in_mask(total_mask)
            # total_mask[:] = 
        else :
            total_mask = freqmask
            

        W.attach_freqmask(total_mask)
        # print(np.stack([freqmask, fro],axis=1))

        self.WienerFilter=W

        self._can_correct=True
        self.freqmask=total_mask
        self.coherence = Cor
        self.statcheck = statcheck

    def  attach_transfer_function(self, WienerFilter) :
        """ Attach a transfer function frequency response that has been already estimated.

        Args:
            WienerFilter (signal_utils.SpectralWienerFilter)  : the transfer function frequency response.
        """
        if isinstance(WienerFilter, SpectralWienerFilter) :
            self.WienerFilter = WienerFilter 
            self.out_chan = WienerFilter.out_channel 
            self.ref_chan = WienerFilter.ref_channel  
            self.windowtype = WienerFilter.windowtype 
            self.Nws = WienerFilter.Nw*np.max(WienerFilter.f)*2.
            self.Noverlaps = WienerFilter.Noverlap*np.max(WienerFilter.f)*2. 
            self.freqmin = None 
            self.freqmax = None 
            self.min_coh = None 
            self.backend = "bruit-fm" # deprecated
            self._can_correct=True

    
    def correct_output_channel(self, average="mean", verbose=True, stream=None, force_freqmin=None, force_freqmax=None) :
        """
        Correct the ouput channel signal from the reference channel using the previously attached or estimated transfer function 
        frequency response. 

        Args:
            average  (str) : the kind of average to use from the transfer function. Can be either 'mean' or 'median'.
            verbose  (bool) : verbose mode.
            stream  (obspy.core.stream.Stream) : a stream instance to correct in place of the stream attribute. By default, the correction will be processed on the current stream attribute.
            force_freqmin  (float) : if given, ensure that the frequencies above  will be corrected despite the freqmask attribute.
            force_freqmax  (float) :  if given, ensure that the frequencies under  will be corrected despite the freqmask attribute.    
            
        """
        if not self._can_correct :
            raise ValueError("The transfer function frequency response has not been estimated or attached yet ! Aborting.")

        if average not in ["mean", "median"] :
            raise ValueError("average must be either mean or mediann got {}.".format(average))

        ref_chan = self.ref_chan 
        out_chan = self.out_chan
        
        if stream is None :
            out_stream = self.stream.copy()
        else :
            out_stream = stream.copy()


        

        masknodatatemp = self.masknodata[out_chan] | self.masknodata[ref_chan]
        # Making sure to taper the edges of the signals to avoid freqmax limit instabilities (spike)
        masknodatatemp[0] = True
        masknodatatemp[-1] = True
        masknodata={}
        for tr in out_stream :
            masknodata[tr.stats.channel] = masknodatatemp


        out_stream.detrend("polynomial", order=10) # reduced order does not work

        out_stream = taper_stream(out_stream, masknodata, dilation_time=60*4, taper_time=60)

        out_trace = out_stream.select(channel=self.out_chan)[0]
        ref_trace = out_stream.select(channel=self.ref_chan)[0]

        # making sure 0 values appears for both channels at the same time
        merged_mask = np.zeros(out_trace.stats.npts, dtype=bool)
        mask_out = np.abs(out_trace.data) < 1e-17
        mask_ref = np.abs(ref_trace.data) < 1e-17
        mask_merged = mask_out | mask_ref
        out_trace.data[mask_merged] = 0.
        ref_trace.data[mask_merged] = 0.



        ### Computation of the old PSDs
        foo, Soo = psd(out_trace.copy(), self.windowtype, self.Nws, self.Noverlaps, return_SpectralStats=True, backend=self.backend, mask_corr=self.mask_corr)#, masknodata=self.maskevents)
        frr, Srr = psd(ref_trace.copy(), self.windowtype, self.Nws, self.Noverlaps, return_SpectralStats=True, backend=self.backend, mask_corr=self.mask_corr)#, masknodata=self.maskevents)
        fro, Sor = cpsd(out_trace.copy(), ref_trace.copy(), self.windowtype, self.Nws, self.Noverlaps, return_SpectralStats=True, backend=self.backend, mask_corr=self.mask_corr)#, masknodata=self.maskevents)

        # npts
        out_npts = out_trace.stats.npts
        ref_npts = ref_trace.stats.npts


        out_data = out_trace.data
        ref_data = ref_trace.data

        fft_len = len(out_data) 


        f = rfftfreq(fft_len, out_trace.stats.delta)
        W = self.WienerFilter.copy().interpolate(f)
        
        freqmask = W.freqmask

        if isinstance(force_freqmax, (float,int)) and isinstance(force_freqmin, (float,int)) :
            new_freqmask = calc_freq_mask(f, force_freqmin, force_freqmax)
            freqmask = new_freqmask
        

        St = rfft(out_data, fft_len)
        Nt = rfft(ref_data, fft_len)

        St[freqmask] = St[freqmask] - Nt[freqmask]*(W[average][freqmask].conj())
        cleaned_out_data = irfft(St, fft_len).real


        out_trace.data[:] = cleaned_out_data

        if verbose :
            print("--> output {} channel time series cleaned ({})s".format(out_chan, out_trace.get_id()), flush=True)
            start = time.process_time()


        if verbose :
            print("Calculating the new overlapping spectrograms, PSDs, CPSDs and Coherence", flush=True)
            start = time.process_time()
        
        ### computation of the new PSDs
        fro, Sor_after = cpsd(ref_trace, out_trace, self.windowtype, self.Nws, self.Noverlaps, return_SpectralStats=True, backend=self.backend)
        foo, Soo_after = psd(out_trace, self.windowtype, self.Nws, self.Noverlaps, return_SpectralStats=True, backend=self.backend)
        # print(np.stack([self.WienerFilter.freqmask, fro],axis=1))
        if verbose :
            print("--> New PSDs and CPSDs computation done ({})s".format(time.process_time() - start), flush=True)
            start = time.process_time()
        ### Getting the new coherence
        Cor = Coherence(Sor, Soo, Srr)
        Cor_after = Coherence(Sor_after, Soo_after, Srr)

        if verbose :
            print("--> Coherence interpolation done ({})s".format(time.process_time() - start), flush=True)
            start = time.process_time()

        self.coherence_corrected = Cor_after
        if not isinstance(self.coherence, Coherence) :
            self.coherence = Cor
        self.corrected_stream = out_stream


    def get_results(self) :
        """Return the results of processing as a OBSWienerDataCleanerResults instance."""
        starttime, endtime = detect_minimal_time_range(self.stream)
        results = OBSWienerDataCleanerResults(TransferFunctionFrequencyResponse=self.WienerFilter, Coherence_before=self.coherence,  
                                            Coherence_after=self.coherence_corrected, corrected_stream=self.corrected_stream, 
                                            output_channel=self.out_chan, reference_channel=self.ref_chan, 
                                            startdate=starttime, enddate=endtime,
                                            stationnarity=self.statcheck)
        
        return(results)
            


############################# DEPRECATED PART

def _OBSTiltDetection(stream, channels, freqmin, freqmax, coherencemax, windowtype, Nws, Noverlaps ):
    """
    Will tell if there is any detectable tilt given the channels, frequency range and a coherence threshold.

    """

    ### Identifying channels
    horizontal_chans = []
    vertical_chan = []
    pressure_chan = []
    for cha in channels :
        if cha[-1] in ["1","2","N","E"] :
            horizontal_chans.append(cha)
        elif cha[-1] in ["Z"] :
            vertical_chan.append(cha)
        elif cha[-1] in ["H"] :
            pressure_chan.append(cha)
        else :
            raise ValueError("Unrecognised channel")


    stream = select_related_channels(stream, [chan[0] for chan in channels], [chan[1] for chan in channels], "*")
    stream.plot(handle=True, equal_scale=False)
    from tiskit import CleanRotator
    azimuths, angles = [], []
    for k, wst in enumerate(stream.slide(window_length=Nws/10, step=Nws/10, include_partial_windows=True) ):
        rotator = CleanRotator(wst, filt_band=(1e-2/100, 1e-2), verbose=False, uselogvar=False, remove_eq=False)
        if k ==0:
            stream_out = rotator.apply(wst.copy())
        else :
            stream_out += rotator.apply(wst.copy())
        azimuths.append(rotator.azimuth)
        angles.append(rotator.angle)

    fig, ax_ang = plt.subplots()
    ax_az = ax_ang.twinx()
    ax_ang.plot(angles, marker="o",color="tab:orange")
    ax_ang.set_ylabel("Angle (°)", color="tab:orange")
    ax_ang.grid()
    ax_az.plot(azimuths, marker="v",color="tab:blue")
    ax_az.set_ylabel("Azimuth (°)", color="tab:blue")
    #print(azimuths, angles)
    stream = stream_out.merge(method=1)
    stream.plot(handle=True, equal_scale=False)
    #stream.rotate(method='LQT->ZNE', inclination=-125 ,back_azimuth=20)
    #stream = RotateStream(30,0., stream.copy())
    trace_z = stream.select(channel=pressure_chan[0])[0]


    #f = rfftfreq(trace_z.stats.npts, trace_z.stats.delta)
    #from scipy.optimize import least_squares, brute
    #azimuth = least_squares(_opt_azimyth_tilt, 20., args=(stream, 1e-3, windowtype, Nws, Noverlaps), bounds=(-180,180))
    #azimuth = brute(_opt_azimyth_tilt, [(-180.,180.)], args=(stream, 1e-3, windowtype, Nws, Noverlaps), Ns=36, workers=4)
    #print(azimuth)

    ### checking coherence between horizontal component, if there is no tilt it should be minimal
    trace_1 = stream.select(channel=horizontal_chans[0])[0]
    trace_2 = stream.select(channel=horizontal_chans[1])[0]
    fc, C_horizontal = _compute_coherence(trace_1, trace_2, windowtype, Nws, Noverlaps, return_CoherenceClass=True)
    freqmask = calc_freq_mask(fc, freqmin, freqmax)
    C_horizontal_mean = np.mean(C_horizontal[freqmask])
    fig = C_horizontal.plot(title="BH1-BH2 Coherence", freqmask=None)

    ### checking coherence between first horizontal and vertical component, if there is no tilt it should be minimal
    trace_1 = stream.select(channel=horizontal_chans[0])[0]
    trace_z = stream.select(channel=vertical_chan[0])[0]
    fc, C_h1v = _compute_coherence(trace_1, trace_z, windowtype, Nws, Noverlaps, return_CoherenceClass=True)
    freqmask = calc_freq_mask(fc, freqmin, freqmax)
    C_h1v_mean = np.mean(C_h1v[freqmask])
    fig = C_h1v.plot(title="BH1-BHZ Coherence", freqmask=None)

    ### checking coherence between second horizontal and vertical component, if there is no tilt it should be minimal
    trace_2 = stream.select(channel=horizontal_chans[1])[0]
    trace_z = stream.select(channel=vertical_chan[0])[0]
    fc, C_h2v = _compute_coherence(trace_2, trace_z, windowtype, Nws, Noverlaps, return_CoherenceClass=True)
    freqmask = calc_freq_mask(fc, freqmin, freqmax)
    C_h2v_mean =np.mean( C_h2v.data[freqmask])
    fig = C_h2v.plot(title="BH2-BHZ Coherence", freqmask=None)

    ### checking coherence between pressure and vertical component, if there is no tilt it should be maximal
    trace_p = stream.select(channel=pressure_chan[0])[0]
    trace_z = stream.select(channel=vertical_chan[0])[0]
    fc, C_pv = _compute_coherence(trace_p, trace_z, windowtype, Nws, Noverlaps, return_CoherenceClass=True)
    freqmask = calc_freq_mask(fc, freqmin, freqmax)
    C_pv_mean = np.mean(C_pv.data[freqmask])
    fig = C_pv.plot(title="BDH-BHZ Coherence", freqmask=None)

    print(C_horizontal_mean, C_h1v_mean, C_h2v_mean, C_pv_mean)

    plt.show()


def _opt_azimyth_tilt(azimuth, stream, freqmax, windowtype, Nws, Noverlaps):
    #stream, freqmax, windowtype, Nws, Noverlaps = params

    ### Identifying channels
    ind_tr = np.full( [2], np.nan)
    for ind, trace in enumerate(stream) :
        if trace.get_id()[-1] in ["1", "N"] :
            ind_tr[0] = ind
        elif trace.get_id()[-1] in ["2", "E"] :
            ind_tr[1] = ind
    ind_tr = ind_tr.astype(int)

    stream_out = RotateStream(0,azimuth, stream.copy())#stream.rotate(method='NE->RT', back_azimuth=azimuth)

    f, coherence = _compute_coherence(stream[ind_tr[0]], stream[ind_tr[1]], windowtype, Nws, Noverlaps, return_CoherenceClass=False)
    out = np.mean(coherence[f<freqmax])
    return(out)
