""" 
Graphical user interface built with streamlit (web-based) to quickly and easely download any seismological data from the FDSN database.
To launch it, run ``streamlit run data_downloader_streamlit.py``. It will open a window in your default web browser.
"""

import numpy as np
import obspy as obs
import obspy
from obspy.clients.fdsn import Client
from obspy import UTCDateTime
import os
import sys
from bruitfm.utils import *
import platform as pl
import datetime as dt

if __name__=="__main__":
    import streamlit as st



    st.set_page_config(layout="wide")

    def get_unique_stations(stream) :
        stations = []
        for trace in stream :
            stations.append(trace.meta["station"])
        unique_stations = np.unique(stations).tolist()
        return(unique_stations)

    if pl.system() in 'Linux' :
        path_separator = '/'
    elif pl.system() in 'Darwin' : # for MacOS
        path_separator = '/'
    elif pl.system() in 'Windows' :
        path_separator = '\\'
    else :
        raise OSError("Unsuported OS, the suported ones are Linux, MacOS and Windows, you are on : " +  pl.system())

    st.title("OBS data downloader")

    base_dir = st.text_input("Please enter the path to the download directory", value=os.getcwd())

    client_address = st.sidebar.text_input("Client address")
    network = st.sidebar.text_input("Network name")
    station = st.sidebar.text_input("Station name", help="Wildcards such as * and ? are accepted")
    loc = st.sidebar.text_input("Location number", help="Wildcards such as * and ? are accepted", value="*")
    cha = st.sidebar.text_input("channel name", help="Wildcards such as * and ? are accepted", value="B*")
    min_date = dt.datetime.today() - dt.timedelta(days=40*365)
    start_datetime = st.sidebar.date_input("Start date", min_value=min_date)
    start_daytime = st.sidebar.time_input("Start hour", value=dt.time(0,0))
    end_datetime = st.sidebar.date_input("End date", value=start_datetime, min_value=min_date)
    end_daytime = st.sidebar.time_input("End hour", value=dt.time(0,0))
    dtstart = dt.datetime.combine(start_datetime , start_daytime)
    dtend = dt.datetime.combine(end_datetime , end_daytime)
    start_time = UTCDateTime(dtstart)
    ending_time = UTCDateTime(dtend)

    split_time = st.sidebar.slider("Interval in seconds for each file", value = min((dtend - dtstart).seconds + (dtend - dtstart).days*86400, 86400), step=1, min_value=0, max_value=24*60*60, help="Select the number of seconds each download data will last. Maximum is a day.")


    level="response"

    periods=[]
    period_start = dtstart
    interval = dt.timedelta(seconds=split_time)
    while period_start < dtend:
        period_end = min(period_start + interval, dtend)
        periods.append((UTCDateTime(period_start), UTCDateTime(period_end)))
        period_start = period_end


    st.markdown("{} files will normaly be created".format(len(periods)))

    plot_all = st.checkbox("Plot times series and power spectral densities for each stream ?", value=False)
    verbose = st.checkbox("Excplicitly display errors and sucesses ?", value=True)
    if st.button("Start download") == False :
        st.stop()






    if not os.path.exists(base_dir + client_address) :
        os.mkdir(base_dir + client_address)
    if not os.path.exists(base_dir + client_address+ path_separator + network) :
        os.mkdir(base_dir + client_address + path_separator + network)
    station_dir = base_dir + client_address+ path_separator + network + path_separator + station
    if not os.path.exists(station_dir) :
        os.mkdir(station_dir)
    progress = st.progress(0.)

    streams = []
    successfuly_saved_files = []
    for indp, (start_time, ending_time) in enumerate(periods) :
        client = Client(client_address)
        station_file_name = station_dir + path_separator + station +"_"+ str(start_time).replace(":", "-") +"_"+ str(ending_time).replace(":", "-") + ".mseed"

        inventory_file_name = base_dir + client_address + path_separator + network + path_separator + network + "_"+station+"_inventory.xml"

        if os.path.exists(station_file_name) :
            stream = obspy.read(station_file_name)
            inventory = obspy.read_inventory(inventory_file_name)
            if plot_all :
                streams.append(stream)
        else :
            try :
                with st.spinner("Retrieving the inventory and data for the {}.{}.{} station period : {} : {}".format(client_address,network, station, start_time, ending_time)) :
                    inventory = client.get_stations(network=network, starttime=start_time, endtime=ending_time, level=level, station=station)
                    stream = client.get_waveforms(network=network, station=station, channel=cha, location=loc,  starttime=start_time, endtime=ending_time ).merge()
                    if plot_all :
                        streams.append(stream)

                unique_stations = get_unique_stations(stream.copy())
                for station in unique_stations :
                    current_stream = stream.select(station=station).split().merge(method=1, fill_value=0)
                    current_stream.write(station_file_name, format="MSEED")
                    if verbose :
                        st.success("Station {} for period : {} : {} saved !".format(station, start_time, ending_time))
                    successfuly_saved_files.append(station_file_name)
                inventory.write(inventory_file_name, "STATIONXML", level=level)


            except obspy.clients.fdsn.header.FDSNNoDataException as error :
                if verbose :
                    st.error("WARNING: Failed to retrieve the data for the {}.{}{} station period : {} : {}".format(client_address,network, station, start_time, ending_time))
                    with st.expander("Traceback :") :
                        st.exception(error)
                else :
                    pass
            except NotImplementedError as error :
                if verbose :
                    st.error("WARNING: Failed to retrieve the data for the {}.{}{} station period : {} : {}".format(client_address,network, station, start_time, ending_time))
                    with st.expander("Traceback :") :
                        st.exception(error)
                else :
                    pass
        progress.progress(indp/len(periods))


    progress.progress(1.)

    st.markdown("### Sucessfully saved files :")
    st.write(successfuly_saved_files)


    if plot_all :
        for indstream, valid_stream in enumerate(streams) :
            with st.spinner("Removing the instrument response and ploting the figures for stream number {}".format(indstream)) :
                valid_stream.attach_response(inventory)
                stream_corrected = RemoveStreamResponse(valid_stream.copy())
                figPSD = PlotStreamPSD_plotly(stream, Nws=500, Noverlaps=25, figsizecoeff=1 )

                fig_traces = stream_corrected.plot(handle=True, show=False, equal_scale=False)

            col1, col2 = st.columns([1,1])
            col1.pyplot(fig_traces)
            col2.plotly_chart(figPSD)


