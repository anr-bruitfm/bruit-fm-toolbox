#! /usr/bin/bash
### Author  :  SIMON REBEYROL
### Company :  IFREMER, UMR GEOOCEAN



""" 
Graphical user interface built with streamlit (web-based) to quickly and easely remove the reference channel component from the output channel signal using Bendat & Piersol methodology.
To launch it, run ``streamlit run data_denoiser_streamlit.py``. It will open a window in your default web browser.


References :
    1. https://docs.scipy.org/doc/scipy/reference/generated/scipy.signal.get_window.html#scipy.signal.get_window
    2. CRAWFORD, Wayne C. y WEBB, Spahr C.. "Identifying and Removing Tilt Noise from Low-Frequency (&lt\mathsemicolon{}{0}{.}1 Hz) Seafloor Vertical Seismic Data". Bulletin of the Seismological Society of America. 2000, vol 90, num. 4, p. 952–963.

"""




import sys
import os
os.environ["OMP_NUM_THREADS"] = "1" # export OMP_NUM_THREADS=4
os.environ["OPENBLAS_NUM_THREADS"] = "1" # export OPENBLAS_NUM_THREADS=4
os.environ["MKL_NUM_THREADS"] = "1" # export MKL_NUM_THREADS=6
os.environ["VECLIB_MAXIMUM_THREADS"] = "1" # export VECLIB_MAXIMUM_THREADS=4
os.environ["NUMEXPR_NUM_THREADS"] = "1" # export NUMEXPR_NUM_THREADS=6
import numpy as np
import obspy
import obspy.core as obs
from obspy import UTCDateTime
import matplotlib.pyplot as plt
plt.rc('font', family='stix')
plt.rcParams.update({'font.size': 16})

from tiskitpy import CleanRotator, DataCleaner, SpectralDensity
from tiskitpy import response_functions as TransferFunctions
from itertools import product
import warnings
import pandas as pd
import datetime as dt
import matplotlib.colors as mcolors

warnings.filterwarnings( "ignore", module = "matplotlib\..*" )
warnings.filterwarnings( "ignore", module = "tiskitpy\..*" )
from tqdm import tqdm

try :
    from bruitfm.utils import *
    from bruitfm.transfer_function import *#OBSWienerDataCleaner, StreamTransferFunction
    from bruitfm.preprocessing import StreamPreprocessor
    from bruitfm.signal_utils import *
except ModuleNotFoundError :
    from utils import *
    from transfer_function import *#OBSWienerDataCleaner, StreamTransferFunction
    from preprocessing import StreamPreprocessor
    from signal_utils import *
# import signal_utils as sptl
# import preprocessing as pre

if __name__=="__main__":
    import streamlit as st


    #######################################################################
    ################# Title and data location #############################
    #######################################################################
    st.set_page_config(layout="wide")
    st.title("OBS DATA DENOISER")
    indata = st.text_input("Mseed location:", value="")
    in_decim = st.text_input("Xml inventory location:", value="", help = "Optionnal, if given, basic info will be extracted like the location and elevation")
    out_cols = st.columns([5,1])
    out_fig_dir = out_cols[0].text_input("Output directory where the figures and estimated Wiener Filter are to be saved", help="Leave it blank to not save the figures")
    out_fig_ext = out_cols[1].selectbox("Extension", options=[".pdf", ".png", ".jpg", ".svg", ".eps"])
    st.markdown("---")


    #######################################################################
    ####################### Reading the files #############################
    #######################################################################
    if indata =="":
        st.stop()
    stream = obspy.read(indata).merge(method=1, fill_value=0)
    input_response_flag = False
    if in_decim != "" :
        input_response_flag = True
        inv_decim = obspy.read_inventory(in_decim, 'STATIONXML')
        stream.attach_response(inv_decim)
    else :
        inv_decim=None

    ### checking min datetime and max datetime
    for ind, tr in enumerate(stream) :
        if ind == 0 :
            mindate=tr.stats.starttime.datetime
            maxdate=tr.stats.endtime.datetime
        if mindate > tr.stats.starttime.datetime :
            mindate = tr.stats.starttime.datetime
        if maxdate < tr.stats.endtime.datetime :
            maxdate = tr.stats.endtime.datetime



    st.markdown("Choose a date range for the signal to be displayed and analysed")
    col_date_min, col_time_min, col_date_max, col_time_max = st.columns([1,1,1,1])
    startdate_trim = col_date_min.date_input("Start date:", value=mindate,  min_value=mindate, max_value=maxdate)
    enddate_trim = col_date_max.date_input("End date:", value=maxdate,  min_value=mindate, max_value=maxdate)
    starttime_trim = col_time_min.time_input("Start time:", value=mindate  )
    endtime_trim = col_time_max.time_input("End time:", value=maxdate)
    # (startdate_trim, enddate_trim) = st.slider("Time range for the display of time series:", value=(mindate, maxdate), min_value=mindate, max_value=maxdate, step=dt.timedelta(minutes=30))

    startdate_trim = datetime(startdate_trim.year, startdate_trim.month, startdate_trim.day, starttime_trim.hour, starttime_trim.minute, starttime_trim.second)
    enddate_trim = datetime(enddate_trim.year, enddate_trim.month, enddate_trim.day, endtime_trim.hour, endtime_trim.minute, endtime_trim.second)
    elapsed_day = (UTCDateTime(enddate_trim) - UTCDateTime(startdate_trim))/60/60/24

    stream = stream.trim(UTCDateTime(startdate_trim), UTCDateTime(enddate_trim))


    #######################################################################
    ################## Populating the sidebar #############################
    #######################################################################
    st.sidebar.markdown("### Data resampling")
    data_resampling = st.sidebar.checkbox("Downsample data ?", help="Will fasten up the vizualizer", value=False)
    if data_resampling :
        new_fs = st.sidebar.number_input("New sampling rate:", value=1.0)
    else :
        new_fs=np.inf

    st.sidebar.markdown("## Preprocessing options")
    detect_anomalies = st.sidebar.checkbox("Detect anomalies in the RAW data ?", help="Detect anomalies like zeros or saturated values, needs the RAW data", value=True)
    correct_instrument_response = st.sidebar.checkbox("Does the data need to be corrected from the instrument response ?", help="Please, note that it can take some times to process, espacially for broadband channels longer than an hour or shortband channels")
    if correct_instrument_response :
        seismounit = st.sidebar.selectbox("Which output unit for the seismometers ?", options=["ACC", "VEL"], help="- ACC: m/s²\n - VEL: m/s")
    dpg_factor = st.sidebar.number_input("DPG correction factor", value=1.00)
    detect_events = st.sidebar.checkbox("Detect transcient events in the stream ?", help="Please, note that it can take some times to process, espacially for broadband channels longer than an hour or shortband channels")
    ### checking min datetime and max datetime
    for ind, tr in enumerate(stream) :
        if ind == 0 :
            mindate=tr.stats.starttime.datetime
            maxdate=tr.stats.endtime.datetime
        if mindate > tr.stats.starttime.datetime :
            mindate = tr.stats.starttime.datetime
        if maxdate < tr.stats.endtime.datetime :
            maxdate = tr.stats.endtime.datetime

    # if detect_events :
    sigma_factor = st.sidebar.number_input("Sigma factor",value =3.0, help="The threshold for each channel is determined by taking the median value of the std inside the given time range, times sigma_factor.")
    st.sidebar.markdown("## Processing options")
    rotate_before = st.sidebar.checkbox("Correct tilt by rotating the data prior to the Wiener fitlering ?", value=False)

    (startdate_visu, enddate_visu) = st.sidebar.slider("Please, give a time range for the corrected stream to be displayed :", value=(mindate, maxdate), min_value=mindate, max_value=maxdate, step=dt.timedelta(minutes=30))
    (startdate_visu_sub, enddate_visu_sub) = st.sidebar.slider("Please, give a time range for the corrected stream to be displayed :", value=(startdate_visu, enddate_visu), min_value=startdate_visu, max_value=enddate_visu, step=dt.timedelta(seconds=30))

    st.sidebar.markdown("## Spectral analysis parameters")
    Na = st.sidebar.number_input("Length of the window (s)", value=900, help="This parameter is only affecting the displayed plot, not the processing")
    Nao = st.sidebar.number_input("Length of the overlap between two windows (s)", value=450, help="This parameter is only affecting the displayed plot, not the processing")
    windowtype_analyze = st.sidebar.selectbox("Taper function to use", options=available_windowtype.keys(), index=19,)
    if available_windowtype[windowtype_analyze] : ## if the window taper requires a parameter to set
        taper_params = st.sidebar.number_input("Enter the taper parameter", value=1.)
        windowtype_analyze = (windowtype_analyze, taper_params)
    else :
        windowtype_analyze = windowtype_analyze
    windowtype_analyze = ("dpss",4)


    #######################################################################
    ############## Making the input parameter layout ######################
    #######################################################################
    channel_list = [tr.stats.channel for tr in stream]


    ### default cutoff frequency calculation
    if input_response_flag :
        ### assuming the traces ar all from the same station at the same depth
        elevation = np.abs(inv_decim.get_coordinates(stream[0].get_id())["elevation"])
        ### using the frequency limit that is proposed by Bell et al. [3] eq(11)
        freqmax_default = np.sqrt(9.81/(2*np.pi*elevation))
    else :
        freqmax_default = 2e-2


    with st.expander("Stream to be processed :") :
        for tr in stream :
            st.write(tr)


    col_refchan, col_outchan = st.columns([1,1])
    ref_chan = col_refchan.selectbox("The reference channel where the noise lies", options=channel_list)
    out_chan = col_outchan.selectbox("The output channel that will be cleaned from the noise", options=channel_list)
    nb_compare = st.slider("How many different parameters you want to compare ?", value=1, max_value=5, min_value=1)
    cols_param = st.columns([1.]*nb_compare)
    Nw = []
    Noverlap = []
    windowtype_design = []
    freqmin = []
    freqmax=[]
    coherence_threshold = []
    correlation_threshold=[]
    averages=[]
    backends=[]
    loadpaths = []
    for k, col in enumerate(cols_param) :
        Nw.append(col.number_input("Length of the window (s)", value=900, key="nw{}".format((k+1)*1)))
        Noverlap.append(col.number_input("Length of the overlaps between windows (s)", value=450, key="no{}".format((k+1)*2)))
        taperfunc = col.selectbox("Taper function to use", options=available_windowtype.keys(), index=19, key="taper{}".format((k+1)*3))
        if available_windowtype[taperfunc] : ## if the window taper requires a parameter to set
            taper_params = col.number_input("Enter the taper parameter", value=4., key="taper_param{}".format((k+1)*4))
            windowtype_design.append((taperfunc, taper_params))
        else :
            windowtype_design.append(taperfunc)
        freqmin.append(col.number_input("minimum frequency", value=freqmax_default/20, key="freqmin{}".format((k+1)*5), step=1e-4, format="%e"))
        freqmax.append(col.number_input("maximum frequency", value=freqmax_default, key="freqmax{}".format((k+1)*6), step=1e-4, format="%e"))
        coherence_threshold.append(col.number_input("Coherence threshold", value=0.95, key="coh_thresh{}".format((k+1)*7)))
        correlation_threshold.append(col.number_input("Correlation threshold", value=np.nan, key="corr_thresh{}".format((k+1)*10), help="Any peer of window that does not show an absolute correlation coefficient value above this threshold in the pass-band will be remove from the analysis."))
        if np.isnan(correlation_threshold[-1]) :
            correlation_threshold[-1] = None
        averages.append(col.selectbox("What spectral average to use ?", options=["mean", "median"], key="average{}".format((k+1)*8)))
        backends.append(col.selectbox("Select a spectral computation backend:", options=["bruit-fm", "scipy", "tiskitpy"], key="backend{}".format((k+1)*9)))
        loadpaths.append(col.text_input("Pickle file from which the Wiener filter will be loaded", key="loadfile{}".format((k+1)*11)))



    #######################################################################
    ##################### PREPROCESSING PART ##############################
    #######################################################################
    ### instrument correction parameters
    obspy_flag_inv = False
    nmirror = 0

    if st.button("start denoising") :
        pass
    else :
        st.stop()

    preprocessor = StreamPreprocessor(stream, inv_decim, output_channel=out_chan, reference_channel=ref_chan)

    ### ANOMALIES DETECTION
    if detect_anomalies :
        with st.spinner("Detecting RAW anomalies"):
            preprocessor.detect_anomalies()
        with st.spinner("Removing RAW anomalies") :
            preprocessor.remove_anomalies()
    else :
        preprocessor.create_valid_data_mask()

    ### RESAMPLING THE STREAM OBJECT
    with st.spinner("Resampling data if asked or needed") :    
        if data_resampling :
            preprocessor.resample(resampling_frequency=new_fs)
        else :
            preprocessor.resample(resampling_frequency=None)

    ### INSTRUMENT RESPONSE REMOVAL PRIOR TO TRANSCIENT EVENT DETECTION
    if correct_instrument_response :
        with st.spinner("Removing the instrument responses") :
            preprocessor.remove_instrument_response(target_seis_unit=seismounit, nmirror=nmirror)
        

    ### DPG CORRECTION 
    preprocessor.dpg_correction(dpg_factor)
    preprocessor.save_stream() #saving the stream prior to the event removal
    preprocessor.update_stream_unit("M/S", "m/s²")
    ### TRANSCIENT EVENT DETECTION
    if detect_events :
        preprocessor.detect_events(freqmin=freqmin[0], freqmax=freqmax[0], sigma=sigma_factor)



    ### PLOTING THE ORIGINAL STREAM TIME SERIES AND PSD
    figs = {}
    # visu_stream = preprocessor.stream.copy()
    # visu_stream = FilterStream(visu_stream, 5, [freqmin[0], freqmax[0]], btype="bandpass")
    # figs["original_stream_plot"] = visu_stream.plot(handle=True, show=False, equal_scale=False)
    figs["original_stream_plot"] = preprocessor.plot_stream(freqmin=freqmin[0], freqmax=freqmax[0])

    with st.expander("Time series plots:"):
        st.pyplot(figs["original_stream_plot"])
        if detect_events :
            nb_event_original = count_continuous_mask(preprocessor.mask_events[out_chan])
            st.markdown("There are **{}** discovered events in the original {} trace".format(nb_event_original, out_chan))
    if detect_events :
        with st.expander("Deviation plots:") :
            figs["deviation_plot"] = preprocessor.deviations.plot_deviations(threshold=preprocessor.deviations.threshold)
            st.pyplot(figs["deviation_plot"])
    channels = [out_chan]
    figPSD, axPSD = PlotStreamPSD(preprocessor.stream,  label="Original", channels=channels, Nws=Na,Noverlaps=Nao, windowtype=windowtype_analyze, linewidth=5.0, figsizecoeff=2., color="black", average=averages[0])
    # psd_list = [preprocessor.stream.select(channel=out_chan)]
    #################################################################################
    ############################ PROCESSING PART ####################################
    #################################################################################

    title = "" #"$N_w$={}s  $N_a$={}s  $W_d$={}  $W_a$={} ".format(Nw, Na, windowtype_design, windowtype_analyze)

    results = []
    dcs = []
    stream_dced_fds = []
    Sds = []
    TFs = []
    st.markdown("## Denoising in progress...")
    progress_bar = st.progress(0.)



    for k in range(nb_compare) :

        #####################################################################
        ### denoising and reconstructing the signal
        #####################################################################
        stream = preprocessor.stream.copy()
        print(preprocessor.mask_nodata[out_chan].size)
        if rotate_before :
            with st.spinner("Correcting the tilt for set of parameters n°{}".format(k+1)):
                rotator = CleanRotator(stream, filt_band=(freqmin[k], freqmax[k]), verbose=False, uselogvar=False)
                stream = rotator.apply(stream)
                stream_rotated = stream.copy()

        with st.spinner("Wiener filtering for set of parameters n°{}".format(k+1)) :
            
            tfprocessor = StreamTransferFunction(stream, masknodata=preprocessor.mask_nodata, maskevents=preprocessor.mask_events)
            if loadpaths[k] == "" :
                tfprocessor.estimate_transfer_function(out_chan, ref_chan, windowtype_design[k], Nw[k], Noverlap[k], freqmin=freqmin[k], freqmax=freqmax[k], min_coh=coherence_threshold[k],backend=backends[k], corr_threshold=correlation_threshold[k])
                if out_fig_dir != "" :
                    tfprocessor.WienerFilter.save(out_fig_dir + "/{}_Wiener_filter_{}_days_{}.mat".format(stream[0].stats.station, elapsed_day, k))
            else :
                W = SpectralWienerFilter(load_path=loadpaths[k])
                tfprocessor.attach_transfer_function(W)
                
            tfprocessor.correct_output_channel(average=averages[k])#, force_freqmax=freqmax[k], force_freqmin=freqmin[k])#, stream=preprocessor.saved_stream)
            result = tfprocessor.get_results()

            results.append(result)
        progress_bar.progress((k+1)/nb_compare)
    progress_bar.progress(1.)


    with st.spinner("## Generating the figures") :
        print("Starting the spectral analysis", flush=True)
        colors = list(mcolors.TABLEAU_COLORS.keys())
        psd_list = [results[0].Coherence_before.psd1]
        label_list = ["Original"]
        for k in range(nb_compare):
            if rotate_before :
                figPSD, axPSD = PlotStreamPSD(stream_rotated,fig_ax = [figPSD, axPSD],  label="Tilt corrected {}".format(k), channels=channels, Nws=Na,Noverlaps=Nao,  windowtype=windowtype_analyze, correct_response=False, linestyle="dotted", linewidth=2. , color=colors[k], average=averages[k])
            figPSD, axPSD = PlotStreamPSD(results[k].corrected_stream,fig_ax = [figPSD, axPSD],  label="Denoised {}".format(k), channels=channels, Nws=Na,Noverlaps=Nao,  windowtype=windowtype_analyze, correct_response=False, linestyle="dashed", linewidth=2. , color=colors[k], average=averages[k], freqmax=freqmax[k], freqmin=freqmin[k])
            psd_list.append(results[k].Coherence_after.psd1)
            label_list.append("Corrected - {}".format(k))




    # figs["out_{}_chan_PSDs".format(out_chan)] = figPSD
    figs["out_{}_chan_PSDs".format(out_chan)] = PlotMagSpectralStats(psd_list, label_list, average="median")
    with st.expander("Output channel power spectral densities:", expanded=True) :
        st.pyplot(figs["out_{}_chan_PSDs".format(out_chan)])
        st.pyplot(figPSD)

    cols_fig = st.columns([1.]*nb_compare)
    col_exp = []
    for k in range(nb_compare) :
        
        results[k].corrected_stream = FilterStream(results[k].corrected_stream, 3, [freqmin[k], freqmax[k]], btype="bandpass")
        saved_stream = FilterStream(preprocessor.saved_stream, 3, [freqmin[k], freqmax[k]], btype="bandpass")
        # saved_stream=preprocessor.saved_stream
        corrected_trace_fig = PlotTrace(saved_stream.select(channel=out_chan)[0].trim(UTCDateTime(startdate_visu_sub),UTCDateTime(enddate_visu_sub) ), alpha=0.5, label="Bp original trace", color="tab:red", linewidth=1., percentile_maxmin=100)
        corrected_trace_fig = PlotTrace(results[k].corrected_stream.select(channel=out_chan)[0].trim(UTCDateTime(startdate_visu_sub),UTCDateTime(enddate_visu_sub)), fig=corrected_trace_fig, alpha=1., label="Bp corrected trace", color="tab:blue", linewidth=0.5)
        corrected_hist_fig = PlotTraceHistogram(saved_stream.select(channel=out_chan)[0].trim(UTCDateTime(startdate_visu_sub),UTCDateTime(enddate_visu_sub) ), alpha=0.5, label="original hist", color="tab:red", linewidth=0.5, )
        corrected_hist_fig = PlotTraceHistogram(results[k].corrected_stream.select(channel=out_chan)[0].trim(UTCDateTime(startdate_visu_sub),UTCDateTime(enddate_visu_sub)), fig=corrected_hist_fig, alpha=1., label="corrected hist", color="tab:blue")
        scatter_correction_fig = PlotTraceScatter(saved_stream.select(channel=out_chan)[0].trim(UTCDateTime(startdate_visu_sub),UTCDateTime(enddate_visu_sub) ), results[k].corrected_stream.select(channel=out_chan)[0].trim(UTCDateTime(startdate_visu),UTCDateTime(enddate_visu) ), "Original values", "Corrected values")
        db_diff = PSDdiffTrace(saved_stream.select(channel=out_chan)[0], results[k].corrected_stream.select(channel=out_chan)[0], Nws=Na,Noverlaps=Nao,  windowtype=windowtype_analyze, freqmin=freqmin[k], freqmax=freqmax[k])

        preprocessor_after_corr = StreamPreprocessor(results[k].corrected_stream.select(channel=out_chan), inv_decim)
        preprocessor_after_corr.update_stream_unit("M/S", "m/s²")
        preprocessor_after_corr.detect_events(sigma=sigma_factor)
        news_event_fig = preprocessor_after_corr.plot_stream()

        cols_fig[k].markdown("## Set of parameters n°{}".format(k))
        col_exp.append(cols_fig[k].expander("Corrected time series: "))
        col_exp[-1].pyplot(corrected_trace_fig)
        col_exp[-1].markdown("The mean difference of PSDs in the frequency range is **{} dB**".format(db_diff))
        col_exp.append(cols_fig[k].expander("Corrected histogram "))
        col_exp[-1].pyplot(corrected_hist_fig)
        col_exp.append(cols_fig[k].expander("Scatter plot Corrected vs Original "))
        col_exp[-1].pyplot(scatter_correction_fig)
        col_exp.append(cols_fig[k].expander("Newly detected events"))
        col_exp[-1].pyplot(news_event_fig)
        nb_event = count_continuous_mask(preprocessor_after_corr.mask_events[out_chan])
        col_exp[-1].markdown("There are **{}** discovered events in the corrected trace".format(nb_event))

            
        col_exp.append(cols_fig[k].expander("Transfer function frequency response: "))
        col_exp[-1].pyplot(results[k].TransferFunctionFrequencyResponse.plot(tight_layout=False,  freqmask=True))
            

        col_exp.append(cols_fig[k].expander("Original power spectral densities: "))
        col_exp[-1].markdown("### Cross spectral densities between {} and {} ".format(out_chan, ref_chan))
        col_exp[-1].pyplot(results[k].Coherence_before.cpsd.plot())
        col_exp[-1].markdown("### Power spectral density of the {} channel".format(ref_chan))
        col_exp[-1].pyplot(results[k].Coherence_before.psd2.plot())
        col_exp[-1].markdown("### Power spectral density of the {} channel".format(out_chan))
        col_exp[-1].pyplot(results[k].Coherence_before.psd1.plot())

        col_exp.append(cols_fig[k].expander("Coherence charts: "))
        col_exp[-1].markdown("### Original Coherence between {} and {} channels".format(out_chan, ref_chan))
        col_exp[-1].pyplot(results[k].Coherence_before.plot(min_coherence=coherence_threshold[k]))
        col_exp[-1].markdown("### New Coherence between {} and {} channels".format(out_chan, ref_chan))
        col_exp[-1].pyplot(results[k].Coherence_after.plot(min_coherence=coherence_threshold[k]))





