#! /usr/bin/bash
### Author  :  SIMON REBEYROL
### Company :  IFREMER, UMR GEOOCEAN


'''
This toolbox contains all the elements to preprocess a stream object.
It is structured around the class StreamPreprocessor which contains all the methods to preprocess the given stream and plot the results.

References :
- [1] https://docs.scipy.org/doc/scipy/reference/generated/scipy.signal.get_window.html#scipy.signal.get_window
- [2] CRAWFORD, Wayne C. y WEBB, Spahr C.. "Identifying and Removing Tilt Noise from Low-Frequency (&lt\mathsemicolon{}{0}{.}1 Hz) Seafloor Vertical Seismic Data". Bulletin of the Seismological Society of America. 2000, vol 90, num. 4, p. 952–963. DOI: 10.1785/0119990121
- [3] BELL, Samuel W. and FORSYTH, Donald W. and RUAN, Youyi. "Removing Noise from the Vertical Component Records of Ocean-Bottom Seismometers: Results from Year One of the Cascadia Initiative". Bulletin of the Seismological Society of America. 2015, vol 105, num. 1, p. 300–313. DOI: 10.1785/0120140054
- [4] Herbers, T. H. C. and Elgar, Steve and Guza, R. T. "Generation and propagation of infragravity waves". Journal of Geophysical Research. 1995, vol 100, num. C12, p. 24863. DOI: 10.1029/95jc02680
- [5] AN, Chao, WEI, S. Shawn, CAI, Chen y YUE, Han. "Frequency Limit for the Pressure Compliance Correction of Ocean-Bottom Seismic Data". Seismological Research Letters. 2020, vol 91, num. 2A, p. 967–976. DOI: 10.1785/0220190259
- [6] TiSKit

'''




import sys
import os
os.environ["OMP_NUM_THREADS"] = "1" # export OMP_NUM_THREADS=1
os.environ["OPENBLAS_NUM_THREADS"] = "1" # export OPENBLAS_NUM_THREADS=1
os.environ["MKL_NUM_THREADS"] = "1" # export MKL_NUM_THREADS=1
os.environ["VECLIB_MAXIMUM_THREADS"] = "1" # export VECLIB_MAXIMUM_THREADS=1
os.environ["NUMEXPR_NUM_THREADS"] = "1" # export NUMEXPR_NUM_THREADS=1
import numpy as np
import obspy
import matplotlib.pyplot as plt
import scipy.io as scio
import scipy.signal as sig 
import scipy.linalg as lin
from scipy.ndimage import binary_closing, binary_dilation, binary_erosion, binary_opening
from datetime import datetime
from obspy.core import UTCDateTime
from tqdm import tqdm
import platform as pl
import time
from pathlib import PurePath

try :
    from ..utils import *
    from .. import utils as tl
except ImportError:
    from utils import *
    import utils as tl
# import signal_utils as sptl



class StreamPreprocessor :
    """ 
    A class designed to preprocess any obspy.core.stream.Stream object with standard procedures.

    Args:
        stream  (obspy.core.stream.Stream) : the stream to be preprocessed.
        inv  (obspy.core.inventory.inventory.Inventory) : the inventory of the corresponding stream station.
        output_channel (str) : the name of the output channel top be kept, the other ones are discarded.
        reference_channel  (str) : the name of the reference channel to be kept, the other ones are discarded.
        starttime  (obspy.UTCDateTime) : the start date and time from when the stream will be cropped.
        endtime  (obspy.UTCDateTime) : the start date and time from when the stream will be cropped. 
    

    -----------
    Attributes:
    -----------

    Attributes
    ----------

        stream  (obspy.core.stream.Stream) : The stream to be preprocessed, this attribute will be updated every time a preprocessing method is called.
        original_stream (obspy.core.stream.Stream) : The original stream passed during the build, this attribute will never be updated.
        saved_stream  (obspy.core.stream.Stream) : A saved version of stream, originaly equal to original_stream, but can be updated by calling the save_stream() method.
        inv  (obspy.core.inventory.Inventory) : The given station inventory corresponding to the stream 
        mask_nodata (dict) : A dict composed of boolean numpy.ndarray where True means invalid data and which are accessed by the corresponding channel name, ex : {"BDH": ndarray, "BHZ": ndarray}
            Updated by the remove_anomalies() method
        mask_event  (dict) : A dict composed of boolean ndarray where True means detected event and which is accessed by the corresponding channel name, ex : {"BDH": ndarray, "BHZ": ndarray}
            Updated by the remove_anomalies() method
        deviations  (spectral_toolbox.StreamTimeSeriesDeviation) : A deviation object that holds the computation of sliding deviation. Is None until detect_events() is called.
        count_events  (int) : The number of detected events. Is None until detect_events() is called.

    --------
    Methods:
    --------

    Methods
    -------

    update_stream_unit(source_unit, target_unit) : A convinience method to update all the traces of the stream attribute holding source_unit to target_unit.
    remove_anomalies(verbose=True, taper_time=60) : Taper all the anomalies that can be detected.
    resample(resampling_frequency=None, verbose=True) : Resample the stream  to the given resampling frequency 
    remove_instrument_response(target_seis_unit="ACC", nmirror=None, water_level=None) : Remove the instrument response from the stream.
    dpf_correction(correction_factor, channel="*H", verbose=True) :  Apply a correction factor to the corresponding channel trace.
    detect_events(freqmin=None, freqmax=None, window_length=225, starttime_event_detection=None, endtime_event_detection=None, sigma=3., verbose=True, window_length_threshold=7200) : Detect events
    filter(N, fn, btype='lowpass', nmirror=None, verbose=True) : filter the stream with a butterworth IIR filter
    save_stream() :  update the saved_stream attribute with the current status of stream.
    get_maskevent_as_stream() : Return the mask_event attribute as a stream
    get_masknodata_as_stream() : Return the mask_nodata attribute as a stream
    plot_stream(linewidth=1., startdate=None, enddate=None, channels=None, freqmin=None, freqmax=None) : Convinient function to plot the stream attribute.

    """

    def __init__(self, stream, inv, output_channel="***", reference_channel="***", starttime=None, endtime=None) :

        """Create an instance of StreamPreprocessor which holds the stream object and
        trims it to the minimal time range detected.    
        """
        ch1 = [output_channel[0], reference_channel[0]]
        ch2 = [output_channel[1], reference_channel[1]]
        stream = select_related_channels(stream, ch1, ch2, ['*'])
        start_t, end_t = detect_minimal_time_range(stream)
        stream.trim(start_t,end_t) ### making sure every trace have the same time range

        if isinstance(starttime, UTCDateTime) :
            pass  
        elif isinstance(starttime, str) :
            starttime = UTCDateTime(starttime)
        elif isinstance(starttime, datetime) :
            starttime = UTCDateTime(starttime)
        else :
            pass

        if isinstance(endtime, UTCDateTime) :
            pass  
        elif isinstance(endtime, str) :
            endtime = UTCDateTime(endtime)
        elif isinstance(endtime, datetime) :
            endtime = UTCDateTime(endtime)
        else :
            pass
        
        if (starttime is not None) or (endtime is not None) :
            stream.trim(starttime, endtime)       

        self.stream = stream 
        if inv is not None :
            self.stream.attach_response(inv)
        self.original_stream = self.stream.copy()
        self.saved_stream = self.stream.copy()
        self.inv = inv
        self.deviations=None
        

        self.mask_nodata = mask_like(stream) 
        self.mask_events = mask_like(stream)

        

        self.count_events = None


    def update_stream_unit(self, source_unit, target_unit) :
        """Update the string of the unit matching source_unit to target_unit.
        It does not convert the data.

        Args:
            source_unit  (str) : a str that match one trace unit held in the Trace.stats.response attribute.
            target_unit  (str) : the unit towards which the Trace.stats.response attribute will be updated (not converted).
        """

        for trace in self.stream :
            try :
                if trace.stats.response.response_stages[0].input_units == source_unit :
                    trace.stats.response.response_stages[0].input_units = target_unit 
            except :
                print("Warning: no responses attached to stream")
        
        for trace in self.saved_stream :
            try :
                if trace.stats.response.response_stages[0].input_units == source_unit :
                    trace.stats.response.response_stages[0].input_units = target_unit 
            except :
                print("Warning: no responses attached to stream")

    
    def detect_anomalies(self, verbose=True) :
        """Detect the anomalies in the stream such as no data or saturated data. 
        The  mask_nodata attribute is updated.
        """
        start = time.process_time()
        mask_nodata = detect_raw_anomalies(self.stream)
        if verbose :
            print("--> Anomaly detection done ({})s".format(time.process_time() - start),flush=True)
            start = time.process_time()
        self.mask_nodata = mask_nodata
    
    def remove_anomalies(self, verbose=True, taper_time=60) :
        """Detect and remove the anomalies in the stream such as no data or saturated data. 
        The stream  attribute is updated.

        Args:
            taper_time (float) :  the amount of time in second defining the length of the smoothing gaussian core that will be convolved to the mask prior of the tapering.
        
        """
        start = time.process_time()
        ### attach the mask_nodata attribute (custom attribute)
        if verbose :
            print("--> Anomaly detection done ({})s".format(time.process_time() - start),flush=True)
            start = time.process_time()
        self.stream = taper_stream(self.stream, self.mask_nodata, dilation_time=taper_time, taper_time=taper_time)

    def create_valid_data_mask(self) :
        """Create an empty valid data mask (all data are valid).
        """
        start = time.process_time()
        mask_nodata = {}
        for tr in self.stream :
            mask_nodata[tr.stats.channel] = np.zeros(tr.stats.npts, dtype=bool)
        self.mask_nodata = mask_nodata



    def resample(self, resampling_frequency=None, verbose=True) :
        """Resample the whole stream to the target resampling frequency using a linear interpolation.

        If no resampling frequency is given, it will check if there is a non unique sampling frequency
        in the stream and if so, resample it to the detected minimal sampling frequency.
        An anti-aliasing filter is applied prior to the resampling.

        Args:

            resampling_frequency (float) : the sampling frequency to which the stream will be downsampled. Calling this method
                without giving the resampling frequency does nothing (no exception raised).
        """
        
        ### resampling data as asked by the user

        single_fs, minfs = Stream_sampling_rate_check(self.stream)
        start = time.process_time()
        if resampling_frequency is not None :
            if minfs <= resampling_frequency :
                raise ValueError("you are trying to up-sample to {}Hz data that are sampled at {}Hz.".format(minfs, resampling_frequency), flush=True)
                sys.exit(1)
            
            self.mask_events = resample_mask(self.stream, self.mask_events, resampling_frequency)
            self.stream, self.mask_nodata  = StreamResampler(self.stream, resampling_frequency, mask_nodata=self.mask_nodata, verbose=True, antialiased_order=5)
            
            if verbose:
                    print("--> Data resampled to {} Hz ({})s".format(resampling_frequency, time.process_time() - start),flush=True)
                    start = time.process_time()
                
        else :
            if not single_fs :
                ### resampling the stream traces to the minimal trace sampling rate
                self.stream = StreamResampler(self.stream, minfs, accountmask=flag["remove_anomalies"], verbose=True)
                if verbose:
                    print("--> Data resampled to {} Hz ({})s".format(minfs, time.process_time() - start),flush=True)
                    start = time.process_time()
            else :
                print("--> The data sampling frequencies don't need any harmonization.")


    def remove_instrument_response(self, target_seis_unit="ACC", nmirror=None, water_level=None) :
        """Remove the instrument response from the stream.

        Args:
            target_seis_unit (str) :  the obspy str defining to which unit the seismometer component must be converted to. Can be ACC for m/s² or VEL m/s, default = ACC.
            nmirror (int) :  the number of sample to be mirrored in the data prior to the Fourier deconvolution, default length/2
            water_level (float) :  the dB threshold under which the instrument response frequency response is clipped.              
        """
        self.stream = RemoveStreamResponse(self.stream, verbose=True, nmirror=nmirror, unit=target_seis_unit, mask_nodata=self.mask_nodata, water_level=water_level)

    def dpg_correction(self, correction_factor, channel="*H", verbose=True) :
        """Correct the pressure signal as mentionned in [5].
        Can be applied on any trace to update it.

        Args:
            correction_factor  (float)  : the correction factor .
        channel  (str) : the name of the channel of the DPG, default "*H".
        
        """
        start = time.process_time()
        stream_pressure = self.stream.select(channel=channel)
        for tr_pressure in stream_pressure :
            tr_pressure.data = tr_pressure.data * correction_factor # also affect stream variable

        
        if verbose:
            print("--> Differential pressure gauges correction factor appplied ({})s".format(time.process_time() - start),flush=True)
            start = time.process_time()

    def detect_events(self, freqmin=None, freqmax=None, window_length=225, sigma=3., verbose=True, window_length_threshold=7200) :
        """Detect events from the stream. The event are detected using the median values times sigma of a sliding STD estimation over a given timespan.
        
        Args:
            freqmin  (float) :  the minimal frequency above which the traces will be analysed.
            freqmax  (float) :  the maximum frequency under which the traces will be analysed.
            window_length  (float) : the length (s) of the sliding window in which the median absolute deviation value will be computed, default 225. 
            sigma  (float) : the coefficient factor that will be applied on the median absolute deviation to get the threshold, default 3.0.
            verbose  (bool) :make the code verbose.
            window_length_threshold  (float) : the computed median absolute deviation values will be divided into segment of length (s) window_length_threshold, default 7200.


        """

        if (freqmin is None) and (freqmax is None) :
            stream_bandpassed = self.stream.copy()
        elif (freqmin is None) and (freqmax is not None) :
            stream_bandpassed = FilterStream(self.stream.copy(), 3, freqmax, btype="lowpass", verbose=True, mask_nodata=self.mask_nodata)
        elif (freqmin is None) and (freqmax is not None) :
            stream_bandpassed = FilterStream(self.stream.copy(), 3, freqmin, btype="highpass", verbose=True, mask_nodata=self.mask_nodata)
        else :
            stream_bandpassed = FilterStream(self.stream.copy(), 3, [freqmin, freqmax], btype="bandpass", verbose=True, mask_nodata=self.mask_nodata)

        deviation = StreamTimeSeriesDeviation(stream_bandpassed, window_length, "second", compute_mad=False, compute_std=True, mask_nodata=self.mask_nodata)
        threshold = deviation.estimate_threshold_sigma(starttime=None, endtime=None, sigma_factor=sigma, window_length=window_length_threshold)
        self.mask_events = deviation.detect_events_from_stream(std_or_mad="std")
        self.deviations=deviation
        self.count_events = deviation.count_events(min_hole_size=0)



    def filter(self, N, fn, btype='lowpass', nmirror=None, verbose=True) :
        """Filter the stream with a zero-phase Butterworth filter.
        
        Args: 
            N : (int):  the order of the filter.
            fn : (float or list of float) : the corner frequency(ies).
            btype  (str) : the type of filter, can be lowpass, bandpass, highpass 
            nmirror (int) :  the number of samples that will be mirrored to avoid edge stability issues, default half the signal size.
            verbose  (bool) : make the code verbose.

        """
        self.stream = FilterStream(self.stream, N, fn, btype=btype, nmirror=nmirror, mask_nodata=self.mask_nodata, verbose=verbose)
        
    def save_stream(self) :
        """
        save the current state of the stream as a copy into the saved_stream attribute
        """
        self.saved_stream = self.stream.copy()

    def get_maskevent_as_stream(self):
        """Return the current maskevents as a stream filled with traces whose data are the values of the mask. 
        """
        maskevent_stream = self.stream.copy()
        for trace in maskevent_stream :
            try :
                trace.data[:] = self.mask_events[trace.stats.channel]
            except AttributeError :
                trace.data[:] = False
        return(maskevent_stream)

    def get_masknodata_as_stream(self):
        """Return the current masknodata as a stream filled with traces whose data are the values of the mask. 
        """
        masknodata_stream = self.stream.copy()
        for trace in masknodata_stream :
            try :
                trace.data[:] = self.mask_nodata[trace.stats.channel]
            except AttributeError :
                trace.data[:] = False
        return(masknodata_stream)

    def plot_stream(self, linewidth=1., startdate=None, enddate=None, channels=None, freqmin=None, freqmax=None, fontsize="small") :
        """Plot current stream state

        Args:

            linewidth  (float) : the width of the lines that will be passed to the pyplot function.
            startdate  (obspy.UTCDateTime or datetime.datetime ) : the date from which the plot will start to be drawn.
            enddate  (obspy.UTCDateTime or datetime.datetime ) :the date toward which the plot will stop to be drawn.
            channels  (str)  : the channels to display.
            freqmin  (float)  :  the min corner frequency, default to None (no filtering)
            freqmax  (float)  : the max corner frequency, default to None (no filtering)
        
        Output:
        -------
            - fig  (Matplotlib.Figure) : the figure instance of the plot.

        """
        if channels is None :
            stream_visu = self.stream 
        elif isinstance(channels, str) :
            
            stream_visu = self.stream.copy().select(channel=channels)
            channels = [channels]
        else :
            stream_visu = self.stream.copy().select(channel=channels)

        if (freqmin is None) and (freqmax is None) :
            pass
        elif (freqmin is None) and (freqmax is not None) :
            stream_visu = FilterStream(stream_visu, 3, freqmax, btype="lowpass", verbose=True, mask_nodata=self.mask_nodata)
        elif (freqmin is None) and (freqmax is not None) :
            stream_visu = FilterStream(stream_visu, 3, freqmin, btype="highpass", verbose=True, mask_nodata=self.mask_nodata)
        else :
            stream_visu = FilterStream(stream_visu, 3, [freqmin, freqmax], btype="bandpass", verbose=True, mask_nodata=self.mask_nodata)

        fig, ax = plt.subplots(len(stream_visu),sharex=True, sharey=False, figsize=(5*len(stream_visu), 2*len(stream_visu)), height_ratios=[1.]*len(stream_visu))
        if len(stream_visu) == 1:
            ax = [ax]
        
        if isinstance(startdate, datetime) :
            startdate = UTCDateTime(startdate)
        elif isinstance(startdate, UTCDateTime) :
            pass
        elif startdate is None :
            pass
        else :
            print("WARNING: startdate must be datetime.datetime or obspy.UTCDateTime, got {}".format(type(startdate)))

        if isinstance(enddate, datetime) :
            enddate= UTCDateTime(enddate)
        elif isinstance(enddate, UTCDateTime) :
            pass
        elif enddate is None :
            pass
        else :
            print("WARNING: enddate must be datetime.datetime or obspy.UTCDateTime, got {}".format(type(enddate)))


        
        stream_visu = stream_visu.slice(startdate, enddate)
        new_sdt, new_edt = detect_minimal_time_range(stream_visu)

        start_t, end_t = detect_minimal_time_range(self.stream)
        t=  np.arange(self.stream[0].stats.npts)*self.stream[0].stats.delta 
        original_datetimes = np.asarray([date_time for date_time in datetime_vector(self.stream[0].stats.starttime.datetime, t)])
        mask_datetime = (original_datetimes >= new_sdt.datetime) & (original_datetimes <= new_edt.datetime) 


        for ind, trace in enumerate(stream_visu) :
            nb_sec = trace.stats.endtime - trace.stats.starttime
            nbpts = trace.stats.npts
            dt =  trace.stats.delta
            t = np.arange(nbpts)*dt
            
            new_xticks = np.linspace(0,len(t)-1, 8, dtype=int, endpoint=True)
            new_t = t[new_xticks]
            new_datetimes = [date_time.replace(microsecond=0) for date_time in datetime_vector(trace.stats.starttime.datetime, new_t)]

            try :
                unit = trace.stats.response.response_stages[0].input_units
            except AttributeError :
                unit=''

            # fig.suptitle(" {} - {} ".format(trace.stats.starttime, trace.stats.starttime))

            mask_nodata = self.mask_nodata[trace.stats.channel][mask_datetime]
            mask_events = self.mask_events[trace.stats.channel][mask_datetime]
            # print(trace.get_id(), len(mask_events), trace.stats.npts, len(trace.data))
            ax[ind].set_ylabel("{}\n{}".format(trace.stats.channel, unit))
            
            ax[ind].plot(trace.data, label="Background")#, linewidth=linewidth)
            if np.sum(mask_nodata) > 0 :
                masked_data = np.full(trace.stats.npts, np.nan)
                masked_data[mask_nodata] = trace.data[mask_nodata]
                ax[ind].plot(masked_data, color="tab:red", label="Anomalies", linewidth=0.5)

            if np.sum(mask_events) > 0 :
                masked_data = np.full(trace.stats.npts, np.nan)
                masked_data[mask_events] = trace.data[mask_events]
                ax[ind].plot(masked_data, color="tab:orange", label="Events", linewidth=0.5)

                min_value = np.min(trace.data[~mask_events])
                max_value = np.max(trace.data[~mask_events])
                ax[ind].set_ylim(bottom=min_value, top=max_value)

            ax[ind].grid()
            ax[ind].set_xticks(new_xticks, new_datetimes, fontsize=fontsize)
            # if ind == len(stream_visu)-1 :
            if (np.sum(self.mask_nodata[trace.stats.channel]) > 0) or (np.sum(self.mask_events[trace.stats.channel]) > 0):
                ax[ind].legend(loc='lower center', bbox_to_anchor=[0.5,1.], fancybox=True, shadow=True, ncols=3, fontsize=fontsize)
            plt.setp(ax[ind].get_xticklabels(), rotation=15)
 
        ax[ind].set_xlabel("Date and time")
        plt.tight_layout()

        return(fig)

##########################################################################################
########################## Toolbox functions #############################################
##########################################################################################


def detect_raw_anomalies(stream, dilate_mask=False) :
    """
    Detect raw anomalies (numeral count in int) like saturation or zeros

    Args:
        stream (obspy.core.Stream.stream) : the input stream
        dilate_mask (bool) :    dilate the mask of 50 sample at each side of any detected anomalie.
                                It is useful to make sure the forecoming mask smoothing (with
                                remove_raw_anomalies) will let the anomalies stay close to zeros,
                                at the cost of loosing a little bit more data
    
    -------
    Return:
    -------

        - nodatamasks (dict of ndarray)  : the detected anomalies wrapped into a dict. Access with the name of the channel.
    """
    nodatamasks = {}

    for k, tr in enumerate(stream) :
        mask_nodata = np.abs(tr.data) < 1e-16
        
        bitlength = tr.data.dtype.itemsize*8
        max_val = 2**(bitlength -1) -1
        min_val = -2**(bitlength-1)
        ### also testing the 24 bit-length value in case the original data recorded in 24bit have been saved in 32bit....
        maxval24bit = 2**23 -1
        minval24bit = -2**23
        mask_satup = tr.data == max_val
        mask_satdown = tr.data == min_val
        mask_sat24up = tr.data == maxval24bit
        mask_sat24down = tr.data == minval24bit
        mask = mask_satup|mask_satdown|mask_sat24up|mask_sat24down|mask_nodata
        mask = binary_opening(mask, structure=np.ones(3, dtype=int), iterations=1)

        mask = binary_closing(mask, structure=np.ones(20, dtype=int), iterations=20)
        if dilate_mask :
            mask = binary_dilation(mask, structure=np.ones(51, dtype=int), iterations=1)
        tr.mask_nodata = mask
        nodatamasks[tr.stats.channel] = mask
        print("--> {} invalid values detected for channel {}".format(mask.sum(), tr.stats.channel))

    return(nodatamasks)


def taper_stream(stream, nodatamasks, dilation_time=60, taper_time=60) :
    """
    Taper each trace in the stream given the corresponding nodatamask. The taper function is generated using a Hamming window.

    Args:
        stream (obspy.core.Stream.stream) : the input stream to taper
        nodatamasks (dict of ndarrays)    : the dict accessed which keys correspond to the channel name and contain the masking ndarray where True means invalid data
        dilation_time (float) : the dilation time in seconds prior to the tapering., default = 60 seconds 
                                
    Output:
    -------
        stream (obspy.core.Stream.stream) : the output stream with each trace having a new mask_nodata  boolean array attribute attached.

    """  
    for trace in stream :
        
        current_mask = ~nodatamasks[trace.stats.channel].copy()
        if (~current_mask).sum() > 0 :
            dilation_kernel_size = int(trace.stats.sampling_rate*dilation_time)
            if dilation_kernel_size > 0 :
                dilation_kernel = np.ones(dilation_kernel_size, dtype=bool)
                print("--> Dilating the mask of channel {}".format(trace.stats.channel))
                current_mask = binary_erosion(current_mask, structure=dilation_kernel, iterations=1)
            
            smoothing_kernel_size = int(trace.stats.sampling_rate*taper_time)
            smoothing_core = sig.windows.hamming(smoothing_kernel_size)
            smoothing_core /= np.sum(smoothing_core)
            print("--> Tapering data of channel {}".format(trace.stats.channel))
            # smoothed_mask = convolve1d(np.expand_dims(current_mask.astype(float), axis=0), smoothing_core, mode="nearest", axis=-1).squeeze()
            smoothed_mask = sig.fftconvolve(current_mask.astype(float), smoothing_core, mode="same").squeeze()
            
            # smoothed_mask = np.roll(smoothed_mask, 1, axis=-1).astype(float) ## for some reason convolve1d translate the result by 1 sample
            trace.data = smoothed_mask * trace.data
        else :
            print("--> No invalid data to taper, bypassing...")
    return(stream)

def create_edge_mask(stream) :
    edge_mask = {}
    for trace in stream :
        temp_mask = np.zeros(trace.stats.npts, dtype=bool)
        temp_mask[0] = True 
        temp_mask[-1] = True
        edge_mask[trace.stats.channel] = temp_mask
    return(edge_mask)
