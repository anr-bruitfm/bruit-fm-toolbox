"""
Module dedicated to the preprocessing of obspy.cor.stream.Stream objects. 
"""

# from .preprocessing_toolbox import *
from .preprocessing_toolbox import StreamPreprocessor, taper_stream, create_edge_mask