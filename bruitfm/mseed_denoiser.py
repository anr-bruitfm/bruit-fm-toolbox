#! /usr/bin/bash
### Author  :  SIMON REBEYROL
### Company :  IFREMER, UMR GEOOCEAN


'''
Python script that allows the user to remove a noise component from a mseed trace using another mseed trace as a reference. This script uses the  transfer function method described by Bendat & Piersol (2011).
It has been developped in the context of seafloor compliance signal removal but should work for any signals provided into a mseed file.
This programm has preprocessing capabilities and can also remove tilt noise from OBS data prior to the denoising stage.

bash call : python mseed_denoiser.py  [parameters]

Notes:

    General Parameters
    ------------------

    -h, --help                      :   
                                        Show this help.

    -i, --input_file      [str]     :   
                                        The path to the  mseed file containing the data to be denoised.
                                        whose instrument response is to be removed.

    --output_dir          [str]     :   
                                        The path where the output files will be created, default is the dir of the input_file.

    -r, --input_response  [str]     :   
                                        The path to the XML file containing the station instrument response.
                                        (optionnal)
    
    Preprocessing Parameters
    ------------------------

    -c, --remove_response           :   
                                        Tell the programm to remove the instrument response given by the input_response option.
                                    
    -u, --seismometer_unit [str]    :   
                                        The target unit to which the seismometer components will be converted.
                                        can be ACC (m/²) or VEL (m/s), default ACC.
                                
    -a, --remove_anomalies          :   
                                        Tell the programm to remove anomalies in the raw data prior to the
                                        instrument response removal. The argument --remove_response must be
                                        given.

    --resampling_frequency [float]  :   
                                        The frequency to which the data will be resampled, must be less than the data sampling rate.
                                        Default: no resampling is performed.
                        

    --dpg_correction_factor [float] :   
                                        The correction factor of the differential pressure gauge, for more info, see the
                                        supplement material in [5]. Default = 1.0
                        
    --sigma_event_factor [float]    :   
                                        If given, will detect events prior to the spectral analysis using a 225-s sliding std. The sliding std is then
                                        divided into 2-hours sup-windows over which the median is calculated and multiplied by sigma_event_factor.
                                
    
    Processing Parameters
    ---------------------

    --wiener_filter_path [str]      :   
                                        Optional. The path to a saved Wiener Filter (Transfer function frequeny response) that will be load.
                                        If given, will bypass the spectral analysis.

    --output_channel      [str]     :   
                                        The name of the channel to be cleaned (must be consistence with
                                        the FDSN standard).

    --reference_channel   [str]     :   
                                        The name of the channel where the noise to be cleaned lies. (must
                                        be consistence with the FDSN standard).

    --window_type         [str]     :   
                                        The window taper type, must be in the list of available windows
                                        in the scipy.signal.window module.

    --window_taper_param  [str]     :   
                                        The parameter values required by the given window type, if none are required
                                        this option can be ommited, if multiple values are needed, they must be separated
                                        by the ";" character (e.g. 5.;3.4;2.)

    --window_length       [int]     :   
                                        The length of each window taper in seconds (default 2000s)

    --window_overlap      [int]     :   
                                        The number of overlapping seconds between two windows (default 0s)

    --freqmin           [float]     :   
                                        The lowest frequency that can be processed (default 4e-3Hz [4])

    --freqmax           [float]     :   
                                        The highest frequency that can be processed (default is calculated using eq 11
                                        of [3] if input_response is given, otherwise default is 4e-2 Hz [4])

    --force_freqlimit               :   
                                        Force the denoising process to remove noise between freqmin and freqmax despite possible bad coherency.

    --coherence_threshold [float]   :   
                                        The lowest coherence value (between 0 and 1) that will be processed.
                                        (default 0.95)
                        
    --correlation_threshold [float] :   
                                        Unbiased correlation coefficient will be assessed between each windowed channel and rejected if 
                                        abs(corr_coeff) < correlation_threshold. (default 0.)
                        
    
    --average_type  [str]           :   
                                        The type of average used to estimate the spectral densities, can be "mean" or "median",
                                        default = "median".

    --figure_only                   :   
                                        Tell the programm to save only the figures and not the processed data (useful if few disk
                                        storage is available)
    
    --tilt_correction               :   
                                        Tell the programm to make a tilt correction by rotating the data prior to the
                                        Wiener filtering. Tiskit backend is used. See [6] to install and run it.

    -v, --verbose                   :   
                                        Tell the programm to display the detailed progression infos.

    --starttime [str]               :   
                                        The date where the programm will start reading the file in UTC format. Example : "2012-09-07T12:15:00"
                                        Default to starttime of the mseed file.

    --endtime [str]                 :   
                                        The date where the programm will start reading the file in UTC format. Example : "2012-09-08T12:15:00"
                                        Default to endtime of the mseed file. One can also give a number of day to be elapsed from starttime.

    --sup_window_size [int]         :   
                                        The length of the sliding super window in which the denoising will be conducted. If not set, the super window
                                        is of the length of the mseed file (or of the difference between --endtime and --starttime). If set, the denoising 
                                        process will be conducted on each super window, thus creating a collection of result which can be used to reduce 
                                        the noise. 
    
    --stationnarity_check           :   
                                        Check stationnarity of the sliced signals using the Kwiatkowski-Phillips-Schmidt-Shin and
                                        Augmented Dickey-Fuller tests. If any corresponding slices are found to be non-stationnary, 
                                        they will be remove from the analysis.

    
    Performance Assessment Parameters 
    ---------------------------------

    --TFFR_comparator [str]         :   
                                        The path to a .mat file containing the real transfer function frequency response.

    --mseed_comparator     [str]    :   
                                        The path to the mseed file that will stand as a reference denoised data. The programm will
                                        compare the denoised data to the reference denoised data and save the resulted metrics and divergences 
                                        in the performance_comparison.csv  file. The compared mseeds must obviously share the same features.
                                
    --blind_performance_assessment [str]   :   
                                                tell the programm to blindly assess the performances. 
                            


OUTPUTS :
    a subdirectory containing the mseed files corrected from the instrument response
    within the input directory will be created.






References :
[1] https://docs.scipy.org/doc/scipy/reference/generated/scipy.signal.get_window.html#scipy.signal.get_window
[2] CRAWFORD, Wayne C. y WEBB, Spahr C.. "Identifying and Removing Tilt Noise from Low-Frequency (&lt\mathsemicolon{}{0}{.}1 Hz) Seafloor Vertical Seismic Data". Bulletin of the Seismological Society of America. 2000, vol 90, num. 4, p. 952–963. DOI: 10.1785/0119990121
[3] BELL, Samuel W. and FORSYTH, Donald W. and RUAN, Youyi. "Removing Noise from the Vertical Component Records of Ocean-Bottom Seismometers: Results from Year One of the Cascadia Initiative". Bulletin of the Seismological Society of America. 2015, vol 105, num. 1, p. 300–313. DOI: 10.1785/0120140054
[4] Herbers, T. H. C. and Elgar, Steve and Guza, R. T. "Generation and propagation of infragravity waves". Journal of Geophysical Research. 1995, vol 100, num. C12, p. 24863. DOI: 10.1029/95jc02680
[5] AN, Chao, WEI, S. Shawn, CAI, Chen y YUE, Han. "Frequency Limit for the Pressure Compliance Correction of Ocean-Bottom Seismic Data". Seismological Research Letters. 2020, vol 91, num. 2A, p. 967–976. DOI: 10.1785/0220190259
[6] TiSKit
[7] BENDAT, Julius S. y PIERSOL, Allan G.. "Chapter 6: Single-Input/Output Relationships". Random data: Analysis and Measurement Procedures. Fourth ed. New York: Wiley-Interscience. 2011.p. 407. 



'''




import sys
import os
os.environ["OMP_NUM_THREADS"] = "1" # export OMP_NUM_THREADS=1
os.environ["OPENBLAS_NUM_THREADS"] = "1" # export OPENBLAS_NUM_THREADS=1
os.environ["MKL_NUM_THREADS"] = "1" # export MKL_NUM_THREADS=1
os.environ["VECLIB_MAXIMUM_THREADS"] = "1" # export VECLIB_MAXIMUM_THREADS=1
os.environ["NUMEXPR_NUM_THREADS"] = "1" # export NUMEXPR_NUM_THREADS=1

import numpy as np
import obspy
from obspy.core import UTCDateTime

import pandas
import matplotlib.pyplot as plt
plt.rc('font', family='stix')
plt.rcParams.update({'font.size': 16})

import scipy.io as scio
from scipy.fft import fft, ifft, fftfreq, fft, ifft, fftfreq, fftshift

from itertools import product
from datetime import datetime
from tqdm import tqdm
from multiprocessing import Pool
import platform as pl
import getopt
import time
from pathlib import PurePath

try :
    from bruitfm.utils import *
    from bruitfm.transfer_function import *
    from bruitfm.signal_utils import *
    from bruitfm.preprocessing import *
except ImportError:
    from utils import *
    from transfer_function import *
    from bruitfm.signal_utils import *
    from preprocessing import *


import warnings
warnings.filterwarnings( "ignore", module = "tiskit\..*" )
warnings.filterwarnings("ignore",  module= "matplotlib\..*")

if pl.system() in 'Linux' :
    path_separator = '/'
elif pl.system() in 'Darwin' : # for MacOS
    path_separator = '/'
elif pl.system() in 'Windows' :
    path_separator = '\\'
else :
    raise OSError("Unsuported OS, the suported ones are Linux, MacOS and Windows, you are on : " +  pl.system())


if __name__ == "__main__" :

    ################################################################################
    ##################### Parameters management with getopt ########################
    ################################################################################

    shortopts = "hi:r:n:u:cavo:"
    longopts = ["help", "input_file=", "input_response=", "output_dir=", "wiener_filter_path=", "remove_response",
                "remove_anomalies","output_channel=", "reference_channel=", "window_type=",
                "window_taper_param=", "window_length=", "window_overlap=",
                "freqmin=", "freqmax=", "force_freqlimit","coherence_threshold=", "average_type=","verbose",
                "figure_only", "resampling_frequency=", "dpg_correction_factor=", "correlation_threshold=",
                "tilt_correction", "mseed_comparator=", "seismometer_unit=",
                "starttime=", "endtime=", "sup_window_size=",
                "stationnarity_check", "sigma_event_factor=", "blind_performance_assessment"]
    try:
        opts, args = getopt.gnu_getopt(sys.argv[1:], shortopts, longopts)
    except getopt.GetoptError as msg:
        #Print help information and exit
        print(msg)
        print("For help use --help")
        sys.exit(2)

    flag = {}
    ### parameter flags
    flag["input_file"] = False
    flag["input_response"] = False
    flag["remove_response"] = False
    flag["remove_anomalies"] = False
    flag["wiener_filter_path"] = False
    flag["output_channel"] = False
    flag["reference_channel"] = False
    flag["window_type"] = False
    flag["window_taper_param"] = False
    flag["window_length"] = False
    flag["window_overlap"] = False
    flag["freqmin"] = False
    flag["freqmax"] = False
    flag["force_freqlimit"] = False
    flag["coherence_threshold"] = False
    flag["correlation_threshold"] = False
    flag["average_type"] = False
    flag["resampling_frequency"] = False
    flag["dpg_correction_factor"] = False
    flag["tilt_correction"] = False
    flag["verbose"] = False
    flag["mseed_comparator"] = False
    flag["starttime"] = False 
    flag["endtime"] = False 
    flag["stationnarity_check"] = False
    flag["seismometer_unit"] = False
    flag["output_dir"] = False
    flag["sigma_event_factor"] = False
    flag["blind_performance_assessment"] = False
    ### induced flags
    flag["need_param_taper"] = False
    flag["figure_only"] = False
    flag["sup_window_size"] = False

    ### Cheking inputs parameters
    for o, a in opts:
        if o in ("-h", "--help"):
            print(__doc__)
            sys.exit(0)

        elif o in ("-i", "--input_file"):
            flag["input_file"] = True
            input_file = a
            input_dir = path_separator.join(input_file.split(path_separator)[:-1]) + path_separator 

        elif o in ("--output_dir") :
            flag["output_dir"] = True 
            output_dir = a

        elif o in ("-r", "--input_response"):
            flag["input_response"] = True
            input_response = obspy.read_inventory(a)

        elif o in ("-u", "--seismometer_unit") :
            flag["seismometer_unit"] = True 
            seismometer_unit = a

        elif o in ("-c", "--remove_response") :
            flag["remove_response"] = True

        elif o in ("-a", "--remove_anomalies") :
            flag["remove_anomalies"] = True

        elif o in ("-n", "--wiener_filter_path"):
            flag["wiener_filter_path"] = True
            wiener_filter_path = a

        elif o in ("--output_channel"):
            flag["output_channel"] = True
            output_channel = a

        elif o in ("--reference_channel"):
            flag["reference_channel"] = True
            reference_channel = a

        elif o in ("--window_type"):
            flag["window_type"] = True
            try :
                flag["need_param_taper"] = available_windowtype[a]
            except KeyError :
                raise ValueError("Unkown window taper type, available window taper types are {}.".format(available_windowtype.keys()))
            window_type = a

        elif o in ("--window_taper_param"):
            flag["window_taper_param"] = True
            window_taper_param = [float(k) for k in a.split(";") ]

        elif o in ("--window_length"):
            flag["window_length"] = True
            window_length = int(a)
            if window_length < 0 :
                raise ValueError("the window length should not be negative")

        elif o in ("--window_overlap"):
            flag["window_overlap"] = True
            window_overlap = int(a)
            if window_overlap < 0 :
                raise ValueError("the window overlap value should not be negative")

        elif o in ("--freqmin"):
            flag["freqmin"] = True
            freqmin = float(a)
            if freqmin < 0 :
                raise ValueError("the minimum frequency value should not be negative")

        elif o in ("--freqmax"):
            flag["freqmax"] = True
            freqmax = float(a)
            if freqmax < 0 :
                raise ValueError("the maximum frequency value should not be negative")

        elif o in ("--force_freqlimit") :
            flag["force_freqlimit"] = True

        elif o in ("--coherence_threshold"):
            flag["coherence_threshold"] = True
            coherence_threshold = float(a)
            if (coherence_threshold < 0) or (coherence_threshold >= 1):
                raise ValueError("the coherence threshold value should be in [0 and 1[")
        
        elif o in ("--correlation_threshold") :
            flag["correlation_threshold"] = True
            correlation_threshold = float(a)
            if (correlation_threshold < 0) or (correlation_threshold >= 1):
                raise ValueError("the correlation threshold value should be in [0 and 1[")

        elif o in ("--average_type") :
            flag["average_type"] = True 
            average_type = a 
            

        elif o in ("--figure_only") :
            flag["figure_only"] = True

        elif o in ("--verbose", "-v") :
            flag["verbose"] = True

        elif o in ("--resampling_frequency") :
            flag["resampling_frequency"] = True
            resampling_frequency = float(a)

        elif o in ("--dpg_correction_factor") :
            flag["dpg_correction_factor"] = True
            dpg_correction_factor = float(a)

        elif o in ("--tilt_correction") :
            flag["tilt_correction"] = True
            from tiskitpy import CleanRotator, DataCleaner

        elif o in ("--mseed_comparator") :
            flag["mseed_comparator"] = True 
            reference_stream_path = a
            reference_stream = obspy.read(reference_stream_path)

        
        elif o in ("--starttime") :
            if a not in ["None", "none", "NONE"] : 
                flag["starttime"] = True
                starttime = UTCDateTime(a) 

        elif o in ("--endtime") :
            flag["endtime"] = True
            try :
                endtime = UTCDateTime(a)
            except ValueError :
                endtime = int(a)
            except TypeError :
                endtime = int(a)

        elif o in ("--sup_window_size"):
            flag["sup_window_size"] = True 
            sup_window_size = int(a)


        elif o in ("--stationnarity_check") :
            flag["stationnarity_check"] = True

        elif o in ("--sigma_event_factor") :
            flag["sigma_event_factor"] = True
            sigma_event_factor = float(a)

        elif o in ("--blind_performance_assessment") :
            flag["blind_performance_assessment"] = True 

    ### Dealing with default parameters



    if not flag["input_response"]  :
        input_response = None

    if not flag["seismometer_unit"] :
        seismometer_unit = "ACC"

    if flag["remove_response"] and not flag["input_response"] :
        print("INPUT ERROR: no input response given but found the option --remove_response. You must provide an input response, see help with -h.", flush=True)
        sys.exit(1)

    if not flag["window_taper_param"]  :
        window_taper_param = []

    if not flag["window_type"] :
        window_type = "dpss"
        window_taper_param = [4.]
        flag["need_param_taper"] = True
        flag["window_type"] = True
        flag["window_taper_param"] = True

    if not flag["window_length"] :
        window_length = 2000

    if not flag["window_overlap"] :
        window_overlap = 0

    if not flag["freqmax"] :
        freqmax = 4e-2

    if not flag["freqmin"] :
        freqmin = 4e-3

    if not flag["dpg_correction_factor"] :
        dpg_correction_factor = 1.



    ### Dealing with input errors
    if not flag["input_file"]  :
        print("INPUT ERROR: you must provide the input folder, see help with -h.", flush=True)
        sys.exit(1)

    if not flag["output_channel"] :
        print("INPUT ERROR: you must provide the output channel name, see help with -h.", flush=True)
        sys.exit(1)

    if not flag["reference_channel"] :
        print("INPUT ERROR: you must provide the reference channel name, see help with -h.", flush=True)
        sys.exit(1)
    # if flag["remove_anomalies"] and not flag["remove_response"] :
    #     print("INPUT ERROR: --remove_anomalies argument given but not --remove_response, see help with -h.", flush=True)
    #     sys.exit(1)

    if flag["need_param_taper"] and (not flag["window_taper_param"]) :
        print("INPUT ERROR: the window taper type you selected need a set of parameter values, see scipy.signal.get_window() documentation.", flush=True)
        sys.exit(1)

    if not flag["coherence_threshold"] :
        coherence_threshold = 0.95

    if not flag["correlation_threshold"] :
        correlation_threshold = 0.

    if not flag["average_type"] :
        average_type = "median"

    if window_length <= window_overlap :
        raise ValueError("The window length must be longer than the window overlap.")

    if freqmax <= freqmin :
        raise ValueError("The maximum frequency value must be higher than the minimum frequency value.")



    ################################################################################
    ################################################################################
    ################################################################################



    start_process_time = time.process_time()
    # ind, input_file,  output_channel, reference_channel, window_type, window_taper_param, window_length, Noverlaps, freqmin, freqmax,  min_coh, flag, input_response, dpg_factor, rms_threshold_out, rms_threshold_ref = input_list
    if flag["verbose"] :
        print("Starting the preprocessing of file {}".format(input_file),flush=True)
        start = time.process_time()


    stream = obspy.read(input_file)
    print(stream, flush=True)


    if not flag["starttime"] :
        starttime = stream[0].stats.starttime
    if not flag["endtime"] :
        endtime = stream[0].stats.endtime
    else :
        print(endtime)
        if isinstance(endtime, int) :
            endtime = starttime + endtime*24*60*60
            print("EndTime : {}".format(endtime), flush=True)
    if flag["starttime"] or flag["endtime"] :
        stream.trim(starttime=starttime, endtime=endtime)
        if len(stream) == 0 :
            print("ERROR: The given starttime-enddtime is not contained in the stream period")
            sys.exit(1)
    if flag["verbose"] :
        print("--> file read in {}s".format(time.process_time() - start),flush=True)
        start = time.process_time()

    ### default cutoff frequency calculation (for compliance only)
    if flag["input_response"] and not flag["freqmax"] :
        ### assuming the traces ar all from the same station at the same depth
        elevation = np.abs(input_response.get_coordinates(stream[0].get_id())["elevation"])
        ### using the frequency limit that is proposed by Bell et al. [3] eq(11)
        freqmax = np.sqrt(9.81/(2*np.pi*elevation))
    if not flag["freqmin"] :
        ### assuming the compliance-related signal spans a decade
        freqmin = freqmax/10.

    ########################################################################################################
    ###################################### Preprocessing data ##############################################
    ########################################################################################################


    preprocessor = StreamPreprocessor(stream.copy(), input_response, output_channel=output_channel, reference_channel=reference_channel)

    if seismometer_unit == "ACC" :
        preprocessor.update_stream_unit("M/S", "M/S²")

    ### ANOMALIES DETECTION
    if flag["remove_anomalies"] :
        preprocessor.detect_anomalies()
        preprocessor.remove_anomalies()

    ### RESAMPLING THE STREAM OBJECT
    if flag["resampling_frequency"] :
        preprocessor.resample(resampling_frequency=resampling_frequency)
    else :
        preprocessor.resample(resampling_frequency=None)

    ### INSTRUMENT RESPONSE REMOVAL PRIOR TO TRANSCIENT EVENT DETECTION
    if flag["remove_response"] :
        preprocessor.remove_instrument_response(target_seis_unit=seismometer_unit)

    ### DPG CORRECTION 
    if flag["dpg_correction_factor"] :
        preprocessor.dpg_correction(dpg_correction_factor)

    ### TRANSCIENT EVENT DETECTION
    if flag["sigma_event_factor"]  :
        preprocessor.detect_events(freqmin=freqmin, freqmax=freqmax, sigma=sigma_event_factor, window_length_threshold=7200)
        
    maskevents_stream = preprocessor.get_maskevent_as_stream()
    masknodata_stream = preprocessor.get_masknodata_as_stream()

    stream_preprocessed = preprocessor.stream
    ########################################################################################################
    ################################## End of preprocessing data ###########################################
    ########################################################################################################
    if flag["sup_window_size"] :
        collection = OBSWienerDataCleanerResultCollection(sup_window_size)
        
    else :
        sup_window_size = endtime - starttime
    cleaned_traces = []
    original_traces = []

    columns = [ "startdate denoising", "enddate denoising", 
                "startdate Wiener filter", "enddate Wiener filter", 
                "fmin", "fmax", "min coherence",
                "std original", "std corrected", "mad original", "mad corrected",
                "mean dB difference","max dBdifference", "detected events original", "detected events corrected"]

    blind_performance_pdf = pandas.DataFrame(columns=columns)
    ########################################################################################################
    ###################################### PROCESSING DATA #################################################
    ########################################################################################################

    max_seconds = endtime - starttime
    max_supwindows = int(np.floor(max_seconds/sup_window_size))

    crop_startdates = (crop_startdate for crop_startdate in np.repeat(starttime, max_supwindows) + np.arange(max_supwindows)*sup_window_size)
    crop_enddates = (crop_startdate for crop_startdate in np.repeat(starttime, max_supwindows) + np.arange(1,max_supwindows+1)*sup_window_size)

    # for k, dates in enumerate(zip(crop_startdates, crop_enddates)):#, maskevents_stream.slide(sup_window_size, sup_window_size, include_partial_windows=False), masknodata_stream.slide(sup_window_size, sup_window_size, include_partial_windows=False))): #stream_preprocessed.slide(sup_window_size, sup_window_size, include_partial_windows=True, nearest_sample=False)
    for k, streams in enumerate(zip(stream_preprocessed.slide(sup_window_size, sup_window_size, include_partial_windows=True, nearest_sample=False), maskevents_stream.slide(sup_window_size, sup_window_size, include_partial_windows=False), masknodata_stream.slide(sup_window_size, sup_window_size, include_partial_windows=False))):

        stream = streams[0].copy()
        current_event_stream = streams[1]
        current_nodata_stream = streams[2]
        # stream = stream_preprocessed.slice(dates[0], dates[1],nearest_sample=False)
        mask_edges = create_edge_mask(stream)
        stream_to_correct = stream.copy()
        stream = taper_stream(stream, mask_edges )
        
        # current_event_stream = maskevents_stream.copy().trim(dates[0], dates[1])
        # current_nodata_stream = masknodata_stream.copy().trim(dates[0], dates[1])

        starttime = stream[0].stats.starttime 
        endtime = stream[0].stats.endtime


        print("\nProcessing super window nb {}, from {} to {}".format(k+1, starttime, endtime), flush=True)

        if flag["verbose"] :
            print("Starting the processing of file {}".format(input_file),flush=True)
            start = time.process_time()

        ### TILT CORRECTION
        if flag["tilt_correction"] :

            if flag["verbose"] :
                print("--> Removing tilt",flush=True)
                start = time.process_time()

            azimuths, angles = [], []
            starttime_tilt = []
            ### slicing the stream to ensure consistent results
            starttime_tilt.append(str(stream[0].stats.starttime))
            for k, wst in tqdm(enumerate(stream.slide(window_length=450., step=450., include_partial_windows=True) ), ):
                try :
                    rotator = CleanRotator(wst, filt_band=(freqmin/100, freqmax/10), verbose=False, uselogvar=False, remove_eq=False)
                    azimuths.append(rotator.azimuth)
                    angles.append(rotator.angle)
                except ValueError as err:
                    print("WARNING : ValueError raised during the tilt correction, skipping : {}".format(err))
                    azimuths.append(np.nan)
                    angles.append(np.nan)

            azimuths = np.asarray(azimuths)
            angles = np.asarray(angles)
            rotator.azimuth = np.median(azimuths[~np.isnan(azimuths)])
            rotator.angle = np.median(angles[~np.isnan(angles)])
            stream = rotator.apply(stream.copy())
            if flag["verbose"] :
                print("--> median tilt removed ({})s".format(reference_channel, time.process_time() - start),flush=True)
                start = time.process_time()
        else :
            azimuths, angles, starttime_tilt = None, None, None
        tilt_params = [azimuths, angles, starttime_tilt]
        ##############################

        ### DENOISING
        mask_events = {}
        for trace_events in current_event_stream :
            mask_events[trace_events.stats.channel] = trace_events.data.astype(bool)

        mask_nodata = {}
        for trace_nodata in current_nodata_stream :
            mask_nodata[trace_nodata.stats.channel] = trace_nodata.data.astype(bool)

        tfprocessor = StreamTransferFunction(stream, maskevents=mask_events, masknodata=mask_nodata)

        if flag["wiener_filter_path"] :
            W = SpectralWienerFilter(load_path=wiener_filter_path)
            tfprocessor.attach_transfer_function(W)
        else :
            if flag["verbose"] :
                print("Starting the Wiener filter estimation on channel {} with channel {} as noise reference".format(output_channel, reference_channel),flush=True)
                start = time.process_time()
            tfprocessor.estimate_transfer_function(output_channel, reference_channel, 
                                                tuple([window_type,] + window_taper_param), 
                                                window_length, window_overlap, freqmin=freqmin, freqmax=freqmax, 
                                                min_coh=coherence_threshold,backend="bruit-fm", corr_threshold=correlation_threshold)


        if flag["verbose"] :
                print("Starting the Wiener filtering on channel {} with channel {} as noise reference".format(output_channel, reference_channel),flush=True)
                start = time.process_time()

        if flag["force_freqlimit"] :
            forced_freqmin, forced_freqmax = freqmin, freqmax 
        else :
            forced_freqmin, forced_freqmax = None, None
        tfprocessor.correct_output_channel(stream=stream, average=average_type, force_freqmax=forced_freqmax, force_freqmin=forced_freqmin)
        results = tfprocessor.get_results()

    ########################################################################################################
    ################################## END OF PROCESSING DATA ##############################################
    ########################################################################################################

    ########################################################################################################
    ################################ SAVING PERFORMANCES AND FIGURES #######################################
    ########################################################################################################

        if not results.error_flag  :

            ### conditionning the original and corrected traces
            preprocessor_before_corr = StreamPreprocessor(stream.select(channel=output_channel).copy(), input_response)
            preprocessor_before_corr.update_stream_unit("M/S", "m/s²")
            preprocessor_before_corr.filter(5, [freqmin,freqmax], btype="bandpass")

            preprocessor_after_corr = StreamPreprocessor(results.corrected_stream.select(channel=output_channel).copy(), input_response)
            preprocessor_after_corr.update_stream_unit("M/S", "m/s²")
            preprocessor_after_corr.filter(5, [freqmin,freqmax], btype="bandpass")

            cleaned_st = results.corrected_stream
            cleaned_tr = cleaned_st.select(channel=output_channel)[0]
            cleaned_traces.append(cleaned_tr.copy())
            original_traces.append(stream.select(channel=output_channel)[0])
            fs_tr = cleaned_tr.stats.sampling_rate

            ### Creating PSD figs
            fig_psd, ax_psd = PlotStreamPSD(stream, label="Original", Nws=results.TransferFunctionFrequencyResponse.Nw*fs_tr, 
                                        Noverlaps=results.TransferFunctionFrequencyResponse.Noverlap*fs_tr, channels=["BHZ"],
                                        windowtype=results.TransferFunctionFrequencyResponse.windowtype, average="mean")
            fig_psd = PlotStreamPSD(cleaned_st, fig_ax=(fig_psd, ax_psd ), label="Corrected", Nws=results.TransferFunctionFrequencyResponse.Nw*fs_tr, 
                                        Noverlaps=results.TransferFunctionFrequencyResponse.Noverlap*fs_tr, channels=["BHZ"],
                                        windowtype=results.TransferFunctionFrequencyResponse.windowtype, average="mean", freqmin=freqmin, freqmax=freqmax)[0]

            ### Creating PSD figs
            figtrace = PlotTrace(preprocessor_before_corr.stream.select(channel=output_channel)[0], color="tab:red", label="Original")
            figtrace = PlotTrace(preprocessor_after_corr.stream.select(channel=output_channel)[0], fig=figtrace,  color="tab:blue", label="Corrected", linewidth=0.7)

            zoomed_starttime = starttime + (endtime-starttime)*0.49
            zoomed_endtime = starttime + (endtime-starttime)*0.51
            zoomed_figtrace = PlotTrace(preprocessor_before_corr.stream.select(channel=output_channel)[0].slice(zoomed_starttime, zoomed_endtime), color="tab:red", label="Original")
            zoomed_figtrace = PlotTrace(preprocessor_after_corr.stream.select(channel=output_channel)[0].slice(zoomed_starttime, zoomed_endtime), fig=zoomed_figtrace, color="tab:blue", label="Corrected")

            add_figs = [fig_psd, figtrace, zoomed_figtrace]
            add_names = ["original_VS_corrected_PSD", "original_VS_corrected_trace", "zoomed_original_VS_corrected_trace"]
            if flag["mseed_comparator"] :
                from metrics_toolbox import *
                if flag["verbose"] :
                    print("Comparing original and estimated denoised streams:")
                    print("--> Spectral Angle calculation")
                sa = SAM(results.corrected_stream.select(channel=output_channel)[0].data,
                            reference_stream.select(channel=output_channel)[0].data, radian=False )
                if flag["verbose"] :
                    print("--> Normalized Root Mean Square Error calculation")
                nrmse = NRMSE(results.corrected_stream.select(channel=output_channel)[0].data,
                            reference_stream.select(channel=output_channel)[0].data )
                if flag["verbose"] :
                    print("--> Spectral Information Divergence calculation")
                sid = SID(results.corrected_stream.select(channel=output_channel)[0].data,
                            reference_stream.select(channel=output_channel)[0].data )

                ### computing the divergence between the mean and median estimate
                sid_tffrs = SID(np.abs(results.TransferFunctionFrequencyResponse["median"]), np.abs(results.TransferFunctionFrequencyResponse["mean"]), bandmask=results.TransferFunctionFrequencyResponse.freqmask)
                sid_tffrs += SID(np.angle(results.TransferFunctionFrequencyResponse["median"]), np.angle(results.TransferFunctionFrequencyResponse["mean"]), bandmask=results.TransferFunctionFrequencyResponse.freqmask)

                ### computing the mean coherence and std in the corresponding frequency range 
                freqmask_coh = (results.Coherence_before.f > freqmin) & (results.Coherence_before.f < freqmax)
                mean_cohb = np.mean(results.Coherence_before.data[freqmask_coh])
                std_cohb  = np.std(results.Coherence_before.data[freqmask_coh])
                mean_coha = np.mean(results.Coherence_after.data[freqmask_coh])
                std_coha  = np.std(results.Coherence_after.data[freqmask_coh])

            #########################################
            ##### BLIND PERFORMANCE ASSESSMENT
            #########################################
            if flag["blind_performance_assessment"] :

                ### computing deviations
                current_corrected_std, current_corrected_mad = trace_deviations(preprocessor_after_corr.stream.select(channel=output_channel)[0])
                current_original_std, current_original_mad = trace_deviations(preprocessor_before_corr.stream.select(channel=output_channel)[0])


                ### counting events
                if flag["sigma_event_factor"] :
                    preprocessor_before_corr.detect_events(sigma=sigma_event_factor)
                    current_original_nbevents = preprocessor_before_corr.count_events[output_channel]
                    preprocessor_after_corr.detect_events(sigma=sigma_event_factor)
                    current_corrected_nbevents = preprocessor_after_corr.count_events[output_channel]
                else :
                    current_original_nbevents = np.nan
                    current_corrected_nbevents = np.nan

                ### assessing the mean dB difference between the psds
                current_mean_dBDiff, current_max_dBDiff = PSDdiffTrace(stream.select(channel=output_channel)[0], cleaned_st.select(channel=output_channel)[0], freqmin=freqmin, freqmax=freqmax)

                current_perf_rows = [starttime, endtime, tfprocessor.WienerFilter.startdate, tfprocessor.WienerFilter.enddate,
                                    freqmin, freqmax, tfprocessor.WienerFilter.min_coherence, 
                                    current_original_std, current_corrected_std, current_original_mad, current_corrected_mad,
                                    current_mean_dBDiff,current_max_dBDiff, current_original_nbevents, current_corrected_nbevents]

                blind_performance_pdf.loc[len(blind_performance_pdf)] = current_perf_rows

            

            

                

        if not results.error_flag  :     
            ### creating the output directory
            dir_name = "Estimation_results_{}_{}_{}_{}_{}_{}_{}_{}_from_{}_to_{}".format(output_channel, 
                                                                                            reference_channel, 
                                                                                            results.TransferFunctionFrequencyResponse.windowtype, 
                                                                                            results.TransferFunctionFrequencyResponse.Nw*results.TransferFunctionFrequencyResponse.f.max()*2, 
                                                                                            results.TransferFunctionFrequencyResponse.Noverlap*results.TransferFunctionFrequencyResponse.f.max()*2, 
                                                                                            freqmin, 
                                                                                            freqmax, coherence_threshold, str(starttime).replace(":", "-"), str(endtime).replace(":", "-"))
            if flag["output_dir"] :
                pass
            else: 
                output_dir = input_dir 
            
            mseed_output_file_path = output_dir + dir_name + path_separator + "Cleaned_" +input_file.split(path_separator)[-1]

            os.makedirs(output_dir + dir_name, exist_ok=True)
            # os.makedirs(figure_dir, exist_ok=True)

            param_file = open(output_dir + dir_name + path_separator + "param_file.csv", "w")
            param_file.write("Parameter name ; Value\n")
            for o, a in opts:
                param_file.write("{} ; {} \n".format(o,a))

            ### default units, will be updated below if possible
            output_unit="numeral count"
            reference_unit="numeral count"
            results.savefig(output_dir + dir_name + path_separator, additionnal_figs=add_figs, additionnal_fig_names=add_names)
            

            if not flag["figure_only"] :
                ### saving the cleaned stream
                
                cleaned_st.write(output_dir + dir_name + path_separator + "Cleaned_" +input_file.split(path_separator)[-1])

            if flag["mseed_comparator"]:
                output_file_parameters = output_dir + dir_name + path_separator + "performance_comparison.csv"

                file_out = open(output_file_parameters, "w")
                if not out_exists :
                    first_line = "{};{};{};{};{};{};{};{};{};{};{};{};{};{}\n".format("Mseed file", "window type", "window parameter", "window length", "window overlap", "average_type",
                                                                    "SA", "NRMSE", "SID", "SID_TFFR", "mean Coherence before", "std Coherence before",
                                                                    "mean Coherence after", "std Coherence after")
                    file_out.write(first_line)

                result_line = "{};{};{};{};{};{};{};{};{};{};{};{};{};{}\n".format(mseed_output_file_path, window_type, window_taper_param, window_length, window_overlap, average_type,
                                                                    sa, nrmse, sid, sid_tffrs, mean_cohb, std_cohb,
                                                                    mean_coha, std_coha)
                file_out.write(result_line)
                file_out.close()
            
            ### checking the channel units
            if flag["input_response"] :
                output_unit = cleaned_st.select(channel=output_channel)[0].stats.response.response_stages[0].input_units
                reference_unit = cleaned_st.select(channel=reference_channel)[0].stats.response.response_stages[0].input_units
                tffr_unit = r"$\frac{"+output_unit+"}{"+reference_unit+"}$"
            else :
                tffr_unit = r""
            ### saving the transfer function, the frequency axis and the coherences values
            current_dict = {"W_median":results.TransferFunctionFrequencyResponse["median"],
                            "W_mean":results.TransferFunctionFrequencyResponse["mean"],
                            "Coherence_after":results.Coherence_after.data,
                            "Coherence_before":results.Coherence_before.data,
                            "f":results.TransferFunctionFrequencyResponse.f,
                            "mask":results.TransferFunctionFrequencyResponse.freqmask,
                            "tffr_unit":tffr_unit,
                            "starttime":str(starttime),
                            "endtime":str(endtime)}
            if flag["tilt_correction"] :
                current_dict["tilt_azimuth"] = tilt_params[0]
                current_dict["tilt_angle"] = tilt_params[1]
                current_dict["tilt_starttime"] = tilt_params[2]
            saved_matfile = output_dir + dir_name + path_separator + (input_file.split(path_separator)[-1]).replace(".mseed", "_results.mat")
            saved_tffr = output_dir + dir_name + path_separator + (input_file.split(path_separator)[-1]).replace(".mseed", "_TFFR.mat")
            scio.savemat(saved_matfile, current_dict, oned_as="column")
            results.TransferFunctionFrequencyResponse.save(saved_tffr)

            param_file.close()
            results.compress()

            if flag["sup_window_size"] :
                collection.append(results)




    ###############################################################################################
    ################################### STACKING THE RESULTS ######################################
    ###############################################################################################
    if flag["sup_window_size"] :

        dir_name_stack = "STACK_results_{}_{}_{}_{}_{}_{}_{}_{}_{}_from_{}_to_{}_{}_samples_{}_s".format(output_channel, reference_channel, str(window_type).replace("[", "").replace("]",""), str(window_taper_param).replace("[", "").replace("]",""), window_length, window_overlap, freqmin, freqmax, coherence_threshold, collection.startdate, collection.enddate, len(collection.collection), sup_window_size).replace(":", "-")
        
        os.makedirs(output_dir + dir_name_stack, exist_ok=True)
        os.makedirs(output_dir + dir_name_stack + path_separator + "pdf", exist_ok=True)
        os.makedirs(output_dir + dir_name_stack + path_separator + "png", exist_ok=True)
        os.makedirs(output_dir + dir_name_stack + path_separator + "svg", exist_ok=True)

        if flag["blind_performance_assessment"] :
            mean = blind_performance_pdf.mean(numeric_only=True)
            std = blind_performance_pdf.std(numeric_only=True) 
            sum_ = blind_performance_pdf.sum(numeric_only=True)
            blind_performance_pdf.loc["mean"] = mean
            blind_performance_pdf.loc["std"] = std
            blind_performance_pdf.loc["sum"] = sum_

            blind_performance_pdf.to_csv(output_dir + path_separator + dir_name_stack.replace("STACK_results_", "BLIND_PERFORMANCE_") + ".csv")
        
        stream_corrected_traces = obspy.core.stream.Stream(traces=cleaned_traces)
        stream_corrected_traces.write(output_dir + dir_name_stack + path_separator + "Corrected_traces.mseed")
        stream_original_traces = obspy.core.stream.Stream(traces=original_traces)
        stream_original_traces.write(output_dir + dir_name_stack + path_separator + "Original_traces.mseed")

        collection.stack_collection(median_or_mean="mean")
        collection.save(output_dir + dir_name_stack + path_separator +"mean_stack_mean_{}_samples_from_{}_to_{}.mat".format(len(collection.collection), collection.startdate, collection.enddate).replace(":", "-"))

        figure_collection_mean = collection.plot()
        figure_spectro_mean = collection.spectrogram(reject_bad_sup_windows=False, freqmask=True)
        figure_collection_mean.savefig(output_dir + dir_name_stack + path_separator + "pdf" + path_separator +"stack_mean_{}_samples_from_{}_to_{}.pdf".format(len(collection.collection), collection.startdate, collection.enddate).replace(":", "-"))
        figure_spectro_mean.savefig(output_dir + dir_name_stack + path_separator + "pdf" + path_separator +"spectro_mean_{}_samples_from_{}_to_{}.pdf".format(len(collection.collection), collection.startdate, collection.enddate).replace(":", "-"))
        figure_collection_mean.savefig(output_dir + dir_name_stack + path_separator + "svg" + path_separator +"stack_mean_{}_samples_from_{}_to_{}.svg".format(len(collection.collection), collection.startdate, collection.enddate).replace(":", "-"))
        figure_spectro_mean.savefig(output_dir + dir_name_stack + path_separator + "svg" + path_separator +"spectro_mean_{}_samples_from_{}_to_{}.svg".format(len(collection.collection), collection.startdate, collection.enddate).replace(":", "-"))
        figure_collection_mean.savefig(output_dir + dir_name_stack + path_separator + "png" + path_separator +"stack_mean_{}_samples_from_{}_to_{}.png".format(len(collection.collection), collection.startdate, collection.enddate).replace(":", "-"))
        figure_spectro_mean.savefig(output_dir + dir_name_stack + path_separator + "png" + path_separator +"spectro_mean_{}_samples_from_{}_to_{}.png".format(len(collection.collection), collection.startdate, collection.enddate).replace(":", "-"))

        figure_spectro_mean_coh = collection.spectrogram_coherence(reject_bad_sup_windows=False, freqmask=True)
        figure_spectro_mean_coh.savefig(output_dir + dir_name_stack + path_separator + "pdf" + path_separator +"coherence_{}_samples_from_{}_to_{}.pdf".format(len(collection.collection), collection.startdate, collection.enddate).replace(":", "-"))
        figure_spectro_mean_coh.savefig(output_dir + dir_name_stack + path_separator + "svg" + path_separator +"coherence_{}_samples_from_{}_to_{}.svg".format(len(collection.collection), collection.startdate, collection.enddate).replace(":", "-"))
        figure_spectro_mean_coh.savefig(output_dir + dir_name_stack + path_separator + "png" + path_separator +"coherence_{}_samples_from_{}_to_{}.png".format(len(collection.collection), collection.startdate, collection.enddate).replace(":", "-"))

        collection.stack_collection(median_or_mean="median")
        collection.save(output_dir + dir_name_stack + path_separator +"median_stack_median_{}_samples_from_{}Z_to_{}Z.mat".format(len(collection.collection), collection.startdate, collection.enddate).replace(" ", "T").replace("ZZ", "Z").replace(":", "-"))


        figure_collection_median = collection.plot()
        figure_spectro_median = collection.spectrogram( reject_bad_sup_windows=False, freqmask=True)
        figure_collection_median.savefig(output_dir + dir_name_stack + path_separator + "pdf" + path_separator +"stack_median_{}_samples_from_{}_to_{}.pdf".format(len(collection.collection), collection.startdate, collection.enddate).replace(":", "-"))
        figure_spectro_median.savefig(output_dir + dir_name_stack + path_separator + "pdf" + path_separator +"spectro_median_{}_samples_from_{}_to_{}.pdf".format(len(collection.collection), collection.startdate, collection.enddate).replace(":", "-"))
        figure_collection_median.savefig(output_dir + dir_name_stack + path_separator + "svg" + path_separator +"stack_median_{}_samples_from_{}_to_{}.svg".format(len(collection.collection), collection.startdate, collection.enddate).replace(":", "-"))
        figure_spectro_median.savefig(output_dir + dir_name_stack + path_separator + "svg" + path_separator +"spectro_median_{}_samples_from_{}_to_{}.svg".format(len(collection.collection), collection.startdate, collection.enddate).replace(":", "-"))
        figure_collection_median.savefig(output_dir + dir_name_stack + path_separator + "png" + path_separator +"stack_median_{}_samples_from_{}_to_{}.png".format(len(collection.collection), collection.startdate, collection.enddate).replace(":", "-"))
        figure_spectro_median.savefig(output_dir + dir_name_stack + path_separator + "png" + path_separator +"spectro_median_{}_samples_from_{}_to_{}.png".format(len(collection.collection), collection.startdate, collection.enddate).replace(":", "-"))




    else :
        if flag["blind_performance_assessment"] :
            blind_performance_pdf.to_csv(output_dir + path_separator + dir_name.replace("Estimation_results_", "BLIND_PERFORMANCE_") + ".csv")

    print("Sucess !", flush=True)


